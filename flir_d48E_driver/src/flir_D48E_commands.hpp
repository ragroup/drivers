/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This is the header file of the WsgCommands class from the wsg_communication ROS package.
 * The class is used in the wsg_command_node.cpp, which is the main file of the wsg_command_node ROS-node
 * This class contains all the functions for communication with the wsg50 gripper.
 * 		both functions for each command type
 * 		and functions for the building, sending, receiving and decoding packages via an UDP connection.
 *
 * 	The class also includes the necessary code for the UDP connection.
 */

#ifndef FLIRD48ECOMMANDS_HPP_
#define FLIRD48ECOMMANDS_HPP_

//#include <wsg_communication/WsgData.hpp>
//#include "WsgReceiver.hpp"

#include <string>
#include <ros/ros.h>
#include <boost/asio/ip/tcp.hpp>

#define OUTPORT 4000
#define POLL_DELAY 20
#define PTU_DEFAULT_HZ 10

// command defines
#define PTU_PAN 'p'
#define PTU_TILT 't'
#define PTU_MIN 'n'
#define PTU_MAX 'x'
#define PTU_MIN_SPEED 'l'
#define PTU_MAX_SPEED 'u'
#define PTU_VELOCITY 'v'
#define PTU_POSITION 'i'


class PTUCommands
{

public:

    //constructor / destructor
    PTUCommands(const ros::NodeHandle& nh);

    ~PTUCommands();

    bool Connect(std::string ipAddress);

    //********* Functions  (gripper commands)*******************

    //int InitCheck();												//check if the gripper is online and initialised

    bool initialize();														//check if the gripper is initialised, if not do initialisation.



    bool home();

    bool setPosAndSpeed(float vel_pan, float vel_tilt, float pos_pan, float pos_tilt, bool block = false);

    bool setPosition(char type, float pos, bool block);

    bool setSpeed(char type, float pos);

    float getPosition(char type);

    float getSpeed(char type);

    bool getPosAndSpeed(float& vel_pan, float& vel_tilt, float& pos_pan, float& pos_tilt);

    int getLimit(char type, char limType);

    float getRes(char type);

    float getResolution(char type)
    {
        return (type == PTU_TILT ? tr : pr);
    }


    void setInProgress(bool progress)
    {
        this->_in_progress = progress;
    }

    bool getInProgress()
    {
        return _in_progress;
    }


private:

    //********* Declarations *******************

    ros::NodeHandle n;
    //WsgReceiver* myWsgReceiver;
    //ProtectedBuffer* myProtectedBuffer;

    //boost::asio::io_service* io_service;
    //boost::asio::ip::tcp::socket* sockfd;														//socket for TCP connection
    int sockfd;
    struct pollfd ufds[1];
    //struct sockaddr_in in_addr;										//struct to hold information about connection for incoming data
    //struct sockaddr_in out_addr;									//struct to hold information about connection for outgoing data

    struct payloadInfo												//Struct to hold the payload and info for a outgoing packet.
    {
        unsigned short length; 										//Length of the message's payload in bytes (0, if the message has no payload)

        unsigned char id; 											//ID of the message

        unsigned char *data; 										//Pointer to the message's payload
    };

    union UStuff
    {
        float   f;
        unsigned char   c[4];
    };

    union UStuff2
    {
        short i;
        unsigned char c[2];
    };

    union UStuff3
    {
        int i;
        unsigned char c[4];
    };

    //********* Functions *******************

    //unsigned char* BuildPackage(payloadInfo *msg, unsigned int *size);		//Builds the packet from the payload

    int SendMessage(std::string msg);									//sends the packet to the gripper via UDP connection

    int ReadMessage(std::string &msg);

    std::string SendCommand(std::string command);

    //int ReceiveMessage(unsigned char * buf_in, int length_in, payloadInfo * payload);	//Receives a packet from the gripper via UDP

    //int ExecuteCommand(payloadInfo * payload, unsigned char * buf_in, int length_in);	//Executes a command, uses send_message and receive message

    int poll_delay;

    // Position Limits
    int TMin;  ///< Min Tilt in Counts
    int TMax;  ///< Max Tilt in Counts
    int PMin;  ///< Min Pan in Counts
    int PMax;  ///< Max Pan in Counts

    // Speed Limits
    int TSMin;  ///< Min Tilt Speed in Counts/second
    int TSMax;  ///< Max Tilt Speed in Counts/second
    int PSMin;  ///< Min Pan Speed in Counts/second
    int PSMax;  ///< Max Pan Speed in Counts/second

    bool initialized_;

    float tr;  ///< tilt resolution (rads/count)
    float pr;  ///< pan resolution (rads/count)


    bool _in_progress;
};


#endif /* FLIRD48ECOMMANDS_HPP_ */
