/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This file demonstrates how to use the WsgClient class and essentially the wsg_command_node ROS node.
 * This file contains only a selection of the functions available.
 * Further documentation on each function is found in the WsgClient class
 * Generally documentation is added in the relevant files.
 */

#include "ros/ros.h"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <actionlib/client/simple_action_client.h>
#include "flir_d48E_driver/PTUAction.h"
#include <math.h>
#include <fcntl.h>
#include <linux/kd.h>
#include <sys/ioctl.h>
#include <logger_sys/logger_sys.hpp>

using namespace std;

actionlib::SimpleActionClient<flir_d48E_driver::PTUAction>* actionClient;

void home()
{
    flir_d48E_driver::PTUGoal myGoal;

    myGoal.mode = "home";

    actionClient->sendGoalAndWait(myGoal);
}

bool test = false;

void move()
{
    float vel_pan = 0, vel_tilt = 0, pos_pan = 0, pos_tilt = 0;

    cout << "Please enter speed for pan" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Speed: ";
    cin >> vel_pan;
    cout << endl << endl;

    cout << "Please enter speed for tilt" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Speed: ";
    cin >> vel_tilt;
    cout << endl << endl; 



    cout << "Please enter position for pan" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Pos: ";
    cin >> pos_pan;
    cout << endl << endl;

    cout << "Please enter position for tilt" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Pos: ";
    cin >> pos_tilt;
    cout << endl << endl;

    flir_d48E_driver::PTUGoal myGoal;



    myGoal.mode = "move";
    myGoal.vel_pan = vel_pan;
    myGoal.vel_tilt = vel_tilt;
    myGoal.pos_pan = pos_pan;
    myGoal.pos_tilt = pos_tilt;

    actionClient->sendGoalAndWait(myGoal);
}

int mainMenu()
{
    int choice;

    cout << "Main Menu - please select a function and press enter" << endl;
    cout << "*************************************************" << endl << endl;
    cout << "Value:	 Name:	              Description:     " << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0    Exit                  Close program" << endl << endl;
    cout << "  1    Home                  Initialise the PTU" << endl;
    cout << "  2    Move                  Move PTU" << endl << endl;

    cout << "Please select: ";
    cin >> choice;
    cout << endl << endl;

    switch(choice)
    {
    case 0:
        return -1;
        break;
    case 1:
        home();
        break;
    case 2:
        move();
        break;
    default:
        cout << "Invalid function!" << endl << endl;
        break;

    }

    return 0;
}


//Main program
int main(int argc, char *argv[])
{
    ros::init(argc, argv, "test_flir_ptu_node");

    ros::NodeHandle n;

    InitLogger("Test_flir_PTU",true,true,true);


    actionClient = new actionlib::SimpleActionClient<flir_d48E_driver::PTUAction>("flir_PTU_action", true);

    debug(FFL, "Waiting for ROS Action Server...");
    actionClient->waitForServer();

    int state = 0;
    while(state == 0)
    {
        state = mainMenu();
    }


    return 0;
}
