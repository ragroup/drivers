/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This file is the main file of the wsg_command_node ROS-node (executable)
 *
 * This file is mainly an action server.
 * Upon receiving a goal from a client, a callback function is called.
 * This function is primarily a case structure switched by the command ID of the goal.
 * Depending on the command ID the corresponding function from the WsgCommands class is executed.

 * The result is returned to the client. The result consists of: a terminal state (error code) and the data returned from the gripper (if any requested)
 *
 * Even though the WsgClient class, which is intended for use on the action client side, will ensure the parameters send to the gripper is within range,
 * 		this is also checked and ensured here (on the server side).
 * 		The reason is, that the WsgClient class is not needed in order to use the action server.
 * 		Example of out of range parameter: Receiving a goal with a speed of 700 mm/s will result in the speed being set to maximum (420 mm/s)
 *
 * The code for the communication with the gripper is found in the WsgCommands class.
 *
 * The name of the action server is "gripper_action"
 */
//#include <boost/thread.hpp>
#include "ros/ros.h"
#include <ros/console.h>
#include <tf/tf.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string>
#include <actionlib/server/simple_action_server.h>
#include <sensor_msgs/JointState.h>
#include "flir_d48E_driver/PTUAction.h"
#include "flir_D48E_commands.hpp"
#include <logger_sys/logger_sys.hpp>


#define DEFAULT_IP "192.168.3.175"


using namespace std;

//Object of the WsgCommands class, which contains all the gripper functions
PTUCommands* ptuCommands;

//Declaration of the action server
actionlib::SimpleActionServer<flir_d48E_driver::PTUAction>* myActionServer;

//Delaration of publisher
ros::Publisher joint_pub;

//Declaration of mutex. A mutex is used to assure that the data that are published is consistent
boost::mutex mymutex;

//Lock to pub progress
bool _pub_in_progress;

void actionCB(const flir_d48E_driver::PTUGoalConstPtr& goal)
{
    /**
     * This is the callback function of the action server.
     * It will be called when receiving a new goal.
     */
    info(FFL,"**************************************");
    FINFO("Received an action goal with command: " << goal->mode);
    mymutex.lock();

    //ptuCommands->setInProgress(true);

    flir_d48E_driver::PTUResult myResult;

    if(goal->mode == "home")
    {
        FINFO("Homing the PTU... please standby for 10 sec");
        if(!ptuCommands->home())
        {
            FERROR("Failed to home the PTU");
            myResult.state = -1;
            myActionServer->setAborted(myResult);

            mymutex.unlock();
            return;
        }
    }
    else if(goal->mode == "move")
    {
        FINFO("Moving to <Pan: " << goal->pos_pan << "> <Tilt: " << goal->pos_tilt << ">");
        if(!ptuCommands->setPosAndSpeed(goal->vel_pan, goal->vel_tilt, goal->pos_pan, goal->pos_tilt, true))
        {
            myResult.state = -1;
            myActionServer->setAborted(myResult);

            mymutex.unlock();
            return;
        }
    }
    else
    {
        FERROR("Unknown mode received!");
        myResult.state = -1;
        myActionServer->setAborted(myResult);

        mymutex.unlock();
        return;
    }

    //all went well!
    myResult.state = 0;

    myActionServer->setSucceeded(myResult);	//set goal to succeeded and send the result to the action client


    mymutex.unlock();
    info(FFL, "**************************************");
}

void pubCallback(const ros::TimerEvent&)
{
    mymutex.lock();

    // Read Position & Speed
    float pan, tilt, panspeed, tiltspeed;
    ptuCommands->getPosAndSpeed(panspeed,tiltspeed,pan,tilt);

    mymutex.unlock();

    // Publish Position & Speed
    sensor_msgs::JointState joint_state;
    joint_state.header.stamp = ros::Time::now();
    joint_state.name.resize(2);
    joint_state.position.resize(2);
    joint_state.velocity.resize(2);
    joint_state.name[0] = "pan";
    joint_state.position[0] = pan;
    joint_state.velocity[0] = panspeed;
    joint_state.name[1] = "tilt";
    joint_state.position[1] = tilt;
    joint_state.velocity[1] = tiltspeed;
    joint_pub.publish(joint_state);
}

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "flir_D48E_driver_node");

    ros::NodeHandle n;

    InitLogger("flir_D48E_driver_node", true, true, true);

    info(FFL, "flir_D48E_driver_node started...");

    string ipAddress = DEFAULT_IP;

    for(int i=2;i<=argc;i++)
    {

        ipAddress = argv[i-1];
    }

    //Obejct of WsgCommands class
    ptuCommands = new PTUCommands(n);

    FINFO("Attempting to connect...");
    if(!ptuCommands->Connect(ipAddress))
    {
        delete ptuCommands;
        return 0;
    }

    FINFO("Connected! Trying to init..");
    ptuCommands->initialize();

    // Publishers : Only publish the most recent reading
    joint_pub = n.advertise<sensor_msgs::JointState>("flir_D48E_ptu/jointState", 1);

    //Mutex to ensure the consistense of the data.
    //boost::mutex::scoped_lock mylock(mymutex, boost::defer_lock); // defer_lock makes it initially unlocke
    _pub_in_progress = false;

    debug(FFL,"Starting ROS Action Server");
    myActionServer = new actionlib::SimpleActionServer<flir_d48E_driver::PTUAction>(n, "flir_PTU_action", boost::bind(&actionCB, _1), false);
    myActionServer->start();

    // Set up polling callback
    int hz;
    ros::param::param<int>("~hz", hz, PTU_DEFAULT_HZ);
    ros::Timer spin_timer = n.createTimer(ros::Duration(1 / hz),&pubCallback);

    //Loop continiously until program is shutdown
    ros::spin();

    ros::waitForShutdown();

}


