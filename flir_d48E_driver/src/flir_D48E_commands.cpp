/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This is the source file of the class "WsgCommands"
 *
 * The purpose and use of this class is documented in the corresponding header file.'
 *
 * Please note in this document:
 * 		A packet refer to the total encapsulated data being either send or received via UDP connection to and from the gripper
 * 			This means both the payload data, but also a header, a size info, a checksum and a command id.
 * 			The packet must be build in a specific order.
 *
 * 		A payload is the actual variable data of a packet.
 * 			A payload must in most cases be of a certain length and type.
 * 			Please note, when executing a command, a payload is send and a nother payload is received.
 *
 * 		The state refer to the error code, which can be fund in "WsgData.hpp"
 *
 * 		Unions are used to convert between bytes and integer/float.
 */

#include "ros/console.h"
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/asio.hpp>

#include "flir_D48E_commands.hpp"
#include <logger_sys/logger_sys.hpp>


using namespace std;

using boost::asio::ip::tcp;


/** Templated wrapper function on lexical_cast to assist with extracting
 * values from serial response strings.
 */
template<typename T>
T parseResponse(std::string responseBuffer)
{
    std::string trimmed = responseBuffer.substr(1);
    boost::trim(trimmed);
    T parsed = boost::lexical_cast<T>(trimmed);
    FDEBUG("Parsed response value: " << parsed);
    return parsed;
}


PTUCommands::PTUCommands(const ros::NodeHandle& nh) : n(nh)
{
    FDEBUG("Instantiating object of WsgCommands");

    poll_delay = POLL_DELAY;
    _in_progress = false;
}

PTUCommands::~PTUCommands()
{
    debug(FFL, "::~WsgCommands"); //send disconnect announcement before exit, to ensure the gripper will be ready for the next connection.

    //sockfd->close();	 //close the socket connection before exit
    close(sockfd);
}

bool PTUCommands::Connect(string ipAddress)
{
    FINFO("Connecting to flir D48E pan/tilt unit on IP: " << ipAddress);

    struct addrinfo hints, *servinfo;
    int rv;
    //char s[INET6_ADDRSTRLEN];

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;  // use IPv4
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

    getaddrinfo(ipAddress.c_str(), "4000" , &hints, &servinfo);

    FINFO("Got address info");

    // make a socket:

    sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    //sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    // bind it to the port we passed in to getaddrinfo():

    if (connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) == -1)
    {
        close(sockfd);
        FERROR("Failed to connect!");
        return false;
    }

    freeaddrinfo(servinfo); // all done with this structure

    //setup so that we can easily poll the socket for incoming data
    ufds[0].fd = sockfd;
    ufds[0].events = POLLIN; // check for normal or out-of-band

    return true;
}

bool PTUCommands::initialize()
{
    debug(FFL, "::Init");

    //setting poll_delay for init:
    poll_delay = 20;

    SendMessage("ft ");  // terse feedback
    SendMessage("ed ");  // disable echo
    SendMessage("ci ");  // position mode


    string initial_text;
    ReadMessage(initial_text);

    // get pan tilt encoder res
    tr = getRes(PTU_TILT);
    pr = getRes(PTU_PAN);

    PMin = getLimit(PTU_PAN, PTU_MIN);
    PMax = getLimit(PTU_PAN, PTU_MAX);
    TMin = getLimit(PTU_TILT, PTU_MIN);
    TMax = getLimit(PTU_TILT, PTU_MAX);
    PSMin = getLimit(PTU_PAN, PTU_MIN_SPEED);
    PSMax = getLimit(PTU_PAN, PTU_MAX_SPEED);
    TSMin = getLimit(PTU_TILT, PTU_MIN_SPEED);
    TSMax = getLimit(PTU_TILT, PTU_MAX_SPEED);

    n.setParam("min_tilt", TMin);
    n.setParam("max_tilt", TMax);
    n.setParam("min_tilt_speed", TSMin);
    n.setParam("max_tilt_speed", TSMax);
    n.setParam("tilt_step", getResolution(PTU_TILT));

    n.setParam("min_pan", PMin);
    n.setParam("max_pan", PMax);
    n.setParam("min_pan_speed", PSMin);
    n.setParam("max_pan_speed", PSMax);
    n.setParam("pan_step", getResolution(PTU_PAN));

    FINFO("Tilt resolution: " << tr );
    FINFO("Pan resolution: " << pr );
    FINFO("Min pan position: " << PMin );
    FINFO("Max pan position: " << PMax );
    FINFO("Min tilt position: " << TMin );
    FINFO("Min tilt position: " << TMax );
    FINFO("Min pan speed: " << PSMin );
    FINFO("Min pan speed: " << PSMax );
    FINFO("Min tilt position: " << TSMin );
    FINFO("Min tilt position: " << TSMax );
 
    if (tr <= 0 || pr <= 0 || PMin == -1 || PMax == -1 || TMin == -1 || TMax == -1)
    {
        initialized_ = false;
        FWARN("PTU NOT initialized!");
    }
    else
    {
        FINFO("PTU initialized...");
        initialized_ = true;
    }

    //resetting poll_delay
    poll_delay = POLL_DELAY;

    return initialized_;
}

bool PTUCommands::home()
{
    ROS_INFO("Sending command to reset PTU.");

    // Issue reset command
    SendMessage(" r ");

    string expected_response("!T!T!P!P*");

    // 30 seconds to receive full confirmation of reset action completed.
    for (int i = 0; i < 100; i++)
    {
        usleep(100000);
    }

    string read_msg;
    int bytes_received = ReadMessage(read_msg);

    return (read_msg == expected_response);
}

bool PTUCommands::setPosition(char type, float pos, bool block)
{
    if (!initialized_) return false;

    // get raw encoder count to move
    int count = static_cast<int>(pos / getResolution(type));

    // Check limits
    if (count < (type == PTU_TILT ? TMin : PMin) || count > (type == PTU_TILT ? TMax : PMax))
    {
        FERROR("Pan Tilt Value out of Range: " << type << " " << pos << " (" << count << ") ("
               << (type == PTU_TILT ? TMin : PMin) << "-" << (type == PTU_TILT ? TMax : PMax) << ")");
        return false;
    }

    string buffer = SendCommand(string() + type + "p" +
                                boost::lexical_cast<string>(count) + " ");

    if (buffer.empty() || buffer[0] != '*')
    {
        FERROR("Error setting pan-tilt pos");
        return false;
    }

    if (block)
        while (getPosition(type) != pos) {};

    return true;
}

bool PTUCommands::setSpeed(char type, float pos)
{
    // get raw encoder speed to move
    int count = static_cast<int>(pos / getResolution(type));

    // Check limits
    if (abs(count) < (type == PTU_TILT ? TSMin : PSMin) || abs(count) > (type == PTU_TILT ? TSMax : PSMax))
    {
        FERROR("Pan Tilt Speed Value out of Range: "<< type << " " << pos << "(" << count << ") ("
               << (type == PTU_TILT ? TSMin : PSMin) << " - " << (type == PTU_TILT ? TSMax : PSMax) << ")");

        return false;
    }

    string buffer = SendCommand(string() + type + "s" + boost::lexical_cast<string>(count) + " ");

    if (buffer.empty() || buffer[0] != '*')
    {
        FERROR("Error setting pan-tilt speed\n");
        return false;
    }

    return true;
}

bool PTUCommands::setPosAndSpeed(float vel_pan, float vel_tilt, float pos_pan, float pos_tilt, bool block)
{
    ///VEL PAN
    int PS_count = static_cast<int>(vel_pan / getResolution(PTU_PAN));
    // Check limits
    if (abs(PS_count) < PSMin || abs(PS_count) > PSMax)
    {
        FERROR("Pan Speed Value out of Range");
        return false;
    }

    ///VEL TILT
    int TS_count = static_cast<int>(vel_tilt / getResolution(PTU_TILT));
    // Check limits
    if (abs(TS_count) < TSMin || abs(TS_count) > TSMax)
    {
        FERROR("Tilt Speed Value out of Range");
        return false;
    }

    ///POS PAN
    int PP_count = static_cast<int>(pos_pan / getResolution(PTU_PAN));

    // Check limits
    if (PP_count < PMin || PP_count > PMax)
    {
        FERROR("Pan Value out of Range");
        return false;
    }

    ///POS TILT
    int TP_count = static_cast<int>(pos_tilt / getResolution(PTU_TILT));

    // Check limits
    if (TP_count < PMin || TP_count > PMax)
    {
        FERROR("TILT Value out of Range");
        return false;
    }

    string buffer = SendCommand(string() + "b"  + boost::lexical_cast<string>(PP_count) + ","
                                + boost::lexical_cast<string>(TP_count) + ","
                                + boost::lexical_cast<string>(PS_count) + ","
                                + boost::lexical_cast<string>(TS_count) + " ");

    if (buffer.empty() || buffer[0] != '*')
    {
        FERROR("Error setting pan-tilt pos and speed");
        return false;
    }

    float cPP, cTP, cPS, cTS;

    if (block)
    {
        do{
            getPosAndSpeed(cPS, cTS, cPP, cTP);

        }while(((pos_pan-0.01) > cPP || cPP > (pos_pan+0.01)) || ((pos_tilt-0.01) > cTP || cTP > (pos_tilt+0.01)));
    }

    return true;
}


float PTUCommands::getPosition(char type)
{
    if (!initialized_) return -1;

    string buffer = SendCommand(string() + type + "p ");

    if (buffer.length() < 3 || buffer[0] != '*')
    {
        FERROR("Error getting pan-tilt pos");
        return -1;
    }

    return parseResponse<double>(buffer) * getResolution(type);
}

float PTUCommands::getSpeed(char type)
{
    if (!initialized_) return -1;

    string buffer = SendCommand(string() + type + "s ");

    if (buffer.length() < 3 || buffer[0] != '*')
    {
        FERROR("Error getting pan-tilt speed");
        return -1;
    }

    return parseResponse<double>(buffer) * getResolution(type);
}

bool PTUCommands::getPosAndSpeed(float& vel_pan, float& vel_tilt, float& pos_pan, float& pos_tilt)
{
    if (!initialized_) return -1;

    string buffer = SendCommand(" b ");

    if (buffer.length() < 3 || buffer[0] != '*')
    {
        FERROR("Error getting pan-tilt pos and speed");
        return -1;
    }

    buffer.erase(0,2);
    stringstream ss(buffer);

    string PP, TP, PS, TS;
    getline(ss, PP, ',');
    getline(ss, TP, ',');
    getline(ss, PS, ',');
    getline(ss, TS);

    pos_pan = boost::lexical_cast<double>(PP) * getResolution(PTU_PAN);
    pos_tilt = boost::lexical_cast<double>(TP)  * getResolution(PTU_TILT);
    vel_pan = boost::lexical_cast<double>(PS)  * getResolution(PTU_PAN);
    vel_tilt = boost::lexical_cast<double>(TS)  * getResolution(PTU_TILT);

    return true;
}


int PTUCommands::getLimit(char type, char limType)
{
    string buffer = SendCommand(string() + type + limType + " ");

    if (buffer.length() < 3 || buffer[0] != '*')
    {
        ROS_ERROR("Error getting pan-tilt limit");
        return -1;
    }

    return parseResponse<int>(buffer);
}

float PTUCommands::getRes(char type)
{
    string buffer = SendCommand(string() + type + "r ");

    if (buffer.length() < 3 || buffer[0] != '*')
    {
        ROS_ERROR("Error getting pan-tilt res");
        return -1;
    }

    double z = parseResponse<double>(buffer);
    z = z / 3600;  // degrees/count
    return z * M_PI / 180;  // radians/count
}

///////////////////


int PTUCommands::SendMessage(string msg)
{
    int len, bytes_sent;

    len = strlen(msg.c_str());

    bytes_sent = send(sockfd, msg.c_str(), len, 0);
    //    cout << "before send" << endl;
    //    int bytes_sent = (int)sockfd->send(boost::asio::buffer(msg));
    //    io_service->run_one();
    //    cout << "afer send" << endl;

    return bytes_sent;
}

int PTUCommands::ReadMessage(string &msg)
{
    //    boost::system::error_code error;
    //    boost::asio::streambuf buffer;

    //    int bytes_received = (int)boost::asio::read_until(*sockfd, buffer, "\n", error);
    //    io_service->run_one();
    //    istream str(&buffer);
    //    string s;
    //    getline(str, s);

    //    cout << "Line read: " << s << endl;

    char buffer[1024];

    int total_received = 0;
    int bytes_received = 0;


    while(poll(ufds, 1, poll_delay) > 0)
    {
        bytes_received = recv(sockfd,buffer + total_received,1024-total_received,0);
        total_received += bytes_received;

    }

    msg = string(buffer,total_received);

    //remove any "newline" characters in the message:
    msg.erase(std::remove(msg.begin(), msg.end(), '\n'), msg.end());
    msg.erase(std::remove(msg.begin(), msg.end(), '\r'), msg.end());

    return total_received;
}

string PTUCommands::SendCommand(string command)
{
    if(SendMessage(command) != command.length())
    {
        FERROR("Failed to send command!");
        return "";
    }

    string response;
    if(ReadMessage(response) == 0)
    {
        FERROR("Failed to receive response!");
        return "";
    }

    return response;
}

