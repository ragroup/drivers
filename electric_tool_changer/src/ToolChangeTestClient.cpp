#include "ros/ros.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string.h>
#include <logger_sys/logger_sys.hpp>
#include <actionlib/client/simple_action_client.h>
#include <electric_tool_changer/ToolChangerAction.h>
#include "electric_tool_changer/ETCDefinitions.hpp"

using namespace std;

actionlib::SimpleActionClient<electric_tool_changer::ToolChangerAction>* myActionClient;

// Function to choose to attach or detach tool from console, mainly for testing purposes
int ConsoleSelector()
{    
    int choice = 1;

    electric_tool_changer::ToolChangerGoal myGoal;

    cout << "Main Menu - please select a funciton and press enter" << endl;
    cout << "*************************************************" << endl << endl;
    cout << "Value:	 Name:	              Description:     " << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0    Exit                  Close program" << endl;
    cout << "  1    Attach                Attach tool" << endl;
    cout << "  2    Detach                Detach tool" << endl << endl;

    cout << "Please select: ";
    cin >> choice;
    cout << endl << endl;

    switch (choice)
    {
    case 0:
        return 10;
        break;
    case 1:
        cout << "Attach tool" << endl;
        myGoal.command = ETC_definitions::ATTACH;
        break;
    case 2:
        cout << "Detach tool" << endl;
        myGoal.command = ETC_definitions::DETACH;
        break;
    default:
        cout << "Unknown command!" << endl;
        return 0;
        break;
    }

    myActionClient->sendGoalAndWait(myGoal);

    return 0;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "electric_tool_changer_test_client");

    ros::NodeHandle n;

    InitLogger("electric_tool_changer_test_client", true, true, true);

    info(FFL, "electric_tool_changer_test_client started...");

    debug(FFL,"Initialising ROS Action Client");
    myActionClient = new actionlib::SimpleActionClient<electric_tool_changer::ToolChangerAction>("tool_changer_action", true);

    int state = 0;
    while(state == 0)
    {
        state = ConsoleSelector();
    }

    return 0;
}
