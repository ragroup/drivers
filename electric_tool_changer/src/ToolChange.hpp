#ifndef TOOLCHANGE_HPP_
#define TOOLCHANGE_HPP_

#define STROKE 20
#define VID 0x04d8
#define PID 0xfc5f

class ETCFunctions
{
public:
    ~ETCFunctions();

    int init();
    int attach();
    int detach();

    int config();

private:
    int connectController();
    int disconnectController();
    int writeUSB(char,unsigned short);

};

libusb_context *ctx;
libusb_device_handle *device_handle;


#endif //TOOLCHANGE_HPP_
