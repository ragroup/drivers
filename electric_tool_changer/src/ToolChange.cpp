#include "ros/ros.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <libusb-1.0/libusb.h>
#include "electric_tool_changer/ToolChange.hpp"
#include <logger_sys/logger_sys.hpp>
#include <actionlib/server/simple_action_server.h>
#include <electric_tool_changer/ToolChangerAction.h>
#include "electric_tool_changer/ETCDefinitions.hpp"


using namespace std;

ETCFunctions* myETCFunctions;
actionlib::SimpleActionServer<electric_tool_changer::ToolChangerAction>* myToolChangerServer;

void toolChangeActionCB(const electric_tool_changer::ToolChangerGoalConstPtr& goal)
{
    FINFO("**************************************");
    FINFO("Received goal from action client.");

    switch(goal->command)
    {
    case ETC_definitions::ATTACH:
        FINFO("Command: ATTACH. Attaching tool...");
        myETCFunctions->attach();
        break;
    case ETC_definitions::DETACH:
        FINFO("Command: DETACH. Detaching tool...");
        myETCFunctions->detach();
        break;
    default:
        FERROR("Unknown command received. Command: " << goal->command);
        break;
    }

    myToolChangerServer->setSucceeded();

    FINFO("**************************************");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "electric_tool_changer_driver");

    ros::NodeHandle n;

    InitLogger("electric_tool_changer_driver", true, true, true);

    info(FFL, "electric_tool_changer_driver started...");

    //Object of ETCFunctions class
    myETCFunctions = new ETCFunctions;

    //initialise - hence, connect to device and configure
    if(myETCFunctions->init() != 0)
    {
        FERROR("Failed to connect to device. Shutting down...");
        return 0;
    }

    FINFO("Electric Tool Changer ready...");

    //Declare and start the ROS action server
    debug(FFL,"Starting ROS Action Server");
    myToolChangerServer = new actionlib::SimpleActionServer<electric_tool_changer::ToolChangerAction>(n, "tool_changer_action", boost::bind(&toolChangeActionCB, _1), false);
    myToolChangerServer->start();

    //Loop continiously until program is shutdown
    ros::spin();

    ros::waitForShutdown();

    return 0;
}

ETCFunctions::~ETCFunctions()
{
    disconnectController();
}

int ETCFunctions::init()
{
    int state = connectController();

    if(state != 0) return state;

    state = config();

    return state;
}

int ETCFunctions::connectController()
{
    FDEBUG("Connecting to controller...");

    libusb_device **devs;   //pointer to pointer of device, used to retrieve a list of devices
    ctx = NULL;            //a libusb session
    int r;                  //for return values
    ssize_t cnt;            //holding number of devices in list

    r = libusb_init(&ctx);  //initialize a library session

    if(r < 0)
    {
        FERROR("Initialisation error: " << r);
        return 1;
    }

    //Get list of connected devices
    libusb_set_debug(ctx, 3); //set verbosity level to 3, as suggested in the documentation

    FDEBUG("Getting list of connected USB devices");
    cnt = libusb_get_device_list(ctx, &devs); //get the list of devices

    if(cnt < 0)
    {
        FERROR("Failed to get list of USB-devices or no devices connected");
        return 1;
    }

    FDEBUG("Found " << cnt << " USB devices connected");


    //Now open the device
    FINFO("Connecting to device - Vendor-ID: " << VID << " Product-ID: " <<  PID);

    device_handle = libusb_open_device_with_vid_pid(ctx, VID, PID);

    if(device_handle == NULL)
    {
        FERROR("Failed to connect!");
        return -1;
    }

    FDEBUG("Device successfully opened...");

    libusb_free_device_list(devs, 1); //free the list, unref the devices in it


    libusb_set_configuration(device_handle, 1);
    if(libusb_kernel_driver_active(device_handle, 0) == 1) //find out if kernel driver is attached
    {
        FDEBUG("Kernel driver already attached. Trying to detach it...");

        if(libusb_detach_kernel_driver(device_handle, 0) == 0) //detach the kernel driver
        {
            FDEBUG("Kernel driver sucessfully detached");
        }
        else //failed to detach
        {
            FERROR("Failed to connect to device. Failed to detach the kernel driver. The device might be in use.");
            return -1;
        }
    }

    //claim the interface
    FDEBUG("Claiming the interface...");
    r = libusb_claim_interface(device_handle, 0); //claim interface 0 (the first) of device (mine had jsut 1)

    if(r < 0)
    {
        FERROR("Failed to claim the interface");
        return -1;
    }

    FDEBUG("Interface claimed successfully");
    FINFO("Electric Tool Changer successfully connected");

    return 0;
}

int ETCFunctions::writeUSB(char control, unsigned short value)
{
    // Make buffer array
    unsigned char buffer[3];
    buffer[0] = control;
    buffer[1] = char(value);
    unsigned short tmp = htons(value);
    buffer [2] = char(tmp);

    FDEBUG("Writing to controller:  " << "Code: 0x" << hex << int(buffer[0]) <<", Value: "<< dec << value);

    // Write to the controller
    int length = sizeof (value);
    int actual;
    int r = libusb_bulk_transfer(device_handle, (1 | LIBUSB_ENDPOINT_OUT), buffer, 3, &actual, 5000);

    if(r == 0 && actual == 3)   //we wrote the 3 bytes successfully
    {
        FDEBUG("Writing Successful!");
    }
    else
    {
        FDEBUG("Write Error");
        return -1;
    }

    return 0;
}
int ETCFunctions::config()
{
    float realvalue;
    float calcvalue;
    unsigned short writevalue;

    FINFO("Configuring the controller of the tool changer...");

    // 0x01 SET_ACCURACY
    // +/- tolerance in mm:
    realvalue = 0.2;
    // writing:
    calcvalue = realvalue * 1024 / STROKE; //calculates from mm
    //calcvalue = 102;
    writevalue = (unsigned short) calcvalue;
    writeUSB(1,writevalue);

    // 0x02 SET_RETRACT_LIMIT
    // limit from endpoint in mm:
    realvalue = 5;
    // writing:
    calcvalue = realvalue * 1023 / STROKE; //calculates from mm
    //calcvalue = 200;
    writevalue = (unsigned short) calcvalue;
    writeUSB(2,writevalue);

    // 0x03 SET_EXTEND_LIMIT
    // limit from endpoint in mm:
    realvalue = 13;
    // writing:
    calcvalue = realvalue * 1023 / STROKE; //calculates from mm
    //calcvalue = 716,
    writevalue = (unsigned short) calcvalue;
    writeUSB(3,writevalue);

    // 0x21 SET_SPEED
    writevalue = 511;
    writeUSB(33,writevalue);

    // 0x30 DISABLE_MANUAL
    writeUSB(48,writevalue);

    FINFO("Configuration successful");
    return 0;
}
int ETCFunctions::disconnectController()
{
    FWARN("Disconnecting from the Electric Tool Changer...");

    libusb_close(device_handle); //close the device we opened
    libusb_exit(ctx); //needs to be called to end the

    FINFO("Successfully disconnected!");
    return 0;
}
// ATTACH TOOL
int ETCFunctions::attach()
{
    FINFO("Attacing tool...");
    float realvalue;
    float calcvalue;
    unsigned short writevalue;

    // 0x20 SET_POSITION
    // actuator position in mm:
    realvalue = 13;
    // writing:
    calcvalue = realvalue * 1023 / STROKE; //calculates from mm
    //calcvalue = 650;
    writevalue = (unsigned short) calcvalue;
    writeUSB(32,writevalue);

    sleep(5);

    FINFO("Tool attached!");
    return 0;
}

// DETACH TOOL
int ETCFunctions::detach()
{
    FINFO("Detaching tool...");
    float realvalue;
    float calcvalue;
    unsigned short writevalue;

    // 0x20 SET_POSITION
    // actuator position in mm:
    realvalue = 5.5;
    // writing:
    calcvalue = realvalue * 1023 / STROKE; //calculates from mm
    //calcvalue = 300;
    writevalue = (unsigned short) calcvalue;
    writeUSB(32,writevalue);

    sleep(2);

    FINFO("Tool detached!");
    return 0;
}
