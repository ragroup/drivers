#include "SerialPort.hpp"
#include "ros/ros.h"
#include <actionlib/server/simple_action_server.h>


#include <anyfeeder_driver/StartAnyFeederAction.h>
#include <anyfeeder_driver/InitAnyFeederAction.h>
#include <anyfeeder_driver/StopAnyFeederAction.h>
#include <anyfeeder_driver/FeedFowardAnyFeederAction.h>
#include <anyfeeder_driver/FeedBackwardAnyFeederAction.h>
#include <anyfeeder_driver/FeedFlipFowardAnyFeederAction.h>
#include <anyfeeder_driver/FeedFlipBackwardAnyFeederAction.h>
#include <anyfeeder_driver/FlipAnyFeederAction.h>
#include <anyfeeder_driver/DispenseAnyFeederAction.h>
#include <anyfeeder_driver/PurgeAnyFeederAction.h>
#include <anyfeeder_driver/HeavyDispenseAnyFeederAction.h>

class any_feeder_server_node : SerialPort
{
protected:
    virtual void on_receive_(const std::string &data);

public:
    any_feeder_server_node(ros::NodeHandle& nh);

    void startFirmwareAnyFeeder(const anyfeeder_driver::StartAnyFeederGoalConstPtr& goal);
    void initAnyFeeder(const anyfeeder_driver::InitAnyFeederGoalConstPtr& goal);
    void stopAnyFeeder(const anyfeeder_driver::StopAnyFeederGoalConstPtr& goal);
    void feedFowardAnyFeeder(const anyfeeder_driver::FeedFowardAnyFeederGoalConstPtr& goal);
    void feedBackwardAnyFeeder(const anyfeeder_driver::FeedBackwardAnyFeederGoalConstPtr& goal);
    void feedFlipFowardAnyFeeder(const anyfeeder_driver::FeedFlipFowardAnyFeederGoalConstPtr& goal);
    void feedFlipBackwardAnyFeeder(const anyfeeder_driver::FeedFlipBackwardAnyFeederGoalConstPtr& goal);
    void flipAnyFeeder(const anyfeeder_driver::FlipAnyFeederGoalConstPtr& goal);
    void dispenseAnyFeeder(const anyfeeder_driver::DispenseAnyFeederGoalConstPtr& goal);
    void purgeAnyFeeder(const anyfeeder_driver::PurgeAnyFeederGoalConstPtr& goal);
    void heavyDispenseAnyFeeder(const anyfeeder_driver::HeavyDispenseAnyFeederGoalConstPtr& goal);

private:
    //Declaration of ROS Action Servers, one for each function group
    actionlib::SimpleActionServer<anyfeeder_driver::StartAnyFeederAction>* startAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::InitAnyFeederAction>* initAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::StopAnyFeederAction>* stopAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::FeedFowardAnyFeederAction>* feedFowardAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::FeedBackwardAnyFeederAction>* feedBackwardAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::FeedFlipFowardAnyFeederAction>* feedFlipFowardAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::FeedFlipBackwardAnyFeederAction>* feedFlipBackwardAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::FlipAnyFeederAction>* flipAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::DispenseAnyFeederAction>* dispenseAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::PurgeAnyFeederAction>* purgeAnyFeederServer;
    actionlib::SimpleActionServer<anyfeeder_driver::HeavyDispenseAnyFeederAction>* heavyDispenseAnyFeederServer;

    //Declaration of NodeHandler
    ros::NodeHandle n;

    //AnyFeeder Feedback Flag
    bool motor_one_command_accepted;
    bool motor_two_command_accepted;
    bool motor_one_command_Finished;
    bool motor_two_command_Finished;
    bool motorOneErrorAnyFeeder;
    bool motorTwoErrorAnyFeeder;
    bool stopStatus;

    boost::mutex mutex_command;

    std::string command_send;
    std::string motorOneFeedbackAnyFeeder;
    std::string motorTwoFeedbackAnyFeeder;

    void resetMotorFlagAnyFeeder();
    bool executeMoveCommandAnyFeeder(std::string moveID, bool execute, bool changeTurns, bool changeSpeed, int turns, int speed, int executeCommandNumber, int speedCommandNumber, std::string *motorOneMessage, std::string *motorTwoMessage);



};

