#include <QtGui/QApplication>
#include "ros/ros.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "GUI");
    ros::NodeHandle n;

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    w.setGeometry(150,0,747,615);
    
    return a.exec();
}
