#include "any_feeder_server_node.hpp"

#include <limits>
#include <iostream>

//#include <logger_sys/logger_sys.hpp>

// Overwrite the function "on-receive_"
void any_feeder_server_node::on_receive_(const std::string &data)
{
    //FDEBUG(data);

    if(data == command_send)
    {

    }

    else if(data == "m10")
    {
        motor_one_command_Finished = true;
    }

    else if(data == "m20")
    {
        motor_two_command_Finished = true;
    }

    else if(data == "m11")
    {
        motor_one_command_accepted = true;
    }

    else if(data == "m21")
    {
        motor_two_command_accepted = true;
    }

    else if(data == "m12")
    {
        motorOneErrorAnyFeeder = true;
        motorOneFeedbackAnyFeeder = "Motor 1 invalid command";
    }

    else if(data == "m22")
    {
        motorTwoErrorAnyFeeder = true;
        motorTwoFeedbackAnyFeeder = "Motor 2 invalid command";
    }

    else if(data == "m13")
    {
        motorOneErrorAnyFeeder = true;
        motorOneFeedbackAnyFeeder = "Motor 1 servo error";
    }

    else if(data == "m23")
    {
        motorTwoErrorAnyFeeder = true;
        motorTwoFeedbackAnyFeeder = "Motor 2 servo error";
    }

    else if(data == "m16")
    {
        motorOneErrorAnyFeeder = true;
        motorOneFeedbackAnyFeeder = "Motor 1 not initialised";
    }

    else if(data == "m26")
    {
        motorTwoErrorAnyFeeder = true;
        motorTwoFeedbackAnyFeeder = "Motor 2 not initialised";
    }

    else if(data == "m17")
    {
        motorOneErrorAnyFeeder = true;
        motorOneFeedbackAnyFeeder = "Motor 1 error state";
    }

    else if(data == "m27")
    {
        motorTwoErrorAnyFeeder = true;
        motorTwoFeedbackAnyFeeder = "Motor 2 error state";
    }

    else if(data == "m28")
    {
        motorTwoErrorAnyFeeder = true;
        motorTwoFeedbackAnyFeeder = "Timeout: no sync-signal from motor 1 to motor 2";
    }

}

// Constructor for anyFeeder server
any_feeder_server_node::any_feeder_server_node(ros::NodeHandle& nh) : n(nh)
{
    //FINFO("AnyFeeder Action Servers Initialization");

    // Initialization of the ROS Action Servers
    startAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::StartAnyFeederAction>(n,"start_any_feeder_action",boost::bind(&any_feeder_server_node::startFirmwareAnyFeeder, this, _1), false);
    startAnyFeederServer->start();

    initAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::InitAnyFeederAction>(n,"init_any_feeder_action",boost::bind(&any_feeder_server_node::initAnyFeeder, this, _1), false);
    initAnyFeederServer->start();

    stopAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::StopAnyFeederAction>(n,"stop_any_feeder_action",boost::bind(&any_feeder_server_node::stopAnyFeeder, this, _1), false);
    stopAnyFeederServer->start();

    feedFowardAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::FeedFowardAnyFeederAction>(n,"feed_foward_any_feeder_action",boost::bind(&any_feeder_server_node::feedFowardAnyFeeder, this, _1), false);
    feedFowardAnyFeederServer->start();

    feedBackwardAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::FeedBackwardAnyFeederAction>(n,"feed_backward_any_feeder_action",boost::bind(&any_feeder_server_node::feedBackwardAnyFeeder, this, _1), false);
    feedBackwardAnyFeederServer->start();

    feedFlipFowardAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::FeedFlipFowardAnyFeederAction>(n,"feed_flip_foward_any_feeder_action",boost::bind(&any_feeder_server_node::feedFlipFowardAnyFeeder, this, _1), false);
    feedFlipFowardAnyFeederServer->start();

    feedFlipBackwardAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::FeedFlipBackwardAnyFeederAction>(n,"feed_flip_backward_any_feeder_action",boost::bind(&any_feeder_server_node::feedFlipBackwardAnyFeeder, this, _1), false);
    feedFlipBackwardAnyFeederServer->start();

    flipAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::FlipAnyFeederAction>(n,"flip_any_feeder_action",boost::bind(&any_feeder_server_node::flipAnyFeeder, this, _1), false);
    flipAnyFeederServer->start();

    dispenseAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::DispenseAnyFeederAction>(n,"dispense_any_feeder_action",boost::bind(&any_feeder_server_node::dispenseAnyFeeder, this, _1), false);
    dispenseAnyFeederServer->start();

    purgeAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::PurgeAnyFeederAction>(n,"purge_any_feeder_action",boost::bind(&any_feeder_server_node::purgeAnyFeeder, this, _1), false);
    purgeAnyFeederServer->start();

    heavyDispenseAnyFeederServer = new actionlib::SimpleActionServer<anyfeeder_driver::HeavyDispenseAnyFeederAction>(n,"heavy_dispense_any_feeder_action",boost::bind(&any_feeder_server_node::heavyDispenseAnyFeeder, this, _1), false);
    heavyDispenseAnyFeederServer->start();

    //
    this->end_of_line_char('\r');
    this->resetMotorFlagAnyFeeder();
    stopStatus = false;

    // Init the connection: portname, baudrate
    this->start("/dev/ttyS0",9600);

}

void any_feeder_server_node::startFirmwareAnyFeeder(const anyfeeder_driver::StartAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::StartAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Start Firmware", false, false, false, 1, 1, 1, 1, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        startAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        startAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::initAnyFeeder(const anyfeeder_driver::InitAnyFeederGoalConstPtr &goal)
{
    anyfeeder_driver::InitAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Init", goal->execute, false, false, 1, 1, 16, 1, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        initAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        initAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::stopAnyFeeder(const anyfeeder_driver::StopAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::StopAnyFeederResult myresult;

    //FINFO("Stop all movement requested")

    std::string finishMessage = "Stop all movement executed";

    this->write_some("x=15\r");

    while(motor_one_command_Finished == false && motor_two_command_Finished == false)
    {
        if(motorOneErrorAnyFeeder == true && motorTwoErrorAnyFeeder == true)
        {
            myresult.statusMotorOneMessage = motorOneFeedbackAnyFeeder;
            myresult.statusMotorTwoMessage = motorTwoFeedbackAnyFeeder;
            stopAnyFeederServer->setAborted(myresult);
            return;
        }

        else if(motorOneErrorAnyFeeder == true && motor_two_command_Finished == true)
        {
            myresult.statusMotorOneMessage = motorOneFeedbackAnyFeeder;
            myresult.statusMotorTwoMessage = finishMessage;
            stopAnyFeederServer->setAborted(myresult);
            return;
        }

        else if(motor_one_command_Finished == true && motorTwoErrorAnyFeeder == true)
        {
            myresult.statusMotorOneMessage = finishMessage;
            myresult.statusMotorTwoMessage = motorTwoFeedbackAnyFeeder;
            stopAnyFeederServer->setAborted(myresult);
            return;
        }
    }

    myresult.statusMotorOneMessage = finishMessage;
    myresult.statusMotorTwoMessage = finishMessage;

    stopStatus = true;

    stopAnyFeederServer->setSucceeded(myresult);

    //FINFO("Stop all movement executed")
}


void any_feeder_server_node::feedFowardAnyFeeder(const anyfeeder_driver::FeedFowardAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::FeedFowardAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Feed Foward", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 1, 17, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        feedFowardAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        feedFowardAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::feedBackwardAnyFeeder(const anyfeeder_driver::FeedBackwardAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::FeedBackwardAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Feed Backward", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 2, 18, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        feedBackwardAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        feedBackwardAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::feedFlipFowardAnyFeeder(const anyfeeder_driver::FeedFlipFowardAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::FeedFlipFowardAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Feed Flip Foward", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 3, 19, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        feedFlipFowardAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        feedFlipFowardAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::feedFlipBackwardAnyFeeder(const anyfeeder_driver::FeedFlipBackwardAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::FeedFlipBackwardAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Feed Flip Backward", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 4, 20, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        feedFlipBackwardAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        feedFlipBackwardAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::flipAnyFeeder(const anyfeeder_driver::FlipAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::FlipAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Flip", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 5, 21, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        flipAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        flipAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::dispenseAnyFeeder(const anyfeeder_driver::DispenseAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::DispenseAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Dispense", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 6, 22, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        dispenseAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        dispenseAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::purgeAnyFeeder(const anyfeeder_driver::PurgeAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::PurgeAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Purge", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 7, 23, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;

    if(status)
    {
        purgeAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        purgeAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::heavyDispenseAnyFeeder(const anyfeeder_driver::HeavyDispenseAnyFeederGoalConstPtr& goal)
{
    anyfeeder_driver::HeavyDispenseAnyFeederResult myresult;

    std::string motorOneMessage;
    std::string motorTwoMessage;

    bool status = this->executeMoveCommandAnyFeeder("Heavy Dispense", goal->execute, goal->changeTurns, goal->changeSpeed, goal->turns, goal->speed, 8, 24, &motorOneMessage, &motorTwoMessage);

    myresult.statusMotorOneMessage = motorOneMessage;
    myresult.statusMotorTwoMessage = motorTwoMessage;;

    if(status)
    {
        heavyDispenseAnyFeederServer->setSucceeded(myresult);
    }

    else
    {
        heavyDispenseAnyFeederServer->setAborted(myresult);
    }
}

void any_feeder_server_node::resetMotorFlagAnyFeeder()
{
    motor_one_command_accepted = false;
    motor_two_command_accepted = false;
    motor_one_command_Finished = false;
    motor_two_command_Finished = false;
    motorOneErrorAnyFeeder = false;
    motorTwoErrorAnyFeeder = false;
}


bool any_feeder_server_node::executeMoveCommandAnyFeeder(std::string moveID, bool execute, bool changeTurns, bool changeSpeed, int turns, int speed, int executeCommandNumber, int speedCommandNumber, std::string *motorOneMessage, std::string *motorTwoMessage)
{
    //boost::mutex::scoped_lock look(mutex_command);

    this->resetMotorFlagAnyFeeder();

    std::string finishMessage;

    if(changeSpeed == true && execute == false && changeTurns == false)
    {
        //FINFO("Changing speed for " << moveID << " to " << speed);
        finishMessage = moveID + ": Set speed to " + boost::lexical_cast<std::string>(speed);
        command_send = "ab[" + boost::lexical_cast<std::string>(speedCommandNumber) + "]=" + boost::lexical_cast<std::string>(speed) + " x=" + boost::lexical_cast<std::string>(speedCommandNumber);
    }

    else if(changeTurns == true && execute == false && changeSpeed == false)
    {
        //FINFO("Changing turns for " << moveID << " to " << turns);
        finishMessage = moveID + ": Set repetitions to " + boost::lexical_cast<std::string>(turns);
        command_send = "ab[" + boost::lexical_cast<std::string>(executeCommandNumber) + "]=" + boost::lexical_cast<std::string>(turns);
    }

    else if(changeTurns == true && execute == true && changeSpeed == false)
    {
        //FINFO("Changing turns for " << moveID << " to " << turns << " and executing movement");
        finishMessage = moveID + ": Set and ran " + boost::lexical_cast<std::string>(turns) + " repetitions";
        command_send = "ab[" + boost::lexical_cast<std::string>(executeCommandNumber) + "]=" + boost::lexical_cast<std::string>(turns) + " x=" + boost::lexical_cast<std::string>(executeCommandNumber);
    }

    else if(execute == true && changeTurns == false && changeSpeed == false)
    {
        //FINFO("Executing movement: " << moveID);
        finishMessage = moveID + ": Executed movement";
        command_send = "x=" + boost::lexical_cast<std::string>(executeCommandNumber);
    }

    else
    {
        //FINFO(moveID);
        finishMessage = moveID + ": Done";
        command_send = "RUN";
    }

    this->write_some(command_send + end_of_line_char_);

    if(changeTurns == true && execute == false && changeSpeed == false)
    {}

    else
    {
        while(motor_one_command_Finished == false && motor_two_command_Finished == false)
        {
            if(motorOneErrorAnyFeeder == true && motorTwoErrorAnyFeeder == true)
            {
                *motorOneMessage = motorOneFeedbackAnyFeeder;
                *motorTwoMessage = motorTwoFeedbackAnyFeeder;
                return 0;
            }

            else if(motorOneErrorAnyFeeder == true && motor_two_command_Finished == true)
            {
                *motorOneMessage = motorOneFeedbackAnyFeeder;
                *motorTwoMessage = finishMessage;
                return 0;
            }

            else if(motor_one_command_Finished == true && motorTwoErrorAnyFeeder == true)
            {
                *motorOneMessage = finishMessage;
                *motorTwoMessage = motorTwoFeedbackAnyFeeder;
                return 0;
            }
        }
    }

    *motorOneMessage = finishMessage;
    *motorTwoMessage = finishMessage;

    //FINFO(moveID << " Action Finished");

    return 1;
}



int main(int argc, char** argv)
{
    ros::init(argc, argv, "any_feeder_node");
    ros::NodeHandle n;

    //InitLogger("any_feeder_node",true,true,true);

    new any_feeder_server_node(n);

    ros::spin();

    return 0;
}













