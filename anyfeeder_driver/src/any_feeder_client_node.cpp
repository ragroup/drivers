#include <anyfeeder_driver/any_feeder_client_node.hpp>


/*
-----------------------------------------------------------
    any_feeder_client_node constructor
-----------------------------------------------------------
*/

any_feeder_client_node::any_feeder_client_node()
{
    startAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::StartAnyFeederAction>("start_any_feeder_action",true);
    startAnyFeederClient->waitForServer();

    initAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::InitAnyFeederAction>("init_any_feeder_action",true);
    initAnyFeederClient->waitForServer();

    stopAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::StopAnyFeederAction>("stop_any_feeder_action",true);
    stopAnyFeederClient->waitForServer();

    feedFowardAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::FeedFowardAnyFeederAction>("feed_foward_any_feeder_action",true);
    feedFowardAnyFeederClient->waitForServer();

    feedBackwardAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::FeedBackwardAnyFeederAction>("feed_backward_any_feeder_action",true);
    feedBackwardAnyFeederClient->waitForServer();

    feedFlipFowardAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::FeedFlipFowardAnyFeederAction>("feed_flip_foward_any_feeder_action",true);
    feedFlipFowardAnyFeederClient->waitForServer();

    feedFlipBackwardAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::FeedFlipBackwardAnyFeederAction>("feed_flip_backward_any_feeder_action",true);
    feedFlipBackwardAnyFeederClient->waitForServer();

    flipAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::FlipAnyFeederAction>("flip_any_feeder_action",true);
    flipAnyFeederClient->waitForServer();

    dispenseAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::DispenseAnyFeederAction>("dispense_any_feeder_action",true);
    dispenseAnyFeederClient->waitForServer();

    purgeAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::PurgeAnyFeederAction>("purge_any_feeder_action",true);
    purgeAnyFeederClient->waitForServer();

    heavyDispenseAnyFeederClient = new actionlib::SimpleActionClient<anyfeeder_driver::HeavyDispenseAnyFeederAction>("heavy_dispense_any_feeder_action",true);
    heavyDispenseAnyFeederClient->waitForServer();
}


anyfeeder_driver::StartAnyFeederResultConstPtr any_feeder_client_node::startFirmware()
{
    anyfeeder_driver::StartAnyFeederGoal goal;

    goal.execute = true;

    startAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::StartAnyFeederResultConstPtr myresult = startAnyFeederClient->getResult();

    return(myresult);

}

anyfeeder_driver::InitAnyFeederResultConstPtr any_feeder_client_node::init()
{
    anyfeeder_driver::InitAnyFeederGoal goal;

    goal.execute = true;

    initAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::InitAnyFeederResultConstPtr myresult = initAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::StopAnyFeederResultConstPtr any_feeder_client_node::stop()
{
    anyfeeder_driver::StopAnyFeederGoal goal;

    goal.execute = true;

    stopAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::StopAnyFeederResultConstPtr myresult = stopAnyFeederClient->getResult();

    return(myresult);
}


/*
-----------------------------------------------------------
    Commands related to feed foward movement
-----------------------------------------------------------
*/

//  Function call for executing a feed forward movement
anyfeeder_driver::FeedFowardAnyFeederResultConstPtr any_feeder_client_node::feedFoward()
{
    anyfeeder_driver::FeedFowardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    feedFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = feedFowardAnyFeederClient->getResult();

    return(myresult);
}

//  Function call for changing the speed of a feed forward movement
anyfeeder_driver::FeedFowardAnyFeederResultConstPtr any_feeder_client_node::changeSpeedFeedForward(int speed)
{
    anyfeeder_driver::FeedFowardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    feedFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = feedFowardAnyFeederClient->getResult();

    return(myresult);
}

//  Function call for changing the turns of a feed forward movement
anyfeeder_driver::FeedFowardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsFeedForward(int turns)
{
    anyfeeder_driver::FeedFowardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = feedFowardAnyFeederClient->getResult();

    return(myresult);
}

//  Function call for changing the turns of a feed forward movement and executing a movement
anyfeeder_driver::FeedFowardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteFeedForward(int turns)
{
    anyfeeder_driver::FeedFowardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = feedFowardAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to feed backward movement
-----------------------------------------------------------
*/

anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr any_feeder_client_node::feedBackward()
{
    anyfeeder_driver::FeedBackwardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    feedBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = feedBackwardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr any_feeder_client_node::changeSpeedFeedBackward(int speed)
{
    anyfeeder_driver::FeedBackwardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    feedBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = feedBackwardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsFeedBackward(int turns)
{
    anyfeeder_driver::FeedBackwardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = feedBackwardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteFeedBackward(int turns)
{
    anyfeeder_driver::FeedBackwardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = feedBackwardAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to feed flip foward movement
-----------------------------------------------------------
*/

anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr any_feeder_client_node::feedFlipFoward()
{
    anyfeeder_driver::FeedFlipFowardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    feedFlipFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = feedFlipFowardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr any_feeder_client_node::changeSpeedFeedFlipForward(int speed)
{
    anyfeeder_driver::FeedFlipFowardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    feedFlipFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = feedFlipFowardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsFeedFlipForward(int turns)
{
    anyfeeder_driver::FeedFlipFowardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedFlipFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = feedFlipFowardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteFeedFlipForward(int turns)
{
    anyfeeder_driver::FeedFlipFowardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedFlipFowardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = feedFlipFowardAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to feed flip backward movement
-----------------------------------------------------------
*/

anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr any_feeder_client_node::feedFlipBackward()
{
    anyfeeder_driver::FeedFlipBackwardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    feedFlipBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = feedFlipBackwardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr any_feeder_client_node::changeSpeedFeedFlipBackward(int speed)
{
    anyfeeder_driver::FeedFlipBackwardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    feedFlipBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = feedFlipBackwardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsFeedFlipBackward(int turns)
{
    anyfeeder_driver::FeedFlipBackwardAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedFlipBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = feedFlipBackwardAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteFeedFlipBackward(int turns)
{
    anyfeeder_driver::FeedFlipBackwardAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    feedFlipBackwardAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = feedFlipBackwardAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to flip movement
-----------------------------------------------------------
*/

anyfeeder_driver::FlipAnyFeederResultConstPtr any_feeder_client_node::flip()
{
    anyfeeder_driver::FlipAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    flipAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = flipAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FlipAnyFeederResultConstPtr any_feeder_client_node::changeSpeedFlip(int speed)
{
    anyfeeder_driver::FlipAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    flipAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = flipAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FlipAnyFeederResultConstPtr any_feeder_client_node::changeTurnsFlip(int turns)
{
    anyfeeder_driver::FlipAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    flipAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = flipAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::FlipAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteFlip(int turns)
{
    anyfeeder_driver::FlipAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    flipAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = flipAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to dispense movement
-----------------------------------------------------------
*/

anyfeeder_driver::DispenseAnyFeederResultConstPtr any_feeder_client_node::dispense()
{
    anyfeeder_driver::DispenseAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    dispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = dispenseAnyFeederClient->getResult();

    return(myresult);

}

anyfeeder_driver::DispenseAnyFeederResultConstPtr any_feeder_client_node::changeSpeedDispense(int speed)
{
    anyfeeder_driver::DispenseAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    dispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = dispenseAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::DispenseAnyFeederResultConstPtr any_feeder_client_node::changeTurnsDispense(int turns)
{
    anyfeeder_driver::DispenseAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    dispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = dispenseAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::DispenseAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteDispense(int turns)
{
    anyfeeder_driver::DispenseAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    dispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = dispenseAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to purge movement
-----------------------------------------------------------
*/

anyfeeder_driver::PurgeAnyFeederResultConstPtr any_feeder_client_node::purge()
{
    anyfeeder_driver::PurgeAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    purgeAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = purgeAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::PurgeAnyFeederResultConstPtr any_feeder_client_node::changeSpeedPurge(int speed)
{
    anyfeeder_driver::PurgeAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    purgeAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = purgeAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::PurgeAnyFeederResultConstPtr any_feeder_client_node::changeTurnsPurge(int turns)
{
    anyfeeder_driver::PurgeAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    purgeAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = purgeAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::PurgeAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecutePurge(int turns)
{
    anyfeeder_driver::PurgeAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    purgeAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = purgeAnyFeederClient->getResult();

    return(myresult);
}

/*
-----------------------------------------------------------
    Commands related to heavy dispense movement
-----------------------------------------------------------
*/

anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr any_feeder_client_node::heavyDispense()
{
    anyfeeder_driver::HeavyDispenseAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = false;

    heavyDispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = heavyDispenseAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr any_feeder_client_node::changeSpeedHeavyDispense(int speed)
{
    anyfeeder_driver::HeavyDispenseAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = true;
    goal.changeTurns = false;
    goal.speed = speed;

    heavyDispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = heavyDispenseAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr any_feeder_client_node::changeTurnsHeavyDispense(int turns)
{
    anyfeeder_driver::HeavyDispenseAnyFeederGoal goal;

    goal.execute = false;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    heavyDispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = heavyDispenseAnyFeederClient->getResult();

    return(myresult);
}

anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr any_feeder_client_node::changeTurnsAndExecuteHeavyDispense(int turns)
{
    anyfeeder_driver::HeavyDispenseAnyFeederGoal goal;

    goal.execute = true;
    goal.changeSpeed = false;
    goal.changeTurns = true;
    goal.turns = turns;

    heavyDispenseAnyFeederClient->sendGoalAndWait(goal);

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = heavyDispenseAnyFeederClient->getResult();

    return(myresult);
}
