#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <anyfeeder_driver/any_feeder_client_node.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
        
private slots:
    void defaultParameters();

    void on_initButton_clicked();

    void on_feedFowardButton_clicked();

    void on_feedBackwardButton_clicked();

    void on_feedFlipForwardButton_clicked();

    void on_feedFlipBackwardButton_clicked();

    void on_flipButton_clicked();

    void on_dispenseButton_clicked();

    void on_heavyDispenseButton_clicked();

    void on_purgeButton_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_applySpeedButton_clicked();

    void on_speedSlider_valueChanged(int value);

    void on_resetSpeedButton_clicked();

    void on_applyTurnsFeedForwardButton_clicked();

    void on_runTurnsFeedForwardButton_clicked();

    void on_applyTurnsFeedBackwardButton_clicked();

    void on_runTurnsFeedBackwardButton_clicked();

    void on_applyTurnsFeedFlipForwardButton_clicked();

    void on_runTurnsFeedFlipForwardButton_clicked();

    void on_applyTurnsFeedFlipBackwardButton_clicked();

    void on_runTurnsFeedFlipBackwardButton_clicked();

    void on_applyTurnsFlipButton_clicked();

    void on_runTurnsFlipButton_clicked();

    void on_applyTurnsDispenseButton_clicked();

    void on_runTurnsDispenseButton_clicked();

    void on_applyTurnsHeavyDispenseButton_clicked();

    void on_runTurnsHeavyDispenseButton_clicked();

    void on_applyTurnsPurgeButton_clicked();

    void on_runTurnsPurgeButton_clicked();

    void on_applyAllTurnsButton_clicked();

    void on_startFirmwareButton_clicked();

    bool turnsCheck(int turns, QString move);

    void on_stopButton_clicked();

    void writeToMotorLineEdit(std::string m1, std::string m2);

private:
    Ui::MainWindow *ui;
    any_feeder_client_node* client;
    bool stopStatus;

    // Speed variables
    std::string listItemSelected;
    int feedForwardSpeed;
    int feedForwardOldSpeed;
    int feedBackwardSpeed;
    int feedBackwardOldSpeed;
    int feedFlipForwardSpeed;
    int feedFlipForwardOldSpeed;
    int feedFlipBackwardSpeed;
    int feedFlipBackwardOldSpeed;
    int flipSpeed;
    int flipOldSpeed;
    int dispenseSpeed;
    int dispenseOldSpeed;
    int heavyDispenseSpeed;
    int heavyDispenseOldSpeed;
    int purgeSpeed;
    int purgeOldSpeed;

    // Turns variables
    int feedForwardTurns;
    int feedBackwardTurns;
    int feedFlipForwardTurns;
    int feedFlipBackwardTurns;
    int flipTurns;
    int dispenseTurns;
    int heavyDisepenseTurns;
    int purgeTurns;

};

#endif // MAINWINDOW_H
