#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

#define DEFAULT_SPEED 1
#define DEFAULT_TURNS 3
#define DEFAULT_TURNS_PURGE 64

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle(tr("Adept AnyFeeder - GUI"));

    // init buttons
    ui->initButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
    ui->executeFrame->setEnabled(false);
    ui->speedFrame->setEnabled(false);
    ui->turnsFrame->setEnabled(false);
    ui->feedBackFrame->setEnabled(false);

    // init speedbar
    ui->speedBar->setValue(0);
    ui->speedSlider->setValue(0);

    stopStatus = false;

    client = new any_feeder_client_node();
}

MainWindow::~MainWindow()
{
    delete ui;    
}

void MainWindow::defaultParameters()
{
    // Default Speed
    feedForwardSpeed = DEFAULT_SPEED;
    feedForwardOldSpeed = DEFAULT_SPEED;
    feedBackwardSpeed = DEFAULT_SPEED;
    feedBackwardOldSpeed = DEFAULT_SPEED;
    feedFlipForwardSpeed = DEFAULT_SPEED;
    feedFlipForwardOldSpeed = DEFAULT_SPEED;
    feedFlipBackwardSpeed = DEFAULT_SPEED;
    feedFlipBackwardOldSpeed = DEFAULT_SPEED;
    flipSpeed = DEFAULT_SPEED;
    flipOldSpeed = DEFAULT_SPEED;
    dispenseSpeed = DEFAULT_SPEED;
    dispenseOldSpeed = DEFAULT_SPEED;
    heavyDispenseSpeed = DEFAULT_SPEED;
    heavyDispenseOldSpeed = DEFAULT_SPEED;
    purgeSpeed = DEFAULT_SPEED;
    purgeOldSpeed = DEFAULT_SPEED;

    // Default Turns
    feedForwardTurns = DEFAULT_TURNS;
    ui->feedForwardCurrentLineEdit->setText(QString::number(feedForwardTurns));
    ui->feedForwardSetLineEdit->setText(QString::number(feedForwardTurns));

    feedBackwardTurns = DEFAULT_TURNS;
    ui->feedBackwardCurrentLineEdit->setText(QString::number(feedBackwardTurns));
    ui->feedBackwardSetLineEdit->setText(QString::number(feedBackwardTurns));

    feedFlipForwardTurns = DEFAULT_TURNS;
    ui->feedFlipForwardCurrentLineEdit->setText(QString::number(feedFlipForwardTurns));
    ui->feedFlipForwardSetLineEdit->setText(QString::number(feedFlipForwardTurns));

    feedFlipBackwardTurns = DEFAULT_TURNS;
    ui->feedFlipBackwardCurrentLineEdit->setText(QString::number(feedFlipBackwardTurns));
    ui->feedFlipBackwardSetLineEdit->setText(QString::number(feedFlipBackwardTurns));

    flipTurns = DEFAULT_TURNS;
    ui->flipCurrentLineEdit->setText(QString::number(feedFlipBackwardTurns));
    ui->flipSetLineEdit->setText(QString::number(feedFlipBackwardTurns));

    dispenseTurns = DEFAULT_TURNS;
    ui->dispenseCurrentLineEdit->setText(QString::number(dispenseTurns));
    ui->dispenseSetLineEdit->setText(QString::number(dispenseTurns));

    heavyDisepenseTurns = DEFAULT_TURNS;
    ui->heavyDispenseCurrentLineEdit->setText(QString::number(heavyDisepenseTurns));
    ui->heavyDispenseSetLineEdit->setText(QString::number(heavyDisepenseTurns));

    purgeTurns = DEFAULT_TURNS_PURGE;
    ui->purgeCurrentLineEdit->setText(QString::number(purgeTurns));
    ui->purgeSetLineEdit->setText(QString::number(purgeTurns));
}

void MainWindow::on_startFirmwareButton_clicked()
{
    defaultParameters();
    anyfeeder_driver::StartAnyFeederResultConstPtr myresult = client->startFirmware();

    ui->motorOneLineEdit->setText(QString::fromStdString(myresult->statusMotorOneMessage));
    ui->motorTwoLineEdit->setText(QString::fromStdString(myresult->statusMotorTwoMessage));

    ui->initButton->setEnabled(true);
}

void MainWindow::on_initButton_clicked()
{
    anyfeeder_driver::InitAnyFeederResultConstPtr myresult = client->init();

    ui->motorOneLineEdit->setText(QString::fromStdString(myresult->statusMotorOneMessage));
    ui->motorTwoLineEdit->setText(QString::fromStdString(myresult->statusMotorTwoMessage));

    ui->executeFrame->setEnabled(true);
    ui->speedFrame->setEnabled(true);
    ui->turnsFrame->setEnabled(true);
    ui->stopButton->setEnabled(true);
}

void MainWindow::on_stopButton_clicked()
{
    anyfeeder_driver::StopAnyFeederResultConstPtr myresult = client->stop();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    stopStatus = true;
}

void MainWindow::on_feedFowardButton_clicked()
{
    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = client->feedFoward();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_feedBackwardButton_clicked()
{
    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = client->feedBackward();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_feedFlipForwardButton_clicked()
{
    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = client->feedFlipFoward();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_feedFlipBackwardButton_clicked()
{
    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = client->feedFlipBackward();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_flipButton_clicked()
{
    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = client->flip();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_dispenseButton_clicked()
{
    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = client->dispense();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_heavyDispenseButton_clicked()
{
    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = client->heavyDispense();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_purgeButton_clicked()
{
    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = client->purge();

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    listItemSelected = item->text().toStdString();

    if(listItemSelected == "Feed Foward") {ui->speedSlider->setValue(feedForwardSpeed);}
    else if(listItemSelected == "Feed Backward"){ui->speedSlider->setValue(feedBackwardSpeed);}
    else if(listItemSelected == "Feed Flip Forward"){ui->speedSlider->setValue(feedFlipForwardSpeed);}
    else if(listItemSelected == "Feed Flip Backward"){ui->speedSlider->setValue(feedFlipBackwardSpeed);}
    else if(listItemSelected == "Flip"){ui->speedSlider->setValue(flipSpeed);}
    else if(listItemSelected == "Dispense"){ui->speedSlider->setValue(dispenseSpeed);}
    else if(listItemSelected == "Heavy Dispense"){ui->speedSlider->setValue(heavyDispenseSpeed);}
    else if(listItemSelected == "Purge"){ui->speedSlider->setValue(purgeSpeed);}
}

void MainWindow::on_speedSlider_valueChanged(int value)
{
    if(listItemSelected == "Feed Foward") {feedForwardSpeed = value;}
    else if(listItemSelected == "Feed Backward"){feedBackwardSpeed = value;}
    else if(listItemSelected == "Feed Flip Forward"){feedFlipForwardSpeed = value;}
    else if(listItemSelected == "Feed Flip Backward"){feedFlipBackwardSpeed = value;}
    else if(listItemSelected == "Flip"){flipSpeed = value;}
    else if(listItemSelected == "Dispense"){dispenseSpeed = value;}
    else if(listItemSelected == "Heavy Dispense"){heavyDispenseSpeed = value;}
    else if(listItemSelected == "Purge"){purgeSpeed = value;}
}

void MainWindow::on_applySpeedButton_clicked()
{
    if(feedForwardSpeed != feedForwardOldSpeed)
    {
        feedForwardOldSpeed = feedForwardSpeed;

        anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = client->changeSpeedFeedForward(feedForwardSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(feedBackwardSpeed != feedBackwardOldSpeed)
    {
        feedBackwardOldSpeed = feedBackwardSpeed;

        anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = client->changeSpeedFeedBackward(feedBackwardSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(feedFlipForwardSpeed != feedFlipForwardOldSpeed)
    {
        feedFlipForwardOldSpeed = feedFlipForwardSpeed;

        anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = client->changeSpeedFeedFlipForward(feedFlipForwardSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(feedFlipBackwardSpeed !=feedFlipBackwardOldSpeed)
    {
        feedFlipBackwardOldSpeed = feedFlipBackwardSpeed;

        anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = client->changeSpeedFeedFlipBackward(feedFlipBackwardSpeed);;

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(flipSpeed != flipOldSpeed)
    {
        flipOldSpeed = flipSpeed;

        anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = client->changeSpeedFlip(flipSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(dispenseSpeed != dispenseOldSpeed)
    {
        dispenseOldSpeed = dispenseSpeed;

        anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = client->changeSpeedDispense(dispenseSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(heavyDispenseSpeed != heavyDispenseOldSpeed)
    {
        heavyDispenseOldSpeed = heavyDispenseSpeed;

        anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = client->changeSpeedHeavyDispense(heavyDispenseSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }

    if(purgeSpeed != purgeOldSpeed)
    {
        purgeOldSpeed = purgeSpeed;

        anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = client->changeSpeedPurge(purgeSpeed);

        this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);
    }
}

void MainWindow::on_resetSpeedButton_clicked()
{
    feedForwardSpeed = feedForwardOldSpeed;
    feedBackwardSpeed = feedBackwardOldSpeed;
    feedFlipForwardSpeed = feedFlipForwardOldSpeed;
    feedFlipBackwardSpeed = feedFlipBackwardOldSpeed;
    flipSpeed = flipOldSpeed;
    dispenseSpeed = dispenseOldSpeed;
    heavyDispenseSpeed = heavyDispenseOldSpeed;
    purgeSpeed = purgeOldSpeed;
}

void MainWindow::on_applyTurnsFeedForwardButton_clicked()
{
    QString turns = ui->feedForwardSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Feed Forward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = client->changeTurnsFeedForward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedForwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsFeedForwardButton_clicked()
{
    QString turns = ui->feedForwardSetLineEdit->text();
    int turns_ = turns.toInt();    

    bool status = this->turnsCheck(turns_, "Feed Forward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteFeedForward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedForwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsFeedBackwardButton_clicked()
{
    QString turns = ui->feedBackwardSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Feed Backward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = client->changeTurnsFeedBackward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedBackwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsFeedBackwardButton_clicked()
{
    QString turns = ui->feedBackwardSetLineEdit->text();
    int turns_ = turns.toInt();    

    bool status = this->turnsCheck(turns_, "Feed Backward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteFeedBackward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedBackwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsFeedFlipForwardButton_clicked()
{
    QString turns = ui->feedFlipForwardSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Feed Flip Forward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = client->changeTurnsFeedFlipForward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedFlipForwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsFeedFlipForwardButton_clicked()
{
    QString turns = ui->feedFlipForwardSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Feed Flip Forward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteFeedFlipForward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedFlipForwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsFeedFlipBackwardButton_clicked()
{
    QString turns = ui->feedFlipBackwardSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Feed Flip Backward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = client->changeTurnsFeedFlipBackward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedFlipBackwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsFeedFlipBackwardButton_clicked()
{
    QString turns = ui->feedFlipBackwardSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Feed Flip Backward");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteFeedFlipBackward(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->feedFlipBackwardCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsFlipButton_clicked()
{
    QString turns = ui->flipSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Flip");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = client->changeTurnsFlip(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->flipCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsFlipButton_clicked()
{
    QString turns = ui->flipSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Flip");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::FlipAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteFlip(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->flipSetLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsDispenseButton_clicked()
{
    QString turns = ui->dispenseSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Dispense");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = client->changeTurnsDispense(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->dispenseCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsDispenseButton_clicked()
{
    QString turns = ui->dispenseSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Dispense");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::DispenseAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteDispense(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->dispenseCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsHeavyDispenseButton_clicked()
{
    QString turns = ui->heavyDispenseSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Heavy Dispense");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = client->changeTurnsHeavyDispense(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->heavyDispenseCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsHeavyDispenseButton_clicked()
{
    QString turns = ui->heavyDispenseSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Heavy Dispense");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr myresult = client->changeTurnsAndExecuteHeavyDispense(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->heavyDispenseCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyTurnsPurgeButton_clicked()
{
    QString turns = ui->purgeSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Purge");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = client->changeTurnsPurge(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->purgeCurrentLineEdit->setText(turns);
}

void MainWindow::on_runTurnsPurgeButton_clicked()
{
    QString turns = ui->purgeSetLineEdit->text();
    int turns_ = turns.toInt();

    bool status = this->turnsCheck(turns_, "Purge");

    if(status == false)
    {
        return;
    }

    anyfeeder_driver::PurgeAnyFeederResultConstPtr myresult = client->changeTurnsAndExecutePurge(turns_);

    this->writeToMotorLineEdit(myresult->statusMotorOneMessage, myresult->statusMotorTwoMessage);

    ui->purgeCurrentLineEdit->setText(turns);
}

void MainWindow::on_applyAllTurnsButton_clicked()
{
    this->on_applyTurnsFeedForwardButton_clicked();
    this->on_applyTurnsFeedBackwardButton_clicked();
    this->on_applyTurnsFeedFlipForwardButton_clicked();
    this->on_applyTurnsFeedFlipBackwardButton_clicked();
    this->on_applyTurnsFlipButton_clicked();
    this->on_applyTurnsDispenseButton_clicked();
    this->on_applyTurnsHeavyDispenseButton_clicked();
    this->on_applyTurnsPurgeButton_clicked();
}

bool MainWindow::turnsCheck(int turns, QString move)
{
    if(turns < 1 || turns > 127)
    {
        QMessageBox::critical(this,move + " Repetitions Settings Error",move + " Repetitions can only be 1-127");

        return 0;
    }

    else
    {
        return 1;
    }

}

void MainWindow::writeToMotorLineEdit(std::string m1, std::string m2)
{
    if(stopStatus)
    {
        stopStatus = false;
        return;
    }

    else
    {
        ui->motorOneLineEdit->setText(QString::fromStdString(m1));
        ui->motorTwoLineEdit->setText(QString::fromStdString(m2));
    }
}
