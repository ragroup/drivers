#ifndef __anyfeeder_driver_any_feeder_client_h
#define __anyfeeder_driver_any_feeder_client_h

#include "ros/ros.h"
#include <actionlib/client/simple_action_client.h>

#include <anyfeeder_driver/StartAnyFeederAction.h>
#include <anyfeeder_driver/InitAnyFeederAction.h>
#include <anyfeeder_driver/StopAnyFeederAction.h>
#include <anyfeeder_driver/FeedFowardAnyFeederAction.h>
#include <anyfeeder_driver/FeedBackwardAnyFeederAction.h>
#include <anyfeeder_driver/FeedFlipFowardAnyFeederAction.h>
#include <anyfeeder_driver/FeedFlipBackwardAnyFeederAction.h>
#include <anyfeeder_driver/FlipAnyFeederAction.h>
#include <anyfeeder_driver/DispenseAnyFeederAction.h>
#include <anyfeeder_driver/PurgeAnyFeederAction.h>
#include <anyfeeder_driver/HeavyDispenseAnyFeederAction.h>

class any_feeder_client_node
{
public:
    any_feeder_client_node();

    anyfeeder_driver::StartAnyFeederResultConstPtr startFirmware();
    anyfeeder_driver::InitAnyFeederResultConstPtr init();
    anyfeeder_driver::StopAnyFeederResultConstPtr stop();

    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr feedFoward();
    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr changeSpeedFeedForward(int speed);
    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr changeTurnsFeedForward(int turns);
    anyfeeder_driver::FeedFowardAnyFeederResultConstPtr changeTurnsAndExecuteFeedForward(int turns);

    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr feedBackward();
    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr changeSpeedFeedBackward(int speed);
    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr changeTurnsFeedBackward(int turns);
    anyfeeder_driver::FeedBackwardAnyFeederResultConstPtr changeTurnsAndExecuteFeedBackward(int turns);

    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr feedFlipFoward();
    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr changeSpeedFeedFlipForward(int speed);
    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr changeTurnsFeedFlipForward(int turns);
    anyfeeder_driver::FeedFlipFowardAnyFeederResultConstPtr changeTurnsAndExecuteFeedFlipForward(int turns);

    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr feedFlipBackward();
    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr changeSpeedFeedFlipBackward(int speed);
    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr changeTurnsFeedFlipBackward(int turns);
    anyfeeder_driver::FeedFlipBackwardAnyFeederResultConstPtr changeTurnsAndExecuteFeedFlipBackward(int turns);

    anyfeeder_driver::FlipAnyFeederResultConstPtr flip();
    anyfeeder_driver::FlipAnyFeederResultConstPtr changeSpeedFlip(int speed);
    anyfeeder_driver::FlipAnyFeederResultConstPtr changeTurnsFlip(int turns);
    anyfeeder_driver::FlipAnyFeederResultConstPtr changeTurnsAndExecuteFlip(int turns);

    anyfeeder_driver::DispenseAnyFeederResultConstPtr dispense();
    anyfeeder_driver::DispenseAnyFeederResultConstPtr changeSpeedDispense(int speed);
    anyfeeder_driver::DispenseAnyFeederResultConstPtr changeTurnsDispense(int turns);
    anyfeeder_driver::DispenseAnyFeederResultConstPtr changeTurnsAndExecuteDispense(int turns);

    anyfeeder_driver::PurgeAnyFeederResultConstPtr purge();
    anyfeeder_driver::PurgeAnyFeederResultConstPtr changeSpeedPurge(int speed);
    anyfeeder_driver::PurgeAnyFeederResultConstPtr changeTurnsPurge(int turns);
    anyfeeder_driver::PurgeAnyFeederResultConstPtr changeTurnsAndExecutePurge(int turns);

    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr heavyDispense();
    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr changeSpeedHeavyDispense(int speed);
    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr changeTurnsHeavyDispense(int turns);
    anyfeeder_driver::HeavyDispenseAnyFeederResultConstPtr changeTurnsAndExecuteHeavyDispense(int turns);

private:

    actionlib::SimpleActionClient<anyfeeder_driver::StartAnyFeederAction>* startAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::InitAnyFeederAction>* initAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::StopAnyFeederAction>* stopAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::FeedFowardAnyFeederAction>* feedFowardAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::FeedBackwardAnyFeederAction>* feedBackwardAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::FeedFlipFowardAnyFeederAction>* feedFlipFowardAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::FeedFlipBackwardAnyFeederAction>* feedFlipBackwardAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::FlipAnyFeederAction>* flipAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::DispenseAnyFeederAction>* dispenseAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::PurgeAnyFeederAction>* purgeAnyFeederClient;
    actionlib::SimpleActionClient<anyfeeder_driver::HeavyDispenseAnyFeederAction>* heavyDispenseAnyFeederClient;


};

#endif
