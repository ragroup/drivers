'''
This is a custom ported rest_simulation for MiR100
with robot software version 2.3.0 and API verison 2.0.0.
should be used with rest_simulation_2_5.py

This port has NOT yet been fully tested!

Author: Emil Blixt Hansen
Mail:   ebh@mp.aau.dk

Any questions feel free to contact, and I will do my best!

'''
import cPickle
import requests
import json

# timeout for http requests
timeout = 5  # seconds

# header
header = {'Content-Type': 'application/json',
          'Accept-Language': 'en_US',
          'Host': 'mir.com:8080',
          'Authorization':
              'Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA=='}
# Note that the authorization code should definitely not be here; however, it is easier for now...

base_url = "http://mir.com/api/v2.0.0/"

register_url = base_url + "register/"
position_url = base_url + "positions/"
mission_url = base_url + "missions/"
sound_url = base_url + "sounds/"
session_url = base_url + "session/"
status_url = base_url + "status/"
robot_url = base_url + "robot/"
map_url = base_url + "maps/"
log_url = base_url + "log/"


def start():
    # set url's as global variables
    global base_url, register_url, position_url
    global mission_url, session_url, status_url, robot_url, map_url


def close():
    try:
        cPickle.dump(base_url, open("previous_ip.p", "wb"))
        print "Robot ip is saved"
    except:
        raise ValueError("Was not able to save ip of robot in close(),\ncheck your path")


# SET/GET/DELETE and PUT functions for the current version of the REST API v2.0.0
# 	Status, Robot, Mission, Session, Map, Position, Register, Log

# ----------------------------------- Status ---------------------------------------#
# #     MiR100 States       Description
# 0     None
# 1     Starting            MiR100 starting up
# 2     Shuttingdown        MiR100 Shutting down
# 3     Ready               Ready to execute
# 4     Pause               Pause from executing
# 5     Executing           Running a mission
# 6     Aborted             Mission aborted
# 7     Completed           Done executing
# 8     Docked              In the dock and charging the batteries
# 9     Docking             Navigating to docking station
# 10    Emergencystop       Emergency button is activated
# 11    Manualcontrol       A pause state where MiR100 can be moved manually
# 12    Error               General error state - requires error handling

def get_status():
    # Retrieve the status of the robot
    # TOBE implemented hook and IO modules see DOC
    response = requests.get(status_url, headers=header, timeout=timeout)
    return response.json()


# defualt state is pause!
def set_robot_state(robot_state):
    gets_possible = {"continue": 3, "pause": 4}
    if robot_state in gets_possible:
        data_robot_state = {"state_id": gets_possible[robot_state]}
        response = requests.put(robot_url, data=json.dumps(data_robot_state), headers=header)
        if (response.json())['success'] == 'false':
            print "robot is already in mode: ", robot_state
        else:
            return response.json()
    else:
        raise ValueError("The robot command %s, is neither of these %d" % (gets_possible, robot_state))


# ----------------------------------- Robots ---------------------------------------#
def put_robot():
    # Add information about other robots in the world to the robot.
    # This is used by the Fleet manager to avoid robot collisions
    raise NotImplementedError


# ----------------------------------- Mission ---------------------------------------#
def delete_mission(mission_id):
    # Erase the mission with the specified GUID
    response = requests.delete(mission_url + str(mission_id), headers=header)
    if (response.json())['success'] == 'false':
        raise ValueError("The mission id: %d was not found and therefore not deleted in delete_mission()" % mission_id)
    else:
        return response.json()


def clear_missions():
    # NOT TESTED; USE delete_mission
    response = requests.delete(mission_url)
    return response.json()


def get_mission_information(mission_info):
    # NOT TESTED!
    get_possible = ['active', 'available', 'positions', 'queue']
    if mission_info in get_possible:
        response = requests.get(mission_url + mission_info, timeout=timeout)
        data_mission_info = response.json()
        return data_mission_info
    else:
        raise ValueError("The argument %s in get_mission_information() doesn't match either one of %s:"
                         % (mission_info, get_possible))


def get_mission_status(mission_id='None'):
    # lists all missions if no mission_id = 'None'
    if mission_id == 'None':
        response = requests.get(mission_url, headers=header, timeout=timeout)
        return response.json()
    else:
        # check if the mission is in our session queue
        response = requests.get(mission_url + str(mission_id), headers=header, timeout=timeout)
        if (response.json())['success'] == 'false':
            raise ValueError("The mission id: %d was not found in get_mission()" % mission_id)
        else:
            return response.json()


# setting mission payload type 1
def set_mission(type_, name):
    # NOT IMPLEMENTED
    raise NotImplementedError
    # try:
    #     data_mission = {"type": type_, "name": name}
    #     response = requests.post(mission_url, data=json.dumps(data_mission))
    #     return response.json()
    # except:
    #     raise ValueError("Unable to create mission with type: %d and name: %s in set_mission()" % (type_, name))



# setting mission payload type 2
def set_taxa(type_, name):
    # NOT IMPLEMENTED
    raise NotImplementedError
    # try:
    #     data_taxa = {"type": type_, "name": name}
    #     response = requests.post(mission_url, data=json.dumps(data_taxa))
    #     return response.json()
    # except:
    #     raise ValueError("Unable to create mission with type: %d and name: %s in set_taxa()" % (type_, name))


def set_taxa_pose(type_, x=0, y=0, orientation=0):
    # NOT IMPLEMENTED
    raise NotImplementedError
    # try:
    #     data_taxa_pose = {"type": type_, "x": x, "y": y, "orientation": orientation}
    #     response = requests.post(mission_url, data=json.dumps(data_taxa_pose))
    #     return response.json()
    # except:
    #     raise ValueError("Unable to create taxi+pose in set_taxa_pose()")


# ----------------------------------- Session ---------------------------------------#
def get_session_list():
    response = requests.get(session_url, timeout=timeout)
    return response.json()


def get_session(session_id):
    response = requests.get(session_url + str(session_id), timeout=timeout)
    if (response.json())['success'] == 'false':
        raise ValueError("The session with id: %d was not found in get_session()" % session_id)
    else:
        return response.json()


# ----------------------------------- Map ---------------------------------------#
def get_map_list():
    response = requests.get(map_url, headers=header, timeout=timeout)
    return response.json()


def get_map(map_id):
    response = requests.get(map_url + str(map_id), headers=header, timeout=timeout)
    return response.json()


# ----------------------------------- Position ---------------------------------------#
def delete_position(position_id):
    response = requests.delete(position_url + str(position_id), headers=header)
    if response:
        return 1
    else:
        return 0


'''
    print response
    if (response.json())['success'] == 'false':
    	raise ValueError("The position id: %d was not found and therefore not deleted in delete_position()" % position_id)
    else:
        return response.json()
'''


def get_position_list():
    response = requests.get(position_url, headers=header, timeout=timeout)
    return response.json()


def get_position(position_id):
    response = requests.get(position_url + str(position_id), timeout=timeout)
    if (response.json())['success'] == 'false':
        raise ValueError("I couldn't find the position with the id:%d" % position_id)
    else:
        return response.json()


def get_position_relevant(map_id):
    # get all positions and extract size
    response = requests.get(map_url + str(map_id) + '/positions/', headers=header, timeout=timeout)
    return response.json()


def post_position(name, pos_x, pos_y, orientation, type_=0, map_id=None, guid=None, parent_id=None, created_by_id=None):

    data_position = {
        "name": name,
        "pos_x": pos_x,
        "pos_y": pos_y,
        "orientation": orientation,
        "type_id": type_
    }

    if map_id is not None:
        data_position.update({"map_id": map_id})
    else:
        map_id = get_status()["map_id"]
        data_position.update({"map_id": map_id})

    if guid is not None:
        data_position.update({"guid": guid})
    if parent_id is not None:
        data_position.update({"parent_id": parent_id})
    if created_by_id is not None:
        data_position.update({"created_by_id": created_by_id})
    print(json.dumps(data_position))
    try:
        response = requests.post(position_url, data=json.dumps(data_position), headers=header)
        print(response.json())
        if "error_code" in (response.json()):
        #if (response.json())['success'] == 'false':
            # raise ValueError("position with id: %d was not found" % position_id)
            # print "position with id:", position_id
            return None
        #            raise ValueError('position with id %d, was not found' % position_id)
        else:
            return response.json()
    except:
        return None


#        raise ValueError("was not able to update position:\nid:%d\nname:%s\ntype_:%d\nmap_id:%d" % (position_id, name, type_, map_id))

# ------------------------------------ Sound -----------------------------------------#
def get_sound_list():
    response = requests.get(sound_url, headers=header, timeout=timeout)
    return response.json()


# ----------------------------------- Register ---------------------------------------#
def get_register_value(register_id):
    if register_id in range(1, 201):
        response = requests.get(register_url + str(register_id), timeout=timeout)
        return (response.json())['value']
    else:
        raise ValueError(
            "The registered id number:%d does not exist.\nValid inputs for get_register_value() are [1,200]" % register_id)


def get_register_list():
    response = requests.get(register_url, timeout=timeout)
    return response.json()


def get_register_range(interval1, interval2):
    if interval1 < interval2 and interval1 in range(1, 201) and interval2 in range(1, 201):
        response = requests.get(register_url + str(interval1) + "/" + str(interval2) + "/", timeout=timeout)
        return response.json()
    elif interval1 > interval2:
        raise ValueError("interval 1 should be less than interval 2 in get_register_range(interval 1, interval 2)")
    else:
        raise ValueError(
            "interval 1 = %d, interval 2 = %d. One of the intervals is not in the range [1,200] in get_register_range()" % (
            interval1, interval2))


def set_register(register_id, register_value):
    if register_id in range(1, 201):
        data_register = {'value': register_value}
        try:
            response = requests.post(register_url + str(register_id), data=json.dumps(data_register))
            print response.json()
        except:
            raise valueError("Error in set_register")
    else:
        raise valueError("The registered id number: %d does not exist.\nValid inputs are [1,200]." % register_id)


def clear_registers():
    data_register = {'value': 0}
    for i in range(1, 201):
        requests.post(register_url + str(i), data=json.dumps(data_register))


# ----------------------------------- Log ---------------------------------------#
def get_log_list():
    response = requests.get(log_url, timeout=timeout)
    return response.json()


def get_log(log_id):
    response = requests.get(log_url + str(log_id), timeout=timeout)
    if (response.json())['success'] == 'false':
        raise ValueError("The log with id: %d was not found in get_log()" % log_id)
    else:
        return response.json()
