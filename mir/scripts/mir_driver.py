#!/usr/bin/env python
#import rosbridge_2_0 as rb
import rosbridge_2_5 as rb
#from rosbridge_2_0 import time
from rosbridge_2_5 import time
#from rosbridge_2_0 import json
from rosbridge_2_5 import json

from mir.srv import *
#import rest_simulation as mir_rest
import rest_simulation_2_5 as mir_rest
import rospy
import unicodedata
import requests
import os
import numpy as np
import time
import sys
from PIL import Image
import uuid
import sys

mir_rest.start()
robot_rosbridge = rb.boot('192.168.12.20')
sym = 0
loader = {0: '-', 1: '/', 2: '\\'}
#while(len(rb.data['pose'])) == 0:
while not robot_rosbridge.is_connected() or len(rb.data['pose']) == 0:

    sys.stdout.write("\r{} Waiting for connection...".format(loader[sym]))
    sys.stdout.flush()
    sym += 1
    if sym >= 3:
        sym = 0
        break  # TODO: only breaks now since ros_bridge is not functional yet
    time.sleep(0.5)
print("\nConnected!")

# Do enable/disable printing of driver info
USE_DEBUG = True


def DEBUG(msg, val=None):
    if USE_DEBUG:
        if val is None:
            print(msg)
        else:
            print(msg, val)


# has been tested: TRUE
def create_position(req):

    position_list = mir_rest.get_position_list()
    # print json.dumps(position_list, indent=2)
    for pos in position_list:
        if pos['name'] == req.name:
            DEBUG('Unable to create position - name already taken')
            return MirCreatePositionResponse(False)

    DEBUG('creating position ', req.name)

    response = mir_rest.post_position(req.name, req.pos_x, req.pos_y, req.orientation, req.type_id)

    if response is None:
        DEBUG('position creation failed!')
        return MirCreatePositionResponse(False)
    else:
        DEBUG('position creation Succeeded')
        return MirCreatePositionResponse(True)


# has been tested: TRUE
def delete_position(req):
    id_ = req.id
    if mir_rest.delete_position(id_):
        return MirDeletePositionResponse(True)
    else:
        return MirDeletePositionResponse(False)


# add some more error checking
# this function can also give the image with the service return value, but it is not currently implemented
# has been tested: false
def get_current_map_image(req):
    # wait for pose data - do not uncomment
    DEBUG('Waiting for connection with mir in get_current_map_image')
    # this should be moved to a general function so we don't have to run it in every function.. or what?
    while(len(rb.data['pose'])) == 0:
        # print "waiting for pos - 0.5s"
        time.sleep(0.5)
    DEBUG('Connected')
    # get the map width and height - used when the image has to be saved
    msg = robot_rosbridge.callService("/map_metadata")
    width = msg['metadata']['width']
    height = msg['metadata']['height']

    # Get currently used map
    msg = robot_rosbridge.callService("/static_map")

    # extract the map(pixel values) from the dict object

    a = msg['map']['data']

    size = int(len(a))

    # do some color correction to enhance visability - can be omitted
    a = [x+64 for x in a]

    # pixel_array= np.array(a)
    # save the map - it will be saved in the catkin_ws folder (not in the mir folder)

    new_img = Image.new("L", (width, height), "white")
    new_img.putdata(a)
    fullpath = os.path.join(req.imagePath, 'curMap.jpg')

    new_img.save(fullpath)
    return MirGetCurrentMapImageResponse(True, height,width)


# add some more error checking -- If robot is not in any mab, this function will fail!!
# has been tested: TRUE
def get_current_map_info(req):
    DEBUG('getting current map info..')
    map_id = mir_rest.get_status()['map_id']
    map_name = mir_rest.get_map(map_id)['name']
    DEBUG('map_id: ', map_id)
    DEBUG('map_name: ', map_name)
    return MirGetCurrentMapInfoResponse(True, str(map_id), str(map_name))


# has been tested: TRUE
def get_current_position(req):
    DEBUG('getting current position')
    try:
        pos = mir_rest.get_status()['position']
        return MirGetCurrentPositionResponse(int(1), float(pos["x"]), float(pos["y"]), float(pos["orientation"]))
    except KeyError:
        DEBUG("Unable to retrieve position")
        return MirGetCurrentPositionResponse(int(0), float(0.0), float(0.0), float(0.0))



# this function can also give the image with the service return value, but it is not currently implemented
# has been tested: false
def get_dynamic_map_image(req):
    # wait for pose data - do not uncomment
    # this function can also give the image with the service return value, but it is not currently implemented
    DEBUG('connecting to robot..')
    while(len(rb.data['pose'])) == 0:
        # print "waiting for pos - 0.5s"
        time.sleep(0.5)
    DEBUG('connected')
    # Get map while its being mapped
    data = robot_rosbridge.callService("/dynamic_map")

    a = data['map']['data']

    # do some color correcting to give a clear image
    for i in range(len(a)):
        if a[i] > 90:
            a[i] = 0
        elif a[i] == 0:
            a[i] = 255
        else:
            a[i] = 100
    b = a

    # ADD: The image is mirrored so it needs to be reversed
    DEBUG('Note: the image is mirrored')
    new_img = Image.new("L", (2000, 2000), "white")
    new_img.putdata(a)
    fullpath = os.path.join(req.imagePath, 'dynamicMap.jpg')

    new_img.save(fullpath)

    return MirGetDynamicMapImageResponse(True)


# add some error checking
# has been tested: TRUE
def get_map_list(req):
    DEBUG('getting map list')
    maps = mir_rest.get_map_list()

    if 'error_code' in maps:
        return MirGetMapListResponse(0, [])

    map_names = []
    for map in maps:
        map_names.append(str(map['name']))

    return MirGetMapListResponse(1, map_names)


# add some error checking
# The function returns one long string with the json object, this could be formatted better so
# the receiver doesn't have to extract all the data manually
# has been tested: TRUE
def get_position_list(req):
    DEBUG('getting position list')
    pos = mir_rest.get_position_list()
    DEBUG(json.dumps(pos, indent=2))
    return MirGetPositionListResponse(True, str(pos))


# add some error checking
# has been tested: TRUE
def get_position_relevant(req):
    DEBUG('getting position relevant')
    _map_id = mir_rest.get_status()['map_id']
    positions = mir_rest.get_position_relevant(_map_id)
    name = []
    map_id = []
    pos_id = []
    size = 0
    for position in positions:
        name.append(position['name'])
        map_id.append(_map_id)
        pos_id.append(position["guid"])
        size += 1

    DEBUG(json.dumps(name, indent=2))

    return MirGetPositionRelevantResponse(1, size, name, map_id, pos_id)


# This function requires the name and not the position_id, which may have to be changed
# has been tested: false
def move_to_position(req):
    # DEBUG('This function has not yet been implemented!')
    # return MirMoveToPositionResponse(False)

    position_name = req.name
    position_retries = req.num_retries
    position_dist_to_goal = req.dist_to_goalthreshold

    DEBUG('request to move to position ', position_name)

    if (position_retries is 0) or (position_dist_to_goal is 0):
        if position_retries is 0:
            position_retries = 10
            DEBUG('Retries (Blocked Path) parameter not specified, defaulting to ', position_retries)
        if position_dist_to_goal is 0:
            position_dist_to_goal = 0.25
            DEBUG('Distance to goal threshold parameter not specified, defaulting to ', position_dist_to_goal)
    else:
        DEBUG('mission parameters ', str('retries ' + str(position_retries) + ', threshold ' + str(position_dist_to_goal)))

    # Make sure the position exists
    position_guid = None
    position_list = requests.get('http://mir.com:8080/v1.0.0/positions').json()
    for position in position_list:
        if position_name == str(position['name']):
            position_guid = str(position['guid'])
            DEBUG('Found position with guid ', position_guid)
            break
    if position_guid == None:
        DEBUG('Unable to find position in current map ', position_name)
        return MirMoveToPositionResponse(False)

    DEBUG('pausing robot')
    mir_rest.set_robot_state('pause')

    ###########################################################################
    headers = {"Content-Type": "application/json"}  # the same header is used for all post requests in this function

    # create a mission
    mission_guid = str(uuid.uuid1())
    message = {"guid": mission_guid,"name": str("Taxa to " + position_name), "description": "Temporary mission used to taxa to given position", "read_only":0, "session_id": None, "show_in_app": 1, "hide_in_missionlist": 0, "delete_after_finish": 1}
    mission_url = 'http://mir.com:8080/v1.0.0/missions'
    response = requests.post(mission_url, data=json.dumps(message), headers=headers)

    if response == 201:
        DEBUG('failed to create mission')
        return MirMoveToPositionResponse(False)
    else:
        DEBUG('mission created with guid ', mission_guid)

    # create an action in the new mission
    action_guid = str(uuid.uuid1())
    action_url = mission_url + '/' + mission_guid + '/actions'
    msg = {'guid': action_guid, 'action_type_id': 200, 'priority': 1}
    response = requests.post(action_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create action')
        return MirMoveToPositionResponse(False)
    else:
        DEBUG('action created with guid ', action_guid)

    # create 3 parameters ('Position', 'Retries (Blocked Path) and 'Distance to goal threshold') for the new action
    parameter_guid = str(uuid.uuid1())
    msg = {'guid': parameter_guid, 'name': 'Position', 'value': position_guid}
    parameter_url = action_url + '/' + action_guid + '/parameters'
    response = requests.post(parameter_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create Position parameter')
        return MirMoveToPositionResponse(False)
    else:
        DEBUG('Position parameter created with guid ', parameter_guid)

    parameter_guid = str(uuid.uuid1())
    msg = {'guid': parameter_guid, 'name': 'Retries (Blocked Path)', 'value': position_retries}
    response = requests.post(parameter_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create Retries (Blocked Path) parameter')
        return MirMoveToPositionResponse(False)
    else:
        DEBUG('Retries (Blocked Path) parameter created with guid ', parameter_guid)

    parameter_guid = str(uuid.uuid1())
    msg = {'guid': parameter_guid, 'name': 'Distance to goal threshold', 'value': position_dist_to_goal}
    response = requests.post(parameter_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create Distance to goal threshold parameter')
        return MirMoveToPositionResponse(False)
    else:
        DEBUG('Distance to goal threshold parameter created with guid ', parameter_guid)

    # add the mission to the queue so it will be executed when we press continue
    mission_queue_url = 'http://mir.com:8080/v1.0.0/mission_queue'
    msg = {'mission':mission_guid, 'taxi':None}
    response = requests.post(mission_queue_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to add mission to mission queue')
        return MirMoveToPositionResponse(False)
    else:
        DEBUG('mission added to mission queue')

    ###########################################################################

    DEBUG('continuing robot')
    response = mir_rest.set_robot_state('continue')

    # wait until we have reached our position to return this service. That way the caller knows when mir_rest is standing still again
    time.sleep(1)
    DEBUG('Robot in transit')
    while mir_rest.get_status()['state'] == 'InTransit':
        time.sleep(0.1)

    DEBUG('done moving\n\n')
    return MirMoveToPositionResponse(1)


# has been tested: false
def save_map(req):
    session_name = req.session_name
    session_note = req.session_note
    map_name = req.map_name

    # 'sessionName' and 'sessionNote' can be null.
    message = {'cmd':'save', 'sessionId':1, 'sessionName': session_name, 'sessionNote': session_note, 'mapName': map_name}
    robot_mapping = robot_rosbridge.callService('/mircontrol/Mapping', msg = message)

    map_url = 'http://mir.com:8080/v1.0.0/maps'
    map_response = requests.get(map_url)  # move this to use mir_rest.get_map_list() and get rid of map_url

    # this loop seems to give a false negative
    # for i in range(len(map_response.json())):
    # 	if map_name == map_response.json()[i]['name']:
    # 		DEBUG("Find a new name this name is taking")
    # 		return MirSaveMapResponse(False)

    time.sleep(1)
    response = robot_mapping['response']
    print 'save response: ', response
    if response != True:
        DEBUG("Check that the MiR is connected and that mir_set_mode is in mode 3(the mapping mode)")
        return MirSaveMapResponse(False)
    else:
        DEBUG('Map saved with name: ', str(req.map_name))
        return MirSaveMapResponse(True)


# add some error checking
# has been tested: false
def set_cmd_vel(req):
    # wait for pose data - do not uncomment
    # this function can also give the image with the service return value, but it is not currently implemented
    DEBUG('Setting robot velocities')
    while(len(rb.data['pose'])) == 0:
        # print("waiting for pos - 0.5s")
        time.sleep(0.5)

    linear_velocity = [req.x, req.y, req.z]
    angular_velocity = [req.a, req.b, req.c]

    rb.move(robot_rosbridge, linear_velocity, angular_velocity)

    return MirSetCmdVelResponse(True)


# has been tested: false
def set_mode(req):
    '''
    Sets the mode of the robot. Choose between the following modes:
    ROBOT_MODE_NONE: 0,
    ROBOT_MODE_NOMAP: 1,
    ROBOT_MODE_MANUEL: 2,
    ROBOT_MODE_MAPPING: 3,
    ROBOT_MODE_TAXA: 4,
    ROBOT_MODE_BUS: 5,
    ROBOT_MODE_POST: 6,
    ROBOT_MODE_MISSION: 7,
    ROBOT_MODE_CHANGING: 255
    '''
    DEBUG('Setting mode to ', req.mode)

    message={"robotMode":req.mode}
    robot_mode = robot_rosbridge.callService('/mircontrol/setMode', msg = message)
    robot_mode = robot_mode['response']

    if robot_mode:
        DEBUG('mode set to ', req.mode)
        return MirSetModeResponse(True)
    else:
        DEBUG('Failed to set mode\nCheck that the MiR is connected and that set_mode has a value corresponding to an exsisting mode')
        return MirSetModeResponse(False)


# has been tested: N/A
def set_position(req):
    DEBUG("DEPRECATED!\nUse create_position to create or move_to_position to move to one.")
    return MirSetPositionResponse(False)
    # id_ = req.id
    # name = req.name
    # x = req.x
    # y = req.y
    # theta = req.theta
    # type_ = req.type
    # map_id = req.map_id
    #
    # if mir_rest.post_position(id_, name, x, y, theta, type_, map_id) == 0:
    #     return MirSetPositionResponse(True)
    # else:
    #     return MirSetPositionResponse(False)


# Function currently not available on MiR100
# has been tested: false
def set_map(req):
    DEBUG('set_map')
    map_name = req.map_name
    DEBUG('Setting map to ' + map_name)
    return MirSetMapResponse(False)


# has been tested: false
def play_sound(req):
    sound = req.sound
    sound_guid = sound
    duration = req.duration
    volume = req.volume

    sound_url = 'http://mir.com:8080/v1.0.0/sounds'
    response = requests.get(sound_url).json()

    # print json.dumps(response, indent=2)

    for i in response:
        if sound.lower() == str(i['name']).lower():
            sound_guid = str(i['guid'])
            break

    if sound == sound_guid:
        DEBUG('The input did not match any sounds in the library')
        return MirPlaySoundResponse(False)

    print 'sound to play \'', sound, '\''
    # 	return MirPlaySoundResponse(False)

    headers = {"Content-Type": "application/json"}  # the same header is used for all post requests in this function

    # create a mission
    mission_guid = str(uuid.uuid1())
    message = {"guid": mission_guid,"name": str("Play sound " + sound), "description": "Temporary mission used to play a sound", "read_only":0, "session_id": None, "show_in_app": 1, "hide_in_missionlist": 0, "delete_after_finish": 1}
    mission_url = 'http://mir.com:8080/v1.0.0/missions'
    response = requests.post(mission_url, data=json.dumps(message), headers=headers)

    if response == 201:
        DEBUG('failed to create mission')
        return MirPlaySoundResponse(False)
    else:
        DEBUG('mission created with guid ', mission_guid)

    # create an action in the new mission
    action_guid = str(uuid.uuid1())
    action_url = mission_url + '/' + mission_guid + '/actions'
    msg = {'guid': action_guid, 'action_type_id': 300, 'priority': 1}
    response = requests.post(action_url, data=json.dumps(msg), headers=headers)

    if response == 201:
        DEBUG('failed to create action')
        return MirPlaySoundResponse(False)
    else:
        DEBUG('action created with guid ', action_guid)

    # create 3 parameters ('Sound', 'Duration' and 'Volume') for the new action
    parameter_guid = str(uuid.uuid1())
    msg = {'guid': parameter_guid, 'name': 'Sound', 'value': sound_guid}
    parameter_url = action_url + '/' + action_guid + '/parameters'
    response = requests.post(parameter_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create Sound parameter')
        return MirPlaySoundResponse(False)
    else:
        DEBUG('Sound parameter created with guid ', parameter_guid)

    parameter_guid = str(uuid.uuid1())
    msg = {'guid': parameter_guid, 'name': 'Duration', 'value': duration}
    response = requests.post(parameter_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create Duration parameter')
        return MirPlaySoundResponse(False)
    else:
        DEBUG('Duration parameter created with guid ', parameter_guid)

    parameter_guid = str(uuid.uuid1())
    msg = {'guid': parameter_guid, 'name': 'Volume', 'value': volume}
    response = requests.post(parameter_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to create Volume parameter')
        return MirPlaySoundResponse(False)
    else:
        DEBUG('Volume parameter created with guid ', parameter_guid)

    # add the mission to the queue so it will be executed when we press continue
    mission_queue_url = 'http://mir.com:8080/v1.0.0/mission_queue'
    msg = {'mission':mission_guid, 'taxi':None}
    response = requests.post(mission_queue_url, data=json.dumps(msg), headers=headers)
    if response == 201:
        DEBUG('failed to add mission to mission queue')
        return MirPlaySoundResponse(False)
    else:
        DEBUG('mission added to mission queue')

    mir_rest.set_robot_state('continue')
    DEBUG('\n\n')
    return MirPlaySoundResponse(True)


# has been tested: TRUE
def get_sound_list(req):
    DEBUG("getting sound list")
    sounds = mir_rest.get_sound_list()
    DEBUG(sounds)
    name = []
    for sound in sounds:
        name.append(sound['name'])

    return MirGetSoundListResponse(1, name)


# has been tested: TRUE
def get_state(req):
    DEBUG("getting state")
    status = mir_rest.get_status()

    state = int(status['state_id'])
    description = str(status['state_text'])
    DEBUG(state)
    DEBUG(description)

    return MirGetStateResponse(True, state, description)


if __name__ == "__main__":
    # init all the services and topics
    rospy.init_node('mir_driver')

    s = rospy.Service('mir_create_position', MirCreatePosition, create_position)
    s = rospy.Service('mir_delete_position', MirDeletePosition, delete_position)
    s = rospy.Service('mir_get_current_map_info', MirGetCurrentMapInfo, get_current_map_info)
    s = rospy.Service('mir_get_current_map_image', MirGetCurrentMapImage, get_current_map_image)
    s = rospy.Service('mir_get_current_position', MirGetCurrentPosition, get_current_position)
    s = rospy.Service('mir_get_dynamic_map_image', MirGetDynamicMapImage, get_dynamic_map_image)
    s = rospy.Service('mir_get_map_list', MirGetMapList, get_map_list)
    s = rospy.Service('mir_get_position_list', MirGetPositionList, get_position_list)
    s = rospy.Service('mir_get_position_relevant', MirGetPositionRelevant, get_position_relevant)
    s = rospy.Service('mir_move_to_position', MirMoveToPosition, move_to_position)
    s = rospy.Service('mir_save_map', MirSaveMap, save_map)
    s = rospy.Service('mir_set_cmd_vel', MirSetCmdVel, set_cmd_vel)
    s = rospy.Service('mir_set_mode', MirSetMode, set_mode)
    s = rospy.Service('mir_set_position', MirSetPosition, set_position)
    s = rospy.Service('mir_play_sound', MirPlaySound, play_sound)
    s = rospy.Service('mir_get_sound_list', MirGetSoundList, get_sound_list)
    s = rospy.Service('mir_get_state', MirGetState, get_state)
    s = rospy.Service('mir_set_map', MirSetMap, set_map)

    print('All services are running...\n')
    rospy.spin()
