import roslibpy

client = roslibpy.Ros(host='192.168.12.20', port=8080, is_secure=True)
client.run(timeout=60)
print('Is ROS connected?', client.is_connected)
client.terminate()