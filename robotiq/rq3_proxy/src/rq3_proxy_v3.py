#!/usr/bin/env python
#############################################################################
### DESCRIPTION
#############################################################################
"""
 ***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2013,  Morten Palmelund-Jensen, Rune Etzerodt,           *
 *  Casper Abildgaard Pedersen                                             *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************


    This proxy handles the connection betweeen the Master executor and ROS node
    "SModelTcpNode.py" in the package "robotiq_s_model_control" created by
    Robotiq, to a Robotiq RQ3 3-finger adaptive gripper


    The proxy is connected to the master via ROS actionlib, receiving  generic
    hardware independent goals. This proxy does not yet send back feedback
    and result, but receives it from the gripper.
    The syntax of the goals are translated to hardware specific commands for
    the Robotiq RQ3 3-finger adaptive gripper.
    The connetion to the gripper is handled by a publisher/subscriber connetion
    through the node "SModelTcpNode-py

"""

#############################################################################
### IMPORTS
#############################################################################

import roslib; roslib.load_manifest('robotiq_s_model_control')
import roslib; roslib.load_manifest('rq3_proxy')
import rospy
import actionlib
import rq3_proxy.msg # importere de msg "typer" der er genereret af .action filen
from robotiq_s_model_control.msg import _SModel_robot_output as outputMsg
from robotiq_s_model_control.msg import _SModel_robot_input as inputMsg
from time import sleep

#############################################################################
### FUNCTIONS
#############################################################################

def printCurrentCommand(command): # Prints out feedback from the gripper of the status
  #currentCommand  = '\nCommand published'
  #currentCommand = '\n----------------------------------------\n'
  currentCommand = 'rACT = '  + str(command.rACT)
  currentCommand += ', rMOD = ' + str(command.rMOD)
  currentCommand += ', rGTO = ' + str(command.rGTO)
  currentCommand += ', rATR = ' + str(command.rATR) + '\n'
  currentCommand += 'rGLV = ' + str(command.rGLV)
  currentCommand += ', rICF = ' + str(command.rICF)
  currentCommand += ', rICS = ' + str(command.rICS) + '\n'

  currentCommand += 'rPRA = ' + str(command.rPRA)
  currentCommand += ', rSPA = ' + str(command.rSPA)
  currentCommand += ', rFRA = ' + str(command.rFRA) + '\n'

  currentCommand += 'rPRB = ' + str(command.rPRB)
  currentCommand += ', rSPB = ' + str(command.rSPB)
  currentCommand += ', rFRB = ' + str(command.rFRB) + '\n'

  currentCommand += 'rPRC = ' + str(command.rPRC)
  currentCommand += ', rSPC = ' + str(command.rSPC)
  currentCommand += ', rFRC = ' + str(command.rFRC) + '\n'

  currentCommand += 'rPRS = ' + str(command.rPRS)
  currentCommand += ', rSPS = ' + str(command.rSPS)
  currentCommand += ', rFRS = ' + str(command.rFRS)

  print currentCommand

gripstat = None
def callback(status): # Saves all the robot input registers and status from the gripper to a class called gripstat
    global gripstat
    gripstat = status

def activategripper(): # Activates the gripper - necessary when the initial connection are made
  print "\nSetup activation command"
  command.rACT = 1
  command.rGTO = 1
  command.rICF = 1                                  # Activate individual control of fingers
  spd = 255
  frc = 255
  command.rSPA = spd
  command.rSPB = spd
  command.rSPC = spd
  command.rFRA = frc
  command.rFRB = frc
  command.rFRC = frc
  rospy.sleep(0.2)

  print "Publishing activation command"
  printCurrentCommand(command)
  pub.publish(command)
  sleep(0.5)

  readyflag = False
  i=0
  while not readyflag:
    """ While loop sleeps until the gripper is activated"""
    if gripstat.gIMC == 3 and gripstat.gACT == 1 and gripstat.gGTO == 1:
      print "Gripper is activated"
      readyflag = True
    else :
      if i >= 30:
        print "Timeout - gripper have used more than 30 iterations to activate"
        break
      if i == 0:
        print "Sleeping until gripper is activated"
      sleep(1)
      readyflag = False
      i+=1

def closegripper(spd,frc): # Closes the gripper with the specified speed and force
  print "\nSetup close command"
  pos = 255
  command.rPRA = pos
  command.rPRB = pos
  command.rPRC = pos
  command.rSPA = spd
  command.rSPB = spd
  command.rSPC = spd
  command.rFRA = frc
  command.rFRB = frc
  command.rFRC = frc

  print "Publishing close command"
  printCurrentCommand(command)
  pub.publish(command)
  sleep(0.5)

def movefingers(pos,spd,frc): # Moves all fingers to the specified position, whit the specified speed and force
  print "\nSetup movefingers command"
  command.rPRA = pos
  command.rPRB = pos
  command.rPRC = pos
  command.rSPA = spd
  command.rSPB = spd
  command.rSPC = spd
  command.rFRA = frc
  command.rFRB = frc
  command.rFRC = frc

  print "Publishing movefingers command"
  printCurrentCommand(command)
  pub.publish(command)
  sleep(0.5)

def opengripper(spd, frc): # Opens the gripper with the specified speed and force
  print "\nSetup open command"
  pos = 0
  command.rPRA = pos
  command.rPRB = pos
  command.rPRC = pos
  command.rSPA = spd
  command.rSPB = spd
  command.rSPC = spd
  command.rFRA = frc
  command.rFRB = frc
  command.rFRC = frc

  print "Publishing open command"
  printCurrentCommand(command)
  pub.publish(command)
  sleep(0.5)

def resetgripper(): # Resets the activation of the gripper
  print "\nSetup reset command"
  command = outputMsg.SModel_robot_output();
  command.rACT = 0
  rospy.sleep(0.2)

  print "Publishing reset command"
  printCurrentCommand(command)
  pub.publish(command)
  #rospy.sleep(0.2)
  sleep(1)

def readytomovegripper(): # Waits until the gripper is activated or ready to move
  readyflag = False
  i=0
  while not readyflag:
    if gripstat.gSTA >= 1 and gripstat.gIMC == 3:
      print "Ready to move"
      readyflag = True
      return readyflag
    else :
      if i >= 30:
        string = "Timeout - gripper have used more than 30 iterations to "
        string += "complete the command"
        print string
        break
      if i == 0:
        print "Not ready to move - sleeping until ready"
      sleep(1)
      readyflag = False
      i+=1

def changeMode(mode): # Changes the mode of the gripper to "mode"
  print "\nSetup change mode command"
  command.rMOD = mode

  print "Publishing change mode command"
  printCurrentCommand(command)
  pub.publish(command)
  sleep(0.5)

def feedbacktest(self): # UNDER DEVELOPMENT - feedback test
  i=0
  while i<=255:
     # Saves data to feedback variables
    self._feedback.getpos =int(((i/float(255)*100))) # gripstat.gPOA
    self._feedback.ismoving = i # gripstat.gMOD

    self._as.publish_feedback(self._feedback) #Publish feedback to the topic
    i+=1
    #sleep(0.05)



class rq3Action(object): # Class to handle the actionlib communication

  _feedback = rq3_proxy.msg.rq3Feedback()           # Create message to publish feedback via the actionlib
  _result   = rq3_proxy.msg.rq3Result()             # Create message to publish result via the actionlib

  def __init__(self, name): # Constructs a SimpleActioServer and starts it

  #  Below is a SimpleActionServer constructed, which takes the following arguments
  #  actionlib::SimpleActionServer< ActionSpec >::SimpleActionServer	(   self,
  #                                                                          name,
  #                                                                          ActionSpec,
  #                                                                          execute_cb = None,
  #                                                                          auto_start = True	 )
  #   name         = The name of the actionserver
  #   execute_cb   = Callback, which is called each time a new goal is received, adding a callback deactivates the goalCallback
  #   auto_start   = bool, should alwys be false to avoid race conditions, and insted should the server be started with a start()
  #                  call after construction of the server
  #  http://mirror.umd.edu/roswiki/doc/diamondback/api/actionlib/html/classactionlib_1_1simple__action__server_1_1SimpleActionServer.html
    self._action_name = name
    self._as = actionlib.SimpleActionServer(self._action_name, rq3_proxy.msg.rq3Action, execute_cb=self.execute_cb, auto_start=False)
    self._as.start()


  def readytomovegripper2(self): # Waits until the gripper is activated or ready to move and publish feedback to the actionserver

    readyflag = False
    firstiteration = True

    while not readyflag:
      if gripstat.gSTA >= 1 and gripstat.gIMC == 3:             # Checks if the status is OK and if it's moving
        readyflag = True
        print "Ready to move"
        return readyflag
      else :
        self._feedback.getpos = int(((gripstat.gPOA/float(255)*100)))
        self._feedback.ismoving = gripstat.gSTA                 # gripstat.gPOA #gripstat.gIMC
        self._as.publish_feedback(self._feedback)               # Publish feedback to the topic

#        #Test af aborted functionen...
#        if gripstat.gPOA > 260:
#          self._as.set_aborted(self._result)
#          return readyflag

        if firstiteration == True:
          print "Not ready to move - sleeping"
          firstiteration = False

        sleep(0.01)
        readyflag = False

  def execute_cb(self, goal):
    # helper variables
    #r = rospy.Rate(1)
    success = True


#### UNDER CONSTRUCTION - start ####################
    #self._feedback.fpos = None
    #self._feedback.fspd = None
    #self._feedback.ffrc = None

    # publish info to the console for the user
    #rospy.loginfo('%s: Executing, pos, spd, frc, open/close: %i, %i, %i, %s' % (self._action_name, goal.gpos, goal.gspd, goal.gfrc, goal.gopenclose))

    # start executing the action
    """
    i = 1
    while i <= goal.gpos:
      # check that preempt has not been requested by the client
      if self._as.is_preempt_requested():
        rospy.loginfo('%s: Preempted' % self._action_name)
        self._as.set_preempted()
        success = False
        break
      self._feedback.fpos = i
      # publish the feedback
      self._as.publish_feedback(self._feedback)
      # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
      r.sleep()
      i += 1

    if success:
      self._result.rpos = self._feedback.fpos
      rospy.loginfo('%s: Succeeded' % self._action_name)
      self._as.set_succeeded(self._result)
    """

    # check that preempt has not been requested by the client
    #if self._as.is_preempt_requested():
    #  rospy.loginfo('%s: Preempted' % self._action_name)
    #  self._as.set_preempted()
    #  success = False
      #break

    #movefingers(goal.gpos,goal.gspd,goal.gfrc)
    #readytomovegripper()

    # Saves data to feedback variables
    #self._feedback.getpos =int(((100/float(255)*100))) # gripstat.gPOA
    #self._feedback.ismoving = 1 # gripstat.gMOD
#### UNDER CONSTRUCTION - end ####################



    if gripstat.gFLT == 0 :                                     # Checks if the gripper is in error state
      #print "No fault"

      if gripstat.gMOD != goal.mode:                            # If the current mode of the gripper is different from the one stated by the goal, the mode is changed
        changeMode(goal.mode)

      if goal.gopenclose == "open":                             # Opens the gripper if the goal states "open"
        opengripper(255, 255)
        success = self.readytomovegripper2()
      elif goal.gopenclose == "close":                          # Closes the gripper if the goal states "close"
        closegripper(255, 255)
        success = self.readytomovegripper2()
      elif goal.gopenclose == "move":                          	# Moves the fingers to a specified position	
        movefingers(goal.gpos,255, 255)
        success = self.readytomovegripper2()
      elif goal.gopenclose == "none":
        pass
      else :
        print "Don't know the open/close goal string"

      #rospy.loginfo('Feedback variables is set to: %i, %i' % (self._feedback.getpos, self._feedback.ismoving))
      # self._as.publish_feedback(self._feedback)

      if success:
          self._result.rpos = self._feedback.getpos
          #rospy.loginfo('%s: Succeeded' % self._action_name)
          self._as.set_succeeded(self._result)
    else :
      print "Fault detected"

#############################################################################
### MAIN PROGRAM
#############################################################################

print "\nInitialising rq3_proxy node"
rospy.init_node('rq3proxynode')
rospy.sleep(0.2)

print "Setup handler for publishing/subscribing to RQ3 gribber"
pub = rospy.Publisher('SModelRobotOutput', outputMsg.SModel_robot_output)   # The handler for publishing to RQ3 gribber
rospy.sleep(0.2)

command = outputMsg.SModel_robot_output()

rospy.Subscriber("SModelRobotInput", inputMsg.SModel_robot_input, callback) # The handler for publishing to RQ3 gribber
sleep(1)

activategripper()

readytomovegripper()

rq3proxyserver = rq3Action(rospy.get_name())                                # Creates the actionlib server
rospy.spin()                                                                # ROS spin

print "Program finished"
