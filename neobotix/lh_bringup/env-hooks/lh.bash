
# If you change this file, please increment the version number in
# CMakeLists.txt to trigger a CMake update.

function lh() {
    local workspace=$(roscd && cd .. && pwd)
    case $1 in
	s|src|source)
	    cd "$workspace/src"
	    ;;
	b|build|make)
	    shift
	    catkin build -w "$workspace" ${STAMINA_CATKIN_ARGS} $*
	    ;;
	l|launch)
	    shift
	    roslaunch stamina_launch $*
	    ;;
	L|LAUNCH)
	    shift
	    rosrun rosmon rosmon --name=rosmon_lh lh_skiros_launch $*
	    ;;
	debug-coredump)
	    shift
	    gdb "$workspace/devel/lib/$1" /tmp/cores/$2
	    ;;
	clean)
	    shift
	    rm /tmp/cores/*
	    ;;
  u|update)
      $(rospack find lh_bringup)/scripts/update_workspace.sh
      ;;
    esac
}

function _lh() {
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local cmd="${COMP_WORDS[1]}"

    case "${COMP_CWORD}" in
	1)
	    COMPREPLY=( $(compgen -W "s source b build make l launch L LAUNCH debug-coredump clean u update" -- $cur) )
	    ;;
	2)
	    local workspace=$(roscd && cd .. && pwd)

	    case "${cmd}" in
		b|build|make)
		    local packages=$(catkin list "${workspace}")
		    COMPREPLY=( $(compgen -W "${packages}" -- $cur) )
		    ;;
		l|launch|L|LAUNCH)
		    local launchfiles=$(find $(rospack find stamina_launch) -name '*.launch' -type f -printf "%f\n")
		    COMPREPLY=( $(compgen -W "${launchfiles}" -- $cur) )
		    ;;
		debug-coredump)
		    local execs=$(cd "$workspace/devel/lib" && find -mindepth 2 -executable -type f -printf "%P\n")
		    COMPREPLY=( $(compgen -W "$execs" -- $cur) )
		    ;;
	    esac
	    ;;
	3)
	    case "${cmd}" in
		debug-coredump)
		    local coredumps=$( cd /tmp/cores/ && echo * )
		    COMPREPLY=( $(compgen -W "$coredumps" -- $cur) )
		    ;;
	    esac
	    ;;
	*)
	    COMPREPLY=""
	    ;;
    esac
}
complete -F _lh lh
