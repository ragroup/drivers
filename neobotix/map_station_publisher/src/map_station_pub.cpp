#include <ros/ros.h>
#include <cstdlib>
#include <sstream>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include <tf/transform_broadcaster.h>
#include <tf/tf.h>
#include <math.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

using namespace std;
using namespace visualization_msgs;
using namespace interactive_markers;

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

boost::shared_ptr<InteractiveMarkerServer> server;

MenuHandler menu_handler;

MenuHandler::EntryHandle menu_send_robot_here;
MenuHandler::EntryHandle menu_toggle_marker;
MenuHandler::EntryHandle menu_toggle_name;

MoveBaseClient* ac;
string current_goal;
string marker_type = "flat";
bool show_marker_name = false;

Marker makeFlatMarker( InteractiveMarker &msg )
{
    Marker marker;

    /*marker.type = Marker::CUBE;
    marker.scale.x = 0.68;
    marker.scale.y = 0.6;
    marker.scale.z = 0.05;*/

    marker.type = Marker::MESH_RESOURCE;
    marker.mesh_resource = "package://map_station_publisher/meshes/flat_marker.dae";
    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;

    if(msg.name == current_goal)
    {
        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
    }
    else
    {
        marker.color.r = 1.0f;
        marker.color.g = 0.0f;
    }
    marker.color.b = 0.0f;
    marker.color.a = 0.6f;


    return marker;
}

Marker makeMeshMarker( InteractiveMarker &msg )
{
    Marker marker;

    marker.type = Marker::MESH_RESOURCE;
    marker.mesh_resource = "package://map_station_publisher/meshes/lh_aal_combined.dae";
    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;

    if(msg.name == current_goal)
    {
        marker.color.r = 0.0f;
        marker.color.g = 1.0f;
    }
    else
    {
        marker.color.r = 1.0f;
        marker.color.g = 0.0f;
    }
    marker.color.b = 0.0f;
    marker.color.a = 0.5f;

    return marker;
}

void doneCb(const actionlib::SimpleClientGoalState& state,
            const move_base_msgs::MoveBaseResultConstPtr& result)
{
    visualization_msgs::InteractiveMarker temp;
    server->get(current_goal,temp);
    temp.controls[0].markers[0].color.g = 0.0;
    temp.controls[0].markers[0].color.r = 1.0;
    server->erase(current_goal);
    server->insert(temp);
    server->applyChanges();

    current_goal = "";

}


void activeCb()
{
    visualization_msgs::InteractiveMarker temp;
    server->get(current_goal,temp);
    temp.controls[0].markers[0].color.g = 1.0;
    temp.controls[0].markers[0].color.r = 0.0;
    server->erase(current_goal);
    server->insert(temp);
    server->applyChanges();
}

void feedbackCb(const move_base_msgs::MoveBaseFeedbackConstPtr& feedback)
{

}

void sendMoveGoalCB( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
    //check if existing goal is present (note: simple action clients does NOT cancel goals when sending a new. The status of the previous goal is simply dropped and note tracked anymore)
    if(current_goal != "")
    {
        visualization_msgs::InteractiveMarker temp;
        server->get(current_goal,temp);
        temp.controls[0].markers[0].color.g = 0.0;
        temp.controls[0].markers[0].color.r = 1.0;
        server->erase(current_goal);
        server->insert(temp);
        server->applyChanges();

        current_goal = "";
    }

    //wait for the action server to come up
    while(!ac->waitForServer(ros::Duration(5.0))){
        ROS_INFO("Waiting for the move_base action server to come up");
    }

    move_base_msgs::MoveBaseGoal goal;

    //we'll send a goal to the robot to move 1 meter forward
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();

    goal.target_pose.pose.position.x = feedback->pose.position.x;
    goal.target_pose.pose.position.y = feedback->pose.position.y;
    goal.target_pose.pose.orientation.w = feedback->pose.orientation.w;


    ROS_INFO("Sending goal");
    ac->sendGoal(goal, boost::bind(&doneCb,_1,_2),boost::bind(&activeCb),boost::bind(&feedbackCb,_1));

    //set current goal
    current_goal = feedback->marker_name;
}

void toggleMarkerViewCB( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
    if(marker_type == "flat")
        marker_type = "mesh";
    else if(marker_type == "mesh")
        marker_type = "flat";
    else                                //error - shouldn't happen.
        marker_type = "flat";
}

void toggleMarkerNameCB( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
    if(show_marker_name == true)
        show_marker_name = false;
    else if(show_marker_name == false)
        show_marker_name = true;
    else                                //error - shouldn't happen.
        show_marker_name = true;
}

void initMenu()
{
    menu_send_robot_here = menu_handler.insert( "Send robot here", &sendMoveGoalCB);
    menu_toggle_marker = menu_handler.insert( "Toggle marker view", &toggleMarkerViewCB);
    menu_toggle_name = menu_handler.insert( "Toggle marker name", &toggleMarkerNameCB);
    //MenuHandler::EntryHandle entry = menu_handler.insert( h_first_entry, "deep" );
    //entry = menu_handler.insert( entry, "sub" );
    //entry = menu_handler.insert( entry, "menu", &deepCb );

    //menu_handler.setCheckState( menu_handler.insert( "Show First Entry", &enableCb ), MenuHandler::CHECKED );

    /*MenuHandler::EntryHandle sub_menu_handle = menu_handler.insert( "Switch" );

  for ( int i=0; i<5; i++ )
  {
    std::ostringstream s;
    s << "Mode " << i;
    h_mode_last = menu_handler.insert( sub_menu_handle, s.str(), &modeCb );
    menu_handler.setCheckState( h_mode_last, MenuHandler::UNCHECKED );
  }
  //check the very last entry
  menu_handler.setCheckState( h_mode_last, MenuHandler::CHECKED );*/
}

Marker makeMarkerText(Marker input, string name)
{
    input.type = Marker::TEXT_VIEW_FACING;
    input.text = name;
    input.scale.x = 0.3;
    input.scale.y = 0.3;
    input.scale.z = 0.3;
    input.color.r = 0.0;
    input.color.g = 0.0;
    input.color.b = 1.0;
    input.color.a = 1.0;

    return input;
}

int main(int argc, char *argv[])
{
    // Start node:
    ros::init(argc, argv, "map_station_publisher");
    ros::NodeHandle n;
    
    ros::Rate loop_rate(2);

    /*
    //Get station-list-file
    if(argc < 2)
    {
        cout << "Error: Please provide the path to a yaml file with stored stations as first argument input! Exiting..." << endl;
    return 0;
        }

    //get the file name and path
    string filename = argv[1];

    //open the file:

*/

    server.reset( new InteractiveMarkerServer("map_station_publisher","",false) );

    initMenu();

    XmlRpc::XmlRpcValue station_list;

    //read ros param
    if(!n.getParam("/stations", station_list))
    {
        cout << "Failed to get stations list from parameter server." << endl;
        return 0;
    }


    //start the publisher
    ros::Publisher all_st_pub = n.advertise<visualization_msgs::MarkerArray>("all_stations", 100);
    ros::Publisher selected_st = n.advertise<visualization_msgs::Marker>("selected_station", 100);

    //tell the action client that we want to spin a thread by default
    ac = new MoveBaseClient("move_base", true);

    while (ros::ok())
    {
        server->clear();
        int count = 0;

        //read ros param
        if(!n.getParam("/stations", station_list))
        {
            cout << "Failed to get stations list from parameter server." << endl;
            return 0;
        }

        for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator itr1 = station_list.begin(); itr1!= station_list.end();itr1++)
        {
            //cout << (string)(itr1->first) << " ";

            count ++;

            InteractiveMarker marker;

            //visualization_msgs::Marker marker;
            marker.header.frame_id = "/map"; // "/station/" + (string)(itr1->first);
            marker.header.stamp = ros::Time::now();
            marker.name = (string)(itr1->first);
            //marker.ns = "basic_shape";
            //marker.id = count;
            //marker.type = visualization_msgs::Marker::CYLINDER;
            //marker.action = visualization_msgs::Marker::ADD;
            marker.scale = 1.0;
            //marker.scale.x = 1.0;
            //marker.scale.y = 1.0;
            //marker.scale.z = 1.0;
            //marker.color.r = 0.0f;
            //marker.color.g = 1.0f;
            //marker.color.b = 0.0f;
            //marker.color.a = 1.0;
            //marker.lifetime = ros::Duration();
            //pose

            for(XmlRpc::XmlRpcValue::ValueStruct::const_iterator itr2 = station_list[itr1->first].begin(); itr2 != station_list[itr1->first].end();itr2++)
            {
                //cout << (string)(itr2->first) << ": " << (double)station_list[itr1->first][itr2->first] << " ";
                if((string)(itr2->first) == "x")
                {
                    marker.pose.position.x = (double)station_list[itr1->first][itr2->first];
                }
                else if((string)(itr2->first) == "y")
                {
                    marker.pose.position.y = (double)station_list[itr1->first][itr2->first];
                }
                else if((string)(itr2->first) == "rx")
                {
                    marker.pose.orientation.x = (double)station_list[itr1->first][itr2->first];
                }
                else if((string)(itr2->first) == "ry")
                {
                    marker.pose.orientation.y = (double)station_list[itr1->first][itr2->first];
                }
                else if((string)(itr2->first) == "rz")
                {
                    marker.pose.orientation.z = (double)station_list[itr1->first][itr2->first];
                }
                else if((string)(itr2->first) == "rw")
                {
                    marker.pose.orientation.w = (double)station_list[itr1->first][itr2->first];
                }

            } //param tra
            //cout << endl;

            InteractiveMarkerControl control;

            control.interaction_mode = InteractiveMarkerControl::BUTTON;
            control.always_visible = true;

            Marker visual_marker;

            if(marker_type == "mesh")
            {
                //control.markers.push_back( makeMeshMarker( marker ) );
                visual_marker = makeMeshMarker( marker );
            }
            else
            {
                //control.markers.push_back( makeFlatMarker( marker ) );
                visual_marker = makeFlatMarker( marker );
            }

            control.markers.push_back(visual_marker);
            if(show_marker_name)
                control.markers.push_back(makeMarkerText(visual_marker,marker.name));
            
            marker.controls.push_back(control);

            server->insert( marker );
            menu_handler.apply( *server, marker.name );
            //station_markers.markers.push_back(marker);

        } //station tra


        //all_st_pub.publish(station_markers);
        server->applyChanges();
        ros::spinOnce();
        
        loop_rate.sleep();

    }
    server.reset();

    return 0;
}


