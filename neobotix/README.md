# Little Helper description: 3D models and hardware drivers

## Table of Contents

- [About](#markdown-header-about)
- [Getting Started](#markdown-header-getting-started)
- [Usage](#markdown-header-usage)
- [Maps](#markdown-header-maps)
- [Known Issues](#markdown-header-known-issues)

## About

This folder contains packages for the Little Helper robot description (URDF) and hardware drivers.

It includes URDF models for:

- Neobotix MP655 mobile platform
- KUKA LWR
- Microsoft Kinect
- Schunk WSG50

## Getting Started

The packages in this folder have been tested with **ROS Melodic** and **Ubuntu 18.04.1 LTS**.

**NOTE!**

There is a known issue with the parsing of URDF in Melodic, which relates to **locales** and **urdfdom**. If you experience the model is not visible in RViz, please check [Known Issues](#markdown-header-known-issues) for a possible solution.

### Installing

Install **lh_description** dependencies:

```
$ rosdep install lh_description
```

## Usage

### Visualize and test robot model in RViz

Running the following, you will be able to visualize the robot model and move the joints in RViz.

Setting the flag `test:=True` will launch RViz for testing.

```
$ roslaunch lh_description upload_little_helper.launch test:=True
```

![picture](images/lh_rviz_test.gif)

### Robot bringup

Starting the hardware drivers

TODO

### Mapping

Steps

1. Launch LH robot_description (URDF)
2. Launch neobotix hardware drivers (bringup)
3. Launch 2D SLAM with gmapping

**1. Launch LH robot_description (URDF)**
```
$ roslaunch lh_description upload_little_helper.launch
```

**2. Launch neobotix hardware drivers (bringup)**

```
$ roslaunch neo_base_mp_655 bringup_mp_655.launch
```

**3. Launch 2D SLAM with gmapping**

Start the mapping process:
```
$ roslaunch neo_2dnav nav2dslam.launch
```

Optional arguments:
```
open_rviz: (default: true)
    Opens RViz with preconfigured UI settings
enable_teleop: (default: true)
    Enables the Logitech joypad for moving the platform
```

Important files and settings:

TODO

### Localization

TODO

### Navigation

TODO

```
$ roslaunch neo_2dnav 2dnav.launch map_name:=map_lab_office.yaml
```

## Maps

Different maps are available in `/neo_base_mp_655/maps/`.

## Known Issues

### Robot model is not visible in RViz and Melodic

The issue may be related to your **locales** and parsing with **urdfdom** so the decimal delimiter becomes `,` instead of `.`, for example:

- origin: -0.500000, 0.000000, 0.000000

vs

- origin: -0,000000, 0,000000, 0,000000

If that is the underlying issue, modifying the following environment variable will fix it for you. This can be added to `.bashrc` for convenience:

```
$ export LC_NUMERIC="en_US.UTF-8"
```

**Sources**

- https://answers.ros.org/question/296227/robot-model-does-not-appear-in-rviz-melodic/
- https://github.com/ros-visualization/rviz/issues/1249
- https://github.com/ros/urdfdom/issues/98