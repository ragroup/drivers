/*
 * teleop.cpp
 *
 *  Created on: Mar 7, 2016
 *      Author: speckle_s
 */

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Joy.h"
#include "cmath"

struct RangeAxis
{
  double min;
  double max;
};

struct components
{
  double lin;
  double rot;
};

struct ParamTeleop
{
  RangeAxis rangeX;
  RangeAxis rangeY;
  RangeAxis rangeWz;
  double offsetX;
  double offsetY;
  double offsetWz;
  components maxVelocity;
  components maxAcceleration;
  int precision;
  bool enable_omni;
  bool enable_ramp;
};

namespace Axis
{
enum Axis
{
  X, Y, Wz
};
}
typedef Axis::Axis _Axis;

class Teleop
{
public:
  Teleop();

private:
  void getParam();
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);
  double calcVel(double, _Axis);
  void interpolateAcceleration(geometry_msgs::Twist*, ros::Time);
  double changePrecision(double);
  ros::NodeHandle mNodeHandler;
  ros::Subscriber mJoySub;
  ros::Publisher mTwistPub;
  ParamTeleop mParam;
  ros::Time mLastTime;
  geometry_msgs::Twist mLastTwist;

};

Teleop::Teleop() :
    mParam()
{
  mJoySub = mNodeHandler.subscribe<sensor_msgs::Joy>("spacenav/joy", 10, &Teleop::joyCallback, this);
  mTwistPub = mNodeHandler.advertise<geometry_msgs::Twist>("cmd_vel", 1);

  getParam();

}

void Teleop::interpolateAcceleration(geometry_msgs::Twist* twist, ros::Time time)
{
  double deltaT = 0;
  geometry_msgs::Twist accTwist;

  deltaT = (time - mLastTime).toSec();
  accTwist.linear.x = (twist->linear.x - mLastTwist.linear.x) / deltaT;
  accTwist.linear.y = (twist->linear.y - mLastTwist.linear.y) / deltaT;
  accTwist.angular.z = (twist->angular.z - mLastTwist.angular.z) / deltaT;

//////////////////////////X-Axis/////////////////////////////////////////////////////
  if (twist->linear.x > 0 && accTwist.linear.x > 0 && (double)std::abs(accTwist.linear.x) > mParam.maxAcceleration.lin)
  { //acceleration ramp positive direction
    twist->linear.x = mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.x;
  }
  else if (twist->linear.x > 0 && accTwist.linear.x < 0
      && (double)std::abs(accTwist.linear.x) > mParam.maxAcceleration.lin)
  { //deceleration ramp positive direction
    twist->linear.x = -mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.x;
  }

  else if (twist->linear.x < 0 && accTwist.linear.x < 0
      && (double)std::abs(accTwist.linear.x) > mParam.maxAcceleration.lin)
  { //acceleration ramp negative direction
    twist->linear.x = -mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.x;
  }
  else if (twist->linear.x < 0 && accTwist.linear.x > 0
      && (double)std::abs(accTwist.linear.x) > mParam.maxAcceleration.lin)
  { //deceleration ramp negative direction
    twist->linear.x = mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.x;
  }
  else if (twist->linear.x == 0 && accTwist.linear.x < 0
      && (double)std::abs(accTwist.linear.x) > mParam.maxAcceleration.lin)
  { //deceleration ramp positive direction
    twist->linear.x = -mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.x;
  }

  else if (twist->linear.x == 0 && accTwist.linear.x > 0
      && (double)std::abs(accTwist.linear.x) > mParam.maxAcceleration.lin)
  { //deceleration ramp negative direction
    twist->linear.x = mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.x;
  }
  //////////////////////////Y-Axis/////////////////////////////////////////////////////

  if (twist->linear.y > 0 && accTwist.linear.y > 0 && (double)std::abs(accTwist.linear.y) > mParam.maxAcceleration.lin)
  { //acceleration ramp positive direction
    twist->linear.y = mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.y;
  }
  else if (twist->linear.y > 0 && accTwist.linear.y < 0
      && (double)std::abs(accTwist.linear.y) > mParam.maxAcceleration.lin)
  { //deceleration ramp positive direction
    twist->linear.y = -mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.y;
  }

  else if (twist->linear.y < 0 && accTwist.linear.y < 0
      && (double)std::abs(accTwist.linear.y) > mParam.maxAcceleration.lin)
  { //acceleration ramp negative direction
    twist->linear.y = -mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.y;
  }
  else if (twist->linear.y < 0 && accTwist.linear.y > 0
      && (double)std::abs(accTwist.linear.y) > mParam.maxAcceleration.lin)
  { //deceleration ramp negative direction
    twist->linear.y = mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.y;
  }
  else if (twist->linear.y == 0 && accTwist.linear.y < 0
      && (double)std::abs(accTwist.linear.y) > mParam.maxAcceleration.lin)
  { //deceleration ramp positive direction
    twist->linear.y = -mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.y;
  }

  else if (twist->linear.y == 0 && accTwist.linear.y > 0
      && (double)std::abs(accTwist.linear.y) > mParam.maxAcceleration.lin)
  { //deceleration ramp negative direction
    twist->linear.y = mParam.maxAcceleration.lin * deltaT + mLastTwist.linear.y;
  }

  //////////////////////////Wz-Axis/////////////////////////////////////////////////////

  if (twist->angular.z > 0 && accTwist.angular.z > 0
      && (double)std::abs(accTwist.angular.z) > mParam.maxAcceleration.rot)
  { //acceleration ramp positive direction
    twist->angular.z = mParam.maxAcceleration.rot * deltaT + mLastTwist.angular.z;
  }
  else if (twist->angular.z > 0 && accTwist.angular.z < 0
      && (double)std::abs(accTwist.angular.z) > mParam.maxAcceleration.rot)
  { //deceleration ramp positive direction
    twist->angular.z = -mParam.maxAcceleration.rot * deltaT + mLastTwist.angular.z;
  }

  else if (twist->angular.z < 0 && accTwist.angular.z < 0
      && (double)std::abs(accTwist.angular.z) > mParam.maxAcceleration.rot)
  { //acceleration ramp negative direction
    twist->angular.z = -mParam.maxAcceleration.rot * deltaT + mLastTwist.angular.z;
  }
  else if (twist->angular.z < 0 && accTwist.angular.z > 0
      && (double)std::abs(accTwist.angular.z) > mParam.maxAcceleration.rot)
  { //deceleration ramp negative direction
    twist->angular.z = mParam.maxAcceleration.rot * deltaT + mLastTwist.angular.z;
  }
  else if (twist->angular.z == 0 && accTwist.angular.z < 0
      && (double)std::abs(accTwist.angular.z) > mParam.maxAcceleration.rot)
  { //deceleration ramp positive direction
    twist->angular.z = -mParam.maxAcceleration.rot * deltaT + mLastTwist.angular.z;
  }

  else if (twist->angular.z == 0 && accTwist.angular.z > 0
      && (double)std::abs(accTwist.angular.z) > mParam.maxAcceleration.rot)
  { //deceleration ramp negative direction
    twist->angular.z = mParam.maxAcceleration.rot * deltaT + mLastTwist.angular.z;
  }

  mLastTwist.linear.x = twist->linear.x;
  mLastTwist.linear.y = twist->linear.y;
  mLastTwist.angular.z = twist->angular.z;
  mLastTime = time;
}

double Teleop::changePrecision(double value)
{
  double result = 0;

  result = (double)round(value * mParam.precision) / mParam.precision;
  return result;
}

double Teleop::calcVel(double value, _Axis axis)
{
  double result = 0;
  switch (axis)
  {
    case Axis::X:
      if (value > 0 && value > mParam.offsetX)
      {
        result = (value - mParam.offsetX) / (mParam.rangeX.max - mParam.offsetX) * mParam.maxVelocity.lin;
      }
      else if (value < 0 && value < -mParam.offsetX)
      {
        result = (value + mParam.offsetX) / (mParam.rangeX.min + mParam.offsetX) * mParam.maxVelocity.lin * -1;
      }
      else
      {
        result = 0;
      }
      if (result >= mParam.maxVelocity.lin)
      {
        result = mParam.maxVelocity.lin;
      }
      break;
    case Axis::Y:
      if (value > 0 && value > mParam.offsetY)
      {
        result = (value - mParam.offsetY) / (mParam.rangeY.max - mParam.offsetY) * mParam.maxVelocity.lin;
      }
      else if (value < 0 && value < -mParam.offsetY)
      {
        result = (value + mParam.offsetY) / (mParam.rangeY.min + mParam.offsetY) * mParam.maxVelocity.lin * -1;
      }
      else
      {
        result = 0;
      }
      if (result >= mParam.maxVelocity.lin)
      {
        result = mParam.maxVelocity.lin;
      }
      break;
    case Axis::Wz:
      if (value > 0 && value > mParam.offsetWz)
      {
        result = (value - mParam.offsetWz) / (mParam.rangeWz.max - mParam.offsetWz) * mParam.maxVelocity.rot;
      }
      else if (value < 0 && value < -mParam.offsetWz)
      {
        result = (value + mParam.offsetWz) / (mParam.rangeWz.min + mParam.offsetWz) * mParam.maxVelocity.rot * -1;
      }
      else
      {
        result = 0;
      }
      if (result >= mParam.maxVelocity.rot)
      {
        result = mParam.maxVelocity.rot;
      }
      break;
  }
  result = changePrecision(result);
  return result;
}

void Teleop::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;
  ros::Time time = joy->header.stamp;

  twist.linear.x = calcVel(joy->axes.at(0), Axis::X);
  if (mParam.enable_omni)
  {
    twist.linear.y = calcVel(joy->axes.at(1), Axis::Y);
  }
  twist.angular.z = calcVel(joy->axes.at(5), Axis::Wz);
  if (mParam.enable_ramp)
  {
    interpolateAcceleration(&twist, joy->header.stamp);
  }
  mTwistPub.publish(twist);

}

void Teleop::getParam()
{

  if (!ros::param::has("~/rangeX/max"))
  {
    ROS_FATAL("no calibration done");
    //TODO Exception
  }

  ros::param::param<double>("~/rangeX/max", mParam.rangeX.max, 1);
  ros::param::param<double>("~/rangeX/min", mParam.rangeX.min, -1);
  ros::param::param<double>("~/rangeY/max", mParam.rangeY.max, 1);
  ros::param::param<double>("~/rangeY/min", mParam.rangeY.min, -1);
  ros::param::param<double>("~/rangeWz/max", mParam.rangeWz.max, 1);
  ros::param::param<double>("~/rangeWz/min", mParam.rangeWz.min, -1);
  ros::param::param<double>("~/maxVelocity/lin", mParam.maxVelocity.lin, 1);
  ros::param::param<double>("~/maxVelocity/rot", mParam.maxVelocity.rot, 1);
  ros::param::param<double>("~/maxAcceleration/lin", mParam.maxAcceleration.lin, 5);
  ros::param::param<double>("~/maxAcceleration/rot", mParam.maxAcceleration.rot, 5);
  ros::param::param<double>("~/offsetX", mParam.offsetX, 0.1);
  ros::param::param<double>("~/offsetY", mParam.offsetY, 0.1);
  ros::param::param<double>("~/offsetWz", mParam.offsetWz, 0.1);
  ros::param::param<int>("~/precision", mParam.precision, 1000);
  ros::param::param<bool>("~/enable_omni", mParam.enable_omni, true);
  ros::param::param<bool>("~/enable_ramp", mParam.enable_ramp, false);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "teleop");
  Teleop teleop;

  ros::spin();
}

