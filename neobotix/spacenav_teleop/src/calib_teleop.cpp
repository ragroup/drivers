/*
 * sepp_calib_teleop.cpp
 *
 *  Created on: Mar 7, 2016
 *      Author: speckle_s
 */

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Joy.h"
#include "stdlib.h"
#include "sstream"
//#include "ros/param.h"

struct RangeAxis
{
	double min;
	double max;
};

struct ParamTeleop
{
	RangeAxis rangeX;
	RangeAxis rangeY;
	RangeAxis rangeWz;
	double offsetX;
	double offsetY;
	double offsetWz;
	double maxVelocity;
	double maxAcceleration;
};

namespace States
{
enum StatesCalibTeleop
{
	XAxisMax,
	XAxisMaxWait,
	XAxisMaxCalc,
	XAxisMin,
	XAxisMinWait,
	XAxisMinCalc,
	YAxisMax,
	YAxisMaxWait,
	YAxisMaxCalc,
	YAxisMin,
	YAxisMinWait,
	YAxisMinCalc,
	WzAxisMax,
	WzAxisMaxWait,
	WzAxisMaxCalc,
	WzAxisMin,
	WzAxisMinWait,
	WzAxisMinCalc,
	Done
};
}

typedef States::StatesCalibTeleop StatesCalibTeleop;

class SeppCalibTeleop
{
public:
	SeppCalibTeleop();
	void doCalibration(const sensor_msgs::Joy::ConstPtr&);
	void setParam();
	bool calibDone(){return mCalculationDone;}
private:

	void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

	ros::NodeHandle mNodeHandler;
	ros::Subscriber mJoySub;
	ParamTeleop mParam;
	StatesCalibTeleop mState;
	int mIterator;
	double mTemp;
	double mThreshold;
	bool mCalculationDone;

};

void SeppCalibTeleop::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
	doCalibration(joy);
}


SeppCalibTeleop::SeppCalibTeleop() :
		mParam(), mState(States::XAxisMax), mIterator(), mTemp(), mThreshold(
				0.5),mCalculationDone(false)
{
	mJoySub = mNodeHandler.subscribe<sensor_msgs::Joy>("spacenav/joy", 10,
			&SeppCalibTeleop::joyCallback, this);

}

void SeppCalibTeleop::setParam()
{
	std::ostringstream command;


	command<<"osparam dump";
	mNodeHandler.setParam("sepp_teleop/rangeX/max",mParam.rangeX.max);
	mNodeHandler.setParam("sepp_teleop/rangeX/min",mParam.rangeX.min);
	mNodeHandler.setParam("sepp_teleop/rangeY/max",mParam.rangeY.max);
	mNodeHandler.setParam("sepp_teleop/rangeY/min",mParam.rangeY.min);
	mNodeHandler.setParam("sepp_teleop/rangeWz/max",mParam.rangeWz.max);
	mNodeHandler.setParam("sepp_teleop/rangeWz/min",mParam.rangeWz.min);
	system("rosparam dump ~/sepp_teleop.yaml /sepp_teleop");

}

void SeppCalibTeleop::doCalibration(const sensor_msgs::Joy::ConstPtr& joy)
{
	switch (mState)
	{
	case States::XAxisMax:
		ROS_INFO("Let's do the calibration\n");
		ROS_INFO("please Move your joystick to very positive X position");
		mState = States::XAxisMaxWait;
		break;
	case States::XAxisMaxWait:
		if (joy->axes.at(0) > mThreshold)
		{
			mIterator = 0;
			mTemp = 0;
			mState = States::XAxisMaxCalc;
		}
		break;
	case States::XAxisMaxCalc:
		mTemp = mTemp + joy->axes.at(0);
		mIterator++;
		if (mIterator > 50)
		{
			mParam.rangeX.max = mTemp / mIterator;
			mState = States::XAxisMin;
		}
		break;

	case States::XAxisMin:
		ROS_INFO("please Move your joystick to very negative X position");
		mState = States::XAxisMinWait;
		break;

	case States::XAxisMinWait:
		if (joy->axes.at(0) < -mThreshold)
		{
			mIterator = 0;
			mTemp = 0;
			mState = States::XAxisMinCalc;
		}
		break;

	case States::XAxisMinCalc:
		mTemp = mTemp + joy->axes.at(0);
		mIterator++;
		if (mIterator > 50)
		{
			mParam.rangeX.min = mTemp / mIterator;
			//mState = States::YAxisMax;
			mState = States::WzAxisMax;
		}
		break;
	/*case States::YAxisMax:
		ROS_INFO("please Move your joystick to very left Y position");
		mState = States::YAxisMaxWait;
		break;

	case States::YAxisMaxWait:
		if (joy->axes.at(1) > mThreshold)
		{
			mIterator = 0;
			mTemp = 0;
			mState = States::YAxisMaxCalc;
		}
		break;

	case States::YAxisMaxCalc:
		mTemp = mTemp + joy->axes.at(1);
		mIterator++;
		if (mIterator > 50)
		{
			mParam.rangeY.max = mTemp / mIterator;
			mState = States::YAxisMin;
		}
		break;

	case States::YAxisMin:
		ROS_INFO("please Move your joystick to very right Y position");
		mState = States::YAxisMinWait;
		break;

	case States::YAxisMinWait:
		if (joy->axes.at(1) < -mThreshold)
		{
			mIterator = 0;
			mTemp = 0;
			mState = States::YAxisMinCalc;
		}
		break;

	case States::YAxisMinCalc:
		mTemp = mTemp + joy->axes.at(1);
		mIterator++;
		if (mIterator > 50)
		{
			mParam.rangeY.min = mTemp / mIterator;
			mState = States::WzAxisMax;
		}
		break;*/

	case States::WzAxisMax:
		ROS_INFO("please turn your joystick counterclockwise to the maximum position");
				mState = States::WzAxisMaxWait;
		break;

	case States::WzAxisMaxWait:
		if (joy->axes.at(5) > mThreshold)
				{
					mIterator = 0;
					mTemp = 0;
					mState = States::WzAxisMaxCalc;
				}
		break;

	case States::WzAxisMaxCalc:
		mTemp = mTemp + joy->axes.at(5);
				mIterator++;
				if (mIterator > 50)
				{
					mParam.rangeWz.max = mTemp / mIterator;
					mState = States::WzAxisMin;
				}
		break;

	case States::WzAxisMin:
		ROS_INFO("please turn your joystick clockwise to the maximum position");
						mState = States::WzAxisMinWait;
		break;

	case States::WzAxisMinWait:
		if (joy->axes.at(5) < -mThreshold)
						{
							mIterator = 0;
							mTemp = 0;
							mState = States::WzAxisMinCalc;
						}
		break;

	case States::WzAxisMinCalc:
		mTemp = mTemp + joy->axes.at(5);
						mIterator++;
						if (mIterator > 50)
						{
							mParam.rangeWz.min = mTemp / mIterator;
							mState = States::Done;
						}
		break;

	case States::Done:
		//store Prameters
		setParam();
		mCalculationDone = true;
		break;

	}
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "sepp_calib_teleop");
	SeppCalibTeleop calibTeleop;

	while (ros::ok() && !calibTeleop.calibDone())
	{
		ros::spinOnce();
	}
	ROS_INFO ("stopping sepp_calib_teleop Node");
}

