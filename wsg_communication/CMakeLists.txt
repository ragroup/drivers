cmake_minimum_required(VERSION 2.8.3)
project(wsg_communication)

set(CMAKE_BUILD_TYPE Debug)
add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  roslib
  logger_sys
  tf
  geometry_msgs
  sensor_msgs
  device_mgr_msgs
)

include_directories(${CMAKE_CURRENT_BINARY_DIR})


#set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

## Generate actions in the 'action' folder
add_action_files(
    DIRECTORY action
    FILES
    Gripper.action
)

## Generate added messages and services with any dependencies listed here
generate_messages(
    DEPENDENCIES
    std_msgs
    sensor_msgs
    geometry_msgs
    actionlib_msgs
)


###################################
## catkin specific configuration ##
###################################
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES WSGClient
  CATKIN_DEPENDS
      roscpp
      roslib
      logger_sys
      tf
      sensor_msgs
      geometry_msgs
      device_mgr_msgs
  DEPENDS Boost
)

###########
## Build ##
###########

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  include
)

add_library(WSGClient
    src/WsgClient.cpp
)
add_executable(wsg_command_node
    src/wsg_command_node.cpp
    src/WsgCommands.cpp
    src/WsgReceiver.cpp
)
add_executable(test_wsg_node
    src/test_wsg_node.cpp
)

add_dependencies(WSGClient ${PROJECT_NAME}_generate_messages_cpp)
add_dependencies(wsg_command_node ${PROJECT_NAME}_generate_messages_cpp)
add_dependencies(test_wsg_node ${PROJECT_NAME}_generate_messages_cpp)

message("catkin_LIBRARIES: " ${catkin_LIBRARIES})

target_link_libraries(WSGClient ${catkin_LIBRARIES} ${Boost_LIBRARIES})
target_link_libraries(wsg_command_node ${catkin_LIBRARIES} ${Boost_LIBRARIES})
target_link_libraries(test_wsg_node ${catkin_LIBRARIES} ${Boost_LIBRARIES} WSGClient)



#############
## Install ##
#############

## Mark executables and/or libraries for installation
install(TARGETS WSGClient wsg_command_node test_wsg_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp"
  PATTERN ".svn" EXCLUDE
)
