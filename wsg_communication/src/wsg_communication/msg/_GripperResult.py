"""autogenerated by genpy from wsg_communication/GripperResult.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import genpy

class GripperResult(genpy.Message):
  _md5sum = "5c296801d044c1a134846e793dba9cbb"
  _type = "wsg_communication/GripperResult"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """# ====== DO NOT MODIFY! AUTOGENERATED FROM AN ACTION DEFINITION ======
int16 goalID
time elapsedTotal
int8 state
float32 wsg_width
float32 wsg_speed
float32 wsg_acc
float32 wsg_force
float32 wsg_minus
float32 wsg_plus
float32 wsg_sys_state
int32 total_part
int32 no_part
int32 lost_part
uint8 grasp_state


"""
  __slots__ = ['goalID','elapsedTotal','state','wsg_width','wsg_speed','wsg_acc','wsg_force','wsg_minus','wsg_plus','wsg_sys_state','total_part','no_part','lost_part','grasp_state']
  _slot_types = ['int16','time','int8','float32','float32','float32','float32','float32','float32','float32','int32','int32','int32','uint8']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       goalID,elapsedTotal,state,wsg_width,wsg_speed,wsg_acc,wsg_force,wsg_minus,wsg_plus,wsg_sys_state,total_part,no_part,lost_part,grasp_state

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(GripperResult, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.goalID is None:
        self.goalID = 0
      if self.elapsedTotal is None:
        self.elapsedTotal = genpy.Time()
      if self.state is None:
        self.state = 0
      if self.wsg_width is None:
        self.wsg_width = 0.
      if self.wsg_speed is None:
        self.wsg_speed = 0.
      if self.wsg_acc is None:
        self.wsg_acc = 0.
      if self.wsg_force is None:
        self.wsg_force = 0.
      if self.wsg_minus is None:
        self.wsg_minus = 0.
      if self.wsg_plus is None:
        self.wsg_plus = 0.
      if self.wsg_sys_state is None:
        self.wsg_sys_state = 0.
      if self.total_part is None:
        self.total_part = 0
      if self.no_part is None:
        self.no_part = 0
      if self.lost_part is None:
        self.lost_part = 0
      if self.grasp_state is None:
        self.grasp_state = 0
    else:
      self.goalID = 0
      self.elapsedTotal = genpy.Time()
      self.state = 0
      self.wsg_width = 0.
      self.wsg_speed = 0.
      self.wsg_acc = 0.
      self.wsg_force = 0.
      self.wsg_minus = 0.
      self.wsg_plus = 0.
      self.wsg_sys_state = 0.
      self.total_part = 0
      self.no_part = 0
      self.lost_part = 0
      self.grasp_state = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_h2Ib7f3iB.pack(_x.goalID, _x.elapsedTotal.secs, _x.elapsedTotal.nsecs, _x.state, _x.wsg_width, _x.wsg_speed, _x.wsg_acc, _x.wsg_force, _x.wsg_minus, _x.wsg_plus, _x.wsg_sys_state, _x.total_part, _x.no_part, _x.lost_part, _x.grasp_state))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.elapsedTotal is None:
        self.elapsedTotal = genpy.Time()
      end = 0
      _x = self
      start = end
      end += 52
      (_x.goalID, _x.elapsedTotal.secs, _x.elapsedTotal.nsecs, _x.state, _x.wsg_width, _x.wsg_speed, _x.wsg_acc, _x.wsg_force, _x.wsg_minus, _x.wsg_plus, _x.wsg_sys_state, _x.total_part, _x.no_part, _x.lost_part, _x.grasp_state,) = _struct_h2Ib7f3iB.unpack(str[start:end])
      self.elapsedTotal.canon()
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_h2Ib7f3iB.pack(_x.goalID, _x.elapsedTotal.secs, _x.elapsedTotal.nsecs, _x.state, _x.wsg_width, _x.wsg_speed, _x.wsg_acc, _x.wsg_force, _x.wsg_minus, _x.wsg_plus, _x.wsg_sys_state, _x.total_part, _x.no_part, _x.lost_part, _x.grasp_state))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(_x))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(_x))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.elapsedTotal is None:
        self.elapsedTotal = genpy.Time()
      end = 0
      _x = self
      start = end
      end += 52
      (_x.goalID, _x.elapsedTotal.secs, _x.elapsedTotal.nsecs, _x.state, _x.wsg_width, _x.wsg_speed, _x.wsg_acc, _x.wsg_force, _x.wsg_minus, _x.wsg_plus, _x.wsg_sys_state, _x.total_part, _x.no_part, _x.lost_part, _x.grasp_state,) = _struct_h2Ib7f3iB.unpack(str[start:end])
      self.elapsedTotal.canon()
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_h2Ib7f3iB = struct.Struct("<h2Ib7f3iB")
