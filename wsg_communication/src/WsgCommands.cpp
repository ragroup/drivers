/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This is the source file of the class "WsgCommands"
 *
 * The purpose and use of this class is documented in the corresponding header file.'
 *
 * Please note in this document:
 * 		A packet refer to the total encapsulated data being either send or received via UDP connection to and from the gripper
 * 			This means both the payload data, but also a header, a size info, a checksum and a command id.
 * 			The packet must be build in a specific order.
 *
 * 		A payload is the actual variable data of a packet.
 * 			A payload must in most cases be of a certain length and type.
 * 			Please note, when executing a command, a payload is send and a nother payload is received.
 *
 * 		The state refer to the error code, which can be fund in "WsgData.hpp"
 *
 * 		Unions are used to convert between bytes and integer/float.
 */

#include "ros/console.h"
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string>
#include <boost/thread.hpp>

#include "WsgReceiver.hpp"
#include "WsgCommands.hpp"
#include <logger_sys/logger_sys.hpp>


using namespace std;

WsgCommands::WsgCommands(const ros::NodeHandle& nh, string ipAddress) : n(nh)
{
	/**
	 * Constructor of the WsgCommands class.
	 * Here the structs for the UDP communication with the gripper is set.
	 * Finally it is checked if the gripper is online, and if it is (initialized)
	 */

	debug(FFL, "Instantiating object of WsgCommands");

	//Struct for incoming data
	//	in_addr.sin_family = AF_INET;						//IPv4
	//	in_addr.sin_port = htons(INPORT);					//Port number
	//	in_addr.sin_addr.s_addr = INADDR_ANY;				//My IP-address, whatever it is
	//	memset(in_addr.sin_zero, '\0', sizeof in_addr.sin_zero);

	//Struct for outgoing data
	out_addr.sin_family = AF_INET;						//IPv4
	out_addr.sin_port = htons(OUTPORT);					//Port number
    out_addr.sin_addr.s_addr = inet_addr(ipAddress.c_str());	//IP-address
	memset(out_addr.sin_zero, '\0', sizeof out_addr.sin_zero);

	//Bind to socket for incoming data.
	sockfd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	FINFO("Connection to gripper IP: '"
			<< inet_ntoa(out_addr.sin_addr) << "'...");

	//	debug(FFL, "Binding to socket");

	//	bind(sockfd, (struct sockaddr *)&in_addr, sizeof(in_addr));

	//	info(FFL, "Connecting to gripper...");

	myProtectedBuffer = new ProtectedBuffer();
	myWsgReceiver = new WsgReceiver(n, myProtectedBuffer);

	//initial check to see whether the gripper is online and initialized
	InitCheck();

}

WsgCommands::~WsgCommands()
{
	debug(FFL, "::~WsgCommands");

	Disconnect();										//send disconnect announcement before exit, to ensure the gripper will be ready for the next connection.

	close(sockfd);										//close the socket connection before exit

	delete myWsgReceiver;

	delete myProtectedBuffer;
}

//Gripper functions

int WsgCommands::InitCheck()
{
	/**
	 * This function will first check if the gripper is online by sending a loop command.
	 * If the gripper is online, it checks whether the gripper is initialized.
	 * It does NOT initialize (home) the gripper, it simply does the check, prints the result to the screen and returns the state.
	 */

	debug(FFL, "::InitCheck");

	if(Loop() != 0)
	{
		error(FFL, "Failed to connect to gripper");
		return 21;
	}
	else
	{
		info(FFL, "Connection established");
	}


	float wsg_sys_state;
	GetSysState(&wsg_sys_state);
	char * bits;
	bits = (char *) malloc(32*sizeof(char));

	DecodeSysState(wsg_sys_state, bits);

	if (bits[0] != 1)
	{
		warn(FFL, "Gripper not initialized");
		return 3;
	}
	else
	{
		info(FFL, "Gripper initialized and ready");
		return 0;
	}
}

int WsgCommands::Init()
{
	/**
	 * This function will first do the initial check from InitCheck() above - check if the gripper is online, if yes,
	 * 		check whether the gripper is initialized.
	 * If the gripper is online and initialized, then it will not do any further.
	 * If the gripper is not initialized, it will initialize the gripper.
	 * Please note, running this command WILL initialize (home) the gripper if it is not initialized.
	 */

	debug(FFL, "::Init");

	debug(FFL, "Checking connection and state of the gripper");

	int init = InitCheck();

	if (init == 21) return init;
	if (init == 0) return init;
	if (init == 3) info(FFL, "Initializing gripper...");

	init = Home();

	if (init != 0) error(FFL, "Initialization of the gripper failed");
	else info(FFL, "Gripper now initialized and ready");

	return init;
}

int WsgCommands::Loop()
{
	/**
	 * This function is used to verify the connection to the gripper.
	 * Custom data is passed to the gripper, and it should return the exact same data.
	 */

	debug(FFL, "::Loop");

	int cmdID = 0x06;					//command ID

	int datasize = 4;					//size of payload

	unsigned char data[datasize];		//declare the array of payload

	data[0] = 0x05;						//fill in the payload
	data[1] = 0x07;
	data[2] = 0x09;
	data[3] = 0x0b;

	//Create payload struct for outgoing package
	struct payloadInfo payload;			//Payload struct for outgoing package
	payload.id = cmdID;					//Command ID
	payload.data = data;				//Pointer to payload data
	payload.length = datasize;			//Size of payload data;

	//declare variables used for the incoming packet.
	int state;
	unsigned char *buf_in;						//pointer to memory to hold incoming payload
	int length = 4;								//size of the returned payload
	buf_in = (unsigned char*) malloc(length);	//allocate memory for the incoming data, and set the pointer to this allocated memory.

	//execute the command
	state = ExecuteCommand(&payload, buf_in, length);

	//check the returned data
	if (state != 0)
	{
		return(state);
		error(FFL, "Looping data failed");
	}

	if(buf_in[0] != data[0] || buf_in[1] != data[1] || buf_in[2] != data[2] || buf_in[3] != data[3])
	{
		error(FFL, "Looped data inconsistent - Gripper connection unstable");
		return(11);
	}

	debug(FFL, "Looping data successful");

	return state;
}

int WsgCommands::Disconnect()
{
	debug(FFL, "::Disconnect");

	int cmdID = 0x07;

	int datasize = 0;

	unsigned char data[datasize];

	data[0] = 0x00;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0)
	{
		debug(FFL, "Disconnecting successful");
		warn(FFL, "Disconnected from Gripper");
	}
	else error(FFL, "Disconnecting failed");

	return state;
}

int WsgCommands::Home()
{
	debug(FFL, "::Home");

	int cmdID = 0x20;

	int datasize = 1;

	unsigned char data[datasize];

	data[0] = 0x00;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Homing successful");
	else error(FFL, "Homing failed");

	return state;
}

int WsgCommands::Move(float width, float speed)
{
	log_msg.str("");
	log_msg << "::Move <Width: " << width << " Speed: " << speed << ">";
	debug(FFL, log_msg.str());

	UStuff a,b;

	a.f = width;
	b.f = speed;

	int cmdID = 0x21;

	int datasize = 9;

	unsigned char data[datasize];

	data[0] = 0x00;

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i + 1] = a.c[i];
	}

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i + 1 + sizeof(UStuff)] = b.c[i];
	}

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) info(FFL, "Move successful");
	else error(FFL, "Move failed");

	return state;
}

int WsgCommands::Stop()
{
	debug(FFL, "::Stop");

	int cmdID = 0x22;

	int datasize = 0;

	unsigned char data[1];

	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Setting stop successful");
	else error(FFL, "Setting stop failed");

	return state;
}

int WsgCommands::FastStop()
{
	debug(FFL, "::FastStop");

	int cmdID = 0x23;

	int datasize = 0;

	unsigned char data[1];

	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0)
	{
		debug(FFL, "Setting fast stop successful");
		warn(FFL, "Fast stop activated");
	}
	else error(FFL, "Setting fast stop failed");

	return state;
}

int WsgCommands::AckStop()
{
	debug(FFL, "::AckStop");

	int cmdID = 0x24;

	int datasize = 3;

	unsigned char data[] = {0x61,0x63,0x6b}; // equal to "a,c,k" (acknowledge)

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Acknowledging stop successful");
	else error(FFL, "Acknowledging stop failed");

	return state;
}

int WsgCommands::Grasp(float width, float speed)
{
	log_msg.str("");
	log_msg << "::Grasp <Width: " << width << " Speed: " << speed << ">";
	debug(FFL, log_msg.str());

	UStuff a,b;

	a.f = width;
	b.f = speed;

	int cmdID = 0x25;

	int datasize = 8;

	unsigned char data[datasize];

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i] = a.c[i];
	}

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i + sizeof(UStuff)] = b.c[i];
	}

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Grasp successful");
	else error(FFL, "Grasp failed");

	return state;
}

int WsgCommands::Release(float width, float speed)
{
	log_msg.str("");
	log_msg << "::Release <Width: " << width << " Speed: " << speed << ">";
	debug(FFL, log_msg.str());

	UStuff a,b;

	a.f = width;
	b.f = speed;

	int cmdID = 0x26;

	int datasize = 8;

	unsigned char data[datasize];

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i] = a.c[i];
	}

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i + sizeof(UStuff)] = b.c[i];
	}

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Release successful");
	else error(FFL, "Release failed");

	return state;
}

int WsgCommands::SetAcc(float acc)
{
	log_msg.str("");
	log_msg << "::SetAcc <Acc: " << acc << ">";
	debug(FFL, log_msg.str());

	UStuff a;

	a.f = acc;

	int cmdID = 0x30;

	int datasize = 4;

	unsigned char data[datasize];

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i] = a.c[i];
	}

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Setting acc successful");
	else error(FFL, "Setting acc failed");

	return state;
}

int WsgCommands::GetAcc(float * acc)
{
	debug(FFL, "::GetAcc");

	int cmdID = 0x31;
	int datasize = 0;
	unsigned char data[1];
	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting acc successful");
	else error(FFL, "Getting acc failed");

	//decode the acc from the payload buffer
	UStuff a;

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	*acc = a.f;

	log_msg.str("");
	log_msg << "Acc returned: " << a.f;
	info(FFL, log_msg.str());

	return state;
}

int WsgCommands::SetForceLimit(float force)
{
	log_msg.str("");
	log_msg << "::SetForceLimit <Force limit: " << force << ">";
	debug(FFL, log_msg.str());

	UStuff a;

	a.f = force;

	int cmdID = 0x32;

	int datasize = 4;

	unsigned char data[datasize];

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i] = a.c[i];
	}

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Setting force limit successful");
	else error(FFL, "Setting force limit failed");

	return state;
}

int WsgCommands::GetForceLimit(float * force)
{
	debug(FFL, "::GetForceLimit");

	int cmdID = 0x33;

	int datasize = 0;

	unsigned char data[1];

	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting force limit successful");
	else error(FFL, "Getting acc failed");

	//decode the force from the payload buffer
	UStuff a;

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	*force = a.f;

	log_msg.str("");
	log_msg << "Force limit returned: " << a.f;
	info(FFL, log_msg.str());

	return state;
}

int WsgCommands::SetLimits(float limit_minus, float limit_plus)
{
	log_msg.str("");
	log_msg << "::SetLimits <Lower: " << limit_minus << " Upper: " << limit_plus << ">";
	debug(FFL, log_msg.str());

	UStuff a,b;

	a.f = limit_minus;
	b.f = limit_plus;

	int cmdID = 0x34;

	int datasize = 8;

	unsigned char data[datasize];

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i] = a.c[i];
	}

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		data[i + sizeof(UStuff)] = b.c[i];
	}

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Setting soft limits successful");
	else error(FFL, "Setting soft limits failed");

	return state;
}

int WsgCommands::GetLimits(float * limit_minus, float * limit_plus)
{
	debug(FFL, "::GetLimits");

	int cmdID = 0x35;

	int datasize = 0;

	unsigned char data[1];

	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 8;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting soft limits successful");
	else error(FFL, "Getting soft limits failed");

	//decode the limits from the payload buffer
	UStuff a,b;

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
		b.c[i] = buf_in[i+4];
	}


	*limit_minus = a.f;
	*limit_plus = b.f;

	log_msg.str("");
	log_msg << "Lower limit returned: " << a.f <<
			" Upper limit returned: " << b.f;
	info(FFL, log_msg.str());

	return state;
}

int WsgCommands::ClearLimits()
{
	debug(FFL, "::ClearLimits");

	int cmdID = 0x36;

	int datasize = 0;

	unsigned char data[1];

	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Clearing soft limits successful");
	else error(FFL, "Clearing soft limits failed");

	return state;
}

int WsgCommands::OverdriveOn()
{
	debug(FFL, "::OverdriveOn");

	int cmdID = 0x37;

	int datasize = 1;

	unsigned char data[datasize];

	data[0] = 0x80;  //Equals bit-vector of (1 0 0 0 0 0 0 0) = decimal: 128

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0)
	{
		debug(FFL, "Activating overdrive successful");
		warn(FFL, "Overdrive activated - Risk of overheating");
	}
	else error(FFL, "Activating overdrive failed");

	return state;
}

int WsgCommands::OverdriveOff()
{
	debug(FFL, "::OverdriveOff");

	int cmdID = 0x37;

	int datasize = 1;

	unsigned char data[datasize];

	data[0] = 0x00;  //Equals bit-vector of (0 0 0 0 0 0 0 0)

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Deactivating overdrive successful");
	else error(FFL, "Deactivating overdrive failed");

	return state;
}

int WsgCommands::TareForce()
{
	debug(FFL, "::TareForce");

	int cmdID = 0x38;

	int datasize = 0;

	unsigned char data[1];

	data[0] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 0;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Taring force sensor successful");
	else error(FFL, "Taring force sensor failed");

	return state;
}

int WsgCommands::GetSysState(float * Bits)
{
	debug(FFL, "::GetSysState");

	int cmdID = 0x40;

	int datasize = 3;

	unsigned char data[datasize];

	data[0] = 0x00;  //Equals bit-vector of (0 0 0 0 0 0 0 0) = automatic update off - receive one packet
	data[1] = 0x00;
	data[2] = 0x00;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting system state successful");
	else error(FFL, "Getting system state failed");

	//decode the system state from the payload buffer and encode it as a float
	UStuff a;

    for (int i = 0; i < (int)sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	*Bits = a.f;


	return state;
}

int WsgCommands::GetGraspState(wsg_definitions::graspState *grasp_state)
{
	debug(FFL, "::GetGraspState");

	int cmdID = 0x41;

	int datasize = 3;

	unsigned char data[datasize];

	data[0] = 0x00;  //Equals bit-vector of (0 0 0 0 0 0 0 0) = automatic update off - receive one packet
	data[1] = 0x00;
	data[2] = 0x00;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;


	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 1;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting grasp state successful");
	else error(FFL, "Getting grasp state failed");

	switch(buf_in[0])
	{
	case 0:
		*grasp_state = wsg_definitions::IDLE;
		info(FFL, "GraspState returned: IDLE");
		break;

	case 1:
		*grasp_state = wsg_definitions::GRASPING;
		info(FFL, "GraspState returned: GRASPING");
		break;

	case 2:
		*grasp_state = wsg_definitions::NO_PART_FOUND;
		info(FFL, "GraspState returned: NO PART FOUND");
		break;

	case 3:
		*grasp_state = wsg_definitions::PART_LOST;
		info(FFL, "GraspState returned: PART LOST");
		break;

	case 4:
		*grasp_state = wsg_definitions::HOLDING;
		info(FFL, "GraspState returned: HOLDING");
		break;

	case 5:
		*grasp_state = wsg_definitions::RELEASING;
		info(FFL, "GraspState returned: RELEASING");
		break;

	case 6:
		*grasp_state = wsg_definitions::POSITIONING;
		info(FFL, "GraspState returned: POSITIONING");
		break;

	default:
		*grasp_state = wsg_definitions::UNDEFINED;
		info(FFL, "GraspState returned: UNDEFINED GRASP STATE");
		break;

	}

	return state;
}

int WsgCommands::GetGraspStatis(int *total, int *no_part, int *lost_part)
{
	debug(FFL, "::GetGraspStatis");

	int cmdID = 0x42;
	int datasize = 1;
	unsigned char data[1];

	data[0] = 0x00;  //Equals bit-vector of (0 0 0 0 0 0 0 0)

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 8;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting grasp statistic successful");
	else error(FFL, "Getting grasp statistic failed");

	//decode from the payload buffer
	UStuff3 a;
	UStuff2 b;
	UStuff2 c;

	for (int i = 0; i < sizeof(UStuff3); ++i)
	{
		a.c[i] = buf_in[i];
	}
	for (int i = 0; i < sizeof(UStuff2); ++i)
	{
		b.c[i] = buf_in[i+4];
	}
	for (int i = 0; i < sizeof(UStuff2); ++i)
	{
		c.c[i] = buf_in[i+6];
	}

	*total = a.i;
	*no_part = b.i;
	int lost_ = (int)c.i;
	*lost_part = lost_;

	log_msg.str("");
	log_msg << "Total returned: " << a.i <<
			" No part returned: " << b.i <<
			" Lost part returned: " << c.i;
	info(FFL, log_msg.str());

	return state;
}

int WsgCommands::ResetGraspStatis()
{
	debug(FFL, "::ResetGraspStatis");

	int cmdID = 0x42;
	int datasize = 1;
	unsigned char data[1];

	data[0] = 0x80;  //Equals bit-vector of (1 0 0 0 0 0 0 0)

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 8;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Reseting grasp statistic successful");
	else error(FFL, "Reseting grasp statistic failed");

	return state;
}

int WsgCommands::GetWidth(float * width) // get a single measurement.
{
	debug(FFL, "::GetWidth");

	int cmdID = 0x43;

	int datasize = 3;

	unsigned char data[1];

	data[0] = 0x00; //Equals bit-vector of (0 0 0 0 0 0 0 0)
	data[1] = 0;
	data[2] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting current width successful");
	else error(FFL, "Getting current width failed");

	//decode the force from the payload buffer
	UStuff a;

	for (int i = 0; i < sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	*width = a.f;

	log_msg.str("");
	log_msg << "Width returned: " << a.f;
	info(FFL, log_msg.str());

	return state;
}

int WsgCommands::GetSpeed(float * speed)
{
	debug(FFL, "::GetSpeed");

	int cmdID = 0x44;

	int datasize = 3;

	unsigned char data[1];

	data[0] = 0x00; //Equals bit-vector of (0 0 0 0 0 0 0 0)
	data[1] = 0;
	data[2] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting current speed successful");
	else error(FFL, "Getting current speed failed");

	//decode the force from the payload buffer
	UStuff a;

	for (int i = 0; i < sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	*speed = a.f;

	log_msg.str("");
	log_msg << "Speed returned: " << a.f;
	info(FFL, log_msg.str());

	return state;
}

int WsgCommands::GetForce(float * force)
{
	debug(FFL, "::GetForce");

	int cmdID = 0x45;

	int datasize = 3;

	unsigned char data[1];

	data[0] = 0x00; //Equals bit-vector of (0 0 0 0 0 0 0 0)
	data[1] = 0;
	data[2] = 0;

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Getting current force successful");
	else error(FFL, "Getting current force failed");

	//decode the force from the payload buffer
	UStuff a;

	for (int i = 0; i < sizeof(UStuff); ++i)
	{
		a.c[i] = buf_in[i];
	}

	*force = a.f;

	log_msg.str("");
	log_msg << "Force returned: " << a.f;
	info(FFL, log_msg.str());

	return state;
}

//Auto functions

int WsgCommands::AutoWidth(int mode, int frequency) 		// get continuous measurements
{
	debug(FFL, "::AutoWidth");

	int interval = 1/frequency;

	int cmdID = 0x43;

	int datasize = 3;

	unsigned char data[datasize];

	if(mode == 0) 	//always update (continuous update)
	{
		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}
	else if(mode == 1)
	{
		data[0] = 0x03; //Equals bit-vector of (0 0 0 0 0 0 1 1)
	}
	else
	{
		log_msg.str("");
		log_msg << "Update mode: " << mode << " invalid! Using continuous update instead (mode 0).";
		error(FFL, log_msg.str());

		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}

	UStuff2 a;

	a.i = interval;

	data[1] = a.c[0];
	data[2] = a.c[1];

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Starting automatic update of width successful");
	else error(FFL, "Starting automatic update of width failed");

	return state;
}

int WsgCommands::AutoSpeed(int mode, int frequency) 		// get continuous measurements
{
	debug(FFL, "::AutoSpeed");

	int interval = 1/frequency;

	int cmdID = 0x44;

	int datasize = 3;

	unsigned char data[datasize];

	if(mode == 0) 	//always update (continuous update)
	{
		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}
	else if(mode == 1)
	{
		data[0] = 0x03; //Equals bit-vector of (0 0 0 0 0 0 1 1)
	}
	else
	{
		log_msg.str("");
		log_msg << "Update mode: " << mode << " invalid! Using continuous update instead (mode 0).";
		error(FFL, log_msg.str());

		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}

	UStuff2 a;

	a.i = interval;

	data[1] = a.c[0];
	data[2] = a.c[1];

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Starting automatic update of speed successful");
	else error(FFL, "Starting automatic update of speed failed");

	return state;
}

int WsgCommands::AutoForce(int mode, int frequency) 		// get continuous measurements
{
	debug(FFL, "::AutoForce");

	int interval = 1/frequency;

	int cmdID = 0x45;

	int datasize = 3;

	unsigned char data[datasize];

	if(mode == 0) 	//always update (continuous update)
	{
		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}
	else if(mode == 1)
	{
		data[0] = 0x03; //Equals bit-vector of (0 0 0 0 0 0 1 1)
	}
	else
	{
		log_msg.str("");
		log_msg << "Update mode: " << mode << " invalid! Using continuous update instead (mode 0).";
		error(FFL, log_msg.str());

		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}

	UStuff2 a;

	a.i = interval;

	data[1] = a.c[0];
	data[2] = a.c[1];

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Starting automatic update of force successful");
	else error(FFL, "Starting automatic update of force failed");

	return state;
}

int WsgCommands::AutoGraspState(int mode, int frequency) // get continuous measurements
{
	debug(FFL, "::AutoGraspState");

	int interval = 1/frequency;

	int cmdID = 0x41;

	int datasize = 3;

	unsigned char data[datasize];

	if(mode == 0) 	//always update (continuous update)
	{
		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}
	else if(mode == 1)
	{
		data[0] = 0x03; //Equals bit-vector of (0 0 0 0 0 0 1 1)
	}
	else
	{
		log_msg.str("");
		log_msg << "Update mode: " << mode << " invalid! Using continuous update instead (mode 0).";
		error(FFL, log_msg.str());

		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}

	UStuff2 a;

	a.i = interval;

	data[1] = a.c[0];
	data[2] = a.c[1];

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Starting automatic update of width successful");
	else error(FFL, "Starting automatic update of width failed");

	return state;
}

int WsgCommands::AutoSysState(int mode, int frequency) 	// get continuous measurements
{
	debug(FFL, "::AutoSystemState");

	int interval = 1/frequency;

	int cmdID = 0x40;

	int datasize = 3;

	unsigned char data[datasize];

	if(mode == 0) 	//always update (continuous update)
	{
		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}
	else if(mode == 1)
	{
		data[0] = 0x03; //Equals bit-vector of (0 0 0 0 0 0 1 1)
	}
	else
	{
		log_msg.str("");
		log_msg << "Update mode: " << mode << " invalid! Using continuous update instead (mode 0).";
		error(FFL, log_msg.str());

		data[0] = 0x01; //Equals bit-vector of (0 0 0 0 0 0 0 1)
	}

	UStuff2 a;

	a.i = interval;

	data[1] = a.c[0];
	data[2] = a.c[1];

	//Create payload struct
	struct payloadInfo payload;		//Payload struct
	payload.id = cmdID;				//Command ID
	payload.data = data;			//Pointer to payload data
	payload.length = datasize;		//Size of payload data;

	//execute the command
	int state;
	unsigned char *buf_in;
	int length = 4;
	buf_in = (unsigned char*) malloc(length);

	state = ExecuteCommand(&payload, buf_in, length);

	if(state == 0) debug(FFL, "Starting automatic update of width successful");
	else error(FFL, "Starting automatic update of width failed");

	return state;
}

void WsgCommands::StartJSPublisher(double frequency)		//start publishing on JointStates
{
	debug(FFL, "::StartJSPublisher");

	myWsgReceiver->startJSPublisher(frequency);
}

void WsgCommands::StopJSPublisher()
{
	debug(FFL, "::StopJSPublisher");

	myWsgReceiver->stopJSPublisher();
}


//Other functions

int WsgCommands::GetErrorCode(unsigned char *buf_in)
{
	/**
	 * This function decodes the error code from a received packet by decoding the two bytes into an integer
	 */

	debug(FFL, "::GetErrorCode");

	int error_code;

	UStuff2 b;

	b.c[0] = buf_in[6];
	b.c[1] = buf_in[7];

	error_code = b.i;

	return(error_code);
}

unsigned char * WsgCommands::BuildPackage(payloadInfo *payload, unsigned int *size) //Build message packet
{
	/**
	 * This function builds a packet for sending a command to the gripper
	 * It receives a payloadInfo struct containing the command ID, the payload it self and the size of the payload.
	 * I will build the package as follows
	 * 		byte 0-2: 		Header bytes (0xAA)
	 * 		byte 3: 		Command ID
	 * 		byte 4-5: 		Size of payload
	 * 		byte 6-: 		Payload
	 * 		last two bytes: Checksum (checksum evaluation disabled, so has no use but must be included)
	 */

	debug(FFL, "::BuildPackage");

	unsigned char *buf_out;											//Create pointer "buf", whitch will point to an unsigned char
	unsigned short chksum;
	unsigned int c, length;

	length = MSG_NUM_HEADER_BYTES + 1 + 2 + 2 + payload->length; 	//Length of the message (3 bytes header, 1 byte ID, 2 byte payload length, 2 byte checksum and payload length

	buf_out = (unsigned char*) malloc(length);						//Allocating memory and pointing "buf" to it.

	if ( !buf_out )													//If buf is not assigned, exit
	{
		*size = 0;
		return NULL;
	}

	// Assemble the message header:
	for( c=0; c<MSG_NUM_HEADER_BYTES; c++ )							// Inserting 0xAA at position 0, 1, 2
	{
		buf_out[c] = MSG_HEADER_BYTE;
	}

	buf_out[MSG_NUM_HEADER_BYTES] = payload->id; 					// Message ID at position 3
	buf_out[MSG_NUM_HEADER_BYTES + 1] = payload->length ; 			// Msg. length first byte at position 4
	buf_out[MSG_NUM_HEADER_BYTES + 2] = 0x00; 						// Msg. length second byte at position 5

	// Copy payload to buffer:
	if ( payload->length ) memcpy( &buf_out[ MSG_NUM_HEADER_BYTES + 3 ], payload->data, payload->length );

	// Add checksum to message:
	buf_out[ MSG_NUM_HEADER_BYTES + 3 + payload->length ] = 0x00;	//Checksum eval has been disabled on Griber, but must still be included. Therefore set to 0
	buf_out[ MSG_NUM_HEADER_BYTES + 4 + payload->length ] = 0x00;

	*size = length;													//set the size pointer equal to the length of the message.

	return(buf_out);
}

int WsgCommands::ExecuteCommand(payloadInfo * payload, unsigned char * buf_in, int length_in)
{
	/**
	 * This function executes a command.
	 * It simply calls first the SendMessage function, and then the ReceiveMessage function
	 */

	debug(FFL, "::ExecuteCommand");

	//set command ID in WsgReceiver class.
	myWsgReceiver->setCmdID(payload->id);

	int send_state, receive_state;

	send_state = SendMessage(payload);	//Send message

	if (send_state == 31)				//check if the send failed
	{
		return(31);						//error code 31: Error in ROS writing to wsg
	}

	receive_state = ReceiveMessage(buf_in, length_in, payload);	//receive a message.

	return(receive_state);
}

int WsgCommands::SendMessage(payloadInfo * payload)
{
	/**
	 * This function first calls the BuildMessage function and then sends the command.
	 * It will take the payloadInfo struct as an argument and will pass that on to the BuildPackage function.
	 */

	debug(FFL, "::SendMessage");

	int bytes_sent;
	unsigned int size;

	// Set a pointer equal to the build package, which will be stored in the memory
	unsigned char *buf_out = BuildPackage( payload, &size );

	//send the package to the gripper via UDP connection.
	bytes_sent = sendto(sockfd, buf_out, size,0,(struct sockaddr *)&out_addr, sizeof(out_addr));

	if (bytes_sent == size)		//if the number of sent bytes is equal to the size of the package, the send went well.
	{
		debug(FFL, "Message sent successfully");
		return 0;
	}
	else
	{
		error(FFL, "Sending command failed");
		return 31;
	}
}

int WsgCommands::ReceiveMessage(unsigned char * buf_in, int length_in, payloadInfo * payload)
{
	/**
	 * This function waits for the WsgReceiver-class to receive a package from the gripper
	 * and fill it into the ProtectedBuffer-class.
	 * Once a package is received in WsgReceiver that matches the cmdID of the requested command,
	 * the CmdID variable in WsgReceiver is set to 0.
	 */

	debug(FFL, "::ReceiveMessage");

	//wait for CmdID-variable in WsgReceiver-class to be set to zero (hence the data has been written to the ProtectedBuffer)
	while(myWsgReceiver->getCmdID() != 0)
	{
		usleep(10000);
	}

	unsigned char *temp_buf;

	int bytes = myProtectedBuffer->get(temp_buf);

	//Check if receive succeeded
	if (bytes == -1)
	{
		error(FFL, "No data received from gripper");
		return(32);
	}

	//get the error code
	int state = GetErrorCode(temp_buf);

	/**
	 * When receiving a command including movement the gripper will initially answer with a "command pending" error code.
	 * Once the movement is finished a result package will be returned from the gripper.
	 * This means, that a check for whether the received packet is a "command pending" or result package is needed.
	 * If a "command pending" error code is received, the code will receive again. This will make the system halt at the recvfrom function
	 * 		until a new packet is returned - in this case the result package.
	 *
	 * Please note:
	 * There is no need to check for command pending twice, since the command pending package is only sent once.
	 * The next package will be the result of the command.
	 */

	if (state == 26)
	{
		debug(FFL, "Received state 26 - Gripper in motion");

		//set command ID again in WsgReceiver class.
		myWsgReceiver->setCmdID(payload->id);

		delete temp_buf;	//delete the data pointed to by temp_buf (new data will be received/assigned instead)

		//wait for CmdID-variable in WsgReceiver-class to be set to zero (hence the data has been written to the ProtectedBuffer)
		while(myWsgReceiver->getCmdID() != 0)
		{
			usleep(10000);
		}

		bytes = myProtectedBuffer->get(temp_buf);

		if (bytes == -1)
		{
			error(FFL, "No data received from wsg");
			return(32);
		}
	}

	//get the error code
	int state2 = GetErrorCode(temp_buf);

	//pass the payload to the payload buffer (found by the argument pointer)
	for(int i = 0; i < length_in; i++)
	{
		buf_in[i] = temp_buf[i+8];
	}
	delete temp_buf;

	return(state2);

}

void WsgCommands::DecodeSysState(float sys_state, char * Bits)
{
	/**
	 * This function decodes the system state from a float.
	 * This function is needed since the GetSysState encodes the system state as a float.
	 * This is done to decrease memory consumption when sending the system state to the action client.
	 * This function is used when local functions need to access the system state, such as the init_check function.
	 */

	debug(FFL, "::DecodeSysState");

	UStuff a;

	a.f = sys_state;

	unsigned char mask = 1; // Bit mask

	// Extract the bits
	for (int i = 0; i < 8; i++) {
		// Mask each bit in the byte and store it
		Bits[i] = (a.c[0] >> i) & mask;
		Bits[i+8] = (a.c[1] >> i) & mask;
		Bits[i+16] = (a.c[2] >> i) & mask;
		Bits[i+24] = (a.c[3] >> i) & mask;
	}
}

void WsgCommands::PrintError(int error_code)
{
	/**
	 * This function is used to print an error code to the screen
	 */

	log_msg.str("");
	log_msg << "::PrintError <Error code: " << error_code << ">";
	debug(FFL, log_msg.str());

	switch(error_code)
	{
	case 0:
		info(FFL, "Error code 0 : No error occurred, operation was successful.");
		break;
	case 1:
		error(FFL, "Error code 1 : Function or data is not available.");
		break;
	case 2:
		error(FFL, "Error code 2 : No measurement converter is connected.");
		break;
	case 3:
		error(FFL, "Error code 3 : Device was not initialized.");
		break;
	case 4:
		error(FFL, "Error code 4 : The data acquisition is already running.");
		break;
	case 5:
		error(FFL, "Error code 5 : The requested feature is currently not available.");
		break;
	case 6:
		error(FFL, "Error code 6 : One or more parameters are inconsistent.");
		break;
	case 7:
		error(FFL, "Error code 7 : Timeout error.");
		break;
	case 8:
		error(FFL, "Error code 8 : Error while reading data.");
		break;
	case 9:
		error(FFL, "Error code 9 : Error while writing data.");
		break;
	case 10:
		error(FFL, "Error code 10 : No more memory available.");
		break;
	case 11:
		error(FFL, "Error code 11 : Checksum error.");
		break;
	case 12:
		error(FFL, "Error code 12 : A Parameter was given, but none expected.");
		break;
	case 13:
		error(FFL, "Error code 13 : Not enough parameters for executing the command.");
		break;
	case 14:
		error(FFL, "Error code 14 : Unknown command.");
		break;
	case 15:
		error(FFL, "Error code 15 : Command format error.");
		break;
	case 16:
		error(FFL, "Error code 16 : Access denied.");
		break;
	case 17:
		error(FFL, "Error code 17 : Interface is already open.");
		break;
	case 18:
		error(FFL, "Error code 18 : Error while executing a command.");
		break;
	case 19:
		warn(FFL, "Error code 19 : Command execution was aborted by the user.");
		break;
	case 20:
		error(FFL, "Error code 20 : Invalid handle.");
		break;
	case 21:
		error(FFL, "Error code 21 : Device or file not found.");
		break;
	case 22:
		error(FFL, "Error code 22 : Device or file not open.");
		break;
	case 23:
		error(FFL, "Error code 23 : Input/Output Error.");
		break;
	case 24:
		error(FFL, "Error code 24 : Wrong parameter.");
		break;
	case 25:
		error(FFL, "Error code 25 : Index out of bounds.");
		break;
	case 26:
		info(FFL, "Error code 26 : No error, but the command was not completed, yet. Another return message will follow including an error code, if the function was completed.");
		break;
	case 27:
		error(FFL, "Error code 27 : Data overrun.");
		break;
	case 28:
		error(FFL, "Error code 28 : Range error.");
		break;
	case 29:
		error(FFL, "Error code 29 : Axis blocked.");
		break;
	case 30:
		error(FFL, "Error code 30 : File already exists.");
		break;
	case 31:
		error(FFL, "Error code 31 : Error writing to WSG from ROS.");
		break;
	case 32:
		error(FFL, "Error code 32 : Error reading from WSG to ROS.");
		break;

	default:
		log_msg.str("");
		log_msg << "Error code " << error_code << " : Unknown error code.";
		error(FFL, log_msg.str());
		break;
	}
}
