/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This file demonstrates how to use the WsgClient class and essentially the wsg_command_node ROS node.
 * This file contains only a selection of the functions available.
 * Further documentation on each function is found in the WsgClient class
 * Generally documentation is added in the relevant files.
 */

#include "ros/ros.h"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <actionlib/client/simple_action_client.h>
#include <wsg_communication/WsgData.hpp>
#include <wsg_communication/WsgClient.hpp>
#include <math.h>
#include <fcntl.h>
#include <linux/kd.h>
#include <sys/ioctl.h>
#include <logger_sys/logger_sys.hpp>

using namespace std;

WsgClient* myWsgClient;

float step;		//float to step through the program

//Main program
int main(int argc, char *argv[])
{
	ros::init(argc, argv, "test_wsg_node");

	ros::NodeHandle n;

	InitLogger("Test_WSG",true,true,true);

	//declare the object of the WsgClient class
	myWsgClient = WsgClient::GetInstance();

	//calling init initially will check if the gripper is online and initialized, if not, initialize it.
	//if the gripper is not initialized it cannot be operated
	//every function of the WsgClient class will return a state, 0 = success.
	int state = myWsgClient->Init();
	if (state != 0)
	{
		cout << "Failed to initialize the gripper" << endl;
		return 0;
	}

	cout << endl << "type a number and press enter to proceed..." << endl << endl;
	cin >> step;

	//get the acceleration on the gripper
	cout << "getting the acceleration from the gripper" << endl;
	float acc;
	myWsgClient->GetAcc(&acc);
	cout << "The acceleration is: " << acc << endl;

	cout << endl << "type a number and press enter to proceed..." << endl << endl;
	cin >>step;

	//now set the acceleration
	cout << "the acceleration will be set to 600" << endl;
	myWsgClient->SetAcc(600);

	cout << endl << "type a number and press enter to proceed..." << endl << endl;
	cin >> step;

	//move the gripper to a location
	cout << "moving to 20 mm width with speed: 200 mm/s" << endl;
	myWsgClient->Move(20,200);			//20 mm width, 200 mm/s speed

	cout << endl << "type a number and press enter to proceed..." << endl << endl;
	cin >> step;

	//Get the system state and print bit flag 1:
	cout << "getting the system state and printing the first bit flag" << endl;
	bool* sys_state;
	sys_state = (bool*)malloc(32);
	myWsgClient->GetSysState(sys_state);
	cout << "bit flag 1 - The fingers are referenced (initalized) " << sys_state[0] << endl;

	cout << endl << "type a number and press enter to proceed..." << endl << endl;
	cin >> step;

	//print the entire system state (only bit flags = 1 (error in most case)
	cout << "getting and printing the system state" << endl;
	myWsgClient->PrintSysState();

	/**
	 * For further information please refer to the documentation in the WsgClient class
	 * Or contact VT4 AAU
	 * Please note, this software is a beta version, no guarantees are given.
	 */

	return 0;
}

