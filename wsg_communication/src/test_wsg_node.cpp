/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe, Casper Schou     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This file demonstrates how to use the WsgClient class and essentially the wsg_command_node ROS node.
 * This file contains only a selection of the functions available.
 * Further documentation on each function is found in the WsgClient class
 * Generally documentation is added in the relevant files.
 */

#include "ros/ros.h"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <actionlib/client/simple_action_client.h>
#include <wsg_communication/WsgData.hpp>
#include <wsg_communication/WsgClient.hpp>
#include <math.h>
#include <fcntl.h>
#include <linux/kd.h>
#include <sys/ioctl.h>
#include <logger_sys/logger_sys.hpp>

using namespace std;

WsgClient* myWsgClient;

void move()
{
    float width, speed;

    cout << "Please enter width (0 - 110)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Width: ";
    cin >> width;
    cout << endl << endl;
    if (width < 0) width = 0, cout << "Width out of range. Range is 0 - 110. Width set to 0." << endl << endl;
    if (width > 110) width = 110, cout << "Width out of range. Range is 0 - 110. Width set to 110." << endl << endl;

    cout << "Please enter speed (5 - 420)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Speed: ";
    cin >> speed;
    cout << endl << endl;
    if (speed < 5) speed = 5, cout << "Speed out of range. Range is 5 - 420. Speed set to 5." << endl << endl;
    if (speed > 420) speed = 420, cout << "Speed out of range. Range is 5 - 420. Speed set to 420." << endl << endl;

    myWsgClient->Move(width, speed);
}

void grasp()
{
    float width, speed;

    cout << "Please enter width (0 - 110)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Width: ";
    cin >> width;
    cout << endl << endl;
    if (width < 0) width = 0, cout << "Width out of range. Range is 0 - 110. Width set to 0." << endl << endl;
    if (width > 110) width = 110, cout << "Width out of range. Range is 0 - 110. Width set to 110." << endl << endl;

    cout << "Please enter speed (5 - 420)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Speed: ";
    cin >> speed;
    cout << endl << endl;
    if (speed < 5) speed = 5, cout << "Speed out of range. Range is 5 - 420. Speed set to 5." << endl << endl;
    if (speed > 420) speed = 420, cout << "Speed out of range. Range is 5 - 420. Speed set to 420." << endl << endl;

    myWsgClient->Grasp(width, speed);
}

void release()
{
    float width, speed;

    cout << "Please enter width (0 - 110)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Width: ";
    cin >> width;
    cout << endl << endl;
    if (width < 0) width = 0, cout << "Width out of range. Range is 0 - 110. Width set to 0." << endl << endl;
    if (width > 110) width = 110, cout << "Width out of range. Range is 0 - 110. Width set to 110." << endl << endl;

    cout << "Please enter speed (5 - 420)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Speed: ";
    cin >> speed;
    cout << endl << endl;
    if (speed < 5) speed = 5, cout << "Speed out of range. Range is 5 - 420. Speed set to 5." << endl << endl;
    if (speed > 420) speed = 420, cout << "Speed out of range. Range is 5 - 420. Speed set to 420." << endl << endl;

    myWsgClient->Release(width, speed);
}

void setAcc()
{
    float acc;

    cout << "Please enter acceleration (100 - 5000)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Acceleration: ";
    cin >> acc;
    cout << endl << endl;
    if (acc < 100) acc = 100, cout << "Acceleration out of range. Range is 100 - 5000. Acceleration set to 100." << endl << endl;
    if (acc > 5000) acc = 5000, cout << "Acceleration out of range. Range is 100 - 5000. Acceleration set to 5000." << endl << endl;

    myWsgClient->SetAcc(acc);
}

void getAcc()
{
    float acc;

    myWsgClient->GetAcc(&acc);

    cout << "Acceleration returned from WSG: " << acc << endl << endl;
}

void setForceLimit()
{
    float force;

    cout << "Please enter force limit (5 - 80)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Force: ";
    cin >> force;
    cout << endl << endl;
    if (force < 5) force = 5, cout << "Force out of range. Range is 5 - 80. Force set to 5." << endl << endl;
    if (force > 80) force = 80, cout << "Force out of range. Range is 5 - 80. Force set to 80." << endl << endl;

    myWsgClient->SetForceLimit(force);
}

void getForceLimit()
{
    float force;

    myWsgClient->GetForceLimit(&force);

    cout << "Force limit returned from WSG: " << force << endl << endl;
}

void setLimits()
{
    float minus, plus;

    cout << "Please enter lower software limit (0 - 110)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Lower limit: ";
    cin >> minus;
    cout << endl << endl;
    if (minus < 0) minus = 0, cout << "Limit out of range. Range is 0 - 110. Limit set to 0." << endl << endl;
    if (minus > 110) minus = 110, cout << "Limit out of range. Range is 0 - 110. Limit set to 110." << endl << endl;

    cout << "Please enter upper software limit (0 - 110)" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Upper limit: ";
    cin >> plus;
    cout << endl << endl;
    if (plus < 0) plus = 0, cout << "Limit out of range. Range is 0 - 110. Limit set to 0." << endl << endl;
    if (plus > 110) plus = 110, cout << "Limit out of range. Range is 0 - 110. Limit set to 110." << endl << endl;

    if (plus < minus)
    {
        cout << "Inadmissible limits, upper limit smaller than lower limit." << endl;
    }

    myWsgClient->SetLimits(minus, plus);
}

void getLimits()
{
    float minus, plus;

    myWsgClient->GetLimits(&minus, &plus);

    cout << "Software limits returned from WSG - Lower: " << minus << " Upper: " << plus << endl << endl;
}

void getGraspState()
{
    wsg_definitions::graspState grasp_state;

    myWsgClient->GetGraspState(&grasp_state);

    cout << "Grasp state returned from gripper: ";

    switch(grasp_state)
    {
    case wsg_definitions::IDLE:
        cout << "IDLE";
        break;
    case wsg_definitions::GRASPING:
        cout << "GRASPING";
        break;
    case wsg_definitions::NO_PART_FOUND:
        cout << "NO_PART_FOUND";
        break;
    case wsg_definitions::PART_LOST:
        cout << "PART_LOST";
        break;
    case wsg_definitions::HOLDING:
        cout << "HOLDING";
        break;
    case wsg_definitions::RELEASING:
        cout << "RELEASING";
        break;
    case wsg_definitions::POSITIONING:
        cout << "POSITIONING";
        break;
    case wsg_definitions::UNDEFINED:
        cout << "UNDEFINED";
        break;
    }

    cout << endl << endl;
}

void getGraspStatis()
{
    int total, no_part, lost_part;

    myWsgClient->GetGraspStatis(&total, &no_part, &lost_part);

    cout << "Grasping statistics returned from WSG - Total: " << total << " No part: " << no_part << " Lost part: " << lost_part << endl << endl;
}

void getWidth()
{
    float width;

    myWsgClient->GetWidth(&width);

    cout << "Width returned from WSG: " << width << endl << endl;
}

void getSpeed()
{
    float speed;

    myWsgClient->GetSpeed(&speed);

    cout << "Speed returned from WSG: " << speed << endl << endl;
}

void getForce()
{
    float force;

    myWsgClient->GetForce(&force);

    cout << "Force returned from WSG: " << force << endl << endl;
}

void autoWidth()
{
    int frequency, mode;

    cout << "Please enter frequency of publishing [hz]" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "frequency: ";
    cin >> frequency;
    cout << endl << endl;

    cout << "Please select mode" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     Continuous update       Fixed frequency update" << endl;
    cout << "  1     Update on change only   Update on change in width only" << endl << endl;
    cout << "Mode: ";
    cin >> mode;
    cout << endl << endl;
    if (mode > 1 || mode < 0) mode = 0, cout << "Mode out of range. Mode set to 0 - continuous update." << endl << endl;

    myWsgClient->AutoWidth(mode, frequency);
}

void autoSpeed()
{
    int frequency, mode;

    cout << "Please enter frequency of publishing [hz]" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "frequency: ";
    cin >> frequency;
    cout << endl << endl;

    cout << "Please select mode" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     Continuous update       Fixed frequency update" << endl;
    cout << "  1     Update on change only   Update on change in width only" << endl << endl;
    cout << "Mode: ";
    cin >> mode;
    cout << endl << endl;
    if (mode > 1 || mode < 0) mode = 0, cout << "Mode out of range. Mode set to 0 - continuous update." << endl << endl;

    myWsgClient->AutoSpeed(mode, frequency);
}

void autoForce()
{
    int frequency, mode;

    cout << "Please enter frequency of publishing [hz]" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "frequency: ";
    cin >> frequency;
    cout << endl << endl;

    cout << "Please select mode" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     Continuous update       Fixed frequency update" << endl;
    cout << "  1     Update on change only   Update on change in width only" << endl << endl;
    cout << "Mode: ";
    cin >> mode;
    cout << endl << endl;
    if (mode > 1 || mode < 0) mode = 0, cout << "Mode out of range. Mode set to 0 - continuous update." << endl << endl;

    myWsgClient->AutoForce(mode, frequency);
}

void autoGraspState()
{
    int frequency, mode;

    cout << "Please enter frequency of publishing [hz]" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "frequency: ";
    cin >> frequency;
    cout << endl << endl;

    cout << "Please select mode" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     Continuous update       Fixed frequency update" << endl;
    cout << "  1     Update on change only   Update on change in width only" << endl << endl;
    cout << "Mode: ";
    cin >> mode;
    cout << endl << endl;
    if (mode > 1 || mode < 0) mode = 0, cout << "Mode out of range. Mode set to 0 - continuous update." << endl << endl;

    myWsgClient->AutoGraspState(mode, frequency);
}

void autoSysState()
{
    int frequency, mode;

    cout << "Please enter frequency of publishing [hz]" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "frequency: ";
    cin >> frequency;
    cout << endl << endl;

    cout << "Please select mode" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     Continuous update       Fixed frequency update" << endl;
    cout << "  1     Update on change only   Update on change in width only" << endl << endl;
    cout << "Mode: ";
    cin >> mode;
    cout << endl << endl;
    if (mode > 1 || mode < 0) mode = 0, cout << "Mode out of range. Mode set to 0 - continuous update." << endl << endl;

    myWsgClient->AutoSysState(mode, frequency);
}

void startPublishers()
{
    int frequency, mode;

    cout << "Please enter frequency of publishing [hz]" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "frequency: ";
    cin >> frequency;
    cout << endl << endl;

    cout << "Please select mode" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     Continuous update       Fixed frequency update" << endl;
    cout << "  1     Update on change only   Update on change in width only" << endl << endl;
    cout << "Mode: ";
    cin >> mode;
    cout << endl << endl;
    if (mode > 1 || mode < 0) mode = 0, cout << "Mode out of range. Mode set to 0 - continuous update." << endl << endl;

    int width, speed, force, grasp, sys;
    bool wi = false, sp = false, fo = false, gr = false, sy = false;

    cout << "Start width publisher?" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     yes" << endl;
    cout << "  1     no" << endl << endl;
    cout << "Select: ";
    cin >> width;
    cout << endl << endl;
    if (width > 1 || width < 0) width = 0, cout << "Width out of range. Yes chosen." << endl << endl;
    if(width == 0) wi = true;

    cout << "Start speed publisher?" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     yes" << endl;
    cout << "  1     no" << endl << endl;
    cout << "Select: ";
    cin >> speed;
    cout << endl << endl;
    if (speed > 1 || speed < 0) speed = 0, cout << "Speed out of range. Yes chosen." << endl << endl;
    if(speed == 0) sp = true;

    cout << "Start force publisher?" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     yes" << endl;
    cout << "  1     no" << endl << endl;
    cout << "Select: ";
    cin >> force;
    cout << endl << endl;
    if (force > 1 || force < 0) force = 0, cout << "Force out of range. Yes chosen." << endl << endl;
    if(force == 0) fo = true;

    cout << "Start grasp state publisher?" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     yes" << endl;
    cout << "  1     no" << endl << endl;
    cout << "Select: ";
    cin >> grasp;
    cout << endl << endl;
    if (grasp > 1 || grasp < 0) grasp = 0, cout << "Grasp state out of range. Yes chosen." << endl << endl;
    if(grasp == 0) gr = true;

    cout << "Start system state publisher?" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0     yes" << endl;
    cout << "  1     no" << endl << endl;
    cout << "Select: ";
    cin >> sys;
    cout << endl << endl;
    if (sys > 1 || sys < 0) sys = 0, cout << "System state out of range. Yes chosen." << endl << endl;
    if(sys == 0) sy = true;

    myWsgClient->StartAutoPublishers(mode, frequency, wi, sp, fo, gr, sy);
}

void quickMoveMenu()
{
    float width = 0;
    float speed = 100;
    myWsgClient->SetForceLimit(80);

    int choice = -1;

    while(choice != 0)
    {

        cout << "Quick move menu - please select a function and press enter" << endl;
        cout << "*************************************************" << endl << endl;
        cout << "Value:	 Name:	              Description:     " << endl;
        cout << "-------------------------------------------------" << endl;
        cout << "  0    Exit                  Return to main menu" << endl << endl;
        cout << "  1    Open                  Open gripper" << endl;
        cout << "  2    Close                 Close gripper" << endl;
        cout << "  3    Move                  Move fingers to width" << endl;
        cout << "  4    Grasp                 Grasp object of width" << endl;
        cout << "  5    Release outwards      Release object by opening the jaws 15 mm" << endl;
        cout << "  6    Release inwards       Release object by closing the jaws 15 mm" << endl;
        cout << "  7    Release to full       Release object and open fully" << endl;
        cout << "  8    SetSpeed              Set speed to 20 (slow) (default is 100)" << endl;
        cout << "  9    SetSpeed              Set speed to 100 (medium)( default is 100)" << endl;
        cout << "  10   SetSpeed              Set speed to 300 (fast) (default is 100)" << endl;
        cout << "  11   SetForce              SetForce to 20 (low) (default is 80)" << endl;
        cout << "  12   SetForce              SetForce to 50 (medium) (default is 80)" << endl;
        cout << "  13   SetForce              SetForce to 80 (max) (default is 80)" << endl << endl;

        cout << "Please select: ";
        cin >> choice;
        cout << endl << endl;


        switch(choice)
        {
        case 0:
            return;
            break;
        case 1:
            myWsgClient->Move(110,speed);
            break;
        case 2:
            myWsgClient->Move(0,speed);
            break;
        case 3:
            cout << "Please enter width (0 - 110)" << endl;
            cout << "-------------------------------------------------" << endl;
            cout << "Width: ";
            cin >> width;
            cout << endl << endl;
            if (width < 0) width = 0, cout << "Width out of range. Range is 0 - 110. Width set to 0." << endl << endl;
            if (width > 110) width = 110, cout << "Width out of range. Range is 0 - 110. Width set to 110." << endl << endl;
            myWsgClient->Move(width, speed);
            break;
        case 4:
            cout << "Please enter width (0 - 110)" << endl;
            cout << "-------------------------------------------------" << endl;
            cout << "Width: ";
            cin >> width;
            cout << endl << endl;
            if (width < 0) width = 0, cout << "Width out of range. Range is 0 - 110. Width set to 0." << endl << endl;
            if (width > 110) width = 110, cout << "Width out of range. Range is 0 - 110. Width set to 110." << endl << endl;
            myWsgClient->Grasp(width, speed);
            break;
        case 5:
            myWsgClient->GetWidth(&width);
            myWsgClient->Release(width+15, speed);
            break;
        case 6:
            myWsgClient->GetWidth(&width);
            myWsgClient->Release(width-15, speed);
            break;
        case 7:
            myWsgClient->Release(110, speed);
            break;
        case 8:
            speed = 20;
            break;
        case 9:
            speed = 100;
            break;
        case 10:
            speed = 300;
            break;
        case 11:
            myWsgClient->SetForceLimit(20);
            break;
        case 12:
            myWsgClient->SetForceLimit(50);
            break;
        case 13:
            myWsgClient->SetForceLimit(80);
            break;
        default:
            cout << "Invalid choice!" << endl;
            break;
        }

    }
}

int mainMenu()
{
    int choice;

    cout << "Main Menu - please select a funciton and press enter" << endl;
    cout << "*************************************************" << endl << endl;
    cout << "Value:	 Name:	              Description:     " << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "  0    Exit                  Close program" << endl << endl;
    cout << "  1    InitCheck             Check if the gripper is initialised" << endl;
    cout << "  2    Init                  Initialise the gripper" << endl;
    cout << "  3    Loop                  Loop data to check connection" << endl;
    cout << "  4    Disconnect            Issue disconnect to gripper" << endl;
    cout << "  5    Home                  Home the gripper" << endl;
    cout << "  6    Move                  Move the jaws of the gripper" << endl;
    cout << "  7    Stop                  Stop a motion" << endl;
    cout << "  8    FastStop              Emergency stop" << endl;
    cout << "  9    AckStop               Acknowledge a stop" << endl ;
    cout << " 10    Grasp                 Grasp a part" << endl;
    cout << " 11    Release               Release a part" << endl;
    cout << " 12    SetAcc                Set acceleration"	<< endl;
    cout << " 13    GetAcc                Get acceleration" << endl;
    cout << " 14    SetForceLimit         Set force limit" << endl;
    cout << " 15    GetForceLimit         Get force limit" << endl;
    cout << " 16    SetLimits             Set software limits on width" << endl;
    cout << " 17    GetLimits             Get software limits on width" << endl;
    cout << " 18    ClearLimits           Clear software limits on width" << endl;
    cout << " 19    OverdriveOn           Activate overdrive mode" << endl;
    cout << " 20    OverdriveOff          Deactivate overdrive mode" << endl;
    cout << " 21    TareForce             Tare the force sensor" << endl;
    cout << " 22    GetSysState           Get system state" << endl;
    cout << " 23    GetGraspState         Get grasp state" << endl;
    cout << " 24    GetGraspStatis        Get grasping statistics" << endl;
    cout << " 25    ResetGraspStatis      Reset the grasping statistics" << endl;
    cout << " 26    GetWidth              Get current width" << endl;
    cout << " 27    GetSpeed              Get current speed" << endl;
    cout << " 28    GetForce              Get current force" << endl;
    cout << " 29    AutoWidth             Automatic publishing of width" << endl;
    cout << " 30    AutoSpeed             Automatic publishing of speed" << endl;
    cout << " 31    AutoForce             Automatic publishing of force" << endl;
    cout << " 32    AutoGraspState        Automatic publishing of Grasp state" << endl;
    cout << " 33    AutoSysState          Automatic publishing of System state" << endl;
    cout << " 34    StartAutoPublishers   Start multiple automatic publishers" << endl;
    cout << " 35    CancelAutoWidth       Stop automatic publisher" << endl;
    cout << " 36    CancelAutoSpeed       Stop automatic publisher" << endl;
    cout << " 37    CancelAutoForce       Stop automatic publisher" << endl;
    cout << " 38    CancelAutoGraspState  Stop automatic publisher" << endl;
    cout << " 39    CancelAutoSysState    Stop automatic publisher" << endl;
    cout << " 40    CancelAutoPublishers  Stop automatic publishers" << endl;
    cout << " 41    StartJSPublisher      Start joint state publisher" << endl;
    cout << " 42    StopJSPublisher       Stop joint state publisher" << endl;
    cout << " 43    Quick command menu    Enter quick move menu" << endl << endl;

    cout << "Please select: ";
    cin >> choice;
    cout << endl << endl;


    switch(choice)
    {
    case 0:
        return -1;
        break;
    case 1:
        myWsgClient->InitCheck();
        break;
    case 2:
        myWsgClient->Init();
        break;
    case 3:
        myWsgClient->Loop();
        break;
    case 4:
        myWsgClient->Disconnect();
        break;
    case 5:
        myWsgClient->Home();
        break;
    case 6:
        move();
        break;
    case 7:
        myWsgClient->Stop();
        break;
    case 8:
        myWsgClient->FastStop();
        break;
    case 9:
        myWsgClient->AckStop();
        break;
    case 10:
        grasp();
        break;
    case 11:
        release();
        break;
    case 12:
        setAcc();
        break;
    case 13:
        getAcc();
        break;
    case 14:
        setForceLimit();
        break;
    case 15:
        getForceLimit();
        break;
    case 16:
        setLimits();
        break;
    case 17:
        getLimits();
        break;
    case 18:
        myWsgClient->ClearLimits();
        break;
    case 19:
        myWsgClient->OverdriveOn();
        break;
    case 20:
        myWsgClient->OverdriveOff();
        break;
    case 21:
        myWsgClient->TareForce();
        break;
    case 22:
        myWsgClient->PrintSysState();
        break;
    case 23:
        getGraspState();
        break;
    case 24:
        getGraspStatis();
        break;
    case 25:
        myWsgClient->ResetGraspStatis();
        break;
    case 26:
        getWidth();
        break;
    case 27:
        getSpeed();
        break;
    case 28:
        getForce();
        break;
    case 29:
        autoWidth();
        break;
    case 30:
        autoSpeed();
        break;
    case 31:
        autoForce();
        break;
    case 32:
        autoGraspState();
        break;
    case 33:
        autoSysState();
        break;
    case 34:
        startPublishers();
        break;
    case 35:
        myWsgClient->CancelAutoWidth();
        break;
    case 36:
        myWsgClient->CancelAutoSpeed();
        break;
    case 37:
        myWsgClient->CancelAutoForce();
        break;
    case 38:
        myWsgClient->CancelAutoGraspState();
        break;
    case 39:
        myWsgClient->CancelAutoSysState();
        break;
    case 40:
        myWsgClient->CancelAutoPublishers();
        break;
    case 41:
        myWsgClient->StartJSPublisher();
        break;
    case 42:
        myWsgClient->StopJSPublisher();
        break;
    case 43:
        quickMoveMenu();
        break;
    default:
        cout << "Invalid function!" << endl << endl;
        break;

    }

    return 0;
}


//Main program
int main(int argc, char *argv[])
{
    ros::init(argc, argv, "test_wsg_node");

    ros::NodeHandle n;

    InitLogger("Test_WSG",true,true,true);

    //declare the object of the WsgClient class
    myWsgClient = WsgClient::GetInstance();

    //calling init initially will check if the gripper is online and initialized, if not, initialize it.
    //if the gripper is not initialized it cannot be operated
    //every function of the WsgClient class will return a state, 0 = success.
    int state = myWsgClient->Init();

    if (state != 0)
    {
        cout << "Failed to initialize the gripper" << endl;
        return 0;
    }

    while(state == 0)
    {
        state = mainMenu();
    }


    return 0;
}
