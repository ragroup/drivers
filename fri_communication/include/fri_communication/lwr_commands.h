/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe,                  *
 *  Oluf Skov Nielsen and Casper Schou                                     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This header file includes enumerators defining the different commands used 
 * in the communication with the manipulator.
 *
 * Furthermore this header file prototypes the functions used in the lwr_commands.cpp
 * These functions are used to execute commands on the manipulator. 
 * This is done by changing 16 integers, 16 floats and 16 bools in a data buffer
 * located in the friremote class. These data are passed to the KRC at the next data exchang.
 * On the KRC these data are used in the FRICommandServer.src program, which based on
 * a case structure executed the requested command. 
 */

#include "fri_communication/friComm.h"
#include "fri_communication/LwrData.h"



#ifndef LWR_COMMANDS_H_
#define LWR_COMMANDS_H_

//Namespace containing enummerators used in the communication to the manipulator
namespace krl_definitions {

enum KRLMotionType {
			PTP_JOINT = 1,
			PTP_REL_JOINT = 2,
			PTP_CART = 3,
			PTP_REL_CART = 4,
			LIN = 5,
			LIN_REL_BASE = 6,
			LIN_REL_TOOL = 7,
			PTP_CONTPATH = 8,
			LIN_XYZ = 10,
			CIRC = 9,
            PTP_XYZ = 11,
            LIN_REL_CONTVEL_TOOL = 12,
            LIN_REL_CONTVEL_BASE = 13,
            PTP_JOINT_CONT = 14

};

enum KRLStifnessStrategy {
			POSITION = 10,
			CARTESIAN_IMPEDANCE_TOOL = 20,
			CARTESIAN_IMPEDANCE_BASE = 25,
			JOINT_IMPEDANCE = 30,
			GRAVITY_COMPENSATION = 101
};

enum KRLActionID {
			SET_CONTROL_MODE = 1,
			MOVE = 2,
			SET_TOOL = 3,
			SET_BASE = 4,
            PAPAS = 5,
            SET_COMMAND_MODE = 7,
            LOAD_IDENT = 8
};

enum KRLPAPASType {
			DESIREDFORCE_X= 1,
			DESIREDFORCE_Y= 2,
			DESIREDFORCE_Z= 3,
			DESIREDFORCE_A= 4,
			DESIREDFORCE_B= 5,
			DESIREDFORCE_C= 6,
			DESIREDFORCE_START= 7,
			DESIREDFORCE_CLEAR= 8,
			INVALID=0
};

enum KRLCommandState {
			COMMUNICATION_ERROR = -2,
			COMMAND_ERROR = -1,
			NO_COMMAND = 0,
			COMMAND_EXECUTING = 1,
			COMMAND_SUCCESS = 2
};

enum KRLInterrupt {
			INTERRUPT_ON = 3,
			INTERRUPT_OFF = 0
};


} // end namespace krl_definitions

using namespace krl_definitions;

//Changes the control mode of the manipulator
void setControlMode(krl_definitions::KRLStifnessStrategy stiffness_strategy, const double* stiffness, const double* damping, tFriKrlData& msg);

//Activates or deactivates Command Mode
void commandMode(int activate, tFriKrlData& msg);

//Moves the manipulator
void move(krl_definitions::KRLMotionType motion_type, const double* position, const double* viaposition, double velocity_scale, double angle, double trig_by_contact_threshold, double vel_CP, double vel_ORI, int num_points, int num_send_points, int vel1, int vel2, int dist_tick_max, tFriKrlData& msg);

//Sets the tool data
void setTool(const double* transform, double mass, const double* cm,  tFriKrlData& msg);

//Interrupt the manipulator
void INTERRUPT(krl_definitions::KRLInterrupt interrupt_type, tFriKrlData& msg);

//Set a base frame of the manipulator
void setBase(const double* transform, tFriKrlData& msg);

//Executes a force-application on the manipulator
void setPAPAS(krl_definitions::KRLPAPASType papas_type, const int force_profile, const int start_now, double amplitude,
		double duration, double max_defletion, double max_velocity, double frequency, double phase, double force_offset,
		double rise_time, double fall_time, tFriKrlData& msg);

//Helper function to parse an int into the state of an action
krl_definitions::KRLCommandState parseKRLCommandState(const int& s);

//Helper function to parse an int into the force application type
krl_definitions::KRLPAPASType parseKRLPAPASType(const int& s);

void loadIdent(tFriKrlData& msg);

void SafetyLoop(const tFriMsrData& msr, tFriKrlData& msg);

extern bool firstSafetyLoop, activateSafetyLoop, safetyContact;

void cleartFriKrlData(tFriKrlData& msg);

void setAbekat();

void commitKRLData(tFriKrlData& msg);

#endif /* LWR_COMMANDS_H_ */
