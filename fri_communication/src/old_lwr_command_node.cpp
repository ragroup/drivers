/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe,                  *
 *  Oluf Skov Nielsen and Casper Schou                                     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * This file is the main file of the lwr_command_node ROS-node (executable)
 * 
 * This node is used to control the KUKA LWR manipulator. 
 *
 * This node includes six ROS Action Servers for requests on six functionality
 * groups. Upon receiving a ROS Action Goal from a client the appropriate 
 * callback function within this file is called.
 *
 * This file also implements a ROS Publisher to publish the data received from
 * the manipulator to a ROS Topic. This is done to automatically share the 
 * manipulator data the user software communicating with this node every time
 * new data is received. 
 */

#include <boost/thread.hpp>

#include <sstream>

#include "ros/ros.h"
#include <tf/tf.h>

#include <fri_communication/XYZABCStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Transform.h>
#include <fri_communication/LwrData.h>

#include "LinearMath/btTransform.h"


#include <actionlib/server/simple_action_server.h>
#include <fri_communication/SetControlModeAction.h>
#include <fri_communication/MoveArmAction.h>
#include <fri_communication/SetToolAction.h>
#include <fri_communication/SetBaseAction.h>
#include <fri_communication/SetPAPASAction.h>
#include <fri_communication/SetSafetyAction.h>
#include "fri_communication/friremote.h"
#include "fri_communication/lwr_commands.h"
#include <logger_sys/logger_sys.hpp>


#ifndef M_PI
#define M_PI 3.141592653589793238462
#endif

//Transformation coefficients
const double RAD2DEG = 180/M_PI;
const double DEG2RAD = 1.0/RAD2DEG;

using namespace std;

//A sequence number to inform when a new action is sent to the KRC
int actionSequenceNr = 0;

vector<string> jointNames;
int dofs = 7;
// The communication frequency should match the frequency the KRC is set up to communicate in,
//Default is 50hz = 20ms between each packet
const double communicationFrequency = 255;
const double publisherFrequency = 25;

friRemote* fc;

//A mutex is used to assure that the data that are published is consistent
boost::mutex mymutex;

//Declaration of publisher
ros::Publisher pubLwrData;
ros::Publisher pubJS;

ros::Time lastMessageTime;

//Initiation of command state
krl_definitions::KRLCommandState current_command_state = krl_definitions::NO_COMMAND;

//Declaration of ROS Action Servers, one for each function group
actionlib::SimpleActionServer<fri_communication::SetControlModeAction>* setControlModeServer;
actionlib::SimpleActionServer<fri_communication::MoveArmAction>* moveServer;
actionlib::SimpleActionServer<fri_communication::SetToolAction>* setToolServer;
actionlib::SimpleActionServer<fri_communication::SetBaseAction>* setBaseServer;
actionlib::SimpleActionServer<fri_communication::SetPAPASAction>* setPAPASServer;
actionlib::SimpleActionServer<fri_communication::MoveArmAction>* setGripperServer;
actionlib::SimpleActionServer<fri_communication::MoveArmAction>* setInterruptServer;
actionlib::SimpleActionServer<fri_communication::SetSafetyAction>* setSafetyServer;

//Helper function that inserts a geometry_msgs::Transform into the FRI datagram.
//The index variable is updated to index the next free element in the array
//It also translates from millimeters and radians on the external device side to meters and degrees on the KRC (note that values received through fri actually are in millimeters and radians...)
double* transformToKRLReal(const geometry_msgs::Transform& transform){
	log_msg.str("");
	log_msg << "::transformToKRLReal: Transform translation x " << transform.translation.x*1000 << " y " << transform.translation.y*1000 << " z " << transform.translation.z*1000 << " Transform rotation x " << transform.rotation.x << " y " << transform.rotation.y << " z " << transform.rotation.z << " w " << transform.rotation.w;
	debug(FFL, log_msg.str());

	//A KRL E6pos is x,y,z,eulerZYX(rz,ry,rz) in moving directions in meters and degrees
	double* e6pos = new double[6];

	e6pos[0] = transform.translation.x*1000;
	e6pos[1] = transform.translation.y*1000;
	e6pos[2] = transform.translation.z*1000;

	//To get the rotation correctly
	btMatrix3x3 R(tf::Quaternion(transform.rotation.x,transform.rotation.y,transform.rotation.z,transform.rotation.w));
	btScalar yaw, pitch, roll;
	R.getEulerYPR(yaw, pitch, roll);
	e6pos[3] = yaw*RAD2DEG;
	e6pos[4] = pitch*RAD2DEG;
	e6pos[5] = roll*RAD2DEG;

	return e6pos;
}

//Helper function that inserts a geometry_msgs::Wrench into the FRI datagram.
//The index variable is updated to index the next free element in the array
double* xyzabcInToKRLReal(const fri_communication::XYZABC& frame){
	log_msg.str("");
	log_msg << "::xyzabcInToKRLReal: Frame x " << frame.x << " y " << frame.y << " z " << frame.z << " a " << frame.A << " b " << frame.B << " c " << frame.C;
	debug(FFL, log_msg.str());

	//A KRL E6pos is x,y,z,eulerZYX(rz,ry,rz) in moving directions
	double* e6pos = new double[6];
	e6pos[0] = frame.x;
	e6pos[1] = frame.y;
	e6pos[2] = frame.z;

	//TODO Assert this!!! To get the rotation correctly (switch x and z)
	e6pos[3] = frame.A;
	e6pos[4] = frame.B;
	e6pos[5] = frame.C;

	return e6pos;
}

double* jointStateToKRLReal(const sensor_msgs::JointState& joint_state){
	log_msg.str("");
	log_msg << "::jointStateToKRLReal: Joint state a1 " << joint_state.position[0] << " a2 " << joint_state.position[1] << " e1 " << joint_state.position[3] << " a3 " << joint_state.position[4] << " a4 " << joint_state.position[5] << " a5 " << joint_state.position[6] << " a6 " << joint_state.position[7];
	debug(FFL, log_msg.str());

	//Switch form radians to degrees here
	double* e6axis = new double[7];
	for(int i=0; i<7; i++){
		e6axis[i] = joint_state.position[i]*RAD2DEG;
		//For joint a2, add 90 degrees in positive direction to comply with different joint offsets of FRI and the KRC
		if(i==1){
			e6axis[i]+=90;
		}
		//cout << e6axis[i] << " ";
	}

	return e6axis;
}

double* jointStateToKRL(const sensor_msgs::JointState& joint_state){
	log_msg.str("");
	log_msg << "::jointStateToKRL: Joint state a1 " << joint_state.position[0] << " a2 " << joint_state.position[1] << " e1 " << joint_state.position[3] << " a3 " << joint_state.position[4] << " a4 " << joint_state.position[5] << " a5 " << joint_state.position[6] << " a6 " << joint_state.position[7];
	debug(FFL, log_msg.str());

	//Switch form radians to degrees here
	double* e6axis = new double[7];
	for(int i=0; i<7; i++){
		e6axis[i] = joint_state.position[i];

	}

	return e6axis;
}

void krlFrame2krlVec(const float* m, float* v){

	tf::Transform krcTfri;
	krcTfri.getBasis().setEulerYPR(0,0,0); // Change from krcTfri.getBasis().setEulerYPR(0,M_PI/2,0);
	tf::Transform friTtcp(btMatrix3x3(m[0],m[1],m[2],
			m[4],m[5],m[6],
			m[8],m[9],m[10]),
			btVector3(m[3],m[7],m[11]));

	tf::Transform krcTtcp=krcTfri*friTtcp;
	/*for(int i=0; i<3; i++){
		for(int j=0; j<4; j++){
			cout << m[(i)*4+j] << " ";
		}
		cout << endl;
	}*/
	v[0]=krcTtcp.getOrigin().getX();
	v[1]=krcTtcp.getOrigin().getY();
	v[2]=krcTtcp.getOrigin().getZ();
	btScalar a,b,c;
	krcTtcp.getBasis().getEulerYPR(a,b,c);
	v[3]=a;
	v[4]=b;
	v[5]=c;
}

//Helper function that detects using the "getFrmKRLInt"values when an action has finished on the robot (polling)
krl_definitions::KRLCommandState waitForCommandFinishedInKRC(){
	debug(FFL, "::krlFrame2krlVec::waitForCommandFinishedInKRC");
	// wait for date received
	ros::Rate r(communicationFrequency);
	bool dataReceived = false;
	log_msg.str("");
	log_msg << "fc->getCmdBuf().krl.intData[0]:  " << fc->getCmdBuf().krl.intData[0] << "fc->getFrmKRLInt(0): " << fc->getFrmKRLInt(0);
	info(FFL, log_msg.str());

	while (ros::ok() && !dataReceived)
	{
		dataReceived = (fc->getCmdBuf().krl.intData[0] == fc->getFrmKRLInt(0));
		//cout << "actionSequenceNr: " << actionSequenceNr << "fc->getFrmKRLInt(0): " << fc->getFrmKRLInt(0) << endl;
		if(dataReceived)
			continue;

		//if the server is restarted
		if(fc->getFrmKRLInt(0) == 0){
			return krl_definitions::NO_COMMAND;
		}

		r.sleep();
	}

	log_msg.str("");
	log_msg << "Data received in KRC with seqnr: " << fc->getFrmKRLInt(0);
	info(FFL, log_msg.str());

	//wait for finished
	while (ros::ok() && parseKRLCommandState(fc->getFrmKRLInt(2)) == krl_definitions::COMMAND_EXECUTING)
	{
		r.sleep();
	}
	//return the end state (which the KRC enters after krl_definitions::ACTION_EXECUTING)
	log_msg.str("");
	log_msg << "Action state when returning from waitForActionFinishedInKRC: " << fc->getFrmKRLInt(2);
	info(FFL, log_msg.str());
	return parseKRLCommandState(fc->getFrmKRLInt(2));
}

//Callback function for the SetControlMode, sets the $stiffness variable on the KRC
void executeSetControlModeCB(const fri_communication::SetControlModeGoalConstPtr& goal){
	debug(FFL, "::executeSetControlModeCB");
	mymutex.lock();

	double* stiffness = new double[0];
	double* damping = new double[0];

	krl_definitions::KRLStifnessStrategy strategy;

	switch (goal->KRLStifnessStrategy) {
	case krl_definitions::POSITION:
		strategy = krl_definitions::POSITION;
		break;
	case krl_definitions::CARTESIAN_IMPEDANCE_TOOL:
		stiffness = xyzabcInToKRLReal(goal->CPStiffness);
		damping = xyzabcInToKRLReal(goal->CPDamping);
		strategy = krl_definitions::CARTESIAN_IMPEDANCE_TOOL;
		break;
	case krl_definitions::CARTESIAN_IMPEDANCE_BASE:
		stiffness = xyzabcInToKRLReal(goal->CPStiffness);
		damping = xyzabcInToKRLReal(goal->CPDamping);
		strategy = krl_definitions::CARTESIAN_IMPEDANCE_BASE;
		break;
	case krl_definitions::JOINT_IMPEDANCE:
		stiffness = jointStateToKRL(goal->axisStiffness);
		damping = jointStateToKRL(goal->axisDamping);
		strategy = krl_definitions::JOINT_IMPEDANCE;
		break;
	case krl_definitions::GRAVITY_COMPENSATION:
		strategy = krl_definitions::GRAVITY_COMPENSATION;
		break;
	default:
		log_msg.str("");
		log_msg << "Invalid krlDefinitions::StiffnessStrategy: " << goal->KRLStifnessStrategy;
		error(FFL, log_msg.str());
		setControlModeServer->setAborted();
		return;
	}

	setControlMode(strategy, stiffness, damping, fc->getCmdBuf().krl);

	mymutex.unlock();

	switch (strategy) {
	case krl_definitions::POSITION:
		info(FFL, "Switching control mode to position control");
		break;
	case krl_definitions::CARTESIAN_IMPEDANCE_TOOL:
		log_msg.str("");
		log_msg << "Switching control mode to cartesian impedance in TOOL with stiffness x: " << stiffness[0] << " y " << stiffness[1] << " z " << stiffness[2] << " a " << stiffness[3] << " b " << stiffness[4] << " c " << stiffness[5] << " Damping x: " << damping[0] << " y " << damping[1] << " z " << damping[2] << " a " << damping[3] << " b " << damping[4] << " c " << damping[5];
		info(FFL, log_msg.str());
		break;
	case krl_definitions::CARTESIAN_IMPEDANCE_BASE:
		log_msg.str("");
		log_msg << "Switching control mode to cartesian impedance in BASE with stiffness x: " << stiffness[0] << " y " << stiffness[1] << " z " << stiffness[2] << " a " << stiffness[3] << " b " << stiffness[4] << " c " << stiffness[5] << " Damping x: " << damping[0] << " y " << damping[1] << " z " << damping[2] << " a " << damping[3] << " b " << damping[4] << " c " << damping[5];
		info(FFL, log_msg.str());
		break;
	case krl_definitions::JOINT_IMPEDANCE:
		ROS_INFO("Switching control mode to joint impedance  with stiffness (a1 a2 e1 a3 a4 a5 a6) (%f,%f,%f,%f,%f,%f,%f) and damping (a1 a2 e1 a3 a4 a5 a6) (%f,%f,%f,%f,%f,%f,%f)",
				stiffness[0],stiffness[1],stiffness[2],stiffness[3],stiffness[4],stiffness[5],stiffness[6], damping[0], damping[1], damping[2], damping[3], damping[4], damping[5], damping[6]);
		log_msg.str("");
		log_msg << "Switching control mode to joint impedance with stiffness a1: " << stiffness[0] << " a2 " << stiffness[1] << " e1 " << stiffness[2] << " a3 " << stiffness[3] << " a4 " << stiffness[4] << " a5 " << stiffness[5] << " a6 " << stiffness[6] << " Damping a1: " << damping[0] << " a2 " << damping[1] << " e1 " << damping[2] << " a3 " << damping[3] << " a4 " << damping[4] << " a5 " << damping[5] << " a6 " << damping[6];
		info(FFL, log_msg.str());
		break;
	case krl_definitions::GRAVITY_COMPENSATION:
		info(FFL, "Switching control mode to gravity compensation ");
		break;
	}

	krl_definitions::KRLCommandState command_state = waitForCommandFinishedInKRC();

	if(command_state==krl_definitions::COMMAND_SUCCESS){
		setControlModeServer->setSucceeded();
	} else {
		log_msg.str("");
		log_msg << "Action finished in krc with krlDefinitions::CommandState: " << command_state;
		error(FFL, log_msg.str());
		setControlModeServer->setAborted();
	}
}

//Callback function for the SetControlMode, sets the $stiffness variable on the KRC
void executeMoveArmCB(const fri_communication::MoveArmGoalConstPtr& goal){
	debug(FFL, "::executeMoveArmCB");

	mymutex.lock();

	//fri_communication::MoveArmResult myResult;

	double* position = new double[6];
	double* viaposition = new double [6];
	double anglecirc;

	krl_definitions::KRLMotionType motion_type;

	switch (goal->KRLMotionType) {
	case krl_definitions::PTP_JOINT:
		motion_type = krl_definitions::PTP_JOINT;
		position = jointStateToKRLReal(goal->joint_state);
		break;
	case krl_definitions::PTP_REL_JOINT:
		motion_type = krl_definitions::PTP_REL_JOINT;
		position = jointStateToKRLReal(goal->joint_state);
		break;
	case krl_definitions::PTP_CART:
		motion_type = krl_definitions::PTP_CART;
		position = transformToKRLReal(goal->transform);
		break;
	case krl_definitions::PTP_XYZ:
		motion_type = krl_definitions::PTP_CART;
		position = xyzabcInToKRLReal(goal->frame);
		break;
	case krl_definitions::PTP_REL_CART:
		motion_type = krl_definitions::PTP_REL_CART;
		position = transformToKRLReal(goal->transform);
		break;
	case krl_definitions::LIN:
		motion_type = krl_definitions::LIN;
		position = transformToKRLReal(goal->transform);
		break;
	case krl_definitions::LIN_XYZ:
		motion_type = krl_definitions::LIN;
		position = xyzabcInToKRLReal(goal->frame);
		break;
	case krl_definitions::LIN_REL_BASE:
		motion_type = krl_definitions::LIN_REL_BASE;
		position = transformToKRLReal(goal->transform);
		break;
	case krl_definitions::LIN_REL_TOOL:
		motion_type = krl_definitions::LIN_REL_TOOL;
		position = transformToKRLReal(goal->transform);
		break;
	case krl_definitions::PTP_CONTPATH:
		motion_type = krl_definitions::PTP_CONTPATH;
		position = xyzabcInToKRLReal(goal->frame);
		break;
	case krl_definitions::CIRC:
		motion_type = krl_definitions::CIRC;
		position = xyzabcInToKRLReal(goal->frame);
		viaposition = xyzabcInToKRLReal(goal->framecirc);
		anglecirc = (goal->angle);
		break;
	default:
		log_msg.str("");
		log_msg << "Invalid krlDefinitions::MotionType: " << goal->KRLMotionType;
		error(FFL, log_msg.str());
		moveServer->setAborted();
		return;
	}

	double velocity_scale = goal->velocity_scale;
	if(velocity_scale<=0 || velocity_scale>1){
		velocity_scale = 0.1;
	}

	move(motion_type, position, viaposition, velocity_scale*100.0, anglecirc, goal->trig_by_contact_threshold, fc->getCmdBuf().krl);

	mymutex.unlock();

	switch (motion_type) {
	case krl_definitions::PTP_JOINT:
		log_msg.str("");
		log_msg << "Moving PTP_JOINT: Motion type: " << motion_type << " Position: a1 " << position[0] << " a2 " << position[1] << " e1 " << position[2] << " a3 " << position[3] << " a4 " << position[4] << " a5 " << position[5] << " a6 " << position[6] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::PTP_REL_JOINT:
		log_msg.str("");
		log_msg << "Moving PTP_REL_JOINT: Motion type: " << motion_type << " Position: a1 " << position[0] << " a2 " << position[1] << " e1 " << position[2] << " a3 " << position[3] << " a4 " << position[4] << " a5 " << position[5] << " a6 " << position[6] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::PTP_CART:
		log_msg.str("");
		log_msg << "Moving PTP_CART: Motion type: " << motion_type << " Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::PTP_REL_CART:
		log_msg.str("");
		log_msg << "Moving  PTP_REL_CART: Motion type: " << motion_type << " Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::LIN:
		log_msg.str("");
		log_msg << "Moving LIN: Motion type: " << motion_type << " Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::LIN_REL_BASE:
		log_msg.str("");
		log_msg << "Moving LIN_REL_BASE: Motion type: " << motion_type << " Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::LIN_REL_TOOL:
		log_msg.str("");
		log_msg << "Moving LIN_REL_TOOL: Motion type: " << motion_type << " Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::PTP_CONTPATH:
		log_msg.str("");
		log_msg << "Moving PTP_CONTPATH: Motion type: " << motion_type << " Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;
	case krl_definitions::CIRC:
		log_msg.str("");
		log_msg << "Moving CIRC to: Position: x " << position[0] << " y " << position[1] << " z " << position[2] << " a " << position[3] << " b " << position[4] << " c " << position[5] << " trough position: x " << viaposition[0] << " y " << viaposition[1] << " z " << viaposition[2] << " a " << viaposition[3] << " b " << viaposition[4] << " c " << viaposition[5] << " with angle" << goal->angle << " with speed: " << goal->velocity_scale << " and trigger force: " << goal->trig_by_contact_threshold;
		info(FFL, log_msg.str());
		break;

	default:
		break;
	}

	krl_definitions::KRLCommandState command_state = waitForCommandFinishedInKRC();

/*	if(safetyContact == true)
	{
		myResult.contact = 1;
	}
	else
	{
		myResult.contact = 0;
	}
*/
	if(command_state==krl_definitions::COMMAND_SUCCESS){
		moveServer->setSucceeded();
	} else {
		log_msg.str("");
		log_msg << "Action finished in krc with krlDefinitions::CommandState: " << command_state;
		error(FFL, log_msg.str());
		moveServer->setAborted();
	}
}

//Callback function for the set interrrupt command
void executeSetInterrupt(const fri_communication::MoveArmGoalConstPtr& goal){
	debug(FFL, "::executeSetInterrupt");

	mymutex.lock();

	krl_definitions::KRLInterrupt interrupt_type;

	switch (goal->KRLInterrupt) {
	case krl_definitions::INTERRUPT_ON:
		interrupt_type = krl_definitions::INTERRUPT_ON;
		break;
	case krl_definitions::INTERRUPT_OFF:
		interrupt_type = krl_definitions::INTERRUPT_OFF;
		break;
	default:
		error(FFL, "Invalid krlDefinitions::InterruptType!");
		setInterruptServer->setAborted();
		return;
	}

	INTERRUPT(interrupt_type, fc->getCmdBuf().krl);

	mymutex.unlock();

	switch (goal->KRLInterrupt) {
	case krl_definitions::INTERRUPT_ON:
		info(FFL, "INTERRUPT ON");
		break;
	case krl_definitions::INTERRUPT_OFF:
		info(FFL, "INTERRUPT OFF");
		break;
	default:
		break;
	}

	krl_definitions::KRLCommandState command_state = waitForCommandFinishedInKRC();

	if(command_state == krl_definitions::COMMAND_SUCCESS){
		setInterruptServer->setSucceeded();
	} else {
		log_msg.str("");
		log_msg << "Action finished in krc with krlDefinitions::CommandState: " << command_state;
		error(FFL, log_msg.str());
		setInterruptServer->setAborted();
	}

}

//Callback function for the SetToolAction, sets both tool transform and loaddata on the KRC
void executeSetToolCB(const fri_communication::SetToolGoalConstPtr& goal){
	debug(FFL, "::executeSetToolCB");

	mymutex.lock();

	// set tool frame
	double* tool_transform = transformToKRLReal(goal->transform);
	double cm[3];
	cm[0] = goal->cog.x*1000;
	cm[1] = goal->cog.y*1000;
	cm[2] = goal->cog.z*1000;

	setTool(tool_transform, goal->mass, cm, fc->getCmdBuf().krl);

	mymutex.unlock();

	log_msg.str("");
	log_msg << "Setting tool to x: " << tool_transform[0] << " y " << tool_transform[1] << " z " << tool_transform[2] << " a " << tool_transform[3] << " b " << tool_transform[4] << " c " << tool_transform[5] << " with mass: " << goal->mass << " and center of mass x " << cm[0] << " y " << cm[1] << " z " << cm[2];
	info(FFL, log_msg.str());

	krl_definitions::KRLCommandState command_state = waitForCommandFinishedInKRC();

	if(command_state==krl_definitions::COMMAND_SUCCESS){
		setToolServer->setSucceeded();
	} else {
		log_msg.str("");
		log_msg << "Action finished in krc with krlDefinitions::CommandState: " << command_state;
		error(FFL, log_msg.str());
		setToolServer->setAborted();
	}
}

//Callback function for the SetBaseAction, sets both tool transform and loaddata on the KRC
void executeSetBaseCB(const fri_communication::SetBaseGoalConstPtr& goal){
	debug(FFL, "::executeSetBaseCB");

	mymutex.lock();

	// set tool frame
	double* tool_transform = transformToKRLReal(goal->transform);

	setBase(tool_transform, fc->getCmdBuf().krl);

	mymutex.unlock();

	log_msg.str("");
	log_msg << "Setting base to x: " << tool_transform[0] << " y " << tool_transform[1] << " z " << tool_transform[2] << " a " << tool_transform[3] << " b " << tool_transform[4] << " c " << tool_transform[5];
	info(FFL, log_msg.str());

	krl_definitions::KRLCommandState command_state = waitForCommandFinishedInKRC();

	if(command_state==krl_definitions::COMMAND_SUCCESS){
		setBaseServer->setSucceeded();
	} else {
		log_msg.str("");
		log_msg << "Action finished in krc with krlDefinitions::CommandState: " << command_state;
		error(FFL, log_msg.str());
		setBaseServer->setAborted();
	}
}

//Callback function for the SetPAPASAction, sets both tool transform and loaddata on the KRC
void executeSetPAPASCB(const fri_communication::SetPAPASGoalConstPtr& goal){
	debug(FFL, "::executeSetPAPASCB");

	mymutex.lock();

	double* cpStiffness = xyzabcInToKRLReal(goal->CPStiffness);
	double* cpDamping = xyzabcInToKRLReal(goal->CPDamping);

	setControlMode(krl_definitions::CARTESIAN_IMPEDANCE_TOOL, cpStiffness, cpDamping, fc->getCmdBuf().krl);

	mymutex.unlock();

	log_msg.str("");
	log_msg << "Switching control mode to cartesian impedance with stiffness x: " << cpStiffness[0] << " y " << cpStiffness[1] << " z " << cpStiffness[2] << " a " << cpStiffness[3] << " b " << cpStiffness[4] << " c " << cpStiffness[5];
	info(FFL, log_msg.str());

	krl_definitions::KRLCommandState command_state = waitForCommandFinishedInKRC();

	for( unsigned int i=0; i< goal->commands.size(); i++){
		if(command_state==krl_definitions::COMMAND_SUCCESS){
			mymutex.lock();
			krl_definitions::KRLPAPASType papas_type = parseKRLPAPASType(goal->commands.at(i).PAPASType);

			setPAPAS(papas_type, goal->commands.at(i).force_profile, goal->commands.at(i).start_now, goal->commands.at(i).amplitude, goal->commands.at(i).duration,
					goal->commands.at(i).max_defletion, goal->commands.at(i).max_velocity,goal->commands.at(i).frequency, goal->commands.at(i).phase,
					goal->commands.at(i).force_offset, goal->commands.at(i).rise_time, goal->commands.at(i).fall_time, fc->getCmdBuf().krl);
			mymutex.unlock();
			command_state = waitForCommandFinishedInKRC();
		}

	}

	if(command_state==krl_definitions::COMMAND_SUCCESS){
		setPAPASServer->setSucceeded();
	} else {
		log_msg.str("");
		log_msg << "Action finished in krc with krlDefinitions::CommandState: " << command_state;
		error(FFL, log_msg.str());
		setPAPASServer->setAborted();
	}
}

//Function to publish manipulator data to topic
void lwrDataPublisherCB(const tFriMsrData& msr){

	mymutex.lock();

	fri_communication::LwrData ld;
	ld.header.stamp = ros::Time(msr.intf.timestamp);

	for(int i=0; i<LBR_MNJ; i++){
		ld.msrJntPos[i]=msr.data.msrJntPos[i];
		ld.cmdJntPos[i]=msr.data.cmdJntPos[i];
		ld.cmdJntPosFriOffset[i]=msr.data.cmdJntPosFriOffset[i];
		ld.msrJntTrq[i]=msr.data.msrJntTrq[i];
		ld.estExtJntTrq[i]=msr.data.estExtJntTrq[i];
		ld.gravity[i]=msr.data.gravity[i];
	}

	for(int i=0; i<FRI_CART_FRM_DIM; i++)
	{
		ld.transMatrix[i]=msr.data.msrCartPos[i];
	}


	tf::Transform msrCartPos_tf(btMatrix3x3(),btVector3());
	tf::Transform cmdCartPos_tf, cmdCartPosFriOffset_tf;


	// The data are received as t 3x4 transformation matrix and forwarded as an (x,y,z,a,b,c) tuple
	krlFrame2krlVec(msr.data.msrCartPos,ld.msrCartPos.elems);
	krlFrame2krlVec(msr.data.cmdCartPos,ld.cmdCartPos.elems);
	krlFrame2krlVec(msr.data.cmdCartPosFriOffset,ld.cmdCartPosFriOffset.elems);

	//ld.cmdCartPos[i]=msr.data.cmdCartPos[i];
	//ld.cmdCartPosFriOffset[i]=msr.data.cmdCartPosFriOffset[i];

	for(int i=0; i<FRI_CART_VEC; i++){
		ld.estExtTcpFT[i]=msr.data.estExtTcpFT[i];
		ld.estExtTcpFTVar[i]=msr.krl.realData[10+i];
		ld.cpstiffness[i]=msr.krl.realData[3+i];
	}

	for(int i=0; i<FRI_CART_VEC*LBR_MNJ; i++){
		ld.jacobian[i]=msr.data.jacobian[i];
	}

	for(int i=0; i<LBR_MNJ*LBR_MNJ; i++){
		ld.massMatrix[i]=msr.data.massMatrix[i];
	}

	ld.control_strategy=msr.robot.control;
	ld.torque_axis_ratio=msr.krl.realData[9];
	ld.krc_command_state=msr.krl.intData[0];
	ld.fri_command_state=fc->getCmdBuf().krl.intData[0];

	for(int i=0; i<16; i++){
		ld.intData[i]=msr.krl.intData[i];
	}

	mymutex.unlock();

	pubLwrData.publish(ld);
}

void executeSafetyLoop(const fri_communication::SetSafetyGoalConstPtr& goal)
{
	debug(FFL, "::executeSafetyLoop");

	if (goal->activate == 1)
	{
		firstSafetyLoop = true;

		activateSafetyLoop = true;

		info(FFL, "Safety activated");
	}
	if (goal->activate == 0)
	{
		activateSafetyLoop = false;
		info(FFL, "Safety deactivated");
	}

	setSafetyServer->setSucceeded();
}

//Callback function that communicates through the FRI, sets the state of the KRC action (krl_action_state)
void FRICB(const ros::WallTimerEvent& event){

	mymutex.lock();

	int returnvalue = fc->doDataExchange();
	if(returnvalue<0)
	{
		current_command_state = krl_definitions::COMMUNICATION_ERROR;
		log_msg.str("");
		log_msg << "Communication error in FRI, returnvalue: " << returnvalue;
		error(FFL, log_msg.str());
		mymutex.unlock();
		return;
	}
	lastMessageTime = ros::Time::now();

	if(fc->getFrmKRLInt(2)!=current_command_state)
	{
		//cout << "old action state: " << krl_action_state << " New action state: " << fc->getFrmKRLInt(2) << endl;
	}

	current_command_state = parseKRLCommandState(fc->getFrmKRLInt(2));

	mymutex.unlock();

	lwrDataPublisherCB(fc->getMsrBuf());

	SafetyLoop(fc->getMsrBuf(), fc->getCmdBuf().krl);
}

void jointStatePublisherCB(const ros::WallTimerEvent& event){

	mymutex.lock();

	//Publish joint state
	sensor_msgs::JointState jsmsg = sensor_msgs::JointState();
	jsmsg.header.stamp = lastMessageTime;
	jsmsg.header.frame_id = "base_link";
	for(unsigned int i=0; i<jointNames.size(); i++){
		jsmsg.name.push_back(jointNames.at(i));
		jsmsg.position.push_back(fc->getMsrMsrJntPosition()[i]);
		jsmsg.effort.push_back(fc->getMsrJntTrq()[i]);
	}
	pubJS.publish(jsmsg);

	mymutex.unlock();

}

int main(int argc, char *argv[])
{

	//Joint names are used to publish joint states to the urdf model (robot_description) of ros
	jointNames.push_back("lwr_arm_0_joint");
	jointNames.push_back("lwr_arm_1_joint");
	jointNames.push_back("lwr_arm_2_joint");
	jointNames.push_back("lwr_arm_3_joint");
	jointNames.push_back("lwr_arm_4_joint");
	jointNames.push_back("lwr_arm_5_joint");
	jointNames.push_back("lwr_arm_6_joint");

	ros::init(argc, argv, "lwr_commands_node");

	ros::NodeHandle n;

	InitLogger("fri_communication",true,true,true);

	//Initialization of manipulator data publisher
	pubLwrData = n.advertise<fri_communication::LwrData>("lwr_data",1000);
	pubJS = n.advertise<sensor_msgs::JointState>("joint_states",1000);

	//this opens the connection to the robot
	info(FFL, "Trying to exchange data with the robot");
	//fc = new friRemote(49938,(char*) "192.168.0.10");
	fc = new friRemote(49938,(char*) "192.168.131.1");

	cleartFriKrlData(fc->getCmdBuf().krl);
	commitKRLData(fc->getCmdBuf().krl);

	//initial send and receive of data to the KRC
	int returnvalueReceive = fc->doReceiveData();	// Receive data from the KRC
	log_msg.str("");
	log_msg << "returnvalueReceive: " << returnvalueReceive;
	info(FFL, log_msg.str());
	int returnvalueSend = fc->doSendData();		// Send data to the KRC
	log_msg.str("");
	log_msg << "returnvalueSend: " << returnvalueSend;
	info(FFL, log_msg.str());
	boost::mutex::scoped_lock mylock(mymutex, boost::defer_lock); // defer_lock makes it initially unlocked

	//timers to wake up the communication with the robot and the ros publishers
	ros::WallTimer timer = n.createWallTimer(ros::WallDuration(1/communicationFrequency), FRICB);
	ros::WallTimer publisherTimer = n.createWallTimer(ros::WallDuration(1/publisherFrequency), jointStatePublisherCB);

	log_msg.str("");
	log_msg << "Fri connection started with frequency: " << communicationFrequency;
	info(FFL, log_msg.str());

	// Initialization of the ROS Action Servers
	setControlModeServer = new actionlib::SimpleActionServer<fri_communication::SetControlModeAction>(n, "set_control_mode_action", boost::bind(&executeSetControlModeCB, _1), false);
	setControlModeServer->start();
	moveServer = new actionlib::SimpleActionServer<fri_communication::MoveArmAction>(n, "move_arm_action", boost::bind(&executeMoveArmCB, _1), false);
	moveServer->start();
	setToolServer = new actionlib::SimpleActionServer<fri_communication::SetToolAction>(n, "set_tool_action", boost::bind(&executeSetToolCB, _1), false);
	setToolServer->start();
	setBaseServer = new actionlib::SimpleActionServer<fri_communication::SetBaseAction>(n, "set_base_action", boost::bind(&executeSetBaseCB, _1), false);
	setBaseServer->start();
	setPAPASServer = new actionlib::SimpleActionServer<fri_communication::SetPAPASAction>(n, "set_papas_action", boost::bind(&executeSetPAPASCB, _1), false);
	setPAPASServer->start();
	setInterruptServer = new actionlib::SimpleActionServer<fri_communication::MoveArmAction>(n, "interrupt_action", boost::bind(&executeSetInterrupt, _1), false);
	setInterruptServer->start();
	setSafetyServer = new actionlib::SimpleActionServer<fri_communication::SetSafetyAction>(n, "set_safety_action", boost::bind(&executeSafetyLoop, _1), false);
	setSafetyServer->start();

	ros::spin();

	info(FFL, "Arm node ready!!!");

	ros::waitForShutdown();

}
