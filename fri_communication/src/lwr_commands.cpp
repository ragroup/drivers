/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2012, Mikkel Hvilshoj, Christian Caroe,                  *
 *  Oluf Skov Nielsen and Casper Schou                                     *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************
 *
 * The fucntions in this class are used to execute commands on the manipulator.
 * This is done by changing 16 integers, 16 floats and 16 bools in a data buffer
 * located in the friremote class. These data are passed to the KRC at the next data exchang.
 * On the KRC these data are used in the FRICommandServer.src program, which based on
 * a case structure executed the requested command.
 */

#include "fri_communication/lwr_commands.h"

#include <iostream>
#include <stdio.h>
#include <logger_sys/logger_sys.hpp>

bool firstSafetyLoop = false;
bool activateSafetyLoop = false;
bool safetyContact = false;

double xSafety[100], ySafety[100], zSafety[100];

//Update the sequence number and commit it to the KRC, hence (in FRICommandServer.src). This will induce the execution of the next command.
void commitKRLData(tFriKrlData& msg){
    debug(FFL, "::commitKRLData");
    msg.intData[0]++;
    log_msg.str("");
    log_msg << "Committing move with msg.intData[0]: " << msg.intData[0];
    debug(FFL, log_msg.str());
}

//Changes the control mode of the manipulator
void setControlMode(krl_definitions::KRLStifnessStrategy stiffness_strategy, const double* stiffness, const double* damping, tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::setControlMode: Stiffness strategy: " << stiffness_strategy;
    debug(FFL, log_msg.str());

    msg.intData[1] = krl_definitions::SET_CONTROL_MODE;
    //set stiffness strategy {10=position, 20= cartesian impedance 101= gravity compensation(for recovery)}
    msg.intData[3] = stiffness_strategy;

    switch (stiffness_strategy)
    {
    case krl_definitions::POSITION:
        //Do nothing
        break;
    case krl_definitions::CARTESIAN_IMPEDANCE_TOOL:
        //set CPstiffness
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = stiffness[i];
        }
        for(int i=0; i<6; i++){
            msg.realData[i+7] = damping[i];
        }
        break;
    case krl_definitions::CARTESIAN_IMPEDANCE_BASE:
        //set CPstiffness
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = stiffness[i];
        }
        for(int i=0; i<6; i++)
        {
            msg.realData[i+7] = damping[i];
        }

        break;
    case krl_definitions::JOINT_IMPEDANCE:
        for(int i=0; i<LBR_MNJ;i++){
            msg.realData[i] = stiffness[i];
        }
        for(int i=0; i<7; i++){
            msg.realData[i+7] = damping[i];
        }
        break;
    case krl_definitions::GRAVITY_COMPENSATION:
        break;
    }
    commitKRLData(msg);
}

//Activates or deactivates Command Mode
void commandMode(int activate, tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::setCommandMode: " << activate;
    debug(FFL, log_msg.str());

    msg.intData[1] = krl_definitions::SET_COMMAND_MODE;
    //set stiffness strategy {10=position, 20= cartesian impedance 101= gravity compensation(for recovery)}
    if (activate == 1)
    {
        msg.intData[3] = 1;
    } else if (activate == 0)
    {
        msg.intData[3] = 2;
    }

    commitKRLData(msg);
}

//Runs Load Identification
void loadIdent(tFriKrlData& msg){
    FDEBUG("::loadIdent");
    msg.intData[1] = krl_definitions::LOAD_IDENT;
    commitKRLData(msg);
}

//Moves the manipulator - this function handles all motion commands
void move(krl_definitions::KRLMotionType motion_type, const double* position, const double* viaposition, double velocity_scale, double angle,
          double trig_by_contact_threshold, double vel_CP, double vel_ORI, int num_points, int num_send_points, int vel1, int vel2, int dist_tick_max, tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::move: Motion type: " << motion_type << " x " << position[0] << " y "
            << position[1] << " z " << position[2] << " a " << position[3] << " b "
            << position[4] << " c " << position[5] << " trough position: x " << viaposition[0]
            << " y " << viaposition[1] << " z " << viaposition[2] << " a " << viaposition[3]
            << " b " << viaposition[4] << " c " << viaposition[5] << " Velocity scale: " << velocity_scale
            << " Angle: " << angle << " Trig by contact treshold: " << trig_by_contact_threshold
            << " Velocity Cont Path: " << vel_CP << " Velocity Cont orientation: " << vel_ORI << " Number of Cont loops: " << dist_tick_max;
    debug(FFL, log_msg.str());

    msg.intData[1] = krl_definitions::MOVE;

    switch (motion_type) {
    case krl_definitions::PTP_JOINT:
        for(int i=0; i<LBR_MNJ;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::PTP_REL_JOINT:
        for(int i=0; i<LBR_MNJ;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::PTP_CART:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::PTP_XYZ:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::PTP_REL_CART:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::LIN:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::LIN_XYZ:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::LIN_REL_BASE:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::LIN_REL_TOOL:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;

    case krl_definitions::PTP_CONTPATH:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        break;
    case krl_definitions::CIRC:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        for(int i=9; i<15;i++){
            msg.realData[i]=viaposition[i-9];
        }
        msg.realData[15]=angle;
        break;
    case krl_definitions::LIN_REL_CONTVEL_TOOL:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        msg.realData[6] = vel_CP;
        msg.realData[9] = vel_ORI;
        msg.intData[4] = dist_tick_max;

        break;
    case krl_definitions::LIN_REL_CONTVEL_BASE:
        for(int i=0; i<FRI_CART_VEC;i++){
            msg.realData[i] = position[i];
        }
        msg.realData[6] = vel_CP;
        msg.realData[7] = vel_ORI;
        msg.intData[4] = dist_tick_max;
        break;
    case krl_definitions::PTP_JOINT_CONT:
        for(int i=0; i<LBR_MNJ;i++){
            msg.realData[i] = position[i];
        }
        msg.realData[7] = vel1;

        if(num_send_points == 2)
        {
            for(int i=0; i<LBR_MNJ;i++){
                msg.realData[i+8] = viaposition[i];
            }
            msg.realData[15] = vel2;
        }

        msg.intData[4] = num_send_points;
        msg.intData[5] = num_points;

        log_msg.str("");
        log_msg << "Speed: " << msg.realData[7] << " , " <<  msg.realData[15];
        info(FFL, log_msg.str());

        break;
    default:
        break;
    }

    msg.intData[3] = motion_type;

    if(motion_type != krl_definitions::PTP_JOINT_CONT)
    {
        msg.realData[7] = velocity_scale;
        msg.realData[8] = trig_by_contact_threshold;
    }

    commitKRLData(msg);
}

//Measure est TCP forces
void SafetyLoop(const tFriMsrData& msr, tFriKrlData& msg)
{

    /*if(activateSafetyLoop != true)
    {
        return;
    }

    msg.intData[14] = 0;

    fri_communication::LwrData ld;
    ld.header.stamp = ros::Time(msr.intf.timestamp);
    double SafetyVari;

    if (firstSafetyLoop == true)
    {
        for (int i = 0; i < 100; i++)
        {
            xSafety[i] = msr.data.estExtTcpFT[0];
            ySafety[i] = msr.data.estExtTcpFT[1];
            zSafety[i] = msr.data.estExtTcpFT[2];
        }
    }


    firstSafetyLoop = false;

    //updating the last value and pushing all of the other values towards the front.
    for (int i = 0; i < 99; i++ )
    {
        int j = i + 1;
        xSafety[i] = xSafety[j];

        ySafety[i] = ySafety[j];

        zSafety[i] = zSafety[j];

    }

    xSafety[99] = msr.data.estExtTcpFT[0];
    ySafety[99] = msr.data.estExtTcpFT[1];
    zSafety[99] = msr.data.estExtTcpFT[2];

    //Compare with last measurement
    SafetyVari = 1.5;

    if(xSafety[99]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[99]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[99]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[99]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[99]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[99]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (Last measurement)");
    }

    //Compare with 3rd last measurement
    SafetyVari = 3;
    if(xSafety[97]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[97]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[97]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[97]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[97]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[97]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (3rd last measurement)");
    }

    //Compare with 5th last measurement
    SafetyVari = 4;
    if(xSafety[95]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[95]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[95]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[95]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[95]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[95]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (5th last measurement)");
    }

    //Compare with 7th last measurement
    SafetyVari = 5;
    if(xSafety[93]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[93]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[93]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[93]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[93]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[93]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (7th last measurement)");
    }

    //Compare with 10th last measurement
    SafetyVari = 6;
    if(xSafety[90]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[90]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[90]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[90]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[90]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[90]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (10th last measurement)");
    }

    //Compare with 50th last measurement
    SafetyVari = 8;
    if(xSafety[50]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[50]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[50]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[50]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[50]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[50]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (50th last measurement)");
    }

    //Compare with 99th last measurement
    SafetyVari = 10;
    if(xSafety[0]-SafetyVari > msr.data.estExtTcpFT[0] || ySafety[0]-SafetyVari > msr.data.estExtTcpFT[1] || zSafety[0]-SafetyVari > msr.data.estExtTcpFT[2] || xSafety[0]+SafetyVari < msr.data.estExtTcpFT[0] || ySafety[0]+SafetyVari < msr.data.estExtTcpFT[1] || zSafety[0]+SafetyVari < msr.data.estExtTcpFT[2])
    {
        msg.intData[14] = 3;
        warn(FFL, "Safety interrupt (99th last measurement)");
    }

    if (msg.intData[14] == 3)
    {
        safetyContact = true;
        return;
    }
    */
}

//Activate interrupt to stop the manipulator
void INTERRUPT(krl_definitions::KRLInterrupt interrupt_type, tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::INTERRUPT: " << interrupt_type;
    debug(FFL, log_msg.str());

    msg.intData[13] = interrupt_type;
    //commitKRLData(msg);
}

//Set tool data of current tool (given as E6Pos {x,y,z,a,b,c}, mass, cm_x, cm_y, cm_z})
void setTool(const double* transform, double mass, const double* cm,  tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::setTool: Transform x: " << transform[0] << " y " << transform[1] << " x " << transform[2] << " y " << transform[3] << " x " << transform[4] << " y " << transform[5] << " Mass: " << mass << " Center of mass x: " << cm[0] << " y " << cm[1] << " z " << cm[2];
    debug(FFL, log_msg.str());

    msg.intData[1] = krl_definitions::SET_TOOL;

    //set tool frame (an E6pos is x,y,z,eulerZYX(rz,ry,rz) in moving directions
    for(int i=0; i<FRI_CART_VEC;i++){
        msg.realData[i] = transform[i];
    }
    msg.realData[6] = mass;
    for(int i=7; i<7+3;i++){
        msg.realData[i] = cm[-7+i];
    }

    commitKRLData(msg);

}

//Set base data of current base (given as E6Pos {x,y,z,a,b,c})
void setBase(const double* transform, tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::setTool: Transform x: " << transform[0] << " y " << transform[1] << " x " << transform[2] << " y " << transform[3] << " x " << transform[4] << " y " << transform[5];
    debug(FFL, log_msg.str());

    msg.intData[1] = krl_definitions::SET_BASE;

    //set tool frame (an E6pos is x,y,z,eulerZYX(rz,ry,rz) in moving directions
    for(int i=0; i<FRI_CART_VEC;i++){
        msg.realData[i] = transform[i];
    }

    commitKRLData(msg);
}

//Set/use a force application on the manipulator, e.g. a desired force in a given direction
void setPAPAS(krl_definitions::KRLPAPASType papas_type, const int force_profile, const int start_now, double amplitude,
              double duration, double max_defletion, double max_velocity, double frequency, double phase, double force_offset,
              double rise_time, double fall_time, tFriKrlData& msg){
    log_msg.str("");
    log_msg << "::setPAPAS: PAPAS type: " << papas_type << " Force profile: " << force_profile << " Start now: " << start_now << " Amplitude: " << amplitude << " Duration: " << duration << " Max deflection: " << max_defletion << " Max velocity: " << max_velocity << " Frequency: " << frequency << " Phase: " << phase << " Force offset: " << force_offset << " Rise time: " << rise_time << " Fall time: " << fall_time;
    debug(FFL, log_msg.str());

    msg.intData[1] = krl_definitions::PAPAS;

    msg.intData[3] = papas_type;

    msg.intData[4] = force_profile;

    //Activation is always set to 0, so the client has to activate manually
    msg.intData[5] = start_now;

    //Dont know what not having a shared time base for the function might yield...
    msg.intData[6] = 1;

    printf("md_int: [%d %d %d %d]\n",papas_type,force_profile,start_now,1);
    printf("md_real: [%f %f %f %f %f %f %f %f %f]\n", amplitude, duration, max_defletion, max_velocity, rise_time, fall_time,frequency, phase, force_offset);

    msg.realData[0] = amplitude;
    msg.realData[1] = duration;
    msg.realData[2] = max_defletion;
    msg.realData[3] = max_velocity;
    if(force_profile==1){
        msg.realData[4] = rise_time;
        msg.realData[5] = fall_time;
    } else if(force_profile==2){
        msg.realData[4] = frequency;
        msg.realData[5] = phase;
        msg.realData[6] = force_offset;
        msg.realData[7] = rise_time;
        msg.realData[8] = fall_time;
    }

    commitKRLData(msg);

}

// Helper function to parse an int into the state of an action
krl_definitions::KRLCommandState parseKRLCommandState(const int& s){

    switch(s)
    {
    case krl_definitions::COMMUNICATION_ERROR:
        return krl_definitions::COMMUNICATION_ERROR;
    case krl_definitions::NO_COMMAND:
        return krl_definitions::NO_COMMAND;
    case krl_definitions::COMMAND_EXECUTING:
        return krl_definitions::COMMAND_EXECUTING;
    case krl_definitions::COMMAND_SUCCESS:
        return krl_definitions::COMMAND_SUCCESS;
    default:
        return krl_definitions::NO_COMMAND;
    }
}

//Helper function to parse an int into the force application type
krl_definitions::KRLPAPASType parseKRLPAPASType(const int& s){
    debug(FFL, "::setPAPAS::parseKRLPAPASType");
    switch (s) {
    case krl_definitions::DESIREDFORCE_X:
        return krl_definitions::DESIREDFORCE_X;
    case krl_definitions::DESIREDFORCE_Y:
        return krl_definitions::DESIREDFORCE_Y;
    case krl_definitions::DESIREDFORCE_Z:
        return krl_definitions::DESIREDFORCE_Z;
    case krl_definitions::DESIREDFORCE_A:
        return krl_definitions::DESIREDFORCE_A;
    case krl_definitions::DESIREDFORCE_B:
        return krl_definitions::DESIREDFORCE_B;
    case krl_definitions::DESIREDFORCE_C:
        return krl_definitions::DESIREDFORCE_C;
    case krl_definitions::DESIREDFORCE_START:
        return krl_definitions::DESIREDFORCE_START;
    case krl_definitions::DESIREDFORCE_CLEAR:
        return krl_definitions::DESIREDFORCE_CLEAR;
    default:
        return krl_definitions::INVALID;
    }
}

void cleartFriKrlData(tFriKrlData& msg){
    debug(FFL, "::setPAPAS::cleartFriKrlData");
    msg.boolData = 0;
    for(unsigned int i=0; i<16; i++){
        msg.intData[i] = 0;
        msg.realData[i] = 0;
    }
}
