#!/usr/bin/env python
import roslib; roslib.load_manifest('ur_driver')
import time, sys, threading, math
import copy
import datetime
import socket, select
import struct
import traceback, code
import optparse
import SocketServer
from BeautifulSoup import BeautifulSoup

import rospy
import actionlib
import std_msgs.msg
#import common_msgs.msg 
#import ur_driver.msg                   # Imports the msg "types" generated from the .action file
#import ur_driver.msg
from ur_driver.msg import urcommandsAction
from ur_driver.msg import urcommandsResult
from ur_driver.msg import urinfo            # Feedback from the robot/driver to the messeagetype
from sensor_msgs.msg import JointState
from control_msgs.msg import FollowJointTrajectoryAction
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from std_msgs.msg import Bool

from deserialize import RobotState, RobotMode

prevent_programming = False

# Joint offsets, pulled from calibration information stored in the URDF
#
# { "joint_name" : offset }
#
# q_actual = q_from_driver + offset
joint_offsets = {}

PORT=30002
REVERSE_PORT = 50001

test_mode = False
# Hardware specific modes:
# mode = 1: moveJ with joint input
# mode = 2: moveL with joint input
# mode = 3: moveJ with Car input
# mode = 4: moveL with Car input
# mode = 5: get Force twist - UNDER DEVELOPMENT
# mode = 6: Force start
# mode = 7: Force update
# mode = 8: Force end
# mode = 9: stop robot script
# mode = 10: Get data
# mode = 11: Set ToolCenterPoint 
MSG_MOVEJ = 1
MSG_MOVEL = 2
MSG_MOVEJCar = 3        # Not tested yet
MSG_MOVELCar = 4        # Not tested yet

MSG_FORCE_START = 6
MSG_FORCE_UPDATE = 7    # Not yet
MSG_FORCE_END = 8

MSG_GET_DATA = 10       # Not yet
MSG_SET_TCP = 11        
MSG_OUT = 12
MSG_QUIT = 13
MSG_JOINT_STATES = 14
MSG_WAYPOINT_FINISHED = 15
MSG_SERVOJ = 16
MSG_STOPJ = 17
MSG_SERVOJ_SINGLE = 20

MSG_TEACH_START = 18
MSG_TEACH_END = 19

MULT_jointstate = 10000.0
MULT_time = 1000000.0
MULT_blend = 1000.0

vt41 = 0 # VT4

STOP = 0

JOINT_NAMES = ['shoulder_pan_joint', 'shoulder_lift_joint', 'elbow_joint',
               'wrist_1_joint', 'wrist_2_joint', 'wrist_3_joint']

Q1 = [2.2,0,-1.57,0,0,0]
Q2 = [1.5,0,-1.57,0,0,0]
Q3 = [1.5,-0.2,-1.57,0,0,0]
  

connected_robot = None
connected_robot_lock = threading.Lock()
connected_robot_cond = threading.Condition(connected_robot_lock)    
pub_joint_states = rospy.Publisher('joint_states', JointState)
pub_urinfo = rospy.Publisher('ur_info', urinfo)   
pub_getdata = rospy.Publisher('ur_info_data', urinfo)               # VT4
#pub_pose = rospy.Publisher('ur_info', urinfo)        # VT4
#dump_state = open('dump_state', 'wb')

class EOF(Exception): pass

def dumpstacks():
    id2name = dict([(th.ident, th.name) for th in threading.enumerate()])
    code = []
    for threadId, stack in sys._current_frames().items():
        code.append("\n# Thread: %s(%d)" % (id2name.get(threadId,""), threadId))
        for filename, lineno, name, line in traceback.extract_stack(stack):
            code.append('File: "%s", line %d, in %s' % (filename, lineno, name))
            if line:
                code.append("  %s" % (line.strip()))
    print "\n".join(code)

def log(s):
    print "[%s] %s" % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'), s)


RESET_PROGRAM = '''def resetProg():
  sleep(0.0)
end
'''
#RESET_PROGRAM = ''


# VT4: Connects to the robot, by use of a TCP socket connection, proberly only for sending of the initial "prog" program
class UR5Connection(object):
    TIMEOUT = 1.0
    
    DISCONNECTED = 0
    CONNECTED = 1
    READY_TO_PROGRAM = 2
    EXECUTING = 3
    
    def __init__(self, hostname, port, program):
        self.__thread = None
        self.__sock = None
        self.robot_state = self.DISCONNECTED
        self.hostname = hostname
        self.port = port
        self.program = program
        self.last_state = None

    def connect(self):
        if self.__sock:
            self.disconnect()
        self.__buf = ""
        self.robot_state = self.CONNECTED
        self.__sock = socket.create_connection((self.hostname, self.port))
        self.__keep_running = True
        self.__thread = threading.Thread(name="UR5Connection", target=self.__run)
        self.__thread.daemon = True
        self.__thread.start()

    def send_program(self):
        global prevent_programming
        if prevent_programming:
            rospy.loginfo("Programming is currently prevented")
            return
        assert self.robot_state in [self.READY_TO_PROGRAM, self.EXECUTING]
        rospy.loginfo("Programming the robot at %s" % self.hostname)
        self.__sock.sendall(self.program)
        self.robot_state = self.EXECUTING

    def send_reset_program(self):
        self.__sock.sendall(RESET_PROGRAM)
        self.robot_state = self.READY_TO_PROGRAM
        
    def send_stop(self):
        self.__sock.sendall("stopj()")    
        
    def disconnect(self):
        if self.__thread:
            self.__keep_running = False
            self.__thread.join()
            self.__thread = None
        if self.__sock:
            self.__sock.close()
            self.__sock = None
        self.last_state = None
        self.robot_state = self.DISCONNECTED

    def ready_to_program(self):
        return self.robot_state in [self.READY_TO_PROGRAM, self.EXECUTING]

    def __trigger_disconnected(self):
        log("Robot disconnected")
        self.robot_state = self.DISCONNECTED
    def __trigger_ready_to_program(self):
        rospy.loginfo("Robot ready to program")
    def __trigger_halted(self):
        log("Halted")

    def __on_packet(self, buf):
        state = RobotState.unpack(buf)
        self.last_state = state
        #import deserialize; deserialize.pstate(self.last_state)

        #log("Packet.  Mode=%s" % state.robot_mode_data.robot_mode)

        if not state.robot_mode_data.real_robot_enabled:
            rospy.logfatal("Real robot is no longer enabled.  Driver is fuxored")
            time.sleep(2)
            sys.exit(1)

        # If the urscript program is not executing, then the driver
        # needs to publish joint states using information from the
        # robot state packet.
        if self.robot_state != self.EXECUTING:
            msg = JointState()
            msg.header.stamp = rospy.get_rostime()
            msg.header.frame_id = "From binary state data"
            msg.name = joint_names
            msg.position = [0.0] * 6
            for i, jd in enumerate(state.joint_data):
                msg.position[i] = jd.q_actual + joint_offsets.get(joint_names[i], 0.0)
            msg.velocity = [jd.qd_actual for jd in state.joint_data]
            msg.effort = [0]*6
            pub_joint_states.publish(msg)
            self.last_joint_states = msg

        # Updates the state machine that determines whether we can program the robot.
        can_execute = (state.robot_mode_data.robot_mode in [RobotMode.READY, RobotMode.RUNNING])
        if self.robot_state == self.CONNECTED:
            if can_execute:
                self.__trigger_ready_to_program()
                self.robot_state = self.READY_TO_PROGRAM
        elif self.robot_state == self.READY_TO_PROGRAM:
            if not can_execute:
                self.robot_state = self.CONNECTED
        elif self.robot_state == self.EXECUTING:
            if not can_execute:
                self.__trigger_halted()
                self.robot_state = self.CONNECTED

        # Report on any unknown packet types that were received
        if len(state.unknown_ptypes) > 0:
            state.unknown_ptypes.sort()
            s_unknown_ptypes = [str(ptype) for ptype in state.unknown_ptypes]
            self.throttle_warn_unknown(1.0, "Ignoring unknown pkt type(s): %s. "
                          "Please report." % ", ".join(s_unknown_ptypes))

    def throttle_warn_unknown(self, period, msg):
        self.__dict__.setdefault('_last_hit', 0.0)
        # this only works for a single caller
        if (self._last_hit + period) <= rospy.get_time():
            self._last_hit = rospy.get_time()
            rospy.logwarn(msg)

    def __run(self):
        while self.__keep_running:
            r, _, _ = select.select([self.__sock], [], [], self.TIMEOUT)
            if r:
                more = self.__sock.recv(4096)
                if more:
                    self.__buf = self.__buf + more

                    # Attempts to extract a packet
                    packet_length, ptype = struct.unpack_from("!IB", self.__buf)
                    if len(self.__buf) >= packet_length:
                        packet, self.__buf = self.__buf[:packet_length], self.__buf[packet_length:]
                        self.__on_packet(packet)
                else:
                    self.__trigger_disconnected()
                    self.__keep_running = False
                    
            else:
                self.__trigger_disconnected()
                self.__keep_running = False


def setConnectedRobot(r):
    global connected_robot, connected_robot_lock
    with connected_robot_lock:
        connected_robot = r
        connected_robot_cond.notify()

def getConnectedRobot(wait=False, timeout=-1):
    started = time.time()
    with connected_robot_lock:
        if wait:
            while not connected_robot:
                if timeout >= 0 and time.time() > started + timeout:
                    break
                connected_robot_cond.wait(0.2)
        return connected_robot

# Receives messages from the robot over the socket
class CommanderTCPHandler(SocketServer.BaseRequestHandler):

    def recv_more(self):
        while True:
            r, _, _ = select.select([self.request], [], [], 0.2) #TIMEOUT!!
            if r:
                more = self.request.recv(4096)
                if not more:
                    raise EOF("EOF on recv")
                return more
            else:
                now = rospy.get_rostime()
                if self.last_joint_states and \
                        self.last_joint_states.header.stamp < now - rospy.Duration(1.0):
                    rospy.logerr("Stopped hearing from robot (last heard %.3f sec ago).  Disconnected" % \
                                     (now - self.last_joint_states.header.stamp).to_sec())
                    raise EOF()

    def handle(self):
        self.socket_lock = threading.Lock()
        self.last_joint_states = None
        self.last_pose_states = None                # VT4
        setConnectedRobot(self)
        print "Handling a request"
        try:
            buf = self.recv_more()
            if not buf: return

            while True:
                #print "Buf:", [ord(b) for b in buf]

                # Unpacks the message type
                mtype = struct.unpack_from("!i", buf, 0)[0]
                buf = buf[4:]
                #print "Message type:", mtype

                if mtype == MSG_OUT:
                    # Unpacks string message, terminated by tilde
                    i = buf.find("~")
                    while i < 0:
                        buf = buf + self.recv_more()
                        i = buf.find("~")
                        if len(buf) > 2000:
                            raise Exception("Probably forgot to terminate a string: %s..." % buf[:150])
                    s, buf = buf[:i], buf[i+1:]
                    log("Out: %s" % s)

                elif mtype == MSG_JOINT_STATES:
                    info_sets = 5                   # Sould be changed according to the "number of" informations to be send
                    #while len(buf) < 3*(6*4):
                    while len(buf) < info_sets*(6*4):    
                        buf = buf + self.recv_more()
                    #state_mult = struct.unpack_from("!%ii" % (3*6), buf, 0)
                    state_mult = struct.unpack_from("!%ii" % (info_sets*6), buf, 0)
                    #buf = buf[3*6*4:]
                    buf = buf[info_sets*6*4:]
                    state = [s / MULT_jointstate for s in state_mult]
                    
                    #Extract joint positions:
                    joint_positions = [0.0] * 6
                    for i, q_meas in enumerate(state[:6]):
                        joint_positions[i] = q_meas + joint_offsets.get(joint_names[i], 0.0)
                    
                    #Extract joint velocities:
                    joint_velocities = state[6:12]
                    
                    #extract joint wrench:
                    joint_wrench = state[12:18]
                    
                    #Extract cartesian pose:
                    tcp_pose = state[18:24] #[0.0] * 6
                    #for i, q_meas in enumerate(state[18:24]):
                    #    tcp_pose[i] = q_meas + joint_offsets.get(joint_names[i], 0.0)
                    
                    #Extract tcp forces:
                    tcp_force = state[24:30]
                    
                    
                    #PUBLISH TO JOINT_STATES TOPIC
                    msg = JointState()
                    msg.header.stamp = rospy.get_rostime()
                    msg.name = joint_names
                    msg.position = joint_positions
                    msg.velocity = joint_velocities
                    msg.effort = joint_wrench
 
                    #PUBLISH TO URINFO
                    info = urinfo()
                    info.pose = tcp_pose
                    info.joints = joint_positions
                    info.force = tcp_force
                    info.wrench = joint_wrench
                    
                    self.last_pose_states = info                 
                    self.last_joint_states = msg
                    
                    pub_urinfo.publish(info)
                    pub_joint_states.publish(msg)
                elif mtype == MSG_QUIT:
                    print "Quitting"
                    raise EOF("Received quit")
                elif mtype == MSG_WAYPOINT_FINISHED:
                    while len(buf) < 4:
                        buf = buf + self.recv_more()
                    waypoint_id = struct.unpack_from("!i", buf, 0)[0]
                    buf = buf[4:]
                    print "Waypoint finished (not handled)"
                else:
                    raise Exception("Unknown message type: %i" % mtype)

                if not buf:
                    buf = buf + self.recv_more()
        except EOF, ex:
            print "Connection closed (command):", ex
            setConnectedRobot(None)

    def send_quit(self):
        with self.socket_lock:
            self.request.send(struct.pack("!i", MSG_QUIT))
            
    def send_servoj(self, waypoint_id, q_actual, t):
        assert(len(q_actual) == 6)
        q_robot = [0.0] * 6
        for i, q in enumerate(q_actual):
            q_robot[i] = q - joint_offsets.get(joint_names[i], 0.0)
        params = [MSG_SERVOJ, waypoint_id] + \
                 [MULT_jointstate * qq for qq in q_robot] + \
                 [MULT_time * t]
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf)
           
           
    def send_movej(self, waypoint_id, q_actual, qt_robotarry, qtt_robotarry, t):     # VT4
        #print "q_actual: ", q_actual                # VT4
        assert(len(q_actual) == 6)
        q_robot = [0.0] * 6
        qt_robot = qt_robotarry[0]
        print "VELOCITY: ", qt_robotarry[0]
        #qtt_robot = qtt_robotarry[0]
        qtt_robot = 0.5  #SET ACC
        for i, q in enumerate(q_actual):
            q_robot[i] = q - joint_offsets.get(joint_names[i], 0.0)
        params = [MSG_MOVEJ, waypoint_id] + \
                 [MULT_jointstate * qq for qq in q_robot] + \
                 [MULT_jointstate * qt_robot] + \
                 [MULT_jointstate * qtt_robot] + \
                 [MULT_time * t]
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf) 
    
    
    def send_movel(self, waypoint_id, q_actual, qt_robotarry, qtt_robotarry, t):     # VT4
        #print "q_actual: ", q_actual                # VT4
        assert(len(q_actual) == 6)
        q_robot = [0.0] * 6
        qt_robot = qt_robotarry[0]
        
         #qtt_robot = qtt_robotarry[0]
        qtt_robot = 0.5  #SET ACC
        for i, q in enumerate(q_actual):
            q_robot[i] = q - joint_offsets.get(joint_names[i], 0.0)
        params = [MSG_MOVEL, waypoint_id] + \
                 [MULT_jointstate * qq for qq in q_robot] + \
                 [MULT_jointstate * qt_robot] + \
                 [MULT_jointstate * qtt_robot] + \
                 [MULT_time * t]
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf)


    def send_movejCar(self, waypoint_id, q_actual, qt_robotarry, qtt_robotarry, t):     # VT4
        #print "q_actual: ", q_actual                # VT4
        assert(len(q_actual) == 6)
        q_robot = [0.0] * 6
        qt_robot = qt_robotarry[0]
         #qtt_robot = qtt_robotarry[0]
        qtt_robot = 0.5  #SET ACC
        for i, q in enumerate(q_actual):
            q_robot[i] = q - joint_offsets.get(joint_names[i], 0.0)
        params = [MSG_MOVEJCar, waypoint_id] + \
                 [MULT_jointstate * qq for qq in q_robot] + \
                 [MULT_jointstate * qt_robot] + \
                 [MULT_jointstate * qtt_robot] + \
                 [MULT_time * t]
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf) 
    
    
    def send_movelCar(self, waypoint_id, q_actual, qt_robotarry, qtt_robotarry, t):     # VT4
        #print "q_actual: ", q_actual                # VT4
        assert(len(q_actual) == 6)
        q_robot = [0.0] * 6
        qt_robot = qt_robotarry[0]
         #qtt_robot = qtt_robotarry[0]
        qtt_robot = 0.5  #SET ACC
        for i, q in enumerate(q_actual):
            q_robot[i] = q - joint_offsets.get(joint_names[i], 0.0)
        params = [MSG_MOVELCar, waypoint_id] + \
                 [MULT_jointstate * qq for qq in q_robot] + \
                 [MULT_jointstate * qt_robot] + \
                 [MULT_jointstate * qtt_robot] + \
                 [MULT_time * t]
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf)            
            
    def send_tcp(self, waypoint_id, tcppose_goal, mass_goal, tcpcog_goal):     # VT4
        #print "q_actual: ", q_actual                # VT4
        #assert(len(q_actual) == 6)
        
        params = [MSG_SET_TCP, waypoint_id] + \
                 [MULT_jointstate * qq for qq in tcppose_goal] + \
                 [MULT_jointstate * mass_goal] + \
                 [MULT_jointstate * qq1 for qq1 in tcpcog_goal] 
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf)
            
 
            
    def send_force_start(self, waypoint_id,q_robot,selection_goal,wrench_goal,limits_goal):     # VT4        
        q_robot = [0.0] * 6
        #for i, q in enumerate(q_robot):    
        #    q_robot[i] = q - joint_offsets.get(joint_names[i], 0.0)
        params = [MSG_FORCE_START, waypoint_id] + \
                 [MULT_jointstate * qq for qq in q_robot] + \
                 [MULT_jointstate * qq1 for qq1 in selection_goal] + \
                 [MULT_jointstate * qq2 for qq2 in wrench_goal] + \
                 [MULT_jointstate * qq3 for qq3 in limits_goal]
        #print "params: ", params                # VT4
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf)  
      
      
    def send_force_end(self, waypoint_id):     # VT4
        #print "q_actual: ", q_actual                # VT4
        params = [MSG_FORCE_END, waypoint_id]
        buf = struct.pack("!%ii" % len(params), *params)
        with self.socket_lock:
            self.request.send(buf)  

    def send_teach_start(self):
        with self.socket_lock:
            self.request.send(struct.pack("!i", MSG_TEACH_START))
            
    def send_teach_end(self):
        with self.socket_lock:
            self.request.send(struct.pack("!i", MSG_TEACH_END))

    def send_stopj(self):
        with self.socket_lock:
            self.request.send(struct.pack("!i", MSG_STOPJ))

    def set_waypoint_finished_cb(self, cb):
        self.waypoint_finished_cb = cb

    # Returns the last JointState message sent out
    def get_joint_states(self):
        return self.last_joint_states
        
    
    # Returns the last JointState message sent out      # VT4
    def get_pose_states(self):                          # VT4
        return self.last_pose_states                    # VT4
    

class TCPServer(SocketServer.TCPServer):
    allow_reuse_address = True  # Allows the program to restart gracefully on crash
    timeout = 5


# Waits until all threads have completed.  Allows KeyboardInterrupt to occur
def joinAll(threads):
    while any(t.isAlive() for t in threads):
        for t in threads:
            t.join(0.2)

# Returns the duration between moving from point (index-1) to point
# index in the given JointTrajectory
def get_segment_duration(traj, index):
    if index == 0:
        return traj.points[0].time_from_start.to_sec()
    return (traj.points[index].time_from_start - traj.points[index-1].time_from_start).to_sec()

# Reorders the JointTrajectory traj according to the order in
# joint_names.  Destructive.
def reorder_traj_joints(traj, joint_names):
    order = [traj.joint_names.index(j) for j in joint_names]

    new_points = []
    for p in traj.points:
        new_points.append(JointTrajectoryPoint(
            positions = [p.positions[i] for i in order],
            velocities = [p.velocities[i] for i in order] if p.velocities else [],
            accelerations = [p.accelerations[i] for i in order] if p.accelerations else [],
            time_from_start = p.time_from_start))
    traj.joint_names = joint_names
    traj.points = new_points


# Cubic interpolation between the two points p0 and p1. Documentation can be found
# on: http://www.paulinternet.nl/?page=bicubic - VT4
def interp_cubic(p0, p1, t_abs):
    T = (p1.time_from_start - p0.time_from_start).to_sec()
    t = t_abs - p0.time_from_start.to_sec()
    q = [0] * 6
    qdot = [0] * 6
    qddot = [0] * 6
    for i in range(len(p0.positions)):
        a = p0.positions[i]
        b = p0.velocities[i]
        c = (-3*p0.positions[i] + 3*p1.positions[i] - 2*T*p0.velocities[i] - T*p1.velocities[i]) / T**2
        d = (2*p0.positions[i] - 2*p1.positions[i] + T*p0.velocities[i] + T*p1.velocities[i]) / T**3



        q[i] = a + b*t + c*t**2 + d*t**3            # 3. degeree polynomial
        qdot[i] = b + 2*c*t + 3*d*t**2              # 1. derivative 3. degeree polynomial
        qddot[i] = 2*c + 6*d*t                      # 2. derivative 3. degeree polynomial
        #q[i] = p1.positions[i]
        #qdot[i] = p1.velocities[i]
        #qddot[i] = 2*c + 6*d*t                      # 2. derivative 3. degeree polynomial
        
    #print "interp_cubic, q = ",q                # VT4
    return JointTrajectoryPoint(positions=q, velocities=qdot, accelerations=qddot, time_from_start=rospy.Duration(t_abs))

# Returns (q, qdot, qddot) for sampling the JointTrajectory at time t.
# The time t is the time since the trajectory was started.
def sample_traj(traj, t):
    # First point
    if t <= 0.0:
        return copy.deepcopy(traj.points[0])
    # Last point
    if t >= traj.points[-1].time_from_start.to_sec():
        return copy.deepcopy(traj.points[-1])
    
    # Finds the (middle) segment containing t
    i = 0
    while traj.points[i+1].time_from_start.to_sec() < t:
        i += 1
    return interp_cubic(traj.points[i], traj.points[i+1], t)

def traj_is_finite(traj):
    for pt in traj.points:
        for p in pt.positions:
            if math.isinf(p) or math.isnan(p):
                return False
        for v in pt.velocities:
            if math.isinf(v) or math.isnan(v):
                return False
    return True
        
def has_limited_velocities(traj):
    for p in traj.points:
        for v in p.velocities:
            if math.fabs(v) > max_velocity:
                return False
    return True

def has_velocities(traj):
    for p in traj.points:
        if len(p.velocities) != len(p.positions):
            return False
    return True

def within_tolerance(a_vec, b_vec, tol_vec):
    for a, b, tol in zip(a_vec, b_vec, tol_vec):
        if abs(a - b) > tol:
            return False
    return True

class UR5TrajectoryFollower(object):
    RATE = 0.02
    _result   = urcommandsResult()   # VT4
    def __init__(self, robot, goal_time_tolerance=None):
        self.goal_time_tolerance = goal_time_tolerance or rospy.Duration(1.0) # VT4, org time: 0.0
        self.joint_goal_tolerances = [0.05, 0.05, 0.05, 0.05, 0.05, 0.05]
        self.following_lock = threading.Lock()
        self.T0 = time.time()
        self.robot = robot
        self.server = actionlib.ActionServer("follow_joint_trajectory",
                                             FollowJointTrajectoryAction,
                                             self.on_goal, self.on_cancel, auto_start=False)
                                             
        self._server_urcommands = actionlib.ActionServer("ur_commands",
                                             urcommandsAction,
                                             self.on_goal_commands, 
                                             self.on_cancel_commands, auto_start=False) #VT4

        self.servo_input_sub = rospy.Subscriber('/ur_servo', JointTrajectoryPoint, self.on_servo_inputCB)
        
        self.servo_start_sub = rospy.Subscriber('/ur_servo_start', Bool, self.on_servo_startCB)

        self.goal_handle_commands = None    # VT4        
        self.goal_handle = None
        
        self.traj = None
        self.traj_t0 = 0.0
        self.first_waypoint_id = 10
        self.tracking_i = 0
        self.pending_i = 0
        self.last_point_sent = True
        self.servo_point = None
        self.servo_time = None
        self.servo_sent = True
        self.servo_running = False

        self.update_timer = rospy.Timer(rospy.Duration(self.RATE), self._update)

    def on_servo_inputCB(self, data):
        print ("received a servo input")
        self.servo_point = data.positions
        self.servo_time = data.time_from_start.to_sec()
        self.servo_sent = False
        
    def on_servo_startCB(self, data):
        print ("received a servo start input")
        if self.goal_handle or self.goal_handle_commands:
            print "cannot change to servo mode - action in progress"
        else:
            self.servo_running = data

    def set_robot(self, robot):
        # Cancels any goals in progress
        print "set_robot - function executing"
        if self.goal_handle:    
            self.goal_handle.set_canceled()
            self.goal_handle = None
        if self.goal_handle_commands:
            self.goal_handle_commands.set_canceled()        # VT4
            self.goal_handle_commands = None                         # VT4
        self.traj = None
        self.robot = robot
        if self.robot:
            self.init_traj_from_robot()

    # Sets the trajectory to remain stationary at the current position
    # of the robot.
    def init_traj_from_robot(self):
        if test_mode == False:
            if not self.robot: raise Exception("No robot connected")
            # Busy wait (avoids another mutex)
            state = self.robot.get_joint_states()
            while not state:
                time.sleep(0.1)
                state = self.robot.get_joint_states()
            self.traj_t0 = time.time()
            self.traj = JointTrajectory()
            self.traj.joint_names = joint_names
            self.traj.points = [JointTrajectoryPoint(
                positions = state.position,
                velocities = [0] * 6,
                accelerations = [0] * 6,
                time_from_start = rospy.Duration(0.0))]
        elif test_mode == True:
            print "init_traj_from_robot - test_mode == true"
        else:
            print "Wrong test_mode"

    def start(self):
        self.init_traj_from_robot()
        self.server.start()
        print "The action server for this driver has been started (follow_joint_trajectory)"
          

    def start_urcommands(self):
        self._server_urcommands.start()
        print "The action server for this driver has been started (ur_commands)" 
        
        
    def on_goal(self, goal_handle):
        ##log("on_goal")
        ##print "on_goal - test oputput"
        #print "Positions", goal_handle.get_goal().trajectory.positions
        # Checks that the robot is connected
        if self.servo_running:
            rospy.logerr("Driver currently in servo_single mode. Please quit this first")
            goal_handle.set_rejected()
            return
        
        if not self.robot:
            rospy.logerr("Received a goal, but the robot is not connected")
            goal_handle.set_rejected()
            return

        # Checks if the joints are just incorrect
        if set(goal_handle.get_goal().trajectory.joint_names) != set(joint_names):
            rospy.logerr("Received a goal with incorrect joint names: (%s)" % \
                         ', '.join(goal_handle.get_goal().trajectory.joint_names))
            goal_handle.set_rejected()
            return

        if not traj_is_finite(goal_handle.get_goal().trajectory):
            rospy.logerr("Received a goal with infinites or NaNs")
            goal_handle.set_rejected(text="Received a goal with infinites or NaNs")
            return
        
        # Checks that the trajectory has velocities
        if not has_velocities(goal_handle.get_goal().trajectory):
            rospy.logerr("Received a goal without velocities")
            goal_handle.set_rejected(text="Received a goal without velocities")
            return

        # Checks that the velocities are withing the specified limits
        if not has_limited_velocities(goal_handle.get_goal().trajectory):
            message = "Received a goal with velocities that are higher than %f" % max_velocity
            rospy.logerr(message)
            goal_handle.set_rejected(text=message)
            return

        # Orders the joints of the trajectory according to joint_names
        reorder_traj_joints(goal_handle.get_goal().trajectory, joint_names)
                
        with self.following_lock:
            if self.goal_handle:
                # Cancels the existing goal
                self.goal_handle.set_canceled()
                self.first_waypoint_id += len(self.goal_handle.get_goal().trajectory.points)
                self.goal_handle = None

            # Inserts the current setpoint at the head of the trajectory
            now = time.time()
            point0 = sample_traj(self.traj, now)
            point0.time_from_start = rospy.Duration(0.0)
            goal_handle.get_goal().trajectory.points.insert(0, point0)
            self.traj_t0 = now

            # Replaces the goal
            self.goal_handle = goal_handle
            self.traj = goal_handle.get_goal().trajectory
            self.goal_handle.set_accepted()
            #print "self.traj = goal_handle.get_goal().trajectory",goal_handle.get_goal().trajectory
        #print "Positions"#, goal_handle.get_goal().trajectory.positions
        #print "Postitions", self.traj

    def on_cancel(self, goal_handle):
        global STOP
        log("on_cancel")
        if goal_handle == self.goal_handle:
            with self.following_lock:
                # Uses the next little bit of trajectory to slow to a stop
                #STOP_DURATION = 0.5
                #now = time.time()
                #point0 = sample_traj(self.traj, now - self.traj_t0)
                #point0.time_from_start = rospy.Duration(0.0)
                #point1 = sample_traj(self.traj, now - self.traj_t0 + STOP_DURATION)
                #point1.velocities = [0] * 6
                #point1.accelerations = [0] * 6
                #point1.time_from_start = rospy.Duration(STOP_DURATION)
                #self.traj_t0 = now
                #self.traj = JointTrajectory()
                #self.traj.joint_names = joint_names
                #self.traj.points = [point0, point1]
                #self.robot.send_stopj()
                self.goal_handle.set_canceled()
                self.goal_handle = None
                #STOP = 1
                
                
        else:
            goal_handle.set_canceled()
            
#VT4
    #def on_goal_commands(self, goal_handle):
    def on_goal_commands(self, goal_handle_commands):
        ##log("on_goal_commands")
        global mode_goal

        if self.servo_running:
            rospy.logerr("Driver currently in servo_single mode. Please quit this first")
            goal_handle.set_rejected()
            return

#        self.mode_goal = goal_handle.get_goal().mode
#        self.mass_goal = goal_handle.get_goal().mass
#        self.tcppose_goal = goal_handle.get_goal().tcppose
#        self.tcpcog_goal = goal_handle.get_goal().tcpcog
#        self.selection_goal = goal_handle.get_goal().selection
#        self.wrench_goal = goal_handle.get_goal().wrench
#        self.limits_goal = goal_handle.get_goal().limits
        self.mode_goal = goal_handle_commands.get_goal().mode
        self.mass_goal = goal_handle_commands.get_goal().mass
        self.tcppose_goal = goal_handle_commands.get_goal().tcppose
        self.tcpcog_goal = goal_handle_commands.get_goal().tcpcog
        self.selection_goal = goal_handle_commands.get_goal().selection
        self.wrench_goal = goal_handle_commands.get_goal().wrench
        self.limits_goal = goal_handle_commands.get_goal().limits
        
        if self.mode_goal == MSG_MOVEJ:
            rospy.loginfo("mode: MSG_MOVEJ")
        elif self.mode_goal == MSG_MOVEL:
            rospy.loginfo("mode: MSG_MOVEL")
        elif self.mode_goal == MSG_MOVEJCar:
            rospy.loginfo("mode: MSG_MOVEJCar")
        elif self.mode_goal == MSG_MOVELCar:
            rospy.loginfo("mode: MSG_MOVELCar")
        elif self.mode_goal == MSG_FORCE_START:
            rospy.loginfo("mode: MSG_FORCE_START")
        elif self.mode_goal == MSG_FORCE_UPDATE:
            rospy.loginfo("mode: MSG_FORCE_UPDATE")
        elif self.mode_goal == MSG_FORCE_END:
            rospy.loginfo("mode: MSG_FORCE_END")
        elif self.mode_goal == MSG_GET_DATA:
            rospy.loginfo("mode: MSG_GET_DATA")
        elif self.mode_goal == MSG_SET_TCP:
            rospy.loginfo("mode: MSG_SET_TCP")
        elif self.mode_goal == MSG_JOINT_STATES:
            rospy.loginfo("mode: MSG_JOINT_STATES")
        elif self.mode_goal == MSG_WAYPOINT_FINISHED:
            rospy.loginfo("mode: MSG_WAYPOINT_FINISHED")
        elif self.mode_goal == MSG_SERVOJ:
            rospy.loginfo("mode: MSG_SERVOJ")
        elif self.mode_goal == MSG_TEACH_START:
            rospy.loginfo("mode: MSG_TEACH_START")
        elif self.mode_goal == MSG_TEACH_END:
            rospy.loginfo("mode: MSG_TEACH_END")
        elif self.mode_goal == MSG_SERVOJ_SINGLE:
            rospy.loginfo("mode: MSG_SERVOJ_SINGLE")
                
      
        ##print "mode: ", self.mode_goal
        ##print "mass: ", self.mass_goal
        ##print "tcppose: ", self.tcppose_goal
        ##print "tcpcog: ", self.tcpcog_goal
        ##print "selection: ", self.selection_goal
        ##print "wrench: ", self.wrench_goal
        ##print "limits: ", self.limits_goal
        
        
        # Checks that the robot is connected
        ##print "on_goal_commands"
        #pose1 = [1,2,3,4,5,6]
        #joints1 = [7,8,9,10,11,12]
        #pub_getdata.publish(pose1, joints1)        
        
        #
        self.goal_handle_commands = goal_handle_commands
        self.goal_handle_commands.set_accepted()
#        
#        self._result.action_result.result.resJoint[1] = 213
#
#        self.goal_handle_commands.set_succeeded(self._result)
        
    #def on_cancel_commands(self, goal_handle):
    def on_cancel_commands(self, goal_handle_commands):
        global STOP
        log("on_cancel_commands")
        print "on_cancel_commands"
        
        #STOP:
        #self.robot.send_stopj()
        
        
        if goal_handle_commands == self.goal_handle_commands:
            self.goal_handle_commands.set_canceled()
            self.goal_handle_commands = None
        else:
            goal_handle_commands.set_canceled()
        
        STOP = 1
#    def publish_result_commands(self, status, result):
       
#VT4            


    last_now = time.time()
    def _update(self, event):
        global STOP
        #print "_update - self.robot.get_joint_states()", self.robot.get_joint_states()     # VT4
        if STOP == 1:
            self.last_point_sent = True
            if self.goal_handle:
                self.goal_handle_commands.set_canceled()        # VT4
                self.goal_handle_commands = None                         # VT4
                self.goal_handle.set_canceled()
                self.goal_handle = None
            return
        
        if self.robot and self.servo_running and self.servo_point and not self.servo_sent:
            print ("servoing...")
            self.robot.send_servoj(999, self.servo_point, self.servo_time)
            #self.servo_sent = True 
            position_in_tol = within_tolerance(self.robot.get_joint_states().position, self.servo_point, self.joint_goal_tolerances)
            if position_in_tol:
                self.servo_sent = True
            
        elif self.robot and self.traj:
            #print ("i got a traj")
            now = time.time()
            #print "now: ", now
#            if 1==1:
#                print "test1", now - self.traj_t0
#                print "test2", self.traj.points[-1].time_from_start.to_sec()
            #print "self.traj: ", self.traj
            if (now - self.traj_t0) <= (self.traj.points[-1].time_from_start.to_sec()):
                #print "(now - self.traj_t0) <= self.traj.points[-1].time_from_start.to_sec():"
                self.last_point_sent = False #sending intermediate points

                if self.mode_goal == MSG_SERVOJ:
                    setpoint = sample_traj(self.traj, now - self.traj_t0)                              # VT4 commented
                else:
                    setpoint = sample_traj(self.traj, self.traj.points[-1].time_from_start.to_sec())    # VT4 !!! OBS !!! MAKE AN IF FUNCTION HERE

                
                if self.mode_goal == MSG_FORCE_START:
                    self.robot.send_force_start(999,[10,10,10,10,10,10],self.selection_goal,self.wrench_goal,self.limits_goal)
                    self.traj_t0 = 2000
                    print "mode == MSG_FORCE_START / 6 - first"
                    
                elif self.mode_goal == MSG_FORCE_END:
                    self.robot.send_force_end(999)
                    self.traj_t0 = 2000
                    print "mode == MSG_FORCE_END / 8 - first"
                    
                elif self.mode_goal == MSG_TEACH_START:
                    self.robot.send_teach_start()
                    self.traj_t0 = 2000
                    print "mode == MSG_TEACH_START / 18 - first"
                    
                elif self.mode_goal == MSG_TEACH_END:
                    self.robot.send_teach_end()
                    self.traj_t0 = 2000
                    print "mode == MSG_TEACH_END / 19 - first"
                    
                elif self.mode_goal == MSG_SET_TCP:
                    print "tcppose", self.tcppose_goal
                    print "mass", self.mass_goal
                    print "tcpcog", self.tcpcog_goal
                    self.robot.send_tcp(999, self.tcppose_goal,self.mass_goal,self.tcpcog_goal)
                    self.traj_t0 = 2000
                    print "mode == MSG_SET_TCP / 11 - first"
                elif self.mode_goal == MSG_GET_DATA:
                    print "_update - self.last_pose_states", self.robot.last_pose_states.pose
                    print "_update - self.last_joint_states", self.robot.last_joint_states.position
                    self.traj_t0 = 2000
                else:
                    try:
                        global vt41
                        vt41 += 1
                        
                                # Hardware specific modes:
                                # mode = 1: moveJ with joint input
                                # mode = 2: moveL with joint input
                                # mode = 3: moveJ with Car input
                                # mode = 4: moveL with Car input
                                # mode = 5: get Force twist - UNDER DEVELOPMENT
                                # mode = 6: Force start
                                # mode = 7: Force update
                                # mode = 8: Force end
                                # mode = 9: stop robot script
                                # mode = 10: Get data
                                # mode = 11: Set ToolCenterPoint 
                                
                        if self.mode_goal == MSG_MOVEJ: 
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVEJ / 1"
                                print "self.traj: ", setpoint.velocities#self.traj.points
                                print "self.traj -end "
                                self.robot.send_movej(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == MSG_MOVEL:    
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVEL / 2"
                                self.robot.send_movel(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == MSG_MOVEJCar:
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVEJCar / 3"
                                print "setpoint.positions", setpoint.positions
                                self.robot.send_movejCar(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == MSG_MOVELCar:
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVELCar / 4"
                                print "setpoint.positions", setpoint.positions
                                self.robot.send_movelCar(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == 5:    
                            print "Mode == 5"
                        elif self.mode_goal == MSG_FORCE_START:    
                            print "Mode == 6"   
                        elif self.mode_goal == MSG_FORCE_UPDATE:    
                            print "Mode == 7" 
                        elif self.mode_goal == MSG_FORCE_END:    
                            print "Mode == 8"
                        elif self.mode_goal == MSG_TEACH_START:
                            print "Mode == 18"
                        elif self.mode_goal == MSG_TEACH_END:
                            print "Mode == 19"
                        elif self.mode_goal == 9:    
                            print "Mode == 9"
                        elif self.mode_goal == MSG_SERVOJ:
                            #if  vt41 % 4 == 0: 
                            ##print "Mode == MSG_SERVOJ / 16" 
                            self.robot.send_servoj(999, setpoint.positions, 4 * self.RATE)
                            #time.sleep(1)
                            #self.robot.send_servoj(999, setpoint.positions, 4 * self.RATE)
                            ##print "setpoint.positions", setpoint.positions
                        elif self.mode_goal == MSG_STOPJ:    
                            print "Mode == MSG_STOPJ / 17"         
                                   
                    except socket.error:
                        pass
                
            elif not self.last_point_sent:
                if self.mode_goal == MSG_FORCE_START:
                    self.robot.send_force_start(999,[10,10,10,10,10,10],self.selection_goal,self.wrench_goal,self.limits_goal)
                    self.last_point_sent = True
                    print "mode == MSG_FORCE_START / 6 - elif - first"
                elif self.mode_goal == MSG_FORCE_END:
                    self.robot.send_force_end(999)
                    self.last_point_sent = True
                    print "mode == MSG_FORCE_END / 8 - elif - first"
                elif self.mode_goal == MSG_TEACH_START:
                    self.robot.send_teach_start()
                    self.last_point_sent = True
                    print "mode == MSG_TEACH_START / 18 - elif - first"
                elif self.mode_goal == MSG_TEACH_END:
                    self.robot.send_teach_end()
                    self.last_point_sent = True
                    print "mode == MSG_TEACH_END / 19 - elif - first"
                elif self.mode_goal == MSG_SET_TCP:
                    self.robot.send_tcp(999, self.tcppose_goal,self.mass_goal,self.tcpcog_goal)
                    self.last_point_sent = True
                    print "tcppose", self.tcppose_goal
                    print "mass", self.mass_goal
                    print "tcpcog", self.tcpcog_goal
                    print "mode == MSG_SET_TCP / 11 - elif - first"
                elif self.mode_goal == MSG_GET_DATA:
                    print "_update - self.last_pose_states", self.robot.last_pose_states.pose
                    print "_update - self.last_joint_states", self.robot.last_joint_states.position
                    self.last_point_sent = True
                else:                
                # All intermediate points sent, sending last point to make sure we
                # reach the goal.
                # This should solve an issue where the robot does not reach the final
                # position and errors out due to not reaching the goal point.
                    last_point = self.traj.points[-1]
                    state = self.robot.get_joint_states()
                    
                    ##print "elif - self.robot.get_joint_states()", self.robot.get_joint_states()
                    if self.mode_goal == MSG_MOVEJCar or self.mode_goal == MSG_MOVELCar:
                        print "self.mode_goal == MSG_MOVEJCar"
                        # self.last_pose_states 
                        position_in_tol = within_tolerance(self.robot.last_pose_states.pose, last_point.positions, self.joint_goal_tolerances) ## OBS !!
                        #position_in_tol = True
                    else:
                        position_in_tol = within_tolerance(state.position, last_point.positions, self.joint_goal_tolerances)
                    # Performing this check to try and catch our error condition.  We will always
                    # send the last point just in case.
                    if not position_in_tol:
                        rospy.logwarn("Trajectory time exceeded and current robot state not at goal, last point required")
                        rospy.logwarn("Current trajectory time: %s, last point time: %s" % \
                                    (now - self.traj_t0, self.traj.points[-1].time_from_start.to_sec()))
                        rospy.logwarn("Desired: %s\nactual: %s\nvelocity: %s" % \
                                              (last_point.positions, state.position, state.velocity))
                    setpoint = sample_traj(self.traj, self.traj.points[-1].time_from_start.to_sec())
    
                    try:
                       
                        if self.mode_goal == MSG_MOVEJ: 
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVEJ / 1 - elif"
                                self.robot.send_movej(999, setpoint.positions,setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == MSG_MOVEL:    
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVEL / 2 - elif"
                                self.robot.send_movel(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == MSG_MOVEJCar:  
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVEJCar / 3 - elif"
                                print "setpoint.positions", setpoint.positions
                                self.robot.send_movejCar(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == MSG_MOVELCar:  
                            if  vt41 % 20 == 0: 
                                print "Mode == MSG_MOVELCar / 4 - elif"
                                print "setpoint.positions", setpoint.positions
                                self.robot.send_movelCar(999, setpoint.positions, setpoint.velocities, setpoint.accelerations, 4 * self.RATE)
                        elif self.mode_goal == 5:    
                            print "Mode == 5 - elif"
                        elif self.mode_goal == MSG_FORCE_START:    
                            print "Mode == 6 - elif"   
                        elif self.mode_goal == MSG_FORCE_UPDATE:    
                            print "Mode == 7 - elif" 
                        elif self.mode_goal == MSG_FORCE_END:    
                            print "Mode == 8 - elif"
                        elif self.mode_goal == 9:    
                            print "Mode == 9 - elif"
                        elif self.mode_goal == MSG_TEACH_START:    
                            print "Mode == 18 - elif" 
                        elif self.mode_goal == MSG_TEACH_END:    
                            print "Mode == 19 - elif"                            
                        elif self.mode_goal == MSG_SERVOJ: 
                            #if  vt41 % 20 == 0: 
                            ##print "Mode == MSG_SERVOJ / 16 - elif" 
                            self.robot.send_servoj(999, setpoint.positions, 4 * self.RATE)
                            ##print "setpoint.positions", setpoint.positions
                        elif self.mode_goal == MSG_STOPJ:    
                            print "Mode == MSG_STOPJ / 17 - elif"
                            self.robot.send_stopj()     
                            
                            
                            
                            
                        self.last_point_sent = True            #VT4 - comment
                    except socket.error:
                        pass

            else:  # Off the end
                #print "Off the end"
                if self.goal_handle:
                    last_point = self.traj.points[-1]
                    state = self.robot.get_joint_states()
                    #position_in_tol = within_tolerance(state.position, last_point.positions, [0.1]*6)
                    #time.sleep(2)                             # VT4 - comment
                    position_in_tol = within_tolerance(state.position, last_point.positions, [0.01]*6)
                    position_in_tol_Car = within_tolerance(self.robot.last_pose_states.pose, last_point.positions, [0.01]*6) ## OBS !!
                    velocity_in_tol = within_tolerance(state.velocity, last_point.velocities, [0.05]*6)
                    #if position_in_tol and velocity_in_tol:
                    
                    if (position_in_tol) or (position_in_tol_Car) or self.mode_goal == MSG_FORCE_START or self.mode_goal == MSG_FORCE_END or self.mode_goal == MSG_SET_TCP or self.mode_goal == MSG_GET_DATA or self.mode_goal == MSG_TEACH_START or self.mode_goal == MSG_TEACH_END:# and velocity_in_tol:    
                        # The arm reached the goal (and isn't moving).  Succeeding
                       
                        pose1 = self.robot.last_pose_states.pose
                        joints1 = self.robot.last_joint_states.position
                        #pub_getdata.publish(pose1, joints1)
                        # VT4
                        time.sleep(0.5)
                        
                        
                        #self._result.result1 = 4
                        self._result.resPos = self.robot.last_pose_states.pose
                        self._result.resJoint = self.robot.last_joint_states.position
                        #print "Result: ", self._result
                        self.goal_handle_commands.set_succeeded(self._result)       
                        self.goal_handle.set_succeeded()                   
                        
                        self.goal_handle = None
                        self.goal_handle_commands = None

                        rospy.loginfo("Command successfully executed")
                   
                  
                   
        
                    #elif now - (self.traj_t0 + last_point.time_from_start.to_sec()) > self.goal_time_tolerance.to_sec():
                    #    # Took too long to reach the goal.  Aborting
                    #    rospy.logwarn("Took too long to reach the goal.\nDesired: %s\nactual: %s\nvelocity: %s" % \
                    #                      (last_point.positions, state.position, state.velocity))
                    #    self.goal_handle.set_aborted(text="Took too long to reach the goal")
                    #    self.goal_handle = None

# joint_names: list of joints
#
# returns: { "joint_name" : joint_offset }
def load_joint_offsets(joint_names):
    robot_description = rospy.get_param("robot_description")
    soup = BeautifulSoup(robot_description)
    
    result = {}
    for joint in joint_names:
        try:
            joint_elt = soup.find('joint', attrs={'name': joint})
            calibration_offset = float(joint_elt.calibration_offset["value"])
            result[joint] = calibration_offset
        except Exception, ex:
            rospy.logwarn("No calibration offset for joint \"%s\"" % joint)
    return result


def get_my_ip(robot_ip, port):
    s = socket.create_connection((robot_ip, port))
    tmp = s.getsockname()[0]
    s.close()
    return tmp 


def main():
    global STOP
    if test_mode == False:
        rospy.init_node('ur_driver', disable_signals=True)
        if rospy.get_param("use_sim_time", False):
            rospy.logwarn("use_sim_time is set!!!")
        ## VT4
        global prevent_programming
        prevent_programming = rospy.get_param("prevent_programming", False)
        prefix = rospy.get_param("~prefix", "")
        print "Setting prefix to %s" % prefix
        global joint_names
        joint_names = [prefix + name for name in JOINT_NAMES]
        
        # Parses command line arguments
        parser = optparse.OptionParser(usage="usage: %prog robot_hostname")
        (options, args) = parser.parse_args(rospy.myargv()[1:])
        if len(args) != 1:
            parser.error("You must specify the robot hostname")
        robot_hostname = args[0]
        
        # Reads the calibrated joint offsets from the URDF
        global joint_offsets
        joint_offsets = load_joint_offsets(joint_names)
        rospy.logerr("Loaded calibration offsets: %s" % joint_offsets)
        
        # Reads the maximum velocity
        global max_velocity
        max_velocity = rospy.get_param("~max_velocity", 2.0)
        
        # Sets up the server for the robot to connect to
        server = TCPServer(("", 50001), CommanderTCPHandler)
        thread_commander = threading.Thread(name="CommanderHandler", target=server.serve_forever)
        thread_commander.daemon = True
        thread_commander.start()
        
        with open(roslib.packages.get_pkg_dir('ur_driver') + '/prog') as fin:
            program = fin.read() % {"driver_hostname": get_my_ip(robot_hostname, PORT)}
        connection = UR5Connection(robot_hostname, PORT, program)
        connection.connect()
        connection.send_reset_program()
        # VT4    
        action_server = None
        try:
            while not rospy.is_shutdown():
                # Checks for disconnect
                if STOP == 1:
                    print("STOP STOP STOP")
                    #connection.send_stop()
                    #STOP = 0
                if getConnectedRobot(wait=False) and STOP != 1:
                    time.sleep(0.2)
                    prevent_programming = rospy.get_param("prevent_programming", False)
                    if prevent_programming:
                        print "Programming now prevented"
                        connection.send_reset_program()
                elif STOP == 1:
                    connection.send_reset_program()
                    getConnectedRobot(wait=False).send_quit()
                    setConnectedRobot(None)
                    connection.connect()
                    connection.send_program()
                    STOP = 0
                else:
                    print "Disconnected.  Reconnecting"
                    if action_server:
                        action_server.set_robot(None)
        
                    rospy.loginfo("Programming the robot")
                    while True:
                        # Sends the program to the robot
                        while not connection.ready_to_program():
                            print "Waiting to program"
                            time.sleep(1.0)
                        prevent_programming = rospy.get_param("prevent_programming", False)
                        #connection.disconnect()
                        #connection = UR5Connection(robot_hostname, PORT, program)
                        #connection.connect()
                        #connection.send_reset_program()
                        connection.send_program()
        
                        r = getConnectedRobot(wait=True, timeout=3.0)
                        if r:
                            break
                    rospy.loginfo("Robot connected")
        
                    if action_server:
                        #action_server.set_robot(r)
                        action_server = UR5TrajectoryFollower(r, rospy.Duration(1.0))
                        action_server.start()
                        action_server.start_urcommands()
                    else:
                        action_server = UR5TrajectoryFollower(r, rospy.Duration(1.0))
                        action_server.start()
                        action_server.start_urcommands()
                    STOP = 0
                    
        
        except KeyboardInterrupt:
            try:
                r = getConnectedRobot(wait=False)
                rospy.signal_shutdown("KeyboardInterrupt")
                if r: r.send_quit()
            except:
                pass
            raise
            
            
            
    elif test_mode == True:
        rospy.init_node('ur_driver', disable_signals=True)
        if rospy.get_param("use_sim_time", False):
            rospy.logwarn("use_sim_time is set!!!")
        ## VT4

        time.sleep(10)
        ## VT4    
        action_server = None
        try:
            print "Try - test"
#            while not rospy.is_shutdown():
#                    print "Disconnected.  Reconnecting"
#                    if action_server:
#                        action_server.set_robot(None)
#        
#                    rospy.loginfo("Programming the robot")
#
#                    prevent_programming = rospy.get_param("prevent_programming", False)
##                        connection.send_program()
#        
##                        r = getConnectedRobot(wait=False, timeout=1.0)
##                    setConnectedRobot(connected_robot)
#                    r = getConnectedRobot(wait=True, timeout=1.0)
##                        if r:
##                            break
#                    rospy.loginfo("Robot connected")
#        
            if action_server:
                #action_server.set_robot(r)
                print "action_server - if"
            else:
                #action_server = UR5TrajectoryFollower(r, rospy.Duration(1.0))
                action_server = UR5TrajectoryFollower(rospy.Duration(1.0))
                action_server.start()
                action_server.start_urcommands()
        
        except KeyboardInterrupt:
            try:
                r = getConnectedRobot(wait=False)
                rospy.signal_shutdown("KeyboardInterrupt")
                if r: r.send_quit()
            except:
                pass
            raise
    else:
        
        print "Wrong ''test_mode'' value"



if __name__ == '__main__': main()
