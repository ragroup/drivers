#!/usr/bin/env python
import roslib; roslib.load_manifest('ur_driver')
import time, sys, threading, math
import copy
import datetime
#import socket, select
import struct
import traceback, code
import optparse
#import SocketServer
from BeautifulSoup import BeautifulSoup

import rospy
import actionlib
import std_msgs.msg
#import common_msgs.msg 
#import ur_driver.msg                   # Imports the msg "types" generated from the .action file
#import ur_driver.msg
from ur_driver.msg import urcommandsAction
from ur_driver.msg import urinfo            # Feedback from the robot/driver to the messeagetype
from sensor_msgs.msg import JointState
from control_msgs.msg import FollowJointTrajectoryAction
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from deserialize import RobotState, RobotMode


vt41 = 0 # VT4


class UR5TrajectoryFollower(object):
    RATE = 0.02
    def __init__(self, robot, goal_time_tolerance=None):
        self.goal_time_tolerance = goal_time_tolerance or rospy.Duration(0.0)
        self.joint_goal_tolerances = [0.05, 0.05, 0.05, 0.05, 0.05, 0.05]
        self.following_lock = threading.Lock()
        self.T0 = time.time()
        self.robot = robot
        self.server = actionlib.ActionServer("follow_joint_trajectory",
                                             FollowJointTrajectoryAction,
                                             self.on_goal, self.on_cancel, auto_start=False)
                                             
        self.server_urcommands = actionlib.ActionServer("ur_commands",
                                             urcommandsAction,
                                             self.on_goal_commands, 
                                             self.on_cancel_commands, auto_start=False) #VT4
         
        
#self.server = actionlib.SimpleActionServer('do_dishes', DoDishesAction, self.execute, False)                                             

#        self.server = actionlib.ActionServer("ur_info",
#                                             urinfo,
#                                             self.on_goal_commands, 
#                                             self.on_cancel_commands, auto_start=False) #VT4
        self.goal_handle = None
        self.traj = None
        self.traj_t0 = 0.0
        self.first_waypoint_id = 10
        self.tracking_i = 0
        self.pending_i = 0
        self.last_point_sent = True

        self.update_timer = rospy.Timer(rospy.Duration(self.RATE), self._update)

    def set_robot(self, robot):
        # Cancels any goals in progress
        if self.goal_handle:
            self.goal_handle.set_canceled()
            self.goal_handle = None
        self.traj = None
        self.robot = robot
        if self.robot:
            #self.init_traj_from_robot()
            print "if self.robot"


    def start(self):
        self.server.start()
        print "The action server for this driver has been started (follow_joint_trajectory)"
        #self.server_urcommands.start()
        #print "The action server for this driver has been started (ur_commands)"        

    def start_urcommands(self):
        self.server_urcommands.start()
        print "The action server for this driver has been started (ur_commands)" 
        
        
    def on_goal(self, goal_handle):
        print "on_goal"
        points_goal = goal_handle.get_goal().trajectory.points
        #print "points: ", points_goal
               
        with self.following_lock:
            if self.goal_handle:
                # Cancels the existing goal
                self.goal_handle.set_canceled()
                self.first_waypoint_id += len(self.goal_handle.get_goal().trajectory.points)
                self.goal_handle = None

            # Inserts the current setpoint at the head of the trajectory
            now = time.time()
#            point0 = sample_traj(self.traj, now)
#            point0 = [1,2,4,5,6,7]
#            point0.time_from_start = rospy.Duration(0.0)
#            goal_handle.get_goal().trajectory.points.insert(0, point0)
            self.traj_t0 = now

            # Replaces the goal
            self.goal_handle = goal_handle
            self.traj = goal_handle.get_goal().trajectory
            self.goal_handle.set_accepted()

    def on_cancel(self, goal_handle):
        if goal_handle == self.goal_handle:
            with self.following_lock:
                # Uses the next little bit of trajectory to slow to a stop
                STOP_DURATION = 0.5
                now = time.time()
#                point0 = sample_traj(self.traj, now - self.traj_t0)
                point0 = [1,2,4,5,6,7]
                point0.time_from_start = rospy.Duration(0.0)
#                point1 = sample_traj(self.traj, now - self.traj_t0 + STOP_DURATION)
                point1 = [1,2,4,5,6,7]
                point1.velocities = [0] * 6
                point1.accelerations = [0] * 6
                point1.time_from_start = rospy.Duration(STOP_DURATION)
                self.traj_t0 = now
                self.traj = JointTrajectory()
#                self.traj.joint_names = joint_names
                self.traj.points = [point0, point1]
                
                self.goal_handle.set_canceled()
                self.goal_handle = None
        else:
            goal_handle.set_canceled()

            
#VT4
    def on_goal_commands(self, goal_handle):
        #goal_handle.get_goal().trajectory
        mode_goal = goal_handle.get_goal().mode
        mass_goal = goal_handle.get_goal().mass
        tcppose_goal = goal_handle.get_goal().tcppose
        tcpcog_goal = goal_handle.get_goal().tcpcog
        selection_goal = goal_handle.get_goal().selection
        wrench_goal = goal_handle.get_goal().wrench
        limits_goal = goal_handle.get_goal().limits
        
        print "mode: ", mode_goal
        print "mass: ", mass_goal
        print "tcppose: ", tcppose_goal
        print "tcpcog: ", tcpcog_goal
        print "selection: ", selection_goal
        print "wrench: ", wrench_goal
        print "limits: ", limits_goal
        
        # Checks that the robot is connected
        print "on_goal_commands"
        
        time.sleep(2)
        self.goal_handle.set_succeeded
        print "goal_handle.set_succeeded - executed"

    def on_cancel_commands(self, goal_handle):
        print "on_cancel_commands"
        
#VT4            


    last_now = time.time()
    def _update(self, event):
       
        if self.robot and self.traj:
            now = time.time()
            if (now - self.traj_t0) <= self.traj.points[-1].time_from_start.to_sec():
                self.last_point_sent = False #sending intermediate points
#                setpoint = sample_traj(self.traj, now - self.traj_t0)
                try:
                    global vt41
                    vt41 += 1
                    if ur_commands.mode == 1:
#                        if  vt41 % 20 == 0: 
#                            self.robot.send_movej(999, setpoint.positions, 4 * self.RATE)
                        print "ur_commands.mode ==1"
                    elif ur_commands.mode == 2:    
                        print "Mode == 2"
#                        if  vt41 % 20 == 0: 
#                            self.robot.send_movel(999, setpoint.positions, 4 * self.RATE)
                    elif ur_commands.mode == 3:    
                        print "Mode == 3"
                    elif ur_commands.mode == 4:    
                        print "Mode == 4"
                    elif ur_commands.mode == 5:    
                        print "Mode == 5"
                    elif ur_commands.mode == 6:    
                        print "Mode == 6"
                    elif ur_commands.mode == 7:    
                        print "Mode == 7" 
                    elif ur_commands.mode == 8:    
                        print "Mode == 8"
                    elif ur_commands.mode == 9:    
                        print "Mode == 9"
                    elif ur_commands.mode == 10:    
                        print "Mode == 10"  
                    elif ur_commands.mode == 11:    
                        print "Mode == 11"         
                
                except:
                    pass
                
                    
            else:  # Off the end
                print "else - (now - self.traj_t0) <= self.traj.points[-1].time_from_start.to_sec(): " 
               


def main():
        rospy.init_node('ur_driver', disable_signals=True)
        print "start of main"
        action_server = None
        r = None
        try:
#                        action_server = UR5TrajectoryFollower(r, rospy.Duration(1.0))
                        action_server = UR5TrajectoryFollower(r, rospy.Duration(1.0))
                        action_server.start()
                        action_server.start_urcommands()
                        print "action_server.start executed"
        
        except KeyboardInterrupt:
            try:
                #r = getConnectedRobot(wait=False)
                rospy.signal_shutdown("KeyboardInterrupt")
#                if r: r.send_quit()
            except:
                pass
            raise



if __name__ == '__main__': main()
            