#############################################################################
### DESCRIPTION
#############################################################################
# authours:  Morten Palmelund-Jensen, Rune Etzerodt, Casper Abildgaard Pedersen
#
# This script is transfered to the Universal Robots controller, by use of the
# program "urinterface.cpp". The script creates a socket client on the controller
# trying to connect to server. When the connection is established the script
# listens for incomming commands which are executed.

def myprog():
#############################################################################
### DEFINITIONS / DECLARATIONS
#############################################################################
    set_digital_out(0, True)
    global qtarget = [ 0,0,0,0,0,0]
    global qtarget1 = [ 0,0,0,0,0,0]
    global posetarget = [ 0,0,0,0,0,0]
    global dqtarget = [ 0,0,0,0,0,0 ]
    global speed = 0.75
    global thrd  = -1
    global motionFinished = 1
    global isServoing = 0
    global isStopped = 1
    global receive_buffer = [8, 0, 0, 0, 0, 0, 0, 0, 0]
    global FLOAT_SCALE = 0.00001
    # Force
    global receive_buffer18 = [18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    global force_selection = [ 0,0,0,0,0,0]
    global wrench = [ 0,0,0,0,0,0]
    global force_limits = [ 0,0,0,0,0,0]
    global force_frame = p[0,0,0,0,0,0]
    global tcp_pose = p[0,0,0,0,0,0]
    global mass = 0
    global cog = [0,0,0]


#############################################################################
### FUNCTIONS
#############################################################################
# Function for stopping the manipulator
    def stopRobot():
        enter_critical
        if thrd != -1:
            kill thrd
            thrd = -1
        end
        exit_critical
        #textmsg("Stop Robot")
        stopj(10)
        isServoing = 0
        isStopped = 1
    end


# Thread for running the moveJ command, with joint input - start
    thread moveJthread():
        textmsg("Calls moveJ, with joint input")
        textmsg(qtarget)
        textmsg(speed)
        movej(qtarget, 3, receive_buffer[8]*FLOAT_SCALE)		# moveJ with qtarget as joint position, 3 as acceleartion, receive[8] as speed
        textmsg("MoveJ with joint input called")
        enter_critical
            thrd = -1
            motionFinished = 1
        exit_critical
        textmsg("MoveJ with joint input done")
    end
# Thread for running the moveJ command, with joint input - end


# Thread for running the moveL command, with joint input - start
    thread moveLQthread():
        textmsg("Calls moveL, with join input")
        movel(qtarget, 1.2, receive_buffer[8]*FLOAT_SCALE)		# moveL with qtarget as joint position, 1.2 as acceleartion, receive[8] as speed
        textmsg("MoveL with joint input called")
        enter_critical
            thrd = -1
            motionFinished = 1
        exit_critical
        textmsg("MoveL with joint input done")
     end
# Thread for running the moveL command, with joint input - end


# Thread for running the moveJ command with Cartesian input - start
    thread moveJCarthread():
        textmsg("Calls moveJ, with Car input")
	# moveJ with Cartesian input, 3 as acceleration and receive[8] as speed
        movej(p[receive_buffer[2]*FLOAT_SCALE,receive_buffer[3]*FLOAT_SCALE,receive_buffer[4]*FLOAT_SCALE,receive_buffer[5]*FLOAT_SCALE,receive_buffer[6]*FLOAT_SCALE,receive_buffer[7]*FLOAT_SCALE],3,receive_buffer[8]*FLOAT_SCALE)
        textmsg("MoveJ with Car called")
        enter_critical
            thrd = -1
            motionFinished = 1
        exit_critical
        textmsg("MoveJ with Car input done")
     end
# Thread for running the moveJ command with Cartesian input - end


# Thread for running the moveL command with Cartesian input - start
    thread moveLCarthread():
        textmsg("Calls moveL, with Car input")
        # moveL with Cartesian input, 1.2 as acceleration and receive[8] as speed
        movel(p[receive_buffer[2]*FLOAT_SCALE,receive_buffer[3]*FLOAT_SCALE,receive_buffer[4]*FLOAT_SCALE,receive_buffer[5]*FLOAT_SCALE,receive_buffer[6]*FLOAT_SCALE,receive_buffer[7]*FLOAT_SCALE],1.2,receive_buffer[8]*FLOAT_SCALE)
        textmsg("MoveL with Car called")
        enter_critical
            thrd = -1
            motionFinished = 1
            #socket_send_line("motionFinished1")
        exit_critical
        textmsg("MoveL with Car input done")
     end
# Thread for running the moveL command with Cartesian input - end


# Threads above     //////////////////////////////
# Definitions below //////////////////////////////

# Definition for moveJoint - start
    def moveJoint():
        textmsg("MoveJoint")
        cnt = 0
        enter_critical
            motionFinished = 0
            while cnt < 6:
                qtarget[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to qtarget
                cnt = cnt + 1
            end
        exit_critical
        textmsg(qtarget)
        speed = receive_buffer[8]*FLOAT_SCALE				# This line are redudant
        textmsg("speed ")
        textmsg(speed)
        enter_critical
            if thrd == -1:
                   if receive_buffer[1] == 1:				# Determines if the move shoud be linear or joint from the received string
                        textmsg("run moveJQthread")
                        thrd = run moveJthread()
                   elif receive_buffer[1] == 2:
                        textmsg("run moveLQthread")
                        thrd = run moveLQthread()
                   end
            end
        exit_critical
    end # def moveJoint - end
# Definition for moveJoint - end


# Definition for moveCar - start
    def moveCar():
        textmsg("MoveCar")
        cnt = 0
      enter_critical
        motionFinished = 0
      exit_critical
        speed = receive_buffer[8]*FLOAT_SCALE				# This line are redudant
        textmsg("speed ")
        textmsg(speed)

        enter_critical
            if thrd == -1:
                    if receive_buffer[1] == 3:				# Determines if the move shoud be linear or joint from the received string
                       textmsg("run moveJCarthread")
                       thrd = run moveJCarthread()
                       #thrd = run test1()
                    elif receive_buffer[1] == 4:
                       textmsg("run moveLCarthread")
                       #thrd = run moveJthread()
                       thrd = run moveLCarthread()
                    end
            end
        exit_critical
    end # def moveCar - end
# Definition for moveCar - end


# Definition for force_mode_start - start
    def force_mode_start():
        textmsg("force_mode_start")
        cnt = 0
        while cnt < 6:
            force_frame[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to force_frame
            cnt = cnt + 1
        end
        receive_buffer18 = socket_read_binary_integer(18)
        cnt = 0
        while cnt < 6:
            force_selection[cnt] = receive_buffer18[cnt+1]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_selection
            wrench[cnt] = receive_buffer18[cnt+1+6]*FLOAT_SCALE		# Assigns values from receive_buffer18 to wrench
            force_limits[cnt] = receive_buffer18[cnt+1+12]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_limits
            cnt = cnt + 1
        end

        textmsg("Force Frame/task_frame: ")
        textmsg(force_frame)
        textmsg("Force Selection/selection_vector: ")
        textmsg(force_selection)
        textmsg("Wrench:")
        textmsg(wrench)
        textmsg("Force Limits/limits:")
        textmsg(force_limits)
        force_mode(force_frame, force_selection, wrench, 2, force_limits)

    end # force_mode_start() - end
# Definition for force_mode_start - end


# Definition for force_mode_update - start
    def force_mode_update():
        textmsg("force_mode_update")
        cnt = 0
        while cnt < 6:
            force_frame[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to force_frame
            cnt = cnt + 1
        end
        receive_buffer18 = socket_read_binary_integer(18)
        cnt = 0
        while cnt < 6:
            force_selection[cnt] = receive_buffer18[cnt+1]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_selection
            wrench[cnt] = receive_buffer18[cnt+1+6]*FLOAT_SCALE		# Assigns values from receive_buffer18 to wrench
            force_limits[cnt] = receive_buffer18[cnt+1+12]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_limits
            cnt = cnt + 1
        end
        textmsg("Wrench Update:")
        textmsg(wrench)

        force_mode(force_frame, force_selection, wrench, 2, force_limits)

    end # force_mode_update() -end
# Definition for force_mode_update - end


# Definition for force_mode_end - start
    def force_mode_end():
        textmsg("force_mode_end")
        end_force_mode()						# Forcemode end
    end # force_mode_end() - end
# Definition for force_mode_end - end

    # All code conserning servo have been deleted


# Thread for setting the TCP - start
    def setTCP():
        textmsg("setTCP")
        cnt = 0
        while cnt < 6:
            tcp_pose[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to force_frame
            cnt = cnt + 1
        end
        mass = receive_buffer[8]*FLOAT_SCALE

        receive_buffer18 = socket_read_binary_integer(18)
        cnt = 0
        while cnt < 3:
            cog[cnt] = receive_buffer18[cnt+1]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_selection
            cnt = cnt + 1
        end

        textmsg("tcp_pose")
        textmsg(tcp_pose)

        textmsg("mass")
        textmsg(mass)

        textmsg("cog")
        textmsg(cog)
        
        set_tcp(tcp_pose)
        set_payload(mass, cog)
    end
# Thread for running the moveJ command, with joint input - end
#############################################################################
### MAIN
#############################################################################
    #Setup the host name
    #movej(p [-0.44, -0.150,0.77,-0.5,-2.5,0.9]) # Cartesian point
    host = "192.168.0.127"
    port1 = PORT
    port = 33334
    opened = socket_open(host, port)					# Opens a socket connection to connect to a PC
    textmsg("Socket Status")
    textmsg(opened)

    while opened == False:
        opened = socket_open(host, port)				# Continues to repoen a socket connection until a conection is established 
    end

    textmsg("Socket opened !!")

    errcnt = 0
    while errcnt < 1:
                	# Determines the type of feedback (joint, TCP (force - UNDER DEVELOPENT))
        if receive_buffer[1] == 1:
            textmsg("mode = 1")
            socket_send_line(get_actual_joint_positions())
        elif receive_buffer[1] == 2:
            textmsg("mode = 2")
            socket_send_line(get_actual_joint_positions())
        elif receive_buffer[1] == 3:
            textmsg("mode = 3")
            socket_send_line(get_actual_tcp_pose())
        elif receive_buffer[1] == 4:
            textmsg("mode = 4")
            socket_send_line(get_actual_tcp_pose())
        elif receive_buffer[1] == 6:
            textmsg("mode = 6")
            socket_send_line(get_actual_tcp_pose())
        elif receive_buffer[1] == 7:
            textmsg("mode = 7")
            socket_send_line(get_actual_tcp_pose())
        elif receive_buffer[1] == 8:
            textmsg("mode = 8")
            socket_send_line(get_actual_tcp_pose())
        elif receive_buffer[1] == 10:
            textmsg("mode = 10")

            enter_critical
                socket_send_string(get_actual_tcp_pose())           ## Good
                socket_send_string(get_actual_joint_positions())      ## Good
                socket_send_string("C")
            exit_critical

        else:

        end


        receive_buffer = socket_read_binary_integer(8)
	# The following determines what functions there should be called
        if receive_buffer[0] != 8:			# Ensures that the received string have the correct length
                   stopRobot()
                   #errcnt = errcnt + 1 # VT3
        elif receive_buffer[1] == 0:                #0: Stop Robot
                if isStopped == 0:
                    stopRobot()
                end
        elif receive_buffer[1] == 1:                #1: Move to Q
                 isStopped = 0
                moveJoint()
        elif receive_buffer[1] == 2:                #1: Move to Q
                isStopped = 0
                moveJoint()
        elif receive_buffer[1] == 3:                #3: Move to Q
                isStopped = 0
                moveCar()
        elif receive_buffer[1] == 4:                #4: Move to T
                isStopped = 0
                moveCar()
        elif receive_buffer[1] == 5:                #5: Get force
                socket_send_line(get_tcp_force())
        elif receive_buffer[1] == 6:                #6: Force mode start
                force_mode_start()
        elif receive_buffer[1] == 7:                #7: Force mode update
                force_mode_update()
        elif receive_buffer[1] == 8:                #8: Force mode end
                force_mode_end()
        elif receive_buffer[1] == 9:                #9: Close
                errcnt = errcnt + 1
        elif receive_buffer[1] == 11:               #11: Set ToolCenterPoint
                textmsg("mode = 11")
                setTCP()
        elif receive_buffer[1] == 9999:             #1: Do nothing
                isStopped = 0
        end

    end #end for While True:
    textmsg("Program Finished")
end
run program
