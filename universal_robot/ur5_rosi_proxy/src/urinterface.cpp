/////////////////////////////////////////////////////////////////////////////
// DESCRIPTION
/////////////////////////////////////////////////////////////////////////////
/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2013,  Morten Palmelund-Jensen, Rune Etzerodt,           *
 *  Casper Abildgaard Pedersen                                             *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************

    This interface connect to a socket connection created by the Universal Robots
    controller, and sends the urscript.hpp.

    urscript.hpp is created from the urscript.ur and the CMakeLists.txt in the package
*/

/* Further improvements
- Specify the different ip's and port's in one place
- Make socket functions
- Make send functions
- Make this entire program as a function (by use of a header file)
*/

/////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////
#include "ros/ros.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <boost/asio.hpp>
#include "ur5_rosi_proxy/urscript.hpp"           // Including the urscript.ur converted to a .hpp file (by use of the CMakeLists.txt in the package)

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// DEFINITIONS / DECLARATIONS
/////////////////////////////////////////////////////////////////////////////
#define SERVER_PORT htons(33334)


/////////////////////////////////////////////////////////////////////////////
// MAIN
/////////////////////////////////////////////////////////////////////////////
int main()
{
            unsigned int callbackPort = 33334;              // The port number is determined from "simpleTest.cpp"

            // Changes the port number in the script
            std::string script = UR_SCRIPT;                 // From RobWork: URCBI.cpp
            int n2 = script.find("PORT");                   // From RobWork: URCBI.cpp
            std::stringstream sstr;                         // From RobWork: URCBI.cpp
            sstr<<script.substr(0, n2)<<callbackPort<<script.substr(n2+4); // The script is searched for PORT and this is replaced with "callbackPort"

            // Socket connection, send and close - begin
            int sockfd, portno;
            struct sockaddr_in serv_addr;
            struct hostent *server;

            //portno = 1080;                                // Port number: TEST PORT - SocketTest program
            portno = 30002;                                 // Port number: UR5 robot
            sockfd = socket(AF_INET, SOCK_STREAM, 0);

            //server = gethostbyname("127.0.0.1");          // IP address: TEST IP - SocketTest program
            server = gethostbyname("192.168.0.18");         // IP address: UR5 robot

            if (server == NULL) {
            fprintf(stderr,"ERROR, no such host\n");
            exit(0);
            }

            bzero((char *) &serv_addr, sizeof(serv_addr));  // Placese zero valued bytes in each adress pointed to
            serv_addr.sin_family = AF_INET;                 // TCP/IP 4 (IPv4) family
            bcopy(  (char *)server->h_addr,                 // bcopy(*s1,*s2,n): copies n bytes from s1 to s2
                    (char *)&serv_addr.sin_addr.s_addr,
                     server->h_length);
            serv_addr.sin_port = htons(portno);             // The port which the socket "listens" to
            // Connects to a socket: connect("unconnected socket","pointer to structure of where the connection should be", "length of the pointer"
            //connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr));


	    boost::asio::io_service io_service;	    

            boost::asio::ip::tcp::socket _socket(io_service);
	    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string("192.168.0.18"), 30002);

            _socket.connect(endpoint);


	    ROS_INFO("Send script");

            //_socket->send(boost::asio::buffer(sstr.str(), sstr.str().size()));

	_socket.send(boost::asio::buffer(sstr.str(), sstr.str().size()));

		//send(sockfd, sstr.str(), sstr.str().size(), 0);

		//boost::asio::write(*_socket,boost::asio::buffer(sstr.str(), sstr.str().size()) );
            ROS_INFO("Close socket");
           _socket.close();            

//close(sockfd);                              // Close the socket connection


  return 0;
} // main - end

