/////////////////////////////////////////////////////////////////////////////
// DESCRIPTION
/////////////////////////////////////////////////////////////////////////////
/***************************************************************************
 * Software License Agreement (BSD License)                                *
 *                                                                         *
 *  Copyright (c) 2013,  Morten Palmelund-Jensen, Rune Etzerodt,           *
 *  Casper Abildgaard Pedersen                                             *
 *	Department of Mechanical and Manufacturing Engineering                 *
 *  Aalborg University, Denmark                                            *
 *  All rights reserved.                                                   *
 *                                                                         *
 *  Redistribution and use in source and binary forms, with or without     *
 *  modification, are permitted provided that the following conditions     *
 *  are met:                                                               *
 *                                                                         *
 *  - Redistributions of source code must retain the above copyright       *
 *     notice, this list of conditions and the following disclaimer.       *
 *  - Redistributions in binary form must reproduce the above              *
 *     copyright notice, this list of conditions and the following         *
 *     disclaimer in the documentation and/or other materials provided     *
 *     with the distribution.                                              *
 *  - Neither the name of Aalborg University nor the names of              *
 *     its contributors may be used to endorse or promote products derived *
 *     from this software without specific prior written permission.       *
 *                                                                         *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    *
 *  'AS IS' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS      *
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE         *
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,    *
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,   *
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;       *
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER       *
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT     *
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN      *
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE        *
 *  POSSIBILITY OF SUCH DAMAGE.                                            *
 ***************************************************************************


    This proxy handles the connection betweeen the Master executor and a script
    on an Universal Robots manipulator.

    The proxy is connected to the master via ROS actionlib, receiving  generic
    hardware independent goals and sending feedback and result to the master.
    The syntax of the goals are translated to hardware specific commands for
    the Universal Robots manipualtor.
    The connetion to the manipulator is handled by a socket connection, created
    on the manipulator by sending a script (urscript.ur and urinterface.cpp)

*/

/* Future improvements
    - Send the script to the robot as a part of this program, to make it more simple to connect to the robot
    - Condition check of the robot? (Is the robot moving, ES, ect.)
    - Test the effect of "ros::Duration(100.0);"
    - Implement blend and acc
    - Implement moveC, moveP, servoX functions
    - Implement force feedback / result
    - Determine how to "release"/know when the robot are within the tolerencens after a force function
    - Make it possible to receive joint feedback without the m to mm convension
*/

/////////////////////////////////////////////////////////////////////////////
// INCLUDES
/////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <algorithm>    // std::search
#include <sstream>      // std::stringstream
#include <math.h>       // ceil
#include <vector>
#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <ur5_rosi_proxy/ur5Action.h>

// VT4
#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <ur_driver/urcommandsAction.h>
//#include "send_script_ur5.hpp"  // UNDER DEVELOPMENT - the possibility to send the URscrip from inside this proxy

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// DEFINITIONS / DECLARATIONS
/////////////////////////////////////////////////////////////////////////////
#define SERVER_PORT htons(33334)            // Port used in socket connection to robot
//#define SERVER_PORT htons(1080)           // TEST Port used in socket connection
#define PI 3.14159265359                    // Pi
#define float_scale 100000                  // Float scale to avoid rounding of decimal points
#define mmm 0.001                           // mm to m convension

// typedef = Definition of "nicknames" for datatypes
typedef actionlib::SimpleActionServer<ur5_rosi_proxy::ur5Action> Server;     // namespace::class <  >
typedef ur5_rosi_proxy::ur5Feedback feedback_;
typedef ur5_rosi_proxy::ur5Result result_;
// VT4
typedef actionlib::SimpleActionClient< trajectory_msgs::JointTrajectory > TrajClient;
typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction > FJTClient;
typedef actionlib::SimpleActionClient<ur_driver::urcommandsAction> URCommands;

// Definition of global variables, used in different functions
std::string movetype, inputtype;
int mode;                                   // Hardware specific
double joint[6], joint1, joint2, joint3, joint4, joint5, joint6, speed;
int selection_vector[6];
int wrench[6];
int force_limits[6];
int acc=0, blend=0;
bool test_mode = false; // testmode makes it possible to execute this file without beeing connected to the robot (disables socket connections and send functions)



/////////////////////////////////////////////////////////////////////////////
// FUNCTIONS
/////////////////////////////////////////////////////////////////////////////
float Deg2Rad (float a) // Convert from deg to rad
{
  float r;
  r=(a/180)*PI;
  return (r);
} // float Deg2Rad - end



void send_move(int clientSock, char * buf, int offset, int mode,int joint[6],int speed,int acc,int blend)
// Sends the move data to the robot via a socket connection
{
    // htonl converts integer from "host byte order" to "network byte order" - necessary for the robot to accept the formatting of the buffer.
    // Below is an assignment of the function inputs to the char pointer "buf"
    *((int *)(buf + offset)) = htonl(mode);
    *((int *)(buf + 4)) = htonl(joint[0]);
    *((int *)(buf + 8)) = htonl(joint[1]);
    *((int *)(buf + 12)) = htonl(joint[2]);
    *((int *)(buf + 16)) = htonl(joint[3]);
    *((int *)(buf + 20)) = htonl(joint[4]);
    *((int *)(buf + 24)) = htonl(joint[5]);
    *((int *)(buf + 28)) = htonl(speed);

    send(clientSock, buf, 32, 0);               // Sends a pointer of a buffer via a socket connection
    // send("connected socket", "pointer to a buffer", "length in bytes of data in buffer pointed to","flags to specify how the call is made")


    //our goal variable
    //trajectory_msgs::JointTrajectoryPoint goal;
    trajectory_msgs::JointTrajectory goal;


    // First, the joint names, which apply to all waypoints
    goal.joint_names.push_back("shoulder_pan_joint");
    goal.joint_names.push_back("shoulder_lift_joint");
    goal.joint_names.push_back("elbow_joint");
    goal.joint_names.push_back("wrist_1_joint");
    goal.joint_names.push_back("wrist_2_joint");
    goal.joint_names.push_back("wrist_3_joint");

    // We will have two waypoints in this goal trajectory
    //goal.points.resize(2);
    goal.points.resize(3);


    // First trajectory point
    // Positions
    int ind = 0;
    goal.points[ind].positions.resize(6);
    for (int i = 0; i < 6; ++i) {
        goal.points[ind].positions[i] = joint[i];
    }
    /*
    goal.points[ind].positions[0] = -0.2;
    goal.points[ind].positions[1] = -1.5;
    goal.points[ind].positions[2] = -1.67;
    goal.points[ind].positions[3] = -1.4;
    goal.points[ind].positions[4] = 1.5;
    goal.points[ind].positions[5] = 0.6;
    */

    // Velocities
    goal.points[ind].velocities.resize(6);
    for (size_t j = 0; j < 6; ++j)
    {
      goal.points[ind].velocities[j] = 0.0;
    }
    // To be reached 1 second after starting along the trajectory
    goal.points[ind].time_from_start = ros::Duration(1.0);

    //we are done; return the goal
    //return goal;

} // void send_move - end



void resend(int clientSock, char * buf, int offset, int mode,int joint[6],int speed,int acc,int blend,  Server* as)
// Resends the "send_move" command untill all joints are within a specified tolerence of the wanted pose.
// Publish feedback through the actionlib for each run in the while loop
{
    int cnt=0, atest=0;
    char receivedStr[2000];
    //double interval = 0.008;
    double interval = 0.004;
    cout << "Resending move command...\n";
    while (atest == 0) {

        send_move(clientSock, buf, offset, mode, joint,speed,acc,blend);
        //sleep(0.5);
        sleep(0.5);
        recv(clientSock, receivedStr, 1600, 0);         // The received feedback
        string str(receivedStr);                        // The received feedback as an string
        cnt = cnt +1;
        *receivedStr = 0;

        double joint1Int = (double)joint[0]/float_scale; // Conversion from int to double of the goal for movment
        double joint2Int = (double)joint[1]/float_scale;
        double joint3Int = (double)joint[2]/float_scale;
        double joint4Int = (double)joint[3]/float_scale;
        double joint5Int = (double)joint[4]/float_scale;
        double joint6Int = (double)joint[5]/float_scale;


       // A "string2double" conversion
        if (mode==1 || mode==2) {
            str.erase (0,1);                            // The first character is erased if the feedback is in joint values [j1,j2,j3,j4,j5,j6]
        } else {
            str.erase (0,2);                            // The two characters is erased if the feedback is in Cartesina values p[x,y,z,rx,ry,rz]
        }
            std::size_t pos = 0;
            std::vector <double>vd;
            double d = 0.0;
            while (pos < str.size ())                   // The string searched for ',' and repacled with ' '
                if ((pos = str.find_first_of (',',pos)) != std::string::npos)
                    str[pos] = ' ';

            std::stringstream ss(str);

            while (ss >> d)                             // Each whitespace seperate value are saved vector containing the feedback
                vd.push_back (d);

            double joint1act,joint2act,joint3act,joint4act,joint5act,joint6act;
            if (vd.size() == 6) {                       // The vector containing the feedback are saved to an array
                joint1act = vd.at(0);
                joint2act = vd.at(1);
                joint3act = vd.at(2);
                joint4act = vd.at(3);
                joint5act = vd.at(4);
                joint6act = vd.at(5);
            } else {
                std::cout << "vd not = 6    " << std::endl;
                joint1act = 10000;
                joint2act = 10000;
                joint3act = 10000;
                joint4act = 10000;
                joint5act = 10000;
                joint6act = 10000;
            }


            double joint1Max, joint1Min, joint2Max, joint2Min, joint3Max, joint3Min, joint4Max, joint4Min, joint5Max, joint5Min, joint6Max, joint6Min;

            // The allowable intervals for the actual position/joint value of the robot compared to the wanted postion/joint values
            joint1Max = joint1Int + interval;
            joint1Min = joint1Int - interval;
            joint2Max = joint2Int + interval;
            joint2Min = joint2Int - interval;
            joint3Max = joint3Int + interval;
            joint3Min = joint3Int - interval;
            joint4Max = joint4Int + interval;
            joint4Min = joint4Int - interval;
            joint5Max = joint5Int + interval;
            joint5Min = joint5Int - interval;
            joint6Max = joint6Int + interval;
            joint6Min = joint6Int - interval;


            ur5_rosi_proxy::ur5Feedback feedback;
            feedback.currentPos[0] = joint1act/mmm;     // This convension is only valid for Cartesian feedback
            feedback.currentPos[1] = joint2act/mmm;     // This convension is only valid for Cartesian feedback
            feedback.currentPos[2] = joint3act/mmm;     // This convension is only valid for Cartesian feedback
            feedback.currentPos[3] = joint4act;
            feedback.currentPos[4] = joint5act;
            feedback.currentPos[5] = joint6act;
            as->publishFeedback(feedback);              // Feedback of the current postion of the manipulator is send to the actionlib

            // If the current postion of the manipulator is within the specified interval, the while-loop is terminated, if not the loop continues
            if(joint1Min < joint1act && joint1act < joint1Max
                    && joint2Min < joint2act && joint2act < joint2Max
                    && joint3Min < joint3act && joint3act < joint3Max
                    && joint4Min < joint4act && joint4act < joint4Max
                    && joint5Min < joint5act && joint5act < joint5Max
                    && joint6Min < joint6act && joint6act < joint6Max){


                        atest = 1;
                        cout << "All joints found \n \n";
             }else {
                        //sleep(0.5);
                        sleep(1.0);
            }// else - end

    } //  while (atest == 0) - end

} // void resend - end


void send_tcp(int clientSock, char * buf, int offset, int mode, int tcp[], int mass,int cog[], Server* as)

{
    cout << "mass ", mass, "\n";

    // htonl converts integer from "host byte order" to "network byte order" - necessary for the robot to accept the formatting of the buffer.
    // Below is an assignment of the function inputs to the char pointer "buf"
    *((int *)(buf + offset)) = htonl(mode);
    *((int *)(buf + 4)) = htonl(tcp[0]);
    *((int *)(buf + 8)) = htonl(tcp[1]);
    *((int *)(buf + 12)) = htonl(tcp[2]);
    *((int *)(buf + 16)) = htonl(tcp[3]);
    *((int *)(buf + 20)) = htonl(tcp[4]);
    *((int *)(buf + 24)) = htonl(tcp[5]);
    *((int *)(buf + 28)) = htonl(mass);
    //*((int *)(buf + 32)) = htonl(cog[0]);
    //*((int *)(buf + 36)) = htonl(cog[1]);
    //*((int *)(buf + 40)) = htonl(cog[2]);

    send(clientSock, buf, 32, 0);                    // Sends a pointer of a buffer via a socket connection

    *((int *)(buf + offset)) = htonl(cog[0]);
    *((int *)(buf + 4)) = htonl(cog[1]);
    *((int *)(buf + 8)) = htonl(cog[2]);



    send(clientSock, buf, 12, 0);                    // Sends a pointer of a buffer via a socket connection


    // send("connected socket", "pointer to a buffer", "length in bytes of data in buffer pointed to","flags to specify how the call is made")

} // void send_tcp - end


void send_force(int clientSock, char * buf, int offset, int selection_vector[], int wrench[],int force_limits[])
// Like "send_move" but concerning parameters needed for execution of a force movement, without the task_frame (robot pose)
{
    // htonl converts integer from "host byte order" to "network byte order" - necessary for the robot to accept the formatting of the buffer.
    // Below is an assignment of the function inputs to the char pointer "buf"
    *((int *)(buf + offset)) = htonl(selection_vector[0]);
    *((int *)(buf + 4)) = htonl(selection_vector[1]);
    *((int *)(buf + 8)) = htonl(selection_vector[2]);
    *((int *)(buf + 12)) = htonl(selection_vector[3]);
    *((int *)(buf + 16)) = htonl(selection_vector[4]);
    *((int *)(buf + 20)) = htonl(selection_vector[5]);
    *((int *)(buf + 24)) = htonl(wrench[0]);
    *((int *)(buf + 28)) = htonl(wrench[1]);
    *((int *)(buf + 32)) = htonl(wrench[2]);
    *((int *)(buf + 36)) = htonl(wrench[3]);
    *((int *)(buf + 40)) = htonl(wrench[4]);
    *((int *)(buf + 44)) = htonl(wrench[5]);
    *((int *)(buf + 48)) = htonl(force_limits[0]);
    *((int *)(buf + 52)) = htonl(force_limits[1]);
    *((int *)(buf + 56)) = htonl(force_limits[2]);
    *((int *)(buf + 60)) = htonl(force_limits[3]);
    *((int *)(buf + 64)) = htonl(force_limits[4]);
    *((int *)(buf + 68)) = htonl(force_limits[5]);

    send(clientSock, buf, 72, 0);                    // Sends a pointer of a buffer via a socket connection
    // send("connected socket", "pointer to a buffer", "length in bytes of data in buffer pointed to","flags to specify how the call is made")

} // void send_force - end



void publishResult(int clientSock, char * buf, int offset, int mode,int joint[6],int speed,int acc,int blend,  Server* as)
// Sends the last command to the robot again and reads the output string from the robot and publishes the result thrugh the action lib
{
        char receivedStr[2000];

        send_move(clientSock, buf, offset, mode, joint,speed,acc,blend);
        sleep(0.5);
        recv(clientSock, receivedStr, 800, 0);          // The received feedback
        string str(receivedStr);                        // The received feedback as an string
        *receivedStr = 0;

       // A "string2double" conversion is conducted below
        if (mode==1 || mode==2) {
            str.erase (0,1);                            // The first character is erased if the feedback is in joint values [j1,j2,j3,j4,j5,j6]
        } else {
            str.erase (0,2);                            // The two characters is erased if the feedback is in Cartesina values p[x,y,z,rx,ry,rz]
        }
        std::size_t pos = 0;
        std::vector <double>vd;
        double d = 0.0;
        while (pos < str.size ())                       // The string searched for ',' and repacled with ' '
            if ((pos = str.find_first_of (',',pos)) != std::string::npos)
                str[pos] = ' ';

        while (pos < str.size ())                       // The string searched for '[' and repacled with ' '
            if ((pos = str.find_first_of ('[',pos)) != std::string::npos)
                str[pos] = ' ';

        std::stringstream ss(str);

        while (ss >> d)                                 // Each whitespace seperate value are saved vector containing the feedback
            vd.push_back (d);

        double joint1act,joint2act,joint3act,joint4act,joint5act,joint6act;
        if (vd.size() == 6) {                           // The vector containing the feedback are saved to an array
            joint1act = vd.at(0);
            joint2act = vd.at(1);
            joint3act = vd.at(2);
            joint4act = vd.at(3);
            joint5act = vd.at(4);
            joint6act = vd.at(5);
        } else {
            std::cout << "ERROR, the string from the robot do NOT contain 6 coordinates" << std::endl;
            joint1act = 10000;
            joint2act = 10000;
            joint3act = 10000;
            joint4act = 10000;
            joint5act = 10000;
            joint6act = 10000;
        }

        ur5_rosi_proxy::ur5Result result;
        result.resPos[0] = joint1act/mmm;
        result.resPos[1] = joint2act/mmm;
        result.resPos[2] = joint3act/mmm;
        result.resPos[3] = joint4act;
        result.resPos[4] = joint5act;
        result.resPos[5] = joint6act;
        as->setSucceeded(result);                       // Result of the postion of the manipulator is send to the actionlib

} // void publishResult - end

void publishData(int clientSock, char * buf, int offset, int mode,int joint[6],int speed,int acc,int blend,  Server* as)
// Sends the last command to the robot again and reads the output string from the robot and publishes the result thrugh the action lib
{
        char receivedStr[2000];

        //send_move(clientSock, buf, offset, mode, joint,speed,acc,blend);
        //sleep(0.5);
        sleep(1);
        recv(clientSock, receivedStr, 1600, 0);          // The received feedback
        cout << "recv HAVE BEEN executed \n" ;
        string str(receivedStr);                        // The received feedback as an string
        cout << "receivedStr - string stream = "<<receivedStr << "\n" ;
        *receivedStr = 0;

       // A "string2double" conversion is conducted below
       /*
        if (mode==1 || mode==2) {
            str.erase (0,1);                            // The first character is erased if the feedback is in joint values [j1,j2,j3,j4,j5,j6]
        } else {
            str.erase (0,2);                            // The two characters is erased if the feedback is in Cartesina values p[x,y,z,rx,ry,rz]
        }
        */

        std::size_t pos = 0;
        std::size_t posbrac = 0;
        int  ipos=0, iposbrac=0;
        std::vector <double>vd;
        double d = 0.0;
        while (pos < str.size ())
            if ((pos = str.find_first_of (',',pos)) != std::string::npos)
                str[pos] = ' ';
        pos = 0; //
        while (pos < str.size ())
            if ((pos = str.find_first_of ('[',pos)) != std::string::npos)
                str[pos] = ' ';
        posbrac = 0;
        while (posbrac < str.size ())
        {
            if ((posbrac = str.find_first_of (']',posbrac)) != std::string::npos)
            {
                str[posbrac] = ' ';
                iposbrac = posbrac;
            }

        }
        pos = 0;
        while (pos < str.size ())
            if ((pos = str.find_first_of ('p',pos)) != std::string::npos)
                str[pos] = ' ';

        pos = 0;
        while (pos < str.size ())
        {
            if ((pos = str.find_first_of ('C',pos)) != std::string::npos)
            {
                str[pos] = ' ';
                ipos = pos;
            }
        }

        std::stringstream ss(str);

        //cout << "string stream = "<<ss.str() << "\n" ;

        while (ss >> d)                                 // Each whitespace seperate value are saved vector containing the feedback
            vd.push_back (d);

        double joint1act,joint2act,joint3act,joint4act,joint5act,joint6act,joint7act,joint8act,joint9act,joint10act,joint11act,joint12act;



        if(ipos > iposbrac)
        {
            joint1act = vd.at(0);
            joint2act = vd.at(1);
            joint3act = vd.at(2);
            joint4act = vd.at(3);
            joint5act = vd.at(4);
            joint6act = vd.at(5);

            joint7act = vd.at(6);
            joint8act = vd.at(7);
            joint9act = vd.at(8);
            joint10act = vd.at(9);
            joint11act = vd.at(10);
            joint12act = vd.at(11);


        }
        else
        {
            cout << "ERROR - wrong order of data recived in string from manupualtor";

            joint1act = 10000;
            joint2act = 10000;
            joint3act = 10000;
            joint4act = 10000;
            joint5act = 10000;
            joint6act = 10000;
            joint7act = 10000;
            joint8act = 10000;
            joint9act = 10000;
            joint10act = 10000;
            joint11act = 10000;
            joint12act = 10000;

        }



        ur5_rosi_proxy::ur5Result result;
        result.resPos[0] = joint1act/mmm;
        result.resPos[1] = joint2act/mmm;
        result.resPos[2] = joint3act/mmm;
        result.resPos[3] = joint4act;
        result.resPos[4] = joint5act;
        result.resPos[5] = joint6act;
        result.resJoint[0] = joint7act;
        result.resJoint[1] = joint8act;
        result.resJoint[2] = joint9act;
        result.resJoint[3] = joint10act;
        result.resJoint[4] = joint11act;
        result.resJoint[5] = joint12act;
        as->setSucceeded(result);                       // Result of the postion of the manipulator is send to the actionlib

} // void publishData - end



void publishResultForce(int clientSock, char * buf, int offset, int mode,int joint[6],int speed,int acc,int blend,  Server* as)
// UNDER DEVELOPMENT: Sends the last command to the robot and reads the output string (including force) from the robot and publishes the result
{

        //int cnt=0;
        char receivedStr[2000];

        send_move(clientSock, buf, offset, mode, joint,speed,acc,blend);
        sleep(0.5);
        //sleep(0.5);
        recv(clientSock, receivedStr, 1600, 0);
        string str(receivedStr);
       // cnt = cnt +1;
        *receivedStr = 0;


        cout << "str(receivedStr) = " << str << "\n";

       // A "string2double" conversion is conducted below
                if (mode==1 || mode==2) {
                    str.erase (0,1);
                } else {
                    str.erase (0,2);
                }
                    std::size_t pos = 0;
                    std::vector <double>vd;
                    double d = 0.0;
                    while (pos < str.size ())
                        if ((pos = str.find_first_of (',',pos)) != std::string::npos)
                            str[pos] = ' ';
                    pos = 0; //
                    while (pos < str.size ())
                        if ((pos = str.find_first_of ('[',pos)) != std::string::npos)
                            str[pos] = ' ';
                    ////////////
                    pos = 0;
                    while (pos < str.size ())
                        if ((pos = str.find_first_of (']',pos)) != std::string::npos)
                            str[pos] = ' ';
                    pos = 0;
                    while (pos < str.size ())
                        if ((pos = str.find_first_of ('p',pos)) != std::string::npos)
                            str[pos] = ' ';
                    ////////////
                    std::stringstream ss(str);

                    cout << "string stream = "<<ss.str() << "\n" ;

                    while (ss >> d)
                        vd.push_back (d);

                    double joint1act,joint2act,joint3act,joint4act,joint5act,joint6act;
                    double forceResult[6] = {-100,-100,-100,-100,-100,-100};
                   // if (vd.size() == 6) {
                        joint1act = vd.at(0);
                        joint2act = vd.at(1);
                        joint3act = vd.at(2);
                        joint4act = vd.at(3);
                        joint5act = vd.at(4);
                        joint6act = vd.at(5);
                        if(vd.size() > 11) {
                        forceResult[0] = vd.at(6);
                        forceResult[1] = vd.at(7);
                        forceResult[2] = vd.at(8);
                        forceResult[3] = vd.at(9);
                        forceResult[4] = vd.at(10);
                        forceResult[5] = vd.at(11);
                        }
                   // } else {
                    /*    std::cout << "ERROR, the string from the robot do NOT contain 6 coordinates" << std::endl;
                        joint1act = 10000;
                        joint2act = 10000;
                        joint3act = 10000;
                        joint4act = 10000;
                        joint5act = 10000;
                        joint6act = 10000; */
                   // }

                        /*
                    cout << "Result 1 = " << joint1act/mmm << "\n";
                    cout << "Result 2 = " << joint2act/mmm << "\n";
                    cout << "Result 3 = " << joint3act/mmm << "\n";
                    cout << "Result 4 = " << joint4act << "\n";
                    cout << "Result 5 = " << joint5act << "\n";
                    cout << "Result 6 = " << joint6act << "\n";
                        */
                        for (int i = 0; i < 6; ++i) {
                            cout << "forceresult" << i << " = " << forceResult[i] << "\n";
                        }
                    cout << "vd.size() = " << vd.size() << "\n";


                    ur5_rosi_proxy::ur5Result result;
                    result.resPos[0] = joint1act/mmm;
                    result.resPos[0] = joint2act/mmm;
                    result.resPos[0] = joint3act/mmm;
                    result.resPos[0] = joint4act;
                    result.resPos[0] = joint5act;
                    result.resPos[0] = joint6act;

                    as->setSucceeded(result);

} // void publishResultForce - end



void publishFeedbackForce(int clientSock, char * buf, int offset, int mode,int joint[6],int speed,
            int acc,int blend,  Server* as)
// UNDER DEVELOPMENT: Sends the last command to the robot and reads the output string (including force) from the robot and publishes the feedback
{
        char receivedStr[2000];

        send_move(clientSock, buf, offset, mode, joint,speed,acc,blend);
        sleep(0.5);
        //sleep(0.5);
        recv(clientSock, receivedStr, 1600, 0);
        string str(receivedStr);
        *receivedStr = 0;

        //cout << "str(receivedStr) = " << str << "\n";

       // A "string2double" conversion is conducted below
        if (mode==1 || mode==2) {
            str.erase (0,1);
        } else {
            str.erase (0,2);
        }
            std::size_t pos = 0;
            std::vector <double>vd;
            double d = 0.0;
            while (pos < str.size ())
                if ((pos = str.find_first_of (',',pos)) != std::string::npos)
                    str[pos] = ' ';
            pos = 0; //
            while (pos < str.size ())
                if ((pos = str.find_first_of ('[',pos)) != std::string::npos)
                    str[pos] = ' ';
            ////////////
            pos = 0;
            while (pos < str.size ())
                if ((pos = str.find_first_of (']',pos)) != std::string::npos)
                    str[pos] = ' ';
            pos = 0;
            while (pos < str.size ())
                if ((pos = str.find_first_of ('p',pos)) != std::string::npos)
                    str[pos] = ' ';
            ////////////
            std::stringstream ss(str);

            cout << "string stream = "<<ss.str() << "\n" ;

            while (ss >> d)
                vd.push_back (d);

            double joint1act,joint2act,joint3act,joint4act,joint5act,joint6act;
            double forceResult[6] = {-100,-100,-100,-100,-100,-100};

            joint1act = vd.at(0);
            joint2act = vd.at(1);
            joint3act = vd.at(2);
            joint4act = vd.at(3);
            joint5act = vd.at(4);
            joint6act = vd.at(5);
            if(vd.size() > 11) {
                forceResult[0] = vd.at(6);
                forceResult[1] = vd.at(7);
                forceResult[2] = vd.at(8);
                forceResult[3] = vd.at(9);
                forceResult[4] = vd.at(10);
                forceResult[5] = vd.at(11);
            } // if - end

                /*
            cout << "Result 1 = " << joint1act/mmm << "\n";
            cout << "Result 2 = " << joint2act/mmm << "\n";
            cout << "Result 3 = " << joint3act/mmm << "\n";
            cout << "Result 4 = " << joint4act << "\n";
            cout << "Result 5 = " << joint5act << "\n";
            cout << "Result 6 = " << joint6act << "\n";
                */
            cout << "vd.size() = " << vd.size() << "\n";


            ur5_rosi_proxy::ur5Feedback feedback;
            for (int i = 0; i < 6; ++i) {
                feedback.currentPos[i] = forceResult[i];
                cout << "forceFeedback" << i << " = " << forceResult[i] << "\n";
            }

            as->publishFeedback(feedback);

} // void publishFeedbackForce - end



void feedbackTest(int joint[6], Server* as)
// Test function to publish feedback
{
    for (int i = 0; i < 10; ++i) {
      ros::Rate r(10);

      ur5_rosi_proxy::ur5Feedback feedback;
      feedback.currentPos[0] = joint[0]+10*i;
      feedback.currentPos[1] = joint[1]+10*i;
      feedback.currentPos[2] = joint[2]+10*i;
      feedback.currentPos[3] = joint[3]+10*i;
      feedback.currentPos[4] = joint[4]+10*i;
      feedback.currentPos[5] = joint[5]+10*i;
      as->publishFeedback(feedback);                    // Publish test feedback, only dependent of the received goal

     r.sleep();

     ROS_INFO("Feedback.currentPos1 = %f", feedback.currentPos[0]);
     }

} // void feedbackTest - end



void execute(const ur5_rosi_proxy::ur5GoalConstPtr& goal, Server* as, int clientSock, char * buf, char * bufForce, int offset)
// Reads the goal from the action client and determines which hardware specific mode the robot should call.
// Calls the following functions: send_move, send_force, resend, publishResult
{
    // Hardware specific modes:
    // mode = 1: moveJ with joint input
    // mode = 2: moveL with joint input
    // mode = 3: moveJ with Car input
    // mode = 4: moveL with Car input
    // mode = 5: get Force twist - UNDER DEVELOPMENT
    // mode = 6: Force start
    // mode = 7: Force update
    // mode = 8: Force end
    // mode = 9: stop robot script
    // mode = 10: Get data
    // mode = 11: Set ToolCenterPoint


    movetype = goal->movetype;                          // The values of the recieved goal are assigned to variables
    inputtype = goal->inputtype;
    joint[0] = goal->coord[0];
    joint[1] = goal->coord[1];
    joint[2] = goal->coord[2];
    joint[3] = goal->coord[3];
    joint[4] = goal->coord[4];
    joint[5] = goal->coord[5];
    speed = goal->speed;

    // The combination of movetype and inputtype are translated to the hardware specific modes used in the script
        if ( (movetype == "j") && (inputtype == "joint") ) {
        mode = 1;
        }
        else if ((movetype == "l") && (inputtype == "joint")){
        mode = 2;
        }
        else if ((movetype == "j") && (inputtype == "car")){
        mode = 3;
        }
        else if ((movetype == "l") && (inputtype == "car")){
        mode = 4;
        }
        else if ((movetype == "force") && (inputtype == "start")){
        mode = 6;
        }
        else if ((movetype == "force") && (inputtype == "update")){
        mode = 7;
        }
        else if ((movetype == "force") && (inputtype == "end")){
        mode = 8;
        }
        else if ((movetype == "get") && inputtype == "data") {
        mode = 10;
        }
        else if ((movetype == "set") && inputtype == "tcp") {
        mode = 11;
        } else {
            cout << "Wrong input in ''movetype'' or ''inputtype''";
        }

        if (inputtype=="joint") {
            for (int i = 0; i < 6; ++i) {
                joint[i] = joint[i] * float_scale;      // Multiplies all recieved joint values with the float_scale
            } // for - end
        } else {
            for (int i = 0; i < 6; ++i) {
                joint[i] = joint[i] * float_scale;      // Multiplies all recieved TCP values with the float_scale
            } // for - end
            for (int i = 0; i < 3; ++i) {
                joint[i] = joint[i] * mmm;              // Multiplies X-Y-Z TCP values with the mmm - to convert from [mm] (input) to [m] (robot syntax)
            } // for - end
        }// else - end

        int jointInt[6];
        for (int i = 0; i < 6; ++i) {
            jointInt[i]= (int) joint[i];                // Converts the double coordinates to ints (floating point values handled by the float_scale)
        } // for - end

        if (movetype =="j") {
            speed = speed * float_scale;

        } else {
            speed = speed * float_scale * mmm;
        } // else - end

    if (movetype == "force") {                          // The values of the recieved goal are assigned to variables
        selection_vector[0] = (goal->selection[0])*float_scale;
        selection_vector[1] = (goal->selection[1])*float_scale;
        selection_vector[2] = (goal->selection[2])*float_scale;
        selection_vector[3] = (goal->selection[3])*float_scale;
        selection_vector[4] = (goal->selection[4])*float_scale;
        selection_vector[5] = (goal->selection[5])*float_scale;

        wrench[0] = (goal->wrench[0])*float_scale;
        wrench[1] = (goal->wrench[1])*float_scale;
        wrench[2] = (goal->wrench[2])*float_scale;
        wrench[3] = (goal->wrench[3])*float_scale;
        wrench[4] = (goal->wrench[4])*float_scale;
        wrench[5] = (goal->wrench[5])*float_scale;

        force_limits[0] = (goal->limits[0])*float_scale;
        force_limits[1] = (goal->limits[1])*float_scale;
        force_limits[2] = (goal->limits[2])*float_scale;
        force_limits[3] = (goal->limits[3])*float_scale;
        force_limits[4] = (goal->limits[4])*float_scale;
        force_limits[5] = (goal->limits[5])*float_scale;
    } // if - end

    /*
    If test_mode == true
        The program do not creat a socket server and waits for an incomming connection. Commands are not send to the manipulator and the
        feedbackTest function is call to "imitate" feedback.
    If test_mode == false
        Opposite of above
    */

    if (test_mode == false) {

        if ((mode != 8) && (mode != 11)) { //If "force end" is to be called, the command is send through the "publishResult" command
            cout << "send_move function \n";
            send_move(clientSock, buf, offset, mode, jointInt,speed,acc,blend);                 // Send the move command
        } // if - end

        if ((mode == 6) || (mode == 7)) {               // If forcemode is chosen the force parameters are send and the status is set to succeeded
            cout << "send_force function \n";
            send_force(clientSock, bufForce, offset, selection_vector, wrench, force_limits);
            as->setSucceeded();

        } else if ((mode == 8)) {                       // Sends "forcemodeend" to the manipulator
            cout << "send_force END function \n";
            publishResult(clientSock, buf,  offset,  mode, jointInt, speed, acc, blend, as);

        } else if ((mode == 10)) {                       // Sends "publishData" to the manipulator
            cout << "get data \n";
            publishData(clientSock, buf,  offset,  mode, jointInt, speed, acc, blend, as);
        } else if ((mode == 11)) {                       // Sends "settcp" to the manipulator
            cout << "set TCP \n";
            //publishData(clientSock, buf,  offset,  mode, jointInt, speed, acc, blend, as);
            int cog[3];
            cog[0] = (goal->wrench[0])*float_scale*mmm;
            cog[1] = (goal->wrench[1])*float_scale*mmm;
            cog[2] = (goal->wrench[2])*float_scale*mmm;

            send_tcp(clientSock, buf, offset, mode, jointInt, speed/mmm, cog, as);
			as->setSucceeded();
        } else {                                       // Resends the command untill the wanted position is obtained
            resend(clientSock, buf, offset, mode, jointInt,speed,acc,blend, as);
            ros::Duration(100.0);
            publishResult(clientSock, buf,  offset,  mode, jointInt, speed, acc, blend, as);    // Sends result of the position to the actionlib

        } // else - end
    }  else if (test_mode == true) {

        cout << "Test_mode enabled";

        feedbackTest(jointInt, as);

        ur5_rosi_proxy::ur5Result result;
        result.resPos[0] = joint1;
        result.resPos[1] = joint2;
        result.resPos[2] = joint3;
        result.resPos[3] = joint4;
        result.resPos[4] = joint5;
        result.resPos[5] = joint6;
        as->setSucceeded(result);                       // Publish test result

    }


} // void execute - end


/////////////////////////////////////////////////////////////////////////////
// MAIN
/////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {

    int clientSock;

    if (test_mode == false) {

        // create a socket
        int serverSock=socket(AF_INET, SOCK_STREAM, 0);

        sockaddr_in serverAddr;                     // sockaddr_in structure for the server socket.
        serverAddr.sin_family = AF_INET;            // TCP/IP 4 (IPv4) family
        serverAddr.sin_port = SERVER_PORT;          // The port which the socket "listens" to
        serverAddr.sin_addr.s_addr = INADDR_ANY;    // Accepts a connection on any IP address

        // bind server socket (serverSock) to server address (serverAddr). Necessary so that server can use a specific port
        bind(serverSock, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr)); // bind (this socket, local address, address length)

        // wait for a client
        listen(serverSock,4);                       // listen (this socket, request queue length)

        // accept a connection request from client
        sockaddr_in clientAddr; // sockaddr_in struct to hold the address of the client

        socklen_t sin_size=sizeof(struct sockaddr_in);

        // Insert sendscript function here
        //sendscript();
        cout << "Waiting for manipulator to create a socket TCP client \n";
        // A new socket to connect to the client socket.
       clientSock=accept(serverSock,(struct sockaddr*)&clientAddr, &sin_size);  // accept (old socket, client socket address, length of client socket address)

    }  else if (test_mode == true) {
        cout << " #### TESTMODE ACTIVE #### \n";

        clientSock =1;            // For testing without the socket connection
    } // else if - end

    char buf[32];                   // Buffer used to send commands regarding mode and coordinates
    int offset = 0;
    char bufForce[72];              // Buffer used to send commands regarding force commands

    ros::init(argc, argv, "ur5Node");
    ros::NodeHandle n;

    /*
      Below is a SimpleActionServer constructed, which takes the following arguments
      actionlib::SimpleActionServer< ActionSpec >::SimpleActionServer	(	ros::NodeHandle 	n,
                                                                            std::string         name,
                                                                            ExecuteCallback 	execute_cb,
                                                                            bool                auto_start	 )

       n            = NodeHandle which the namespace is created under
       name         = The name of the actionserver
       execute_cb   = Callback, which is called each time a new goal is received, adding a callback deactivates the goalCallback
       auto_start   = bool, should alwys be false to avoid race conditions, and insted should the server be started with a start()
                      call after construction of the server
      http://mirror.umd.edu/roswiki/doc/diamondback/api/actionlib/html/classactionlib_1_1SimpleActionServer.html#a271d4f0bd10d2cb6cdd50c609642b0b3
    */
    Server server(n, "ur5Node", boost::bind(&execute, _1, &server, clientSock, buf,  bufForce, offset), false);
    cout << "Starting actionlib server \n";
    server.start();
    ros::spin();

    //  close(clientSock);

    return 0;
    } // end main
