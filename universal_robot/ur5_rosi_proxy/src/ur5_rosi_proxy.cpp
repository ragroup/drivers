#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/action_client.h>
#include <ur_driver/urcommandsAction.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib_msgs/GoalStatus.h>

#include <actionlib/client/simple_action_client.h>
#include <iostream>

#include <actionlib/server/simple_action_server.h>
#include <ur5_rosi_proxy/ur5Action.h>
#include <ros/subscriber.h>
#include "std_msgs/String.h"
#include <ur_driver/urinfo.h>
#include <vector>

#include "device_mgr_msgs/Advertiser.hpp"


using namespace std;

#define mmm 1000;

// Creates a "nickname" for variables of a datatype: typedef (DataType) (Nickname)
typedef actionlib::SimpleActionClient< trajectory_msgs::JointTrajectory > TrajClient;           // Unnecessary ?
typedef actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction > FJTClient;
typedef actionlib::SimpleActionClient<ur_driver::urcommandsAction> URCommands;
typedef actionlib::SimpleActionClient<ur_driver::urcommandsGoal> URCommandsGoal;
typedef actionlib::SimpleActionServer<ur5_rosi_proxy::ur5Action> ur5Server;

typedef actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> FJTServer;     // MoveIt
typedef actionlib::SimpleActionClient< trajectory_msgs::JointTrajectory > TrajServer;           // MoveIt - Unnecessary ?

// Creates variables of different data-types
ur_driver::urcommandsResult result_commands;
ur_driver::urcommandsGoal goal_commandsOC;
trajectory_msgs::JointTrajectory goal_traj;
ur5_rosi_proxy::ur5Result result;
ur5_rosi_proxy::ur5Goal goal_server;

control_msgs::FollowJointTrajectoryGoal incomming_goal;             // MoveIt
trajectory_msgs::JointTrajectory goal_traj_incomming;               // MoveIt


///CS FIXES///
//Action server for UR_commands:
ur5Server* commands_server_;


// Creating of deference (opposite of pointer) varialbes
FJTClient* fjt_client;
URCommands* ur_commands; // ur_server

FJTServer* fjt_server;                                              // MoveIt

// Misc. variables                                     // Not necessary?
double resPostemp[6], resJointtemp[6], point1[6];
bool test_mode = false;


void doneCb(const actionlib::SimpleClientGoalState& state,
            const ur_driver::urcommandsResultConstPtr& result_commands)
// Manipulator - Called when the status of the ur_commands actionlib is succeeded
// Used to assign the Position and Joint values of the manipulator to temperary variables.
{
    for (int i = 0; i < 6; ++i) {
        if (i<=2) {
            resPostemp[i] = result_commands->resPos[i]*mmm;
        } else if (i > 2) {
            resPostemp[i] = result_commands->resPos[i];
        }

        resJointtemp[i] = result_commands->resJoint[i];
        cout << "result_commands, Pos: "<< result_commands->resPos[i] << "\n";
        cout << "result_commands, Joint: "<< result_commands->resJoint[i] << "\n";
    }

    cout <<"\nDone executing 'doneCb' \n\n";

} // void doneCb - end


void activeCb()
// Manipulator - Called once when the ur_commands goal becomes active - EMPTY
{

} // activeCb() - end


void feedbackCb(const ur_driver::urcommandsFeedbackConstPtr& feedback)
// Manipulator - Called every time a ur_commands feedback is received - EMPTY
{
    double currentPos1=0;
    currentPos1 = feedback->feedback1;


} // void feedbackCb - end


actionlib::SimpleClientGoalState getStateOC()
// Returns the current state of the action - NOT USED
{
    //return ur_commands->getState();
    return fjt_client->getState();
}

void sendgoalcommands(ur_driver::urcommandsGoal goal_commandsOC,
                      URCommands* ur_commands,
                      //trajectory_msgs::JointTrajectory goal_traj,
                      control_msgs::FollowJointTrajectoryGoal goal1,
                      FJTClient* fjt_client)
// Sends the goals from the proxy to the driver, by use of actionlibs
{
    // Sends the goal to the ur_commands actionlib, and creates some callbacks, where the doneCb is used for "publishing" of the result
    ur_commands->sendGoal(goal_commandsOC, boost::bind(&doneCb,_1,_2),boost::bind(&activeCb),boost::bind(&feedbackCb,_1));

    // Sends the goal to the FollowJointTrajectory actionlib and waits for the finish status from the driver
    fjt_client->sendGoalAndWait(goal1);

    while(fjt_client->getState() == actionlib::SimpleClientGoalState::PENDING ||
          fjt_client->getState() == actionlib::SimpleClientGoalState::ACTIVE)
    {
        usleep(100000);
        if(commands_server_->isPreemptRequested())
        {
            ur_commands->cancelAllGoals();
            fjt_client->cancelAllGoals();
        }
    }

    //    sleep(2);

} // void sendgoalcommands - end


void execute(const ur5_rosi_proxy::ur5GoalConstPtr& goal_server,
             ur5_rosi_proxy::ur5Result result,
             ur_driver::urcommandsGoal goal_commandsOC,
             trajectory_msgs::JointTrajectory goal_traj,
             FJTClient* fjt_client,
             URCommands* ur_commands)
// Gets called when a ur5Node goal is recieved and assigns values to ur_commands and follow_joint_trajectory goals
{

    std::string movetype, inputtype;
    int mode;
    int ind = 0;
    movetype = goal_server->movetype;
    inputtype = goal_server->inputtype;
    /*
    cout << "Movetype: ";
    cout << movetype;
    cout << " Inputtype: " ;
    cout << inputtype;
    cout << "\n";
    */

    // Creation of the goals to be send to the /ur_driver

    // Force
    for (int i = 0; i < 6; ++i) {
        goal_commandsOC.selection[i] = goal_server->selection[i];
        goal_commandsOC.wrench[i] = goal_server->wrench[i];
        goal_commandsOC.limits[i] = goal_server->limits[i];
    }

    // Joint names, which apply to all waypoints
    goal_traj.joint_names.push_back("shoulder_pan_joint");
    goal_traj.joint_names.push_back("shoulder_lift_joint");
    goal_traj.joint_names.push_back("elbow_joint");
    goal_traj.joint_names.push_back("wrist_1_joint");
    goal_traj.joint_names.push_back("wrist_2_joint");
    goal_traj.joint_names.push_back("wrist_3_joint");


    // Points, velocities, accelerations and time
    if (test_mode == false) {
        double vel;
        goal_traj.points.resize(1);

        goal_traj.points[ind].positions.resize(6);
        for (size_t j = 0; j < 6; ++j)
        {
            if (inputtype == "joint"){
                goal_traj.points[ind].positions[j] = goal_server->coord[j];
            }
            else if (inputtype == "car") {

                if (j<=2) {
                    goal_traj.points[ind].positions[j] = goal_server->coord[j]/mmm;
                } else if (j > 2) {
                    goal_traj.points[ind].positions[j] = goal_server->coord[j];
                }
            }
        }
        // Velocities
        goal_traj.points[ind].velocities.resize(6);
        for (size_t j = 0; j < 6; ++j)
        {
            if (inputtype == "joint"){
                goal_traj.points[ind].velocities[j] = goal_server->speed;
            }
            else if (inputtype == "car") {
                vel = goal_server->speed;
                goal_traj.points[ind].velocities[j] = vel/mmm;
                cout << "The velocity: ";
                cout << vel/mmm;
                cout << "\n";
            }

        }
        // Accelerations
        goal_traj.points[ind].accelerations.resize(6);
        for (size_t j = 0; j < 6; ++j)
        {
            goal_traj.points[ind].accelerations[j] = goal_server->acc;
        }
        // To be reached a given time after starting along the trajectory
        goal_traj.points[ind].time_from_start = ros::Duration(0.8);

    } else if (test_mode == true) {

        goal_traj.points.resize(1);

        goal_traj.points[ind].positions.resize(6);
        goal_traj.points[ind].positions[0] = -0.87;
        goal_traj.points[ind].positions[1] = -0.8;
        goal_traj.points[ind].positions[2] = -1.78;
        goal_traj.points[ind].positions[3] = -1.41;
        goal_traj.points[ind].positions[4] = 1.72;
        goal_traj.points[ind].positions[5] = 0.94;
        // Velocities
        goal_traj.points[ind].velocities.resize(6);
        for (size_t j = 0; j < 6; ++j)
        {
            goal_traj.points[ind].velocities[j] = 0.2;
        }
        // Accelerations
        goal_traj.points[ind].accelerations.resize(6);
        for (size_t j = 0; j < 6; ++j)
        {
            goal_traj.points[ind].accelerations[j] = 0.4;
        }
        // To be reached 1 second after starting along the trajectory
        goal_traj.points[ind].time_from_start = ros::Duration(3.0);

    }



    // The combination of movetype and inputtype are translated to the hardware specific modes used in the script
    if ( (movetype == "j") && (inputtype == "joint") ) {
        mode = 1;
    }
    else if ((movetype == "l") && (inputtype == "joint")){
        mode = 2;
    }
    else if ((movetype == "j") && (inputtype == "car")){
        mode = 3;
    }
    else if ((movetype == "l") && (inputtype == "car")){
        mode = 4;
    }
    else if ((movetype == "force") && (inputtype == "start")){
        mode = 6;
    }
    else if ((movetype == "force") && (inputtype == "update")){
        mode = 7;
    }
    else if ((movetype == "force") && (inputtype == "end")){
        mode = 8;
    }
    else if ((movetype == "teach") && (inputtype == "start")){
        mode = 18;
    }
    else if ((movetype == "teach") && (inputtype == "end")){
        mode = 19;
    }
    else if ((movetype == "get") && inputtype == "data") {
        mode = 10;
    }
    else if ((movetype == "set") && inputtype == "tcp") {
        mode = 11;
        for (int i = 0; i < 6; ++i) {
            goal_commandsOC.tcppose[i] = goal_server->coord[i]/mmm;
            goal_traj.points[ind].velocities[i] = 0.1;                  // To acoid errors regarding a velocity greater than 2.0
        }
        for (int i = 0; i < 3; ++i) {
            goal_commandsOC.tcpcog[i] = goal_server->wrench[i]/mmm;
        }
        goal_commandsOC.mass = goal_server->speed;

    }
    else if ((movetype == "servo") && inputtype == "joint") {
        mode = 16;
    }
    else if ((movetype == "servo") && inputtype == "cart") {
        mode = 21;
    }
    else if ((movetype == "motion") && inputtype == "plan") {
        //mode = 17;
        mode = 16; // OBS - servo mode
    }
    else if (movetype == "stop") {
        cout << "Received a STOP!!!" << endl;
        mode = 17;
        goal_commandsOC.mode = 17;
        ur_commands->sendGoal(goal_commandsOC);
        commands_server_->setSucceeded(result);
        return;
    }
    else {
        cout << "Wrong input in ''movetype'' or ''inputtype'' \n";
    }

    goal_commandsOC.mode = mode;

    control_msgs::FollowJointTrajectoryGoal goal1;

    if ((movetype == "motion") && inputtype == "plan") {
        goal1.trajectory = goal_traj_incomming;
    } else {
        goal_traj.header.stamp = ros::Time::now() + ros::Duration(1.0);
        goal1.trajectory = goal_traj;
    }

    //cout << "goal1.trajectory: \n";
    //cout << goal1.trajectory;
    //cout << "goal - commands: \n";
    //cout << goal_commandsOC;

    sendgoalcommands(goal_commandsOC, ur_commands, goal1, fjt_client);


    cout << "\nDone with sendgoalcommands(goal_commandsOC, ur_commands, goal1, fjt_client) \n\n";


    for (int i = 0; i < 6; ++i) {
        //cout << "Incomming goal: " << goal_traj_incomming.points[0].positions[i] << "\n";
        result.resPos[i]=resPostemp[i];
        result.resJoint[i]=resJointtemp[i];
    }

    if(commands_server_->isPreemptRequested())
    {
        cout << "Cancelling goal!" << endl;
        commands_server_->setPreempted(result);
        return;
    }

    // Sets the status of the actionlib to the master to succeeded and "publishes" the result (resPos and resJoint)
    cout << "About to set succeeded \n";
    commands_server_->setSucceeded(result);

} // void execute - end





void execute_incomming(const control_msgs::FollowJointTrajectoryGoalConstPtr& incomming_goal,
                       ur_driver::urcommandsGoal goal_commandsOC,
                       FJTClient* fjt_client,
                       URCommands* ur_commands,
                       FJTServer* as_incomming)
// Gets called when a goal is recieved from the MoveIt node and assigns values to ur_commands
// (only the mode specifying the servo movement) and follow_joint_trajectory goals
{
    //cout << incomming_goal->trajectory;

    for (int i = 0; i < 6; ++i) {
        //cout << "Joint name: " <<incomming_goal->trajectory.joint_names[i] << "\n" ;
        //cout << "Positions : " <<incomming_goal->trajectory.points[0].positions[i] << "\n" ;
        point1[i] = incomming_goal->trajectory.points[0].positions[i];
    }

    control_msgs::FollowJointTrajectoryGoal goal1;
    goal_traj_incomming = incomming_goal->trajectory;
    goal1.trajectory = goal_traj_incomming;

    for (int i = 0; i < 6; ++i) {
        cout << "Joint name: " <<goal_traj_incomming.joint_names[i] << "\n" ;
        cout << "Positions : " <<goal_traj_incomming.points[0].positions[i] << "\n" ;
        //point1[i] = incomming_goal->trajectory.points[0].positions[i];
    }
    cout << "Size of points: " << incomming_goal->trajectory.points.size() << "\n";

    /*
    goal_traj.header.stamp = ros::Time::now() + ros::Duration(1.0);
    control_msgs::FollowJointTrajectoryGoal goal1;
    goal1.trajectory = goal_traj;
    fjt_client->sendGoalAndWait(goal1);

    //control_msgs::FollowJointTrajectoryAction

    //const trajectory_msgs::JointTrajectoryConstPtr& incomming_goal,

    //const ur5_rosi_proxy::ur5GoalConstPtr& goal_server,

    //cout << "\n \n TEST" << incomming_goal->positions[1] << "\n \n ";        // MoveIt incomming_goal->points[1].positions[1]
    //incomming_goal->points[1].velocities[1];
    //fjt_server->goalCallback(control_msgs::FollowJointTrajectoryAction incomming_goal);
    */


    // Mode 16 corrosponds to servo movement of the manipualtor
    goal_commandsOC.mode = 16;

    // The goals is send via the two action clients, ur_commands and fjt_client
    sendgoalcommands(goal_commandsOC, ur_commands, goal1, fjt_client);


    cout << "\n \n Setting Succeeded for execute_incomming \n \n";
    as_incomming->setSucceeded();

} // void execute_incomming - end

void execute_incomming_cart(const control_msgs::FollowJointTrajectoryGoalConstPtr& incomming_goal,
                       ur_driver::urcommandsGoal goal_commandsOC,
                       FJTClient* fjt_client,
                       URCommands* ur_commands,
                       FJTServer* as_incomming)
// Gets called when a goal is recieved from the MoveIt node and assigns values to ur_commands
// (only the mode specifying the servo movement) and follow_joint_trajectory goals
{
    //cout << incomming_goal->trajectory;

    for (int i = 0; i < 6; ++i) {
        //cout << "Joint name: " <<incomming_goal->trajectory.joint_names[i] << "\n" ;
        //cout << "Positions : " <<incomming_goal->trajectory.points[0].positions[i] << "\n" ;
        point1[i] = incomming_goal->trajectory.points[0].positions[i];
    }

    control_msgs::FollowJointTrajectoryGoal goal1;
    goal_traj_incomming = incomming_goal->trajectory;
    goal1.trajectory = goal_traj_incomming;

    for (int i = 0; i < 6; ++i) {
        cout << "Joint name: " <<goal_traj_incomming.joint_names[i] << "\n" ;
        cout << "Positions : " <<goal_traj_incomming.points[0].positions[i] << "\n" ;
        //point1[i] = incomming_goal->trajectory.points[0].positions[i];
    }
    cout << "Size of points: " << incomming_goal->trajectory.points.size() << "\n";

    /*
    goal_traj.header.stamp = ros::Time::now() + ros::Duration(1.0);
    control_msgs::FollowJointTrajectoryGoal goal1;
    goal1.trajectory = goal_traj;
    fjt_client->sendGoalAndWait(goal1);

    //control_msgs::FollowJointTrajectoryAction

    //const trajectory_msgs::JointTrajectoryConstPtr& incomming_goal,

    //const ur5_rosi_proxy::ur5GoalConstPtr& goal_server,

    //cout << "\n \n TEST" << incomming_goal->positions[1] << "\n \n ";        // MoveIt incomming_goal->points[1].positions[1]
    //incomming_goal->points[1].velocities[1];
    //fjt_server->goalCallback(control_msgs::FollowJointTrajectoryAction incomming_goal);
    */


    // Mode 21 is servo cart
    goal_commandsOC.mode = 21;

    // The goals is send via the two action clients, ur_commands and fjt_client
    sendgoalcommands(goal_commandsOC, ur_commands, goal1, fjt_client);


    cout << "\n \n Setting Succeeded for execute_incomming \n \n";
    as_incomming->setSucceeded();

} // void execute_incomming - end

void callbackGetdata(const ur_driver::urinfoConstPtr& msg)
// Under development - properly not nesecarry - NOT USED
{
    vector <float> posevector, jointvector;

    posevector.resize(6);
    jointvector.resize(6);
    for (int i = 0; i < 6; ++i) {
        posevector[i] = msg->pose[i];
        jointvector[i] = msg->joints[i];
    }
    for (int i = 0; i < 6; ++i) {
        cout << "Pose: " << posevector[i] << "\n";
        cout << "Joints: " << jointvector[i] << "\n";
        result.resPos[i] = posevector[i];
        result.resJoint[i] = jointvector[i];
    }

    //as->setSucceeded(result);
    //return result;

}



int main(int argc, char** argv)
{
    // Init the ROS node
    //ros::init(argc, argv, "robot_driver");
    ros::init(argc, argv, "ur5_rosi_proxy");

    // Create two actionlib clients, to communciate with the driver
    fjt_client = new FJTClient("follow_joint_trajectory", NULL); //client
    ur_commands = new URCommands("ur_commands", NULL);  //client

    // Create the ROS nodehandler
    ros::NodeHandle n;

    // The set below works:
    /* //////////////////////////
    ros::AsyncSpinner spinner(3); // Use 2 threads
    spinner.start();
    cout << "spinner.start - executed \n";

    RobotArm arm;
    // Start the trajectory
    //ros::Subscriber sub1 = n.subscribe("ur_info_data", 1, &RobotArm::callback, &arm);

    ur5Server server(n, "ur5Node", boost::bind(&execute, _1, result,result_commands, &server), false);      // What to call the node?
    server.start();
    //cout << "Starting actionlib server \n";
    */


    // Create the servers (one to be triggered by the master node (ur5Server) and one to be triggered by the MoveIt node (FJTServer))
    FJTServer server_incomming(n, "follow_joint_trajectory_MOVEIT", boost::bind(&execute_incomming, _1, goal_commandsOC, fjt_client, ur_commands, &server_incomming), false);
    FJTServer server_incomming_cart(n, "follow_joint_trajectory_cart", boost::bind(&execute_incomming_cart, _1, goal_commandsOC, fjt_client, ur_commands, &server_incomming_cart), false);   // MoveIt
    commands_server_ = new ur5Server(n,"ur5Node", boost::bind(&execute, _1, result, goal_commandsOC, goal_traj, fjt_client, ur_commands), false);
    ///ur5Server server(n, "ur5Node", boost::bind(&execute, _1, result, goal_commandsOC, goal_traj, fjt_client, ur_commands, &server), false);

    // Starting the (incomming) servers
    commands_server_->start();
    //fjt_server->start();                                                        // MoveIt
    server_incomming.start();                                                       // MoveIt
    server_incomming_cart.start();
    cout << "\nThe actionlib servers for 'follow_joint_trajectory_MOVEIT' and 'ur5Node' has been started \n";

    //ros::AsyncSpinner spinner(3); // Use 2 threads
    //spinner.start();
    //ros::spin();

    Advertiser myAdvertiser(n, "UR5", "arm", "articulated");
    if(myAdvertiser.getState() == device_mgr::CommunicationState::ADVERTISED)
    {
        myAdvertiser.changeState(device_mgr::OperationalState::IDLE);
    }

    // Spins the ROS threads
    ros::MultiThreadedSpinner spinner(3); // Use 4 threads
    spinner.spin(); // spin() will not return until the node has been shutdown

    cout << "end of main \n";

} // main - end
