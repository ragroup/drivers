const std::string NEW_LINE= "\n"; const std::string QUOTATION = "\""; std::string UR_SCRIPT = "#############################################################################" + NEW_LINE  + 
"### DESCRIPTION" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"# authours:  Morten Palmelund-Jensen, Rune Etzerodt, Casper Abildgaard Pedersen" + NEW_LINE  + 
"#" + NEW_LINE  + 
"# This script is transfered to the Universal Robots controller, by use of the" + NEW_LINE  + 
"# program "+QUOTATION+"urinterface.cpp"+QUOTATION+". The script creates a socket client on the controller" + NEW_LINE  + 
"# trying to connect to server. When the connection is established the script" + NEW_LINE  + 
"# listens for incomming commands which are executed." + NEW_LINE  + 
"" + NEW_LINE  + 
"def myprog():" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"### DEFINITIONS / DECLARATIONS" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"    set_digital_out(0, True)" + NEW_LINE  + 
"    global qtarget = [ 0,0,0,0,0,0]" + NEW_LINE  + 
"    global qtarget1 = [ 0,0,0,0,0,0]" + NEW_LINE  + 
"    global posetarget = [ 0,0,0,0,0,0]" + NEW_LINE  + 
"    global dqtarget = [ 0,0,0,0,0,0 ]" + NEW_LINE  + 
"    global speed = 0.75" + NEW_LINE  + 
"    global thrd  = -1" + NEW_LINE  + 
"    global motionFinished = 1" + NEW_LINE  + 
"    global isServoing = 0" + NEW_LINE  + 
"    global isStopped = 1" + NEW_LINE  + 
"    global receive_buffer = [8, 0, 0, 0, 0, 0, 0, 0, 0]" + NEW_LINE  + 
"    global FLOAT_SCALE = 0.00001" + NEW_LINE  + 
"    # Force" + NEW_LINE  + 
"    global receive_buffer18 = [18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]" + NEW_LINE  + 
"    global force_selection = [ 0,0,0,0,0,0]" + NEW_LINE  + 
"    global wrench = [ 0,0,0,0,0,0]" + NEW_LINE  + 
"    global force_limits = [ 0,0,0,0,0,0]" + NEW_LINE  + 
"    global force_frame = p[0,0,0,0,0,0]" + NEW_LINE  + 
"    global tcp_pose = p[0,0,0,0,0,0]" + NEW_LINE  + 
"    global mass = 0" + NEW_LINE  + 
"    global cog = [0,0,0]" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"### FUNCTIONS" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"# Function for stopping the manipulator" + NEW_LINE  + 
"    def stopRobot():" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"        if thrd != -1:" + NEW_LINE  + 
"            kill thrd" + NEW_LINE  + 
"            thrd = -1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"        #textmsg("+QUOTATION+"Stop Robot"+QUOTATION+")" + NEW_LINE  + 
"        stopj(10)" + NEW_LINE  + 
"        isServoing = 0" + NEW_LINE  + 
"        isStopped = 1" + NEW_LINE  + 
"    end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Thread for running the moveJ command, with joint input - start" + NEW_LINE  + 
"    thread moveJthread():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Calls moveJ, with joint input"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(qtarget)" + NEW_LINE  + 
"        textmsg(speed)" + NEW_LINE  + 
"        movej(qtarget, 3, receive_buffer[8]*FLOAT_SCALE)		# moveJ with qtarget as joint position, 3 as acceleartion, receive[8] as speed" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveJ with joint input called"+QUOTATION+")" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            thrd = -1" + NEW_LINE  + 
"            motionFinished = 1" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveJ with joint input done"+QUOTATION+")" + NEW_LINE  + 
"    end" + NEW_LINE  + 
"# Thread for running the moveJ command, with joint input - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Thread for running the moveL command, with joint input - start" + NEW_LINE  + 
"    thread moveLQthread():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Calls moveL, with join input"+QUOTATION+")" + NEW_LINE  + 
"        movel(qtarget, 1.2, receive_buffer[8]*FLOAT_SCALE)		# moveL with qtarget as joint position, 1.2 as acceleartion, receive[8] as speed" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveL with joint input called"+QUOTATION+")" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            thrd = -1" + NEW_LINE  + 
"            motionFinished = 1" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveL with joint input done"+QUOTATION+")" + NEW_LINE  + 
"     end" + NEW_LINE  + 
"# Thread for running the moveL command, with joint input - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Thread for running the moveJ command with Cartesian input - start" + NEW_LINE  + 
"    thread moveJCarthread():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Calls moveJ, with Car input"+QUOTATION+")" + NEW_LINE  + 
"	# moveJ with Cartesian input, 3 as acceleration and receive[8] as speed" + NEW_LINE  + 
"        movej(p[receive_buffer[2]*FLOAT_SCALE,receive_buffer[3]*FLOAT_SCALE,receive_buffer[4]*FLOAT_SCALE,receive_buffer[5]*FLOAT_SCALE,receive_buffer[6]*FLOAT_SCALE,receive_buffer[7]*FLOAT_SCALE],3,receive_buffer[8]*FLOAT_SCALE)" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveJ with Car called"+QUOTATION+")" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            thrd = -1" + NEW_LINE  + 
"            motionFinished = 1" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveJ with Car input done"+QUOTATION+")" + NEW_LINE  + 
"     end" + NEW_LINE  + 
"# Thread for running the moveJ command with Cartesian input - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Thread for running the moveL command with Cartesian input - start" + NEW_LINE  + 
"    thread moveLCarthread():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Calls moveL, with Car input"+QUOTATION+")" + NEW_LINE  + 
"        # moveL with Cartesian input, 1.2 as acceleration and receive[8] as speed" + NEW_LINE  + 
"        movel(p[receive_buffer[2]*FLOAT_SCALE,receive_buffer[3]*FLOAT_SCALE,receive_buffer[4]*FLOAT_SCALE,receive_buffer[5]*FLOAT_SCALE,receive_buffer[6]*FLOAT_SCALE,receive_buffer[7]*FLOAT_SCALE],1.2,receive_buffer[8]*FLOAT_SCALE)" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveL with Car called"+QUOTATION+")" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            thrd = -1" + NEW_LINE  + 
"            motionFinished = 1" + NEW_LINE  + 
"            #socket_send_line("+QUOTATION+"motionFinished1"+QUOTATION+")" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveL with Car input done"+QUOTATION+")" + NEW_LINE  + 
"     end" + NEW_LINE  + 
"# Thread for running the moveL command with Cartesian input - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Threads above     //////////////////////////////" + NEW_LINE  + 
"# Definitions below //////////////////////////////" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Definition for moveJoint - start" + NEW_LINE  + 
"    def moveJoint():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveJoint"+QUOTATION+")" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            motionFinished = 0" + NEW_LINE  + 
"            while cnt < 6:" + NEW_LINE  + 
"                qtarget[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to qtarget" + NEW_LINE  + 
"                cnt = cnt + 1" + NEW_LINE  + 
"            end" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"        textmsg(qtarget)" + NEW_LINE  + 
"        speed = receive_buffer[8]*FLOAT_SCALE				# This line are redudant" + NEW_LINE  + 
"        textmsg("+QUOTATION+"speed "+QUOTATION+")" + NEW_LINE  + 
"        textmsg(speed)" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            if thrd == -1:" + NEW_LINE  + 
"                   if receive_buffer[1] == 1:				# Determines if the move shoud be linear or joint from the received string" + NEW_LINE  + 
"                        textmsg("+QUOTATION+"run moveJQthread"+QUOTATION+")" + NEW_LINE  + 
"                        thrd = run moveJthread()" + NEW_LINE  + 
"                   elif receive_buffer[1] == 2:" + NEW_LINE  + 
"                        textmsg("+QUOTATION+"run moveLQthread"+QUOTATION+")" + NEW_LINE  + 
"                        thrd = run moveLQthread()" + NEW_LINE  + 
"                   end" + NEW_LINE  + 
"            end" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"    end # def moveJoint - end" + NEW_LINE  + 
"# Definition for moveJoint - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Definition for moveCar - start" + NEW_LINE  + 
"    def moveCar():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"MoveCar"+QUOTATION+")" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"      enter_critical" + NEW_LINE  + 
"        motionFinished = 0" + NEW_LINE  + 
"      exit_critical" + NEW_LINE  + 
"        speed = receive_buffer[8]*FLOAT_SCALE				# This line are redudant" + NEW_LINE  + 
"        textmsg("+QUOTATION+"speed "+QUOTATION+")" + NEW_LINE  + 
"        textmsg(speed)" + NEW_LINE  + 
"" + NEW_LINE  + 
"        enter_critical" + NEW_LINE  + 
"            if thrd == -1:" + NEW_LINE  + 
"                    if receive_buffer[1] == 3:				# Determines if the move shoud be linear or joint from the received string" + NEW_LINE  + 
"                       textmsg("+QUOTATION+"run moveJCarthread"+QUOTATION+")" + NEW_LINE  + 
"                       thrd = run moveJCarthread()" + NEW_LINE  + 
"                       #thrd = run test1()" + NEW_LINE  + 
"                    elif receive_buffer[1] == 4:" + NEW_LINE  + 
"                       textmsg("+QUOTATION+"run moveLCarthread"+QUOTATION+")" + NEW_LINE  + 
"                       #thrd = run moveJthread()" + NEW_LINE  + 
"                       thrd = run moveLCarthread()" + NEW_LINE  + 
"                    end" + NEW_LINE  + 
"            end" + NEW_LINE  + 
"        exit_critical" + NEW_LINE  + 
"    end # def moveCar - end" + NEW_LINE  + 
"# Definition for moveCar - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Definition for force_mode_start - start" + NEW_LINE  + 
"    def force_mode_start():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"force_mode_start"+QUOTATION+")" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        while cnt < 6:" + NEW_LINE  + 
"            force_frame[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to force_frame" + NEW_LINE  + 
"            cnt = cnt + 1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"        receive_buffer18 = socket_read_binary_integer(18)" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        while cnt < 6:" + NEW_LINE  + 
"            force_selection[cnt] = receive_buffer18[cnt+1]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_selection" + NEW_LINE  + 
"            wrench[cnt] = receive_buffer18[cnt+1+6]*FLOAT_SCALE		# Assigns values from receive_buffer18 to wrench" + NEW_LINE  + 
"            force_limits[cnt] = receive_buffer18[cnt+1+12]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_limits" + NEW_LINE  + 
"            cnt = cnt + 1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Force Frame/task_frame: "+QUOTATION+")" + NEW_LINE  + 
"        textmsg(force_frame)" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Force Selection/selection_vector: "+QUOTATION+")" + NEW_LINE  + 
"        textmsg(force_selection)" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Wrench:"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(wrench)" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Force Limits/limits:"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(force_limits)" + NEW_LINE  + 
"        force_mode(force_frame, force_selection, wrench, 2, force_limits)" + NEW_LINE  + 
"" + NEW_LINE  + 
"    end # force_mode_start() - end" + NEW_LINE  + 
"# Definition for force_mode_start - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Definition for force_mode_update - start" + NEW_LINE  + 
"    def force_mode_update():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"force_mode_update"+QUOTATION+")" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        while cnt < 6:" + NEW_LINE  + 
"            force_frame[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to force_frame" + NEW_LINE  + 
"            cnt = cnt + 1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"        receive_buffer18 = socket_read_binary_integer(18)" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        while cnt < 6:" + NEW_LINE  + 
"            force_selection[cnt] = receive_buffer18[cnt+1]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_selection" + NEW_LINE  + 
"            wrench[cnt] = receive_buffer18[cnt+1+6]*FLOAT_SCALE		# Assigns values from receive_buffer18 to wrench" + NEW_LINE  + 
"            force_limits[cnt] = receive_buffer18[cnt+1+12]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_limits" + NEW_LINE  + 
"            cnt = cnt + 1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"        textmsg("+QUOTATION+"Wrench Update:"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(wrench)" + NEW_LINE  + 
"" + NEW_LINE  + 
"        force_mode(force_frame, force_selection, wrench, 2, force_limits)" + NEW_LINE  + 
"" + NEW_LINE  + 
"    end # force_mode_update() -end" + NEW_LINE  + 
"# Definition for force_mode_update - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Definition for force_mode_end - start" + NEW_LINE  + 
"    def force_mode_end():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"force_mode_end"+QUOTATION+")" + NEW_LINE  + 
"        end_force_mode()						# Forcemode end" + NEW_LINE  + 
"    end # force_mode_end() - end" + NEW_LINE  + 
"# Definition for force_mode_end - end" + NEW_LINE  + 
"" + NEW_LINE  + 
"    # All code conserning servo have been deleted" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"# Thread for setting the TCP - start" + NEW_LINE  + 
"    def setTCP():" + NEW_LINE  + 
"        textmsg("+QUOTATION+"setTCP"+QUOTATION+")" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        while cnt < 6:" + NEW_LINE  + 
"            tcp_pose[cnt] = receive_buffer[cnt+2]*FLOAT_SCALE	# Assigns values from receive_buffer to force_frame" + NEW_LINE  + 
"            cnt = cnt + 1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"        mass = receive_buffer[8]*FLOAT_SCALE" + NEW_LINE  + 
"" + NEW_LINE  + 
"        receive_buffer18 = socket_read_binary_integer(18)" + NEW_LINE  + 
"        cnt = 0" + NEW_LINE  + 
"        while cnt < 3:" + NEW_LINE  + 
"            cog[cnt] = receive_buffer18[cnt+1]*FLOAT_SCALE	# Assigns values from receive_buffer18 to force_selection" + NEW_LINE  + 
"            cnt = cnt + 1" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"" + NEW_LINE  + 
"        textmsg("+QUOTATION+"tcp_pose"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(tcp_pose)" + NEW_LINE  + 
"" + NEW_LINE  + 
"        textmsg("+QUOTATION+"mass"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(mass)" + NEW_LINE  + 
"" + NEW_LINE  + 
"        textmsg("+QUOTATION+"cog"+QUOTATION+")" + NEW_LINE  + 
"        textmsg(cog)" + NEW_LINE  + 
"        " + NEW_LINE  + 
"        set_tcp(tcp_pose)" + NEW_LINE  + 
"        set_payload(mass, cog)" + NEW_LINE  + 
"    end" + NEW_LINE  + 
"# Thread for running the moveJ command, with joint input - end" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"### MAIN" + NEW_LINE  + 
"#############################################################################" + NEW_LINE  + 
"    #Setup the host name" + NEW_LINE  + 
"    #movej(p [-0.44, -0.150,0.77,-0.5,-2.5,0.9]) # Cartesian point" + NEW_LINE  + 
"    host = "+QUOTATION+"192.168.0.127"+QUOTATION+"" + NEW_LINE  + 
"    port1 = PORT" + NEW_LINE  + 
"    port = 33334" + NEW_LINE  + 
"    opened = socket_open(host, port)					# Opens a socket connection to connect to a PC" + NEW_LINE  + 
"    textmsg("+QUOTATION+"Socket Status"+QUOTATION+")" + NEW_LINE  + 
"    textmsg(opened)" + NEW_LINE  + 
"" + NEW_LINE  + 
"    while opened == False:" + NEW_LINE  + 
"        opened = socket_open(host, port)				# Continues to repoen a socket connection until a conection is established " + NEW_LINE  + 
"    end" + NEW_LINE  + 
"" + NEW_LINE  + 
"    textmsg("+QUOTATION+"Socket opened !!"+QUOTATION+")" + NEW_LINE  + 
"" + NEW_LINE  + 
"    errcnt = 0" + NEW_LINE  + 
"    while errcnt < 1:" + NEW_LINE  + 
"                	# Determines the type of feedback (joint, TCP (force - UNDER DEVELOPENT))" + NEW_LINE  + 
"        if receive_buffer[1] == 1:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 1"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_joint_positions())" + NEW_LINE  + 
"        elif receive_buffer[1] == 2:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 2"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_joint_positions())" + NEW_LINE  + 
"        elif receive_buffer[1] == 3:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 3"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_tcp_pose())" + NEW_LINE  + 
"        elif receive_buffer[1] == 4:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 4"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_tcp_pose())" + NEW_LINE  + 
"        elif receive_buffer[1] == 6:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 6"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_tcp_pose())" + NEW_LINE  + 
"        elif receive_buffer[1] == 7:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 7"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_tcp_pose())" + NEW_LINE  + 
"        elif receive_buffer[1] == 8:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 8"+QUOTATION+")" + NEW_LINE  + 
"            socket_send_line(get_actual_tcp_pose())" + NEW_LINE  + 
"        elif receive_buffer[1] == 10:" + NEW_LINE  + 
"            textmsg("+QUOTATION+"mode = 10"+QUOTATION+")" + NEW_LINE  + 
"" + NEW_LINE  + 
"            enter_critical" + NEW_LINE  + 
"                socket_send_string(get_actual_tcp_pose())           ## Good" + NEW_LINE  + 
"                socket_send_string(get_actual_joint_positions())      ## Good" + NEW_LINE  + 
"                socket_send_string("+QUOTATION+"C"+QUOTATION+")" + NEW_LINE  + 
"            exit_critical" + NEW_LINE  + 
"" + NEW_LINE  + 
"        else:" + NEW_LINE  + 
"" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"" + NEW_LINE  + 
"" + NEW_LINE  + 
"        receive_buffer = socket_read_binary_integer(8)" + NEW_LINE  + 
"	# The following determines what functions there should be called" + NEW_LINE  + 
"        if receive_buffer[0] != 8:			# Ensures that the received string have the correct length" + NEW_LINE  + 
"                   stopRobot()" + NEW_LINE  + 
"                   #errcnt = errcnt + 1 # VT3" + NEW_LINE  + 
"        elif receive_buffer[1] == 0:                #0: Stop Robot" + NEW_LINE  + 
"                if isStopped == 0:" + NEW_LINE  + 
"                    stopRobot()" + NEW_LINE  + 
"                end" + NEW_LINE  + 
"        elif receive_buffer[1] == 1:                #1: Move to Q" + NEW_LINE  + 
"                 isStopped = 0" + NEW_LINE  + 
"                moveJoint()" + NEW_LINE  + 
"        elif receive_buffer[1] == 2:                #1: Move to Q" + NEW_LINE  + 
"                isStopped = 0" + NEW_LINE  + 
"                moveJoint()" + NEW_LINE  + 
"        elif receive_buffer[1] == 3:                #3: Move to Q" + NEW_LINE  + 
"                isStopped = 0" + NEW_LINE  + 
"                moveCar()" + NEW_LINE  + 
"        elif receive_buffer[1] == 4:                #4: Move to T" + NEW_LINE  + 
"                isStopped = 0" + NEW_LINE  + 
"                moveCar()" + NEW_LINE  + 
"        elif receive_buffer[1] == 5:                #5: Get force" + NEW_LINE  + 
"                socket_send_line(get_tcp_force())" + NEW_LINE  + 
"        elif receive_buffer[1] == 6:                #6: Force mode start" + NEW_LINE  + 
"                force_mode_start()" + NEW_LINE  + 
"        elif receive_buffer[1] == 7:                #7: Force mode update" + NEW_LINE  + 
"                force_mode_update()" + NEW_LINE  + 
"        elif receive_buffer[1] == 8:                #8: Force mode end" + NEW_LINE  + 
"                force_mode_end()" + NEW_LINE  + 
"        elif receive_buffer[1] == 9:                #9: Close" + NEW_LINE  + 
"                errcnt = errcnt + 1" + NEW_LINE  + 
"        elif receive_buffer[1] == 11:               #11: Set ToolCenterPoint" + NEW_LINE  + 
"                textmsg("+QUOTATION+"mode = 11"+QUOTATION+")" + NEW_LINE  + 
"                setTCP()" + NEW_LINE  + 
"        elif receive_buffer[1] == 9999:             #1: Do nothing" + NEW_LINE  + 
"                isStopped = 0" + NEW_LINE  + 
"        end" + NEW_LINE  + 
"" + NEW_LINE  + 
"    end #end for While True:" + NEW_LINE  + 
"    textmsg("+QUOTATION+"Program Finished"+QUOTATION+")" + NEW_LINE  + 
"end" + NEW_LINE  + 
"run program" + NEW_LINE  + 
"";